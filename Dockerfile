ARG CI_REGISTRY
ARG PYTHON_VERSION
FROM ${CI_REGISTRY}/datapool2/datapool:linux-base-image-${PYTHON_VERSION}

ENV http_proxy=http://proxy.ethz.ch:3128
ENV https_proxy=http://proxy.ethz.ch:3128


USER 1111

# chown still needed, copy in default settings always copies files as root:
COPY --chown=1111 . datapool/

RUN pip install --no-cache-dir -e ./datapool \
    && pip install -r datapool/requirements_dev.txt >/tmp/log 2>&1 || { cat /tmp/log; exit 1; }

ARG DOCKER_FILES
COPY ${DOCKER_FILES}/entry_point.sh \
     ${DOCKER_FILES}/setup_db.sql /tmp/

RUN mkdir -p /home/nonroot/etc/datapool
ENV ETC=/home/nonroot/etc

ENTRYPOINT ["/tmp/entry_point.sh"]
