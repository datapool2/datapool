import time
from queue import Queue

from datapool.poolkit.observer import (
    CREATED_EVENT,
    ILLEGAL_EVENT,
    shutdown_observer,
    start_observer,
)


def test_call_back(tmpdir):
    changes = Queue()

    def call_back(event, rel_path, timestamp):
        changes.put((event, str(rel_path), timestamp))

    o = start_observer(tmpdir.strpath, call_back)

    # create yaml file
    fh = open(tmpdir.join("site.yaml").strpath, "w")
    fh.write("x")
    fh.close()

    time.sleep(0.1)

    for i in range(5):
        time.sleep(0.1)
        # create raw file "inwrite" mode, must not trigger event:
        fh = open(tmpdir.join("data.raw.inwrite").strpath, "w")
        fh.write("abc")
        fh.close()

        # should trigger a file creation event
        tmpdir.join("data.raw.inwrite").rename(tmpdir.join("data-%03d.raw" % i))

    time.sleep(0.1)

    tmpdir.join("site.yaml").remove()

    # average latency on linux is 500 msec, on mac it is faster:
    time.sleep(0.7)

    shutdown_observer(o)

    CE = CREATED_EVENT
    IE = ILLEGAL_EVENT

    changes = sorted(changes.queue, key=lambda t: t[2])
    events, rel_paths, timestamps = zip(*changes)

    assert rel_paths == (
        "site.yaml",
        "data-000.raw",
        "data-001.raw",
        "data-002.raw",
        "data-003.raw",
        "data-004.raw",
        "site.yaml",
    )
    assert events == (CE, CE, CE, CE, CE, CE, IE)
