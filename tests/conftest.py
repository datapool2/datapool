#!/usr/bin/env python

# flake8: noqa

import os
import re
import signal
import sys
import time
from subprocess import PIPE, STDOUT, Popen
from threading import Thread

import coverage
import pytest
import pytest_regtest
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from datapool.poolkit.config import MagicConfig
from datapool.poolkit.config_handling import config_for_develop_db


@pytest.fixture(scope="session", autouse=True)
def datamodel():
    import datapool  # make sure that Yaml subclasses et al are created and registered

    yield


from datapool.poolkit.testing import (
    Print,
    add,
    check,
    create_example_landing_zone_fixture,
    fake_ask,
    fix_regtest_output,
    free_port,
    operational_lz,
    overwrite,
    patch_etc,
    print_,
    proto_operational_lz,
    run_server,
    write,
)


def config_factory(tmp_path):
    config = MagicConfig()
    config.python.executable = sys.executable
    config.conversion.block_size = 100

    root_folder = config.landing_zone.folder = tmp_path / "lz"
    config.backup_landing_zone.folder = tmp_path / "backup_lz"

    config.http_server.log_file = tmp_path / "datapool_server.log"

    config.site.fields = "street,postcode,city,country"

    config.db = config_for_develop_db(config, root_folder)[0].db
    return config


example_landing_zone, example_landing_zone_config = create_example_landing_zone_fixture(
    config_factory
)


def pytest_addoption(parser):
    parser.addoption(
        "--run-cli-tests", action="store_true", default=False, help="run slow cli tests"
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--run-cli-tests"):
        return
    skip_slow = pytest.mark.skip(reason="need --run-cli-tests option to run")
    for item in items:
        if item.parent.name.startswith("test_cli"):
            item.add_marker(skip_slow)


@pytest.fixture
def start_process(print_, request):
    def _shell(program, command, **env_settings):
        env = os.environ.copy()
        env_settings = {k: str(v) for k, v in env_settings.items()}
        env["HOSTUID"] = "0"
        env.update(env_settings)
        process = Popen(
            f"{program}  {command}",
            stdin=PIPE,
            stdout=PIPE,
            stderr=STDOUT,
            universal_newlines=True,
            shell=True,
            env=env,
        )
        return process

    return _shell


@pytest.fixture
def pool(start_process, print_):
    def _pool(command):
        process = start_process(
            "pool",
            command,
            PYTHONUNBUFFERED=1,
            COVERAGE_PROCESS_START="setup.cfg",
        )
        for line in iter(process.stdout.readline, ""):
            print_(line.rstrip())
            # print(repr(line.rstrip()))
        # print("WAIT")
        process.wait()
        return process.returncode

    return _pool


# change scope to session? dependant fixtures need to be changed too then
@pytest.fixture
def setup_cli(patch_etc, free_port, tmp_path, pool):
    lz_folder = tmp_path / "lz"
    blz_folder = tmp_path / "blz"
    lz_folder.mkdir(exist_ok=True)
    log_file = tmp_path / "datapool.log"

    pool(
        f"admin init-config {lz_folder} --use-sqlitedb"
        f" backup_landing_zone.folder={blz_folder}"
        f" http_server.port={free_port}"
        f" http_server.log_file={log_file}"
        " site.fields=street,postcode,city,country"
    )
    return tmp_path


@pytest.fixture
def run_server_cli(start_process, setup_cli, print_):
    server = start_process(
        "exec pool",
        "admin run-server",
        COVERAGE_PROCESS_START="setup.cfg",
    )
    time.sleep(1)

    lines = []

    def read_server():
        nonlocal lines
        for line in iter(server.stdout.readline, ""):
            lines.append(line.rstrip())
            # print("SERVER", line.rstrip())

    t = Thread(target=read_server)
    t.start()

    # wait for server to start up
    while True:
        if any("main loop ready" in l for l in lines):
            break
        time.sleep(0.05)

    class run_server_cli:
        landing_zone = setup_cli / "lz"

        @staticmethod
        def print_report(file=sys.stdout):
            print_("\n-- START OUTPUT FROM SERVER " + 70 * "-", file=file)
            print_(file=file)
            print_("\n".join(lines), file=file)
            print_("\n-- END OUTPUT FROM SERVER --" + 70 * "-", file=file)
            print_(file=file)

    yield run_server_cli

    server.send_signal(signal.SIGINT)
    t.join()


@pytest.fixture(scope="session")
def postgres_running():
    s = socket.socket()
    s.settimeout(1.0)
    postgres_server = os.environ.get("POSTGRES_SERVER")
    if postgres_server is None:
        return False
    try:
        s.connect((postgres_server, 5432))
        return True
    except socket.error:
        return False


@pytest.fixture
def temp_postgis_config(postgres_running):
    if not postgres_running:
        pytest.skip("cannot connect to postgres port")
        return

    server = os.environ.get("POSTGRES_SERVER")
    user = os.environ.get("POSTGRES_USER")
    password = os.environ.get("POSTGRES_PASSWORD")

    db_name = "datapool_" + uuid.uuid4().hex
    conn = psycopg2.connect(
        user=user, password=password, host=server, dbname="postgres"
    )
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    cursor.execute(f"CREATE DATABASE {db_name};")

    connection_string = f"postgresql://{user}:{password}@{server}:5432/{db_name}"
    config = MagicConfig()
    config.db.connection_string = connection_string
    yield config


@pytest_regtest.register_converter_post
def fix_binary_fingerprints(txt):
    # e.g. cadb2886c2ef672a910a169b4dd97259
    return re.sub(r"[a-z0-9]{32}", "<32 digit md5sum>".ljust(32), txt)


@pytest_regtest.register_converter_post
def fix_datapool_version(txt):
    # this is datapool version 2.0.0
    return re.sub(r"datapool version \d+.\d+\.\d+", "datapool version VERSION", txt)
