#!/usr/bin/env python


def test_add_regular_source(
    add, check, example_landing_zone, example_landing_zone_config
):
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source",
            source_type="sensor_from_xyz",
            name="new_source",
            description="does something",
            serial="999-666-333",
            project="sis_datapool",
        )
        == 0
    )
    assert check(example_landing_zone) == 0


def test_source_invalid_source_type(
    add, check, example_landing_zone, example_landing_zone_config
):
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source",
            source_type="sensor_which_does_not_exist",
            name="new_source",
            description="does something",
            serial="999-666-333",
            project="sis_datapool",
        )
        == 1
    )


def test_add_funky_source(
    add, check, example_landing_zone, example_landing_zone_config
):
    # takes a lot longer why?
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source",
            source_type="sensor_from_xyz",
            name="new_source-A",
            description="the desc contains some 'strange' symbols: "
            "1. % [] - ' () / °C & # + * > <",
            serial="Acd9_442_da/4as",
            project="sis_datapool",
        )
        == 0
    )
    check(example_landing_zone) == 0
