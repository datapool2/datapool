#!/usr/bin/env python

import csv
import shutil
import time
from functools import partial
from io import StringIO
from textwrap import dedent

import click
import pytest

import datapool  # noqa: F401
from datapool.poolkit.commands.run_server import ServerState
from datapool.poolkit.database import dump_db


def copy_files(dlz, rel_paths, lz, _):
    for rel_path in rel_paths:
        target = lz / rel_path
        target.parent.mkdir(parents=True, exist_ok=True)
        shutil.copy(dlz / rel_path, target)

        # sleep makes sure that files are processed in incoming order:
        time.sleep(0.2)


def test_run_server_import_example_lz_files(print_, example_landing_zone, run_server):
    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "projects.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    stream = StringIO()

    def print__(*a, **kw):
        kw["file"] = stream
        kw["color"] = False
        click.secho(*a, **kw)

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    def schedule_multiple_raw_data_files(lz, _):
        rel_path = "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
        target_folder = (lz / rel_path).parent
        source_file = example_landing_zone / rel_path
        shutil.copy(source_file, target_folder / "data-001.raw")
        time.sleep(0.05)
        shutil.copy(source_file, target_folder / "data-002.raw")
        time.sleep(0.05)
        shutil.copy(source_file, target_folder / "data-003.raw")
        time.sleep(0.05)

    run_server.schedule(schedule_multiple_raw_data_files)

    run_server(
        max_time_seconds=10,
        until_state=None,
        max_dispatch=999,
        print_=print__,
    )
    print_("")
    print_("OUTPUT FROM RUN_SERVER:")
    print_("")
    print_(stream.getvalue())
    dump_db(run_server.config.db, print_=print_, max_rows=10)


def test_run_server_import_example_lz_files_no_source_type(
    print_, example_landing_zone, run_server
):
    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        # "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )


def test_run_server_import_example_lz_files_no_variable(
    print_, example_landing_zone, run_server
):
    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        # "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )


def test_run_server_with_site(print_, example_landing_zone, run_server):
    (
        example_landing_zone / "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py"
    ).write_text(
        dedent(
            """def convert(in_path, out_path):
                   import shutil
                   shutil.copy(in_path, out_path)
            """
        )
    )

    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).write_text(
        dedent(
            """
            timestamp;value;variable;site
            2020-10-10 10:00:00;1.234;Water Level;test_site
            """
        )
    )

    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        # "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )
    dump_db(run_server.config.db, ["signal"], print_=print_, max_rows=10)


invalid_timestamp = """
timestamp;value;variable;site
2020-10-10 1x:00:00;1.234;Water Level;test_site
"""

missing_site_and_xyz = """
timestamp;value;variable
2020-10-10 10:00:00;1.234;Water Level
"""

invalid_site = """
timestamp;value;variable;site
2020-10-10 10:00:00;1.234;Water Level;test_site2
"""


@pytest.mark.parametrize(
    "data",
    [invalid_timestamp, missing_site_and_xyz, invalid_site],
    ids=["invalid_timestamp", "missing_site_and_xyz", "invalid_site"],
)
def test_run_server_with_invalid_data_file(
    print_, example_landing_zone, run_server, data
):
    (
        example_landing_zone / "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py"
    ).write_text(
        dedent(
            """def convert(in_path, out_path):
                   import shutil
                   shutil.copy(in_path, out_path)
            """
        )
    )

    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).write_text(dedent(data))

    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        # "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )


def test_run_server_import_duplicate_signals(print_, example_landing_zone, run_server):
    data = (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).read_bytes()

    lines = data.split(b"\n")
    duplicate_lines = lines + lines[2:]
    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).write_bytes(b"\n".join(duplicate_lines))

    rel_paths = (
        "projects.yaml",
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )

    dump_db(run_server.config.db, ["signal"], print_=print_, max_rows=10)


@pytest.mark.parametrize(
    "delimiter", [";", ","], ids=["correct delimiter", "wrong delimiter"]
)
def test_run_server_direct_import_signals(
    print_, example_landing_zone, run_server, delimiter
):
    p = (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/direct_data/data.csv"
    )
    p.parent.mkdir()

    with p.open("w") as fh:
        w = csv.writer(fh, delimiter=delimiter)
        w.writerow(["site", "variable", "timestamp", "value"])
        w.writerow(["test_site", "Temperature", "2023-06-09 14:50:12", "27.0"])
        w.writerow(["test_site", "Temperature", "2023-06-09 15:50:13", "28.0"])

    rel_paths = (
        "projects.yaml",
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/direct_data/data.csv",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )

    dump_db(run_server.config.db, ["signal"], print_=print_, max_rows=10)


def test_run_server_no_conversion_script(print_, example_landing_zone, run_server):
    rel_paths = (
        "sites/test_site/images/IMG_0312.JPG",
        "sites/test_site/images/IMG_0732.JPG",
        "sites/test_site/site.yaml",
        "data/variables.yaml",
        "data/sensor_from_xyz/source_type.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",
        # "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
        "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
    )

    run_server.schedule(
        partial(copy_files, example_landing_zone, rel_paths),
    )

    run_server(
        max_time_seconds=10,
        until_state=ServerState.WAITING,
        max_dispatch=len(rel_paths),
    )
