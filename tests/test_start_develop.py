#!/usr/bin/env python

from datapool.poolkit.commands.start_develop import _start_develop


def test_start_develop(tmp_path, operational_lz, print_):
    config = operational_lz(silent=True)

    dlz = tmp_path / "dlz"
    print_("")
    print_("FROM OPERATIONAL LZ")
    print_("")
    _start_develop(config, dlz, False, False, print_, print_)

    print_("")
    print_("FROM OPERATIONAL LZ AGAIN, NO RESET:")
    print_("")
    _start_develop(config, dlz, False, False, print_, print_)

    print_("")
    print_("FROM OPERATIONAL LZ AGAIN, RESET:")
    print_("")
    _start_develop(config, dlz, True, False, print_, print_)
