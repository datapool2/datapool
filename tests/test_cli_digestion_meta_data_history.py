#!/usr/bin/env python
import time
from textwrap import dedent


def test_add_meta_data_history(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"
    lz = tmp_path / "lz"
    time.sleep(3)

    print(f"create example lz at {example_lz}")
    assert pool(f"create-example {example_lz}") == 0
    time.sleep(3)
    print(f"uptdate operational from {example_lz}")
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(3)

    (
        lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history.yaml"
    ).write_text(
        dedent(
            """-
                   site: test_site
                   source: sensor_abc_from_xyz
                   meta_log_type: new_sensor
                   meta_action_type: installed
                   meta_flag: OK
                   timestamp_start: 2021-01-24 00:00:00
                   timestamp_end: 2021-01-31 00:00:00
                   person: schmittu
                   additional_meta_info:
                       -
                           problem: A crocodile ate my leg.
                       -
                           weather: horrible
            """
        )
    )

    time.sleep(1)
    assert pool("admin show meta_data_history") == 0


def test_add_meta_data_history_incorrect_date(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"
    lz = tmp_path / "lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(3)
    (
        lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history.yaml"
    ).write_text(
        dedent(
            """-
                   site: test_site
                   source: sensor_abc_from_xyz
                   meta_log_type: new_sensor
                   meta_action_type: installed
                   meta_flag: OK
                   timestamp_start: 2021-01- 00:00:00
                   timestamp_end: 2021-01-31 00:00:00
                   person: schmittu
                   additional_meta_info:
                       -
                           problem: A crocodile ate my leg.
                       -
                           weather: horrible
            """
        )
    )

    time.sleep(1)
    assert pool("admin show meta_data_history") == 0
