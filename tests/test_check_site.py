#!/usr/bin/env python


def test_check_invalid_entries(check, example_landing_zone, overwrite):
    overwrite(example_landing_zone / "sites" / "test_site" / "site.yaml", name="")
    overwrite(
        example_landing_zone / "sites" / "test_site" / "site.yaml",
        pictures=[
            dict(path="images/IMG_0732.JPG", description="", timestamp="2020/01/01"),
            dict(path="images/IMG_0312.JPG", description="", timestamp="2020/01/01"),
            dict(path="images/invalid.JPG", description="", timestamp="2020/01/01"),
        ],
    )

    check(example_landing_zone)


def test_delete_all_pictures(check, example_landing_zone, overwrite):
    # delete pictures from landing zone
    img_path = example_landing_zone / "sites" / "test_site" / "images"
    for img in img_path.iterdir():
        img.unlink()

    # change yaml entry
    overwrite(
        example_landing_zone / "sites" / "test_site" / "site.yaml",
        pictures=[],
    )

    assert check(example_landing_zone) == 1


def test_change_pictures(check, example_landing_zone, overwrite):
    # delete a picture
    img_path_1 = (
        example_landing_zone / "sites" / "test_site" / "images" / "IMG_0732.JPG"
    )
    img_path_1.unlink()

    # rename a picture
    img_path_2 = (
        example_landing_zone / "sites" / "test_site" / "images" / "IMG_0312.JPG"
    )
    img_path_2_new = (
        example_landing_zone / "sites" / "test_site" / "images" / "img_new.png"
    )
    img_path_2.rename(img_path_2_new)

    # adding a "new" picture
    overwrite(
        example_landing_zone / "sites" / "test_site" / "site.yaml",
        pictures=[
            dict(
                path="images/img_new.png",
                description="new desc",
                timestamp="2020-01-01",
            ),
        ],
    )

    assert check(example_landing_zone) == 0


def test_change_picture_descriptions(check, example_landing_zone, overwrite):
    overwrite(
        example_landing_zone / "sites" / "test_site" / "site.yaml",
        pictures=[
            dict(path="images/IMG_0732.JPG", description="", timestamp="2020/01/01"),
            dict(
                path="images/IMG_0312.JPG",
                description="Some new description",
                timestamp="2020/01/01",
            ),
        ],
    )
    assert check(example_landing_zone) == 0
