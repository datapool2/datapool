#!/usr/bin/env python

from datapool.poolkit.yaml_parser import print_yaml


def test_corrupt_meta_action_types(
    example_landing_zone, example_landing_zone_config, add
):
    # trigger error handler

    (example_landing_zone / "meta_data" / "meta_action_types.yaml").write_text(
        "invalid"
    )
    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_action_type",
        meta_log_type="new_brew_room",
        name="planning",
        description="plan",
    )


def test_corrupt_meta_log_types(example_landing_zone, example_landing_zone_config, add):
    # trigger error handler

    (example_landing_zone / "meta_data" / "meta_log_types.yaml").write_text("invalid")

    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_action_type",
        meta_log_type="new_brew_room",
        name="planning",
        description="plan",
    )

    with (example_landing_zone / "meta_data" / "meta_log_types.yaml").open("w") as fh:
        print_yaml(
            [dict(name="new_brew_room"), dict(name="new_brew_room")],
            fh=fh,
        )

    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_action_type",
        meta_log_type="new_brew_room",
        name="planning",
        description="plan",
    )


def test_empty_meta_action_types(
    example_landing_zone, example_landing_zone_config, add
):
    # trigger handling nonexisting file
    (example_landing_zone / "meta_data" / "meta_action_types.yaml").unlink()
    (example_landing_zone / "meta_data" / "meta_log_types.yaml").unlink()

    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_action_type",
        meta_log_type="new_brew_room",
        name="planning",
        description="plan",
    )

    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_log_type",
        name="new_brew_room",
        description="build new brew room",
    )

    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_action_type",
        meta_log_type="new_brew_room",
        name="planning",
        description="plan",
    )


def test_meta_action_type_exists_already(
    example_landing_zone, example_landing_zone_config, add
):
    add(
        example_landing_zone_config,
        example_landing_zone,
        "meta_log_type",
        name="new_brew_room",
        description="build new brew room",
    )

    for _ in range(2):
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_action_type",
            meta_log_type="new_brew_room",
            name="planning",
            description="plan",
        )
