#!/usr/bin/env python

from datapool.poolkit.commands.delete_facts import _delete_facts
from datapool.poolkit.database import dump_db


def test_delete_signals(operational_lz, print_):
    config = operational_lz(silent=True)

    dump_db(config.db, print_=print_, max_rows=10)

    _delete_facts(
        config,
        True,
        50,
        "signals",
        [
            "timestamp>=2013-11-13",
            "timestamp<2013-11-14",
            "variable==Water Level",
            "source==sensor_abc_from_xyz",
            "site==test_site",
        ],
        print_,
        print_,
    )
    print_("")

    _delete_facts(
        config,
        False,
        50,
        "signals",
        ["variable==Temperature", "source_type==sensor_from_xyz"],
        print_,
        print_,
    )
    print_("")

    _delete_facts(
        config,
        True,
        50,
        "signals",
        ["variable==Temperature", "source_type==sensor_from_xyz"],
        print_,
        print_,
    )


def test_delete_meta_data_history(operational_lz, print_):
    config = operational_lz(silent=True)

    dump_db(
        config.db,
        print_=print_,
        max_rows=10,
        table_names=["meta_data_history", "meta_picture"],
    )

    _delete_facts(
        config,
        True,
        50,
        "meta_data_history",
        [
            "meta_log_type_id==1",
        ],
        print_,
        print_,
    )

    dump_db(
        config.db,
        print_=print_,
        max_rows=10,
        table_names=["meta_data_history", "meta_picture"],
    )
