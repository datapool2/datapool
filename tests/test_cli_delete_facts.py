#!/usr/bin/env python
import time


def test_delete_signal_source(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-facts signals "source==sensor_abc_from_xyz"') == 0


def test_delete_signal_unknown_site(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-facts signals "site==test_site2"') == 1


def test_delete_signal_site(tmp_path, pool, print_, run_server_cli, regtest):
    # no site specified in conversion script
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-facts signals "site==test_site"') == 0


def test_delete_signal_site_coord_timestamp(
    tmp_path, pool, print_, run_server_cli, regtest
):
    # coordinates instead of sites
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert (
        pool(
            'delete-facts signals "coord_x==682558" "coord_y==239404"'
            ' "timestamp>=2013-11-15 00:00:00"'
        )
        == 0
    )


def test_delete_signal_site_coord_timestamp_date(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert (
        pool(
            'delete-facts signals "coord_x==682558" "coord_y==239404"'
            ' "timestamp>=2013-11-15"'
        )
        == 0
    )


def test_delete_signal_source_timestamp(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert (
        pool(
            'delete-facts signals "source==sensor_abc_from_xyz"'
            ' "timestamp<=2013-11-15 00:00:00"'
        )
        == 0
    )


def test_delete_meta_data_history_filtered_source_timestamp(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert (
        pool(
            'delete-facts meta_data_history "source==sensor_abc_from_xyz"'
            ' "timestamp_start==2020-12-31 00:00:00"'
        )
        == 0
    )


def test_delete_meta_data_history_filtered_person(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-facts meta_data_history "person==schmittu"') == 0


def test_delete_meta_data_history_filtered_flag(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-facts meta_data_history "meta_flag==ALERT"') == 0


def test_delete_meta_data_history_filtered_flag_action_type(
    tmp_path, pool, print_, run_server_cli, regtest
):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert (
        pool(
            'delete-facts meta_data_history "meta_flag==ALERT"'
            ' "meta_action_type==checked"'
        )
        == 0
    )
