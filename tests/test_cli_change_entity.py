#!/usr/bin/env python
import time
from textwrap import dedent


def test_change_site_address(tmp_path, pool, print_, run_server_cli):
    example_lz = tmp_path / "example_lz"
    dlz = tmp_path / "dlz"
    site_yaml = dlz / "sites" / "test_site" / "site.yaml"

    assert pool(f"create-example {example_lz}") == 0
    pool(f"update-operational {example_lz}")

    time.sleep(5)  # wait for update operational

    assert pool(f"start-develop {dlz}") == 0

    site_yaml.unlink()

    site_yaml.write_text(
        dedent(
            """
               name: test_site
               description: All the details of the site.All the details
                   the site.All the details of the site.All the details of the
                   site.All the details of the site.All the details of the
                   site.All the details of the site.
               street: Saumackerstrasse 666
               postcode: 8048
               city: Zurich, Altstetten, Switzerland

               # pictures are optional:
               pictures:
                   -
                     path: images/IMG_0312.JPG
                     description: outside
                     # timestamp is optional:
                     timestamp: 2016/02/01 12:00:00
                   -
                     path: images/IMG_0732.JPG
                     description: manholeD1

            """
        )
    )

    pool(f"update-operational {dlz}")

    time.sleep(2)  # wait for update operational

    assert pool("admin show site") == 0
