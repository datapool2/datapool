#!/usr/bin/env python

import time
from textwrap import dedent

import pytest

LOCATION_CONVERSION_SOURCE = "data/sensor_from_xyz/sensor_abc_from_xyz"
LOCATION_CONVERSION_SOURCE_TYPE = "data/sensor_from_xyz"
LOCATION_DATA_SOURCE = "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data"
LOCATION_DATA_SOURCE_TYPE = "data/sensor_from_xyz/raw_data"


@pytest.fixture
def data_line():
    def _data_line(day):
        return (
            (
                "HEADERLINE\n"
                "Date Time|Level(mm)|Velocity(m/s)|Flow(L/s)|Temperature(°C)|"
                " Surface Velocity(m/s)|Distance(mm)|Distance Reading Count(Counts)"
                "|Surcharge Level(mm)|PMR()|NOS()|Power Supply(V)\n"
                f"{day}.01.2022 10:06|"
                "159.25|0.65|39.21|14.31|0.73|525.75|168|0|30.31|17|NA"
                "\n"
            )
            .lstrip("\n")
            .replace("|", "\t")
        )

    return _data_line


def test_change_conversion_py_to_r(tmp_path, pool, run_server_cli, regtest, data_line):
    lz = tmp_path / "lz"
    example_lz = tmp_path / "example_lz"
    dlz = tmp_path / "dlz"

    pool(f"create-example {example_lz}") >= 0

    #################################
    # overwriting example files
    (example_lz / LOCATION_DATA_SOURCE / "data-001.raw").write_text(data_line("01"))
    (example_lz / LOCATION_DATA_SOURCE_TYPE / "data-001.raw").write_text(
        data_line("02")
    )
    #################################

    pool(f"update-operational --copy-raw-files {example_lz}") >= 0

    started = time.time()
    while not (lz / LOCATION_DATA_SOURCE).exists():
        time.sleep(0.5)
        assert time.time() < started + 10, "folder was not created within 10 seconds"

    (lz / LOCATION_DATA_SOURCE / "data-0123456789.raw").write_text(data_line("03"))

    pool(f"start-develop {dlz}")

    (dlz / LOCATION_CONVERSION_SOURCE / "conversion.py").unlink()

    (dlz / LOCATION_CONVERSION_SOURCE / "conversion.r").write_text(
        dedent(
            """
            convert <- function(raw_file, output_file){
                data.raw <- read.delim(raw_file, sep="\t", skip=1,
                          header=TRUE,check.names = F, dec = ".", stringsAsFactors=F
                          )

                data.raw[is.na(data.raw)] <- -9999

                pos <- "test_site"

                time <- as.POSIXct(data.raw[,1], format = "%d.%m.%Y %H:%M", tz="UTC")
                time <- format(time, "%Y-%m-%d %H:%M:%S")

                data<-data.frame(time, "Water Level", data.raw[,2], pos)
                data<-rbind(data,
                            setNames(data.frame(time, "Average Flow Velocity",
                                                data.raw[,3], pos),
                                     names(data))
                            )
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Flow", data.raw[,4], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Temperature", data.raw[,5], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Surface Flow Velocity",
                                           data.raw[,6], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Distance", data.raw[,7], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Distance Reading Count",
                                           data.raw[,8], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Surcharge Level", data.raw[,9], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Peak to Mean Ratio",
                                           data.raw[,10], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Number of Samples",
                                           data.raw[,11], pos),
                                names(data)))
                data<-rbind(data,
                            setNames(
                                data.frame(time, "Battery Voltage", data.raw[,12], pos),
                                names(data)))

                colnames(data) <- c("timestamp", "variable", "value","site")

                write.table(data, file=output_file, row.names=FALSE, col.names=TRUE,
                                      quote=FALSE, sep=";")
            }
            """
        )
    )

    (dlz / LOCATION_DATA_SOURCE / "data-123456789.raw").write_text(data_line("04"))

    pool(f"update-operational --copy-raw-files {dlz}") >= 0

    time.sleep(2)

    (lz / LOCATION_DATA_SOURCE / "data-23456789.raw").write_text(data_line("05"))

    time.sleep(3)
    pool("admin show signal --no-pager")

    assert (lz / LOCATION_CONVERSION_SOURCE / "conversion.r").exists()


def test_change_conversion_py_to_jl(tmp_path, pool, run_server_cli, regtest, data_line):
    lz = tmp_path / "lz"
    example_lz = tmp_path / "example_lz"
    dlz = tmp_path / "dlz"

    pool(f"create-example {example_lz}")

    #################################
    # overwriting example files
    (example_lz / LOCATION_DATA_SOURCE / "data-001.raw").write_text(data_line("01"))
    (example_lz / LOCATION_DATA_SOURCE_TYPE / "data-001.raw").write_text(
        data_line("02")
    )
    #################################

    pool(f"update-operational --copy-raw-files {example_lz}")

    time.sleep(2)

    (lz / LOCATION_DATA_SOURCE / "data-0123456789.raw").write_text(data_line("03"))

    pool(f"start-develop {dlz}")

    (
        dlz / "data" / "sensor_from_xyz" / "sensor_abc_from_xyz" / "conversion.py"
    ).unlink()

    (
        dlz / "data" / "sensor_from_xyz" / "sensor_abc_from_xyz" / "conversion.jl"
    ).write_text(
        dedent(
            """
            module conversion

                using DataFrames, CSV

                function convert(raw_file, output_file)
                    data = CSV.read(raw_file, DataFrame; delim='\t',
                                    dateformat="dd.mm.yyyy HH:MM",
                                    missingstring="NA",header=2)
                    data = coalesce.(data, -9999)

                    column_names = [
                        "Date Time",
                        "Water Level",
                        "Average Flow Velocity",
                        "Flow",
                        "Temperature",
                        "Surface Flow Velocity",
                        "Distance",
                        "Distance Reading Count",
                        "Surcharge Level",
                        "Peak to Mean Ratio",
                        "Number of Samples",
                        "Battery Voltage",
                    ]
                    rename!(data, column_names)
                    formatted_data = stack(data,2:12)
                    insertcols!(formatted_data, 4, :site => "test_site")
                    rename!(formatted_data, [:timestamp, :variable, :value, :site])

                    CSV.write(output_file,
                              formatted_data;
                              delim=';',
                              dateformat="yyyy-mm-dd HH:MM:SS")
                end
            end
            """
        )
    )

    (dlz / LOCATION_DATA_SOURCE / "data-123456789.raw").write_text(data_line("04"))

    pool(f"update-operational --copy-raw-files {dlz}")

    time.sleep(2)

    (lz / LOCATION_DATA_SOURCE / "data-23456789.raw").write_text(data_line("05"))

    time.sleep(3)
    pool("admin show signal --no-pager")

    assert True or (lz / LOCATION_CONVERSION_SOURCE / "conversion.jl").exists()


def test_change_conversion_source_type_py_to_jl(
    tmp_path, pool, run_server_cli, regtest, data_line
):
    lz = tmp_path / "lz"
    example_lz = tmp_path / "example_lz"
    dlz = tmp_path / "dlz"

    pool(f"create-example {example_lz}")

    #################################
    # overwriting example files
    (example_lz / LOCATION_DATA_SOURCE / "data-001.raw").write_text(data_line("01"))
    (example_lz / LOCATION_DATA_SOURCE_TYPE / "data-001.raw").write_text(
        data_line("02")
    )
    #################################

    pool(f"update-operational --copy-raw-files {example_lz}")

    time.sleep(2)

    (lz / LOCATION_DATA_SOURCE_TYPE / "data-0123456789.raw").write_text(data_line("03"))

    pool(f"start-develop {dlz}")

    (dlz / "data" / "sensor_from_xyz" / "conversion.py").unlink()

    (dlz / "data" / "sensor_from_xyz" / "conversion.jl").write_text(
        dedent(
            """
            module conversion

                using DataFrames, CSV

                function convert(raw_file, output_file)
                    data = CSV.read(raw_file,
                           DataFrame;
                           delim='\t',
                           dateformat="dd.mm.yyyy HH:MM", missingstring="NA",header=2)
                    data = coalesce.(data, -9999)

                    column_names = [
                        "Date Time",
                        "Water Level",
                        "Average Flow Velocity",
                        "Flow",
                        "Temperature",
                        "Surface Flow Velocity",
                        "Distance",
                        "Distance Reading Count",
                        "Surcharge Level",
                        "Peak to Mean Ratio",
                        "Number of Samples",
                        "Battery Voltage",
                    ]
                    rename!(data, column_names)
                    formatted_data = stack(data,2:12)
                    insertcols!(formatted_data, 4, :site => "test_site")
                    insertcols!(formatted_data, 5, :source => "sensor_abc_from_xyz")
                    rename!(formatted_data,
                            [:timestamp, :variable, :value, :site, :source])

                    CSV.write(output_file, formatted_data; delim=';',
                              dateformat="yyyy-mm-dd HH:MM:SS")
                end
            end
            """
        )
    )

    (dlz / LOCATION_DATA_SOURCE_TYPE / "data-123456789.raw").write_text(data_line("04"))

    pool(f"update-operational --copy-raw-files {dlz}")

    time.sleep(2)

    (lz / LOCATION_DATA_SOURCE_TYPE / "data-23456789.raw").write_text(data_line("05"))

    time.sleep(3)
    pool("admin show signal --no-pager")

    assert True or (lz / LOCATION_CONVERSION_SOURCE_TYPE / "conversion.jl").exists()
