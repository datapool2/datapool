#!/usr/bin/env python


def test_update_operational_example_lz(tmp_path, pool, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0


def test_update_operational_missing_raw(tmp_path, pool, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    raw = (
        example_lz
        / "data"
        / "sensor_from_xyz"
        / "sensor_abc_from_xyz"
        / "raw_data"
        / "data-001.raw"
    )
    raw.unlink()

    assert pool(f"update-operational {example_lz}") == 1
