#!/usr/bin/env python


from datapool.poolkit.commands.delete_entity import _delete_entity
from datapool.poolkit.database import dump_db


def test_delete_site(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(
        config.db,
        ["site", "site_field", "site_field_value", "picture"],
        print_=print_,
        max_rows=10,
    )

    print_("\nDELETE SITE\n")
    _delete_entity(config, True, 20, "site", "test_site", print_, print_)
    dump_db(
        config.db,
        ["site", "site_field", "site_field_value", "picture"],
        print_=print_,
        max_rows=10,
    )

    print_("\nTRY DELETE SAME SITE\n")
    _delete_entity(config, True, 20, "site", "test_site", print_, print_)


def test_delete_people(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nDELETE PEOPLE\n")
    _delete_entity(config, True, 20, "person", "schmittu", print_, print_)
    dump_db(config.db, ["person"], print_=print_, max_rows=10)


def test_delete_source_type(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nTRY DELETE SOURCE TYPE\n")
    _delete_entity(config, True, 20, "source_type", "sensor_from_xyz", print_, print_)


def test_delete_variable(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nDELETE variable\n")
    _delete_entity(config, True, 20, "variable", "Water Level", print_, print_)
    dump_db(config.db, ["variable", "signal"], print_=print_, max_rows=10)

    print_("\nTRY DELETE SAME variable\n")
    _delete_entity(config, True, 20, "variable", "Water Level", print_, print_)


def test_delete_source_and_source_type(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nDELETE SOURCE\n")
    _delete_entity(config, True, 20, "source", "sensor_abc_from_xyz", print_, print_)
    dump_db(config.db, ["source", "signal"], print_=print_, max_rows=10)

    print_("\nTRY DELETE SAME SOURCE\n")
    _delete_entity(config, True, 20, "source", "sensor_abc_from_xyz", print_, print_)

    print_("\nDELETE SOURCE TYPE\n")
    _delete_entity(config, True, 20, "source_type", "sensor_from_xyz", print_, print_)
    dump_db(config.db, ["source_type"], print_=print_, max_rows=10)


def test_delete_project(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nTRY TO DELETE PROJECT\n")
    _delete_entity(config, True, 20, "project", "sis_datapool", print_, print_)
    dump_db(config.db, ["project", "source", "signal"], print_=print_, max_rows=10)

    print_("\nDELETE SOURCE\n")
    _delete_entity(config, True, 20, "source", "sensor_abc_from_xyz", print_, print_)
    dump_db(config.db, ["source", "signal"], print_=print_, max_rows=10)

    print_("\nDELETE PROJECT\n")
    _delete_entity(config, True, 20, "project", "sis_datapool", print_, print_)
    dump_db(config.db, ["project", "source", "signal"], print_=print_, max_rows=10)


def test_delete_meta_log_and_action_type(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nTRY TO DELETE META LOG TYPE\n")
    _delete_entity(config, True, 20, "meta_log_type", "new_sensor", print_, print_)

    print_("\nDELETE META ACTION TYPE\n")
    _delete_entity(config, True, 20, "meta_action_type", "installed", print_, print_)
    dump_db(config.db, ["meta_action_type"], print_=print_, max_rows=10)

    print_("\nDELETE META ACTION TYPE\n")
    _delete_entity(config, True, 20, "meta_action_type", "checked", print_, print_)
    dump_db(config.db, ["meta_action_type"], print_=print_, max_rows=10)

    print_("\nDELETE META LOG TYPE\n")
    _delete_entity(config, True, 20, "meta_log_type", "new_sensor", print_, print_)
    dump_db(config.db, ["meta_log_type"], print_=print_, max_rows=10)


def test_delete_meta_flag(operational_lz, print_):
    config = operational_lz(silent=False)
    dump_db(config.db, print_=print_, max_rows=10)

    print_("\nDELETE META FLAG\n")
    _delete_entity(config, True, 20, "meta_flag", "ALERT", print_, print_)
    dump_db(config.db, ["meta_flag"], print_=print_, max_rows=10)
