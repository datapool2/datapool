#!/usr/bin/env python

from datapool.main import app_config
from datapool.poolkit.commands.init_config import init_config
from datapool.poolkit.commands.init_db import init_db
from datapool.poolkit.config_handling import read_config
from datapool.poolkit.database import dump_db


def test_init_db(regtest, print_, monkeypatch, tmp_path):
    temp_etc = tmp_path / "etc"
    temp_landing_zone = tmp_path / "landing_zone"
    temp_landing_zone.mkdir()
    monkeypatch.setenv("ETC", str(temp_etc))

    init_config(
        temp_landing_zone,
        sqlite_db=True,
        reset=False,
        print_ok=print_,
        print_err=print_,
        verbose=False,
        app_config=app_config,
        extra_config_settings=[],
    )

    init_db(reset=False, print_ok=print_, print_err=print_, verbose=False)

    config, path = read_config()
    dump_db(config.db, print_=print_)
