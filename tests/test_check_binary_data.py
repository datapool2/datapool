#!/usr/bin/env python


def test_check_binary_data_no_yaml(write, check, example_landing_zone, print_):
    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/binary_data_spec.yaml"
    ).unlink()
    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/empty.npz"
    ).write_bytes(b"")
    check(example_landing_zone)
