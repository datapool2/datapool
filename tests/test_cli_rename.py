#!/usr/bin/env python
import shutil
import time

import pytest

import datapool.main  # noqa: F401 sets application name
from datapool.poolkit.config_handling import read_config
from datapool.poolkit.database import dump_db


@pytest.fixture
def dump(print_):
    def dump_(tables):
        config, _ = read_config()
        assert config is not None
        dump_db(config.db, tables, print_=print_)

    return dump_


@pytest.fixture
def dump_lz(run_server_cli, print_):
    lz = run_server_cli.landing_zone

    def dump_(*folders):
        if not folders:
            folders = [""]
        for f in folders:
            for p in (lz / f).glob("**"):
                print_(str(p))

    return dump_


def test_rename_variable(tmp_path, pool, run_server_cli, regtest, dump):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    assert (
        pool(
            f"start-rename --what variable {rename_lz}"
            ' "Water Level" "Renamed Water Level"'
        )
        == 0
    )
    assert pool(f"update-operational {rename_lz}") == 0
    time.sleep(1.0)
    run_server_cli.print_report(regtest)
    dump(["variable"])


def test_rename_site(tmp_path, pool, run_server_cli, regtest, dump, dump_lz):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    assert (
        pool(f"start-rename --what site {rename_lz}" ' "test_site" "renamed_site"') == 0
    )

    for rp in (
        "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/binary_data_spec.yaml",
        "lab_results/lab_results.csv",
        "meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml",
    ):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("test_site", "renamed_site"))

    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    time.sleep(1.0)
    dump(["site"])
    dump_lz("sites")


def test_rename_source(tmp_path, pool, run_server_cli, regtest, dump, dump_lz):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(
        f"start-rename --what source {rename_lz}"
        ' "sensor_abc_from_xyz" "renamed_source"'
    )
    for rp in ("meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml",):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("sensor_abc_from_xyz", "renamed_source"))

    shutil.copyfile(
        example_lz / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
        rename_lz / "data/sensor_from_xyz/renamed_source/raw_data/data-001.raw",
    )

    pool(f"update-operational {rename_lz}")
    run_server_cli.print_report(regtest)
    time.sleep(1.0)
    dump(["source"])
    dump_lz("data/sensor_from_xyz")


def test_rename_source_type(tmp_path, pool, run_server_cli, regtest, dump, dump_lz):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(
        f"start-rename --what source_type {rename_lz}"
        ' "sensor_from_xyz" "renamed_source_type"'
    )
    shutil.copyfile(
        example_lz / "data/sensor_from_xyz/raw_data/data-001.raw",
        rename_lz / "data/renamed_source_type/raw_data/data-001.raw",
    )
    shutil.copyfile(
        example_lz / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
        rename_lz
        / "data/renamed_source_type/sensor_abc_from_xyz/raw_data/data-001.raw",
    )
    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    dump(["source_type"])
    dump_lz("data")


def test_rename_person(tmp_path, pool, run_server_cli, regtest, dump):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(f"start-rename --what person {rename_lz}" ' "schmittu" "schmittu.renamed"')

    for rp in (
        "lab_results/lab_results.csv",
        "meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml",
    ):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("schmittu", "schmittu.renamed"))

    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    dump(["person"])


def test_rename_project(tmp_path, pool, run_server_cli, regtest, dump):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(f"start-rename --what project {rename_lz}" ' "sis_datapool" "renamed_project"')
    for rp in ("data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml",):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("sis_datapool", "renamed_project"))

    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    dump(["project"])


def test_rename_meta_action_type(tmp_path, pool, run_server_cli, regtest, dump):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(
        f"start-rename --what meta_action_type {rename_lz}"
        ' "installed" "renamed_action_type"'
    )

    for rp in ("meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml",):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("installed", "renamed_action_type"))

    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    dump(["meta_action_type"])


def test_rename_meta_log_type(tmp_path, pool, run_server_cli, regtest, dump):
    example_lz = tmp_path / "example_lz"
    rename_lz = tmp_path / "rename_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0
    pool(
        f"start-rename --what meta_log_type {rename_lz}"
        ' "new_sensor" "renamed_log_type"'
    )

    for rp in (
        "meta_data/meta_action_types.yaml",
        "meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml",
    ):
        ap = rename_lz / rp
        ap.write_text(ap.read_text().replace("new_sensor", "renamed_log_type"))

    assert pool(f"update-operational {rename_lz}") == 0
    run_server_cli.print_report(regtest)
    dump(["meta_log_type"])
