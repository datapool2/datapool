from datapool.poolkit.yaml_parser import print_yaml


def test_add_info(pool, setup_cli, regtest):
    for what in [
        "meta_action_type",
        "meta_data",
        "meta_flag",
        "meta_log_type",
        "variable",
        "person",
        "project",
        "site",
        "source",
        "source_type",
    ]:
        pool(f"add {what} --info")


def test_add_source_type(tmp_path, pool, setup_cli, regtest, patch_etc):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f" add source_type {example_lz} "
            'name="new_source_type" '
            'description="this is a description" '
            'manufacturer="Hallo hallo"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_project(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add project {example_lz}"
            " title=UWO"
            " description='this is a desc.'"
            " abbrev="
        )
        == 0
    )
    assert pool(f"check {example_lz}") >= 0


def test_add_source(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add project {example_lz}"
            " title=UWO"
            " description='this is a desc.'"
            " abbrev="
        )
        == 0
    )
    assert (
        pool(
            f" add source {example_lz} "
            'source_type="sensor_from_xyz" '
            'name="new_source" '
            'description="this is a description" '
            'serial="3j34ü" '
            'project="UWO"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") >= 0


def test_add_site(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f" add site {example_lz} "
            'name="new_site" '
            'description="this is a description" '
            'street="dreamstreet" '
            'postcode="9182" '
            'city="DreamCity" '
            'country="Atlantis" '
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_person(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add person {example_lz} "
            'name="Bill Bronsen" '
            'abbreviation="BB" '
            'email="bill.bronsen@email.ch"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_variable(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add variable {example_lz} "
            'name="new_variable" '
            'description="some description for the new variable" '
            'unit="m/(s^2)"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_meta_data_a_second_time(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add meta_data {example_lz} "
            'source="sensor_abc_from_xyz" '
            'site="test_site" '
            'description="some meta data entry!"'
        )
        == 1
    )

    assert pool(f"check {example_lz}") == 0


def test_add_meta_data_cli(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "images"
        / "IMG_0732.JPG"
    ).unlink()

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history.yaml"
    ).unlink()

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "images"
        / "IMG_0312.JPG"
    ).unlink()

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data.yaml"
    ).unlink()

    assert (
        pool(
            f"add meta_data {example_lz} "
            f'source="sensor_abc_from_xyz" '
            f'site="test_site"  '
            f'description="some meta data entry!"'
        )
        == 0
    )

    assert pool(f"check {example_lz}") == 0


def test_add_meta_data_direct(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "images"
        / "IMG_0732.JPG"
    ).unlink()

    (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history.yaml"
    ).unlink()

    meta_data_yaml = (
        example_lz
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data.yaml"
    )
    meta_data = {
        "site": "test_site",
        "source": "sensor_abc_from_xyz",
        "description": "so much information!",
        "pictures": [
            {
                "path": "images/IMG_0312.JPG",
                "description": "this is and example picture",
            }
        ],
        "additional_meta_info": [
            {
                "problems": "none at all",
                "nested": [{"nested_info_1": "info_1", "nested_info_2": "info_2"}],
            }
        ],
    }

    with meta_data_yaml.open("w") as fh:
        print_yaml(meta_data, fh=fh)

    assert pool(f"check {example_lz}") == 0


def test_add_meta_flag(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add meta_flag {example_lz} "
            'name="bad_values" '
            'description="these values are not to be trusted"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_meta_log_type(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add meta_log_type {example_lz} "
            'name="installation_deinstallation" '
            'description="a sensor was installed or uninstalled"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0


def test_add_meta_action_type(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert (
        pool(
            f"add meta_action_type {example_lz} "
            'meta_log_type="new_sensor" '
            'name="accident" '
            'description="somebody fell off the ladder"'
        )
        == 0
    )
    assert pool(f"check {example_lz}") == 0
