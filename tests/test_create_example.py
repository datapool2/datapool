#!/usr/bin/env python

import glob


def test_create_example(regtest, example_landing_zone, check, print_):
    print_()
    print_("content created example landing zone:")
    for p in sorted(glob.glob(str(example_landing_zone / "**"), recursive=True)):
        print_("   ", p)
    print_()

    check(example_landing_zone)
