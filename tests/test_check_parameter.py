#!/usr/bin/env python


def test_add_regular_variable(
    example_landing_zone, example_landing_zone_config, add, check, regtest
):
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "variable",
            name="Water-Temperature",
            description="temp of water",
            unit="°C",
        )
        == 0
    )

    check(example_landing_zone)


def test_create_duplicate_variable_name(
    example_landing_zone, example_landing_zone_config, add, overwrite, check, regtest
):
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "variable",
            name="Water-Temperature",
            description="temp of water",
            unit="°C",
        )
        == 0
    )

    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "variable",
            name="Water-Temperature",
            description="temp of water 2",
            unit="°C 2",
        )
        == 1
    )

    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "variable",
            name="Water-Temperature 2",
            description="temp of water 2",
            unit="°C 2",
        )
        == 0
    )

    check(example_landing_zone)
