#!/usr/bin/env python
import time


def test_delete_site(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational

    assert pool("delete-entity --what site test_site") == 0

    run_server_cli.print_report()


def test_delete_non_existing_source(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational

    assert pool("delete-entity --what source sensor_from_xyz") == 1
    run_server_cli.print_report()


def test_delete_source(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what source sensor_abc_from_xyz") == 0


def test_delete_variable(tmp_path, pool, print_, run_server_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool('delete-entity --what variable "Water Level"') == 0


def test_delete_project(tmp_path, pool, print_, run_server_cli, regtest):
    # why does project deletion need source deletion?
    # uwe: source has reference to project, so just deleting project could yield in
    #      invalid db entries for source. we have the same when you want to delete
    #      a source_type in the old datapool already.
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what project sis_datapool") == 1


def test_delete_persons(tmp_path, pool, print_, run_server_cli, regtest):
    # no info about meta data deletion? What happens with meta data?
    # how many meta_data_history, reference_measurement entries affected
    # uwe: meta_data only links to source and site and this is not related to person,
    #      fixed this for meta_data_history entries
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what person schmittu") == 0


def test_delete_meta_flag(tmp_path, pool, print_, run_server_cli, regtest):
    # no info about meta data deletion? What happens with meta data?
    # how many meta_data_history, reference_measurement entries affected
    # uwe: meta_data is not linked, fixed this for meta_data_history entries
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what meta_flag ALERT") == 0


def test_delete_meta_action_type(tmp_path, pool, print_, run_server_cli, regtest):
    # no info about meta data deletion? What happens with meta data?
    # how many meta_data_history, reference_measurement entries affected
    # uwe: meta_data is not linked, fixed this for meta_data_history entries
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what meta_action_type installed") == 0


def test_delete_meta_log_type(tmp_path, pool, print_, run_server_cli, regtest):
    # no info about meta data deletion? What happens with meta data?
    # how many meta_data_history entries affected
    # uwe: meta_data is not linked, fixed this for meta_data_history entries
    # meta_action_type deletion required
    # uwe: similar to projects question.
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"update-operational {example_lz}") == 0

    time.sleep(2)  # wait for update operational
    run_server_cli.print_report()

    assert pool("delete-entity --what meta_log_type new_sensor") == 1
