#!/usr/bin/env python
from textwrap import dedent


def test_check_raw_data_conversion(check, example_landing_zone, print_):
    check(example_landing_zone)


def test_check_raw_data_conversion_broken_script(
    write, check, example_landing_zone, print_
):
    (
        example_landing_zone / "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py"
    ).write_text(
        dedent(
            """
            def convert(raw_file, output_file):
                1 / 0
            """
        ),
    )
    check(example_landing_zone)


def test_check_raw_data_conversion_duplicate_data(
    write, check, example_landing_zone, print_
):
    data = (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).read_bytes()

    lines = data.split(b"\n")
    duplicate_lines = lines + lines[2:]
    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).write_bytes(b"\n".join(duplicate_lines))

    check(example_landing_zone)


def test_check_raw_data_conversion_no_source(check, example_landing_zone):
    (
        example_landing_zone / "data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml"
    ).unlink()
    check(example_landing_zone)


def test_check_raw_data_conversion_no_variables(check, example_landing_zone):
    (example_landing_zone / "data/variables.yaml").unlink()
    check(example_landing_zone)


def test_check_raw_data_conversion_no_script(check, example_landing_zone):
    (
        example_landing_zone / "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py"
    ).unlink()
    check(example_landing_zone)


def test_check_raw_data_conversion_no_data(check, example_landing_zone):
    (
        example_landing_zone
        / "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw"
    ).unlink()
    check(example_landing_zone)
