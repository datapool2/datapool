#!/usr/bin/env python
import os
import socket
import sys
import uuid
from io import StringIO

import click
import psycopg2
import pytest
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import select

from datapool.poolkit.commands.delete_facts import _delete_facts
from datapool.poolkit.commands.update_operational import _update_operational
from datapool.poolkit.config import MagicConfig
from datapool.poolkit.config_handling import config_for_develop_db
from datapool.poolkit.database import connect_to_db, fetch_many
from datapool.poolkit.testing import _RunServer
from datapool.raster_data import RasterDataDecl


@pytest.fixture(scope="session")
def postgres_running():
    s = socket.socket()
    s.settimeout(1.0)
    postgres_server = os.environ.get("POSTGRES_SERVER", "postgis")
    try:
        s.connect((postgres_server, 5432))
        return True
    except socket.error:
        print(f"cannot connect to {postgres_server}", file=sys.stderr)
        return False


@pytest.fixture
def temp_postgis_config(postgres_running, tmp_path):
    _config = MagicConfig()
    if not postgres_running:
        assert (
            os.environ.get("RUNNING_SYSTEM_TESTS") is None
        ), "cannot connect to postgres db on CI"

        db_config, _ = config_for_develop_db(_config, tmp_path)
        yield db_config
        return

    server = os.environ.get("POSTGRES_SERVER", "postgis")
    user = os.environ.get("POSTGRES_USER", "postgres")
    password = os.environ.get("POSTGRES_PASSWORD")

    db_name = "datapool_" + uuid.uuid4().hex
    conn = psycopg2.connect(
        user=user, password=password, host=server, dbname="postgres"
    )
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    cursor.execute(f"CREATE DATABASE {db_name};")
    conn.commit()
    conn.close()

    conn = psycopg2.connect(
        user=user,
        password=password,
        host=server,
        dbname=db_name,
    )
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cursor = conn.cursor()
    cursor.execute("CREATE EXTENSION IF NOT EXISTS postgis;")
    cursor.execute("CREATE EXTENSION IF NOT EXISTS postgis_raster;")
    cursor.execute(
        """
        CREATE TABLE raster_data (
               raster_data_id SERIAL PRIMARY KEY,
               source_id INTEGER,
               variable_id INTEGER,
               timestamp  TIMESTAMP,
               data RASTER
        );
    """
    )
    conn.commit()
    conn.close()

    connection_string = f"postgresql://{user}:{password}@{server}:5432/{db_name}"
    db_config = MagicConfig()
    db_config.connection_string = connection_string
    _config.db = db_config
    yield _config


def test_inject_raster_data(
    temp_postgis_config,
    tmp_path,
    example_landing_zone,
    print_,
    postgres_running,
    regtest,
):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    run_server = _RunServer(tmp_path, print_, temp_postgis_config.db)

    run_server.schedule(setup)

    stream = StringIO()

    def print__(*a, **kw):
        kw["file"] = stream
        kw["color"] = False
        click.secho(*a, **kw)

    engine = connect_to_db(temp_postgis_config.db)
    assert len(fetch_many(engine, RasterDataDecl)) == 0

    run_server(
        max_time_seconds=1.0,
        until_state=None,
        max_dispatch=999,
        print_=print__,
        debug=False,
    )

    output = stream.getvalue()
    for line in (
        "dispatch data/sensor_from_xyz/sensor_abc_from_xyz/raster_data/masked-1990.tif",
        "skip data/sensor_from_xyz/sensor_abc_from_xyz/raster_data/masked-1990.tif",
        "dispatch data/sensor_from_xyz/sensor_abc_from_xyz/raster_data/"
        "raster_data_spec.yaml",
    ):
        if line not in output:
            print(output, file=sys.stderr)
            raise IOError("tests failed.")

    dbcols = RasterDataDecl._schema.c
    dbos = engine.execute(
        select([dbcols.timestamp, dbcols.variable_id, dbcols.source_id])
    ).fetchall()

    _delete_facts(
        run_server.config,
        True,
        50,
        "raster_data",
        [
            "variable==Temperature",
        ],
        print__,
        print__,
    )

    dbcols = RasterDataDecl._schema.c
    dbos = engine.execute(
        select([dbcols.timestamp, dbcols.variable_id, dbcols.source_id])
    ).fetchall()

    assert len(dbos) == 0
