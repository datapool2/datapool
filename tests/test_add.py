#!/usr/bin/env python


def test_add_ok(example_landing_zone, example_landing_zone_config, add, check, regtest):
    add(
        example_landing_zone_config,
        example_landing_zone,
        "project",
        title="X-mas",
        description="Christmas party",
        abbrev="xmas",
    )

    add(
        example_landing_zone_config,
        example_landing_zone,
        "project",
        title="Eeaster",
        description="Happy easter",
        abbrev="easter",
    )

    for _ in range(2):
        for what, setup in [
            (
                "site",
                dict(
                    name="beer_factory",
                    description="description",
                    street="street",
                    postcode="8048",
                    city="Zurich",
                    country="Switzerland",
                ),
            ),
            (
                "variable",
                dict(
                    name="International Bittering Units",
                    description="How bitter is my beer?",
                    unit="IBU",
                ),
            ),
            (
                "source_type",
                dict(
                    name="brew_room",
                    manufacturer="brewing company",
                    description="holy church of beer",
                ),
            ),
            (
                "source",
                dict(
                    source_type="brew_room",
                    name="brew_kettle_0",
                    description="holy church of beer",
                    serial="4242",
                    project="X-mas",
                ),
            ),
            (
                "meta_data",
                dict(
                    site="beer_factory",
                    source="brew_kettle_0",
                    description="",
                ),
            ),
            (
                "meta_flag",
                dict(
                    name="OVERFLOW",
                    description="too much wort in the kettle",
                ),
            ),
            (
                "meta_log_type",
                dict(
                    name="new_brew_room",
                    description="build new brew room",
                ),
            ),
            (
                "meta_log_type",
                dict(
                    name="new_shop",
                    description="build new shop",
                ),
            ),
            (
                "meta_action_type",
                dict(
                    meta_log_type="new_brew_room",
                    name="planning",
                    description="plan",
                ),
            ),
            (
                "meta_action_type",
                dict(
                    meta_log_type="new_shop",
                    name="planning",
                    description="plan",
                ),
            ),
            (
                "meta_action_type",
                dict(
                    meta_log_type="new_brew_room",
                    name="new_brew_room_works",
                    description="checks passed",
                ),
            ),
            (
                "person",
                dict(
                    abbreviation="cfoerster",
                    name="Christian Foerster",
                    email="christian.foerster@eawag.ch",
                ),
            ),
        ]:
            add(example_landing_zone_config, example_landing_zone, what, **setup)

    check(example_landing_zone)
