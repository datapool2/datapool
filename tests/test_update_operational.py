#!/usr/bin/env python

import csv
import os
import shutil
import time
from functools import partial
from io import StringIO
from textwrap import dedent

import click
import numpy as np
import pytest

from datapool.poolkit.commands.update_operational import _update_operational
from datapool.poolkit.database import dump_db
from datapool.poolkit.yaml_parser import parse_file, print_yaml


def copy_folder_content(from_, to):
    for p in sorted(from_.iterdir()):
        target = to / p.relative_to(from_)
        target.parent.mkdir(exist_ok=True)
        shutil.copy(p, target)
        time.sleep(0.05)


def _setup_operational_lz(run_server, print_, max_dispatch, max_time_seconds):
    stream = StringIO()

    def print__(*a, **kw):
        kw["file"] = stream
        kw["color"] = False
        click.secho(*a, **kw)

    run_server(
        max_time_seconds=max_time_seconds,
        until_state=None,
        max_dispatch=max_dispatch,
        print_=print__,
        debug=False,
    )
    print_("")
    print_("OUTPUT FROM RUN_SERVER:")
    print_("")
    print_(stream.getvalue())


def test_update_operational(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    def update_again(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    run_server.schedule(setup)
    run_server.schedule(update_again)
    _setup_operational_lz(run_server, print_, max_dispatch=9, max_time_seconds=0.5)


def test_update_operational_with_raw_data(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, True, print_, print_
        )

    def update_again(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, True, print_, print_
        )

    run_server.schedule(setup)
    run_server.schedule(update_again)
    _setup_operational_lz(run_server, print_, max_dispatch=9, max_time_seconds=0.5)


def test_update_operational_meta_action_types(
    run_server, example_landing_zone, example_landing_zone_config, add, print_
):
    def setup(root_folder, _):
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_log_type",
            name="log_type_1",
            description="description",
        )
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_log_type",
            name="log_type_2",
            description="description",
        )
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_action_type",
            name="action_type_1",
            meta_log_type="log_type_1",
            description="description",
        )
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def add_action_type_same_name(root_folder, print_):
        print_("")
        print_("ADD new meta action type")
        print_("")
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_action_type",
            name="action_type_1",
            meta_log_type="log_type_2",
            description="description",
        )
        print_("RUN UPDATE OPERATIONAL")
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def modify_descriptions(root_folder, print_):
        print_("")
        print_("MODIFY DESCRIPTIONS")
        print_("")
        path = root_folder / "meta_data" / "meta_action_types.yaml"
        data = parse_file(path)
        for entry in data:
            entry["description"] = "changed"
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.1)

    def add_meta_data_history(root_folder, print_):
        print_("WRITE META DATA HISTORY")
        (
            root_folder
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "meta_data_history-001.yaml"
        ).write_text(
            dedent(
                """
            -
                site: test_site
                source: sensor_abc_from_xyz
                meta_log_type: log_type_2
                meta_action_type: action_type_1
                meta_flag: ALERT
                timestamp_start: 2020-12-31 00:00:00
                timestamp_end: 2020-12-31 12:00:00
                person: schmittu
                comment: I set the sensor on fire
            """
            )
        )
        time.sleep(0.1)

    run_server.schedule(setup)
    run_server.schedule(add_action_type_same_name)
    run_server.schedule(modify_descriptions)
    run_server.schedule(add_meta_data_history)
    _setup_operational_lz(run_server, print_, max_dispatch=10, max_time_seconds=0.5)

    shutil.copy(
        example_landing_zone
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history.yaml",
        run_server.root_folder
        / "meta_data"
        / "sensor_abc_from_xyz"
        / "test_site"
        / "meta_data_history-001.yaml",
    )

    def unknown_log_type(root_folder, print_):
        print_("")
        print_("ADD meta action type with unknown log type")

        # add will fail
        add(
            example_landing_zone_config,
            example_landing_zone,
            "meta_action_type",
            print__=print_,
            name="action_type_1",
            meta_log_type="unknown",
            description="description",
        )
        print_("")
        print_("RUN UPDATE OPERATIONAL")
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

        # server will detect unknown log type
        with (root_folder / "meta_data" / "meta_action_types.yaml").open("a") as fh:
            fh.write(
                dedent(
                    """
                -
                   meta_log_type: unknown
                   name: action_type_1
                   description: none
                   """
                )
            )
        time.sleep(0.1)

    def duplicate_entry(root_folder, print_):
        print_("DUPLICATE ENTRIES")

        with (root_folder / "meta_data" / "meta_action_types.yaml").open("a") as fh:
            fh.write(
                dedent(
                    """
                -
                   meta_log_type: log_type_1
                   name: action_type_1
                   description: des
                -
                   meta_log_type: log_type_1
                   name: action_type_1
                   description: des
                   """
                )
            )
        time.sleep(0.1)

    def action_type_removed(root_folder, print_):
        print_("REMOVED ENTRIES")
        path = root_folder / "meta_data" / "meta_action_types.yaml"
        data = parse_file(path)
        with path.open("w") as fh:
            print_yaml(data[:2], fh=fh)
        time.sleep(0.1)

    def invalid_action_type_yaml(root_folder, print_):
        print_("INVALID YAML")
        (root_folder / "meta_data" / "meta_action_types.yaml").write_text(
            "invalid yaml"
        )
        time.sleep(0.1)

    def invalid_history_yaml(root_folder, print_):
        print_("WRITE INVALIE META DATA HISTORY YAML")
        (
            root_folder
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "meta_data_history-001.yaml"
        ).write_text("invalid")
        time.sleep(0.1)

    def write_history_yaml(root_folder, entries):
        default = dict(
            site="test_site",
            source="sensor_abc_from_xyz",
            meta_log_type="log_type_2",
            meta_action_type="action_type_1",
            meta_flag="ALERT",
            timestamp_start="2020-12-31 00:00:00",
            timestamp_end="2020-12-31 12:00:00",
            person="schmittu",
            comment="I set the sensor on fire",
        )
        data = []
        for entry in entries:
            data.append({**default, **entry})
        with (
            root_folder
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "meta_data_history-001.yaml"
        ).open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.1)

    def invalid_history_yaml_structure(root_folder, print_):
        print_("WRITE META DATA HISTORY WITH INVALID PICTURES")
        write_history_yaml(
            root_folder,
            [
                dict(pictures="abc"),
                dict(
                    pictures=[
                        dict(path="images/abc.PNG"),
                        dict(description="no picture"),
                    ]
                ),
            ],
        )
        time.sleep(0.1)

    def invalid_history_yaml_missing_and_duplicate_pictures(root_folder, print_):
        print_("WRITE META DATA HISTORY WITH DUPLICATE PICTURES")
        write_history_yaml(
            root_folder,
            [
                dict(
                    pictures=[
                        dict(path="images/abc.PNG", description="none"),
                        dict(path="images/abc.PNG", description="none"),
                    ]
                )
            ],
        )
        time.sleep(0.1)

    def invalid_history_yaml_unknown_references(root_folder, print_):
        print_("WRITE META DATA HISTORY WITH UNKNOWN REFERENCES")
        write_history_yaml(
            root_folder,
            [
                dict(
                    site="at home",
                    source="tap",
                    meta_log_type="no",
                    meta_action_type="no action",
                    meta_flag="no flag",
                    person="homer simpson",
                )
            ],
        )
        time.sleep(0.1)

    run_server.schedule(unknown_log_type)
    run_server.schedule(duplicate_entry)
    run_server.schedule(action_type_removed)
    run_server.schedule(invalid_action_type_yaml)
    run_server.schedule(invalid_history_yaml)
    run_server.schedule(invalid_history_yaml_structure)
    run_server.schedule(invalid_history_yaml_missing_and_duplicate_pictures)
    run_server.schedule(invalid_history_yaml_unknown_references)

    for i, (message, entries) in enumerate(
        [
            (
                "WRITE BOTH IMAGES",
                [
                    dict(
                        pictures=[
                            dict(path="images/IMG_0312.JPG", description=""),
                            dict(path="images/IMG_0732.JPG", description=""),
                        ]
                    )
                ],
            ),
            (
                "WRITE ONLY IMG_0312.JPG",
                [dict(pictures=[dict(path="images/IMG_0312.JPG", description="")])],
            ),
            (
                "MODIFY IMG_0312.JPG",
                [
                    dict(
                        pictures=[
                            dict(path="images/IMG_0312.JPG", description="modified"),
                            dict(path="images/IMG_0732.JPG", description=""),
                        ]
                    )
                ],
            ),
        ]
    ):

        def first_write(root_folder, print_, entries=entries, i=i):
            print_("")
            print_(f"WRITE META DATA MULTIPLE TIMES {i}")
            print_("")
            write_history_yaml(root_folder, [dict()])
            time.sleep(0.1)

        def second_write(root_folder, print_, message=message, entries=entries):
            print_("")
            print_(message)
            print_("")

            picture_folder_source = (
                example_landing_zone
                / "meta_data"
                / "sensor_abc_from_xyz"
                / "test_site"
                / "images"
            )
            picture_folder_target = (
                root_folder
                / "meta_data"
                / "sensor_abc_from_xyz"
                / "test_site"
                / "images"
            )
            shutil.rmtree(picture_folder_target)
            copy_folder_content(picture_folder_source, picture_folder_target)
            write_history_yaml(root_folder, entries)
            time.sleep(0.01)

        run_server.schedule(first_write)
        run_server.schedule(second_write)

    print_("\nRESTART SERVER AND CHECK IF PENDING FILES ARE PROCESSED\n")
    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)


def test_update_operational_meta_data(run_server, example_landing_zone, add, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    def write(message, root_folder, print_, **data):
        print_()
        print_(message)
        print_()
        picture_folder_source = (
            example_landing_zone
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "images"
        )
        picture_folder_target = (
            root_folder / "meta_data" / "sensor_abc_from_xyz" / "test_site" / "images"
        )
        shutil.rmtree(picture_folder_target)
        copy_folder_content(picture_folder_source, picture_folder_target)
        with (
            root_folder
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "meta_data.yaml"
        ).open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.1)

    def picture_is_not_a_list(root_folder, print_):
        write(
            "PICTURES DOES NOT COMPLY SPECIFICATION",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures="none",
        )

    def invalid_pictures_entry(root_folder, print_):
        write(
            "PICTURE ENTRY INVALID",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[[dict(a=3)]],
        )

    def picture_does_not_exist(root_folder, print_):
        write(
            "PICTURE DOES NOT EXIST",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[
                dict(path="not_there.png"),
                dict(path="images/IMG_0312.JPG"),
                dict(path="images/IMG_0732.JPG"),
            ],
        )

    def duplicate_picture(root_folder, print_):
        write(
            "DUPLICATE PICTURES",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[
                dict(path="images/IMG_0312.JPG"),
                dict(path="images/IMG_0312.JPG"),
                dict(path="images/IMG_0732.JPG"),
            ],
        )

    def site_does_not_match(root_folder, print_):
        write(
            "SITE DOES NOT MATCH",
            root_folder,
            print_,
            site="other_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[
                dict(path="images/IMG_0312.JPG"),
                dict(path="images/IMG_0732.JPG"),
            ],
        )

    def picture_modified(root_folder, print_):
        write(
            "MODIIED PICTURE",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[
                dict(path="images/IMG_0312.JPG", description="changed"),
                dict(path="images/IMG_0732.JPG"),
            ],
        )

    def picture_removed(root_folder, print_):
        (
            root_folder
            / "meta_data"
            / "sensor_abc_from_xyz"
            / "test_site"
            / "images"
            / "IMG_0732.JPG"
        ).unlink()
        write(
            "REMOVED PICTURE",
            root_folder,
            print_,
            site="test_site",
            source="sensor_abc_from_xyz",
            description="meta_data_entry",
            pictures=[dict(path="images/IMG_0312.JPG")],
        )

    run_server.schedule(setup)
    run_server.schedule(picture_is_not_a_list)
    run_server.schedule(invalid_pictures_entry)
    run_server.schedule(picture_does_not_exist)
    run_server.schedule(duplicate_picture)
    run_server.schedule(site_does_not_match)
    run_server.schedule(picture_modified)
    # run_server.schedule(picture_added)
    # run_server.schedule(picture_data_changed)
    run_server.schedule(picture_removed)
    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)

    print_("")
    print_("")

    dump_db(
        run_server.config.db,
        ["meta_data", "meta_picture"],
        print_=print_,
        max_rows=9999,
    )


def test_update_operational_invalid_binary_data(
    run_server, example_landing_zone, print_
):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    yaml_path = (
        run_server.root_folder
        / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/"
        / "binary_data_spec.yaml"
    )

    def invalid_yaml(root_folder, print_):
        time.sleep(0.15)
        print_("")
        print_("NEXT ISSUE: invalid yaml")
        print_("")
        yaml_path.write_text("abc")

    run_server.schedule(setup)
    run_server.schedule(invalid_yaml)

    def write_yaml(message, data, yaml_path, root_folder, print_):
        time.sleep(0.15)
        print_("")
        print_(f"NEXT ISSUE: {message}")
        print_("")
        # empty npz file
        data_file = (
            run_server.root_folder
            / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/"
            / "data.npz"
        )
        np.savez(data_file)
        os.sync()

        # atomic write
        with open(yaml_path, "w") as fh:
            print_yaml(data, fh=fh)
        os.sync()

        print_(f"WROTE {yaml_path}")

        time.sleep(0.05)

    for message, data in [
        (
            "write correct",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="2020-01-01 00:00:00",
                file="data.npz",
            ),
        ),
        (
            "same entry again",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="2020-01-01 00:00:00",
                file="data.npz",
            ),
        ),
        (
            "yaml malformed",
            dict(
                site=[dict(name="test_site")],
                variable=dict(name="Water Level"),
                timestamp="2020-01-01 00:00:00",
                file="data.npz",
            ),
        ),
        (
            "entry file missing, invalid field 'whatever'",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="2020-01-01 00:00:00",
                whatever=3,
            ),
        ),
        (
            "malformed timestamp",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="never",
                file="data.npz",
            ),
        ),
        (
            "invalid data file extension",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="2020-01-01 00:00:00",
                file="dataxyz.bin",
            ),
        ),
        (
            "data file missing",
            dict(
                site="test_site",
                variable="Water Level",
                timestamp="2020-01-01 00:00:00",
                file="dataxyz.npz",
            ),
        ),
        (
            "unknown site and variable",
            dict(
                site="test_site_2",
                variable="Water Level X",
                timestamp="2020-01-01 00:00:00",
                file="data.npz",
            ),
        ),
    ]:
        run_server.schedule(partial(write_yaml, message, data, yaml_path))

    data = dict(
        site="test_site",
        variable="Water Level",
        timestamp="2020-01-01 00:00:00",
        file="data.npz",
    )

    def write_unknown_source(root_folder, print_, data=data):
        folder = (
            root_folder / "data" / "sensor_abc_from_xyz" / "new_sensor" / "binary_data"
        )
        folder.mkdir(parents=True)
        np.savez(folder / "data.npz")
        time.sleep(0.05)
        with (folder / "binary_data_spec.yaml").open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.05)

    run_server.schedule(write_unknown_source)

    def invalid_binary_data(root_folder, print_, data=data):
        time.sleep(0.15)
        # empty npz file
        print_("NEXT ISSUE: npz file corrupt")
        p = (
            run_server.root_folder
            / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/"
            / "data.npz"
        )
        if p.exists():
            p.chmod(0o777)
        p.write_text("this is not a npz file")
        time.sleep(0.05)
        with yaml_path.open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.05)

    run_server.schedule(invalid_binary_data)

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)


@pytest.mark.skipif(os.getuid() == 0, reason="root can read the file despite chmod")
def test_update_operational_binary_data_not_readable(
    run_server, example_landing_zone, print_
):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    run_server.schedule(setup)

    yaml_path = (
        run_server.root_folder
        / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/"
        / "binary_data_spec.yaml"
    )

    data = dict(
        site="test_site",
        variable="Water Level",
        timestamp="2020-01-01 00:00:00",
        file="data.npz",
    )

    def can_not_read_binary_data(root_folder, print_, data=data):
        time.sleep(0.15)
        print_("")
        print_("NEXT ISSUE: file not readable yaml")
        print_("")
        # empty npz file
        p = (
            run_server.root_folder
            / "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data/"
            / "data.npz"
        )
        np.savez(p)
        p.chmod(0o000)
        time.sleep(0.05)
        with yaml_path.open("w") as fh:
            print_yaml(data, fh=fh)
        time.sleep(0.05)

    run_server.schedule(can_not_read_binary_data)
    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)


def test_update_operational_invalid_lab_data(
    run_server, example_landing_zone, example_landing_zone_config, print_, add
):
    def setup(root_folder, _):
        (example_landing_zone / "lab_results" / "lab_results.csv").unlink()
        add(
            example_landing_zone_config,
            example_landing_zone,
            "site",
            name="beer_factory",
            description="description",
            street="street",
            postcode="8048",
            city="Zurich",
            country="Switzerland",
            x=1,
            y=2,
            z=3,
        )
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    run_server.schedule(setup)

    fields = [
        "lab_identifier",
        "variable",
        "sample_identifier",
        "filter_lab",
        "dilution_lab",
        "method_lab",
        "value_lab",
        "description_lab",
        "person_abbrev_lab",
        "timestamp_start_lab",
        "timestamp_end_lab",
        "site",
        "person_abbrev_sample",
        "filter_sample",
        "dilution_sample",
        "method_sample",
        "timestamp_sample",
        "description_sample",
    ]

    defaults = [
        "NE18004_000_labDOC",
        "Flow",
        "NE18004",
        "MN_640_d",
        "1",
        "Shimadzu_TOCL",
        "20.3",
        "",
        "dollc,schmittu",
        "2018-08-22 00:00:00",
        "2018-08-25 00:00:00",
        "test_site",
        "dollc,schmittu",
        "None",
        "5",
        "",
        "2018-06-21 15:10:10",
        "",
    ]
    default_data = dict(zip(fields, defaults))

    def write(message, modification, root_folder, print_):
        print_("")
        print_(f"NEXT ISSUE: {message}")
        data = default_data.copy()
        data.update(modification)
        p = root_folder / "lab_results" / "lab_results.csv"
        p.parent.mkdir(parents=True, exist_ok=True)
        with p.open("w") as fh:
            writer = csv.DictWriter(fh, fieldnames=fields, delimiter=";")
            writer.writeheader()
            writer.writerow(data)
        time.sleep(0.11)

    run_server.schedule(partial(write, "initial import", dict()))

    def dump_initial_db(root_folder, print_):
        print_()
        print_("DUMP DB")
        dump_db(
            run_server.config.db,
            [
                "lab_result",
                "lab_result_person_sample_association",
                "lab_result_person_lab_association",
            ],
            print_=print_,
            max_rows=9999,
        )
        # we must trigger dispatch else the server loop will stop!
        write("initial import again", dict(), root_folder, print_)

    run_server.schedule(dump_initial_db)

    for message, modification in [
        (
            "unknown persons, variable, site",
            dict(
                person_abbrev_lab="misterx",
                person_abbrev_sample="missesy",
                variable="milk content",
                site="at home",
            ),
        ),
        (
            "invalid changes",
            dict(
                timestamp_start_lab="2020-02-01 00:00:00",
                timestamp_end_lab="2020-02-02 23:59:59",
                timestamp_sample="2020-01-30 12:00:00",
                value_lab=-1,
                sample_identifier="my sample",
            ),
        ),
        (
            "valid changes",
            dict(
                variable="Temperature",
                filter_lab="new lab filter",
                dilution_lab=10.0,
                method_lab="new lab method",
                description_lab="no idea",
                person_abbrev_lab="schmittu",
                site="beer_factory",
                person_abbrev_sample="dollc",
                filter_sample="new sample filter",
                dilution_sample=33.0,
                method_sample="new sample method",
                description_sample="no idea either",
            ),
        ),
        (
            "invalid timestamps",
            dict(
                timestamp_start_lab="2020-02-01",
                timestamp_end_lab="2020-02-02",
                timestamp_sample="2020-01-30",
            ),
        ),
    ]:
        run_server.schedule(partial(write, message, modification))

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)

    dump_db(
        run_server.config.db,
        [
            "lab_result",
            "lab_result_person_sample_association",
            "lab_result_person_lab_association",
        ],
        print_=print_,
        max_rows=9999,
    )


def test_modify_variable(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def modify_variable(root_folder, print_):
        path = root_folder / "data" / "variables.yaml"
        data = parse_file(path)
        # change unit and description
        data[-1] = dict(name="images", unit="none", description="")
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        print_(f"updated {path}")
        time.sleep(0.1)

    run_server.schedule(setup)
    run_server.schedule(modify_variable)

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)


def test_modify_site(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def modify_site(root_folder, print_):
        path = root_folder / "sites" / "test_site" / "site.yaml"
        data = parse_file(path)
        # change unit and description
        data["description"] = data["street"] = data["postcode"] = data[
            "country"
        ] = data["city"] = "modified"
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        print_(f"updated {path}")
        time.sleep(0.1)

    run_server.schedule(setup)
    run_server.schedule(modify_site)

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)

    dump_db(
        run_server.config.db,
        ["site", "site_field_value"],
        print_=print_,
        max_rows=9999,
    )


def test_modify_source(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def add_project(root_folder, print_):
        path = root_folder / "projects.yaml"
        data = parse_file(path)
        data.append(dict(title="new_project", description="no yet"))
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        print_(f"updated {path}")
        time.sleep(0.1)

    def modify_source(root_folder, print_):
        path = (
            root_folder
            / "data"
            / "sensor_from_xyz"
            / "sensor_abc_from_xyz"
            / "source.yaml"
        )
        data = parse_file(path)
        # change unit and description
        data["description"] = "description modified"
        data["project"] = "new_project"
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        print_(f"updated {path}")
        time.sleep(0.1)

    run_server.schedule(setup)
    run_server.schedule(add_project)
    run_server.schedule(modify_source)

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)

    dump_db(
        run_server.config.db,
        ["project", "source"],
        print_=print_,
        max_rows=9999,
    )


def test_modify_source_type(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )
        time.sleep(0.1)

    def modify_source_type(root_folder, print_):
        path = root_folder / "data" / "sensor_from_xyz" / "source_type.yaml"
        data = parse_file(path)
        # change unit and description
        data["description"] = "description modified"
        with path.open("w") as fh:
            print_yaml(data, fh=fh)
        print_(f"updated {path}")
        time.sleep(0.1)

    run_server.schedule(setup)
    run_server.schedule(modify_source_type)

    _setup_operational_lz(run_server, print_, max_dispatch=999, max_time_seconds=0.5)

    dump_db(
        run_server.config.db,
        ["source_type"],
        print_=print_,
        max_rows=9999,
    )


def test_update_operational_add_site(run_server, example_landing_zone, print_):
    def setup(root_folder, _):
        _update_operational(
            run_server.config, example_landing_zone, False, True, False, print_, print_
        )

    def add_site_ok(root_folder, print_):
        folder = root_folder / "sites" / "new_site"
        folder.mkdir(parents=True)
        (folder / "site.yaml").write_text(
            dedent(
                """
                name: new_site
                description: lame
                street: lame
                postcode: lame
                city: lame
                country: lame
            """
            )
        )

    def add_site_unknown_fields(root_folder, print_):
        folder = root_folder / "sites" / "new_site_2"
        folder.mkdir(parents=True)
        (folder / "site.yaml").write_text(
            dedent(
                """
                name: new_site_2
                description: lame
                street: lame
                postcode: lame
                city: lame
                country: lame
                coordinates:   # old style site.yml
                      x: 3
                      y: 3
                      z: 3

            """
            )
        )

    run_server.schedule(setup)
    run_server.schedule(add_site_ok)
    run_server.schedule(add_site_unknown_fields)
    _setup_operational_lz(run_server, print_, max_dispatch=9, max_time_seconds=0.5)
