#!/usr/bin/env python


def test_check_example_lz(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0
    assert pool(f"check {example_lz}") == 0


def test_check_empty_dlz(tmp_path, pool, setup_cli, regtest):
    dlz = tmp_path / "dlz"

    assert pool(f"start-develop {dlz}") == 0
    assert pool(f"check {dlz}") == 0


def test_check_corrupt_dlz_additional_folder(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    artefact_folder = example_lz / "misplaced"
    artefact_folder.mkdir()

    assert pool(f"check {example_lz}") == 0

    artefact_folder2 = example_lz / "data" / "strange_source_type"
    artefact_folder2.mkdir()

    assert pool(f"check {example_lz}") == 0


def test_check_corrupt_dlz_additional_file(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    artefact_file = example_lz / "misplaced.txt"
    artefact_file.touch()

    assert pool(f"check {example_lz}") == 1

    artefact_file.unlink()
    artefact_file2 = example_lz / "data" / "misplaced_2.txt"
    artefact_file2.touch()

    assert pool(f"check {example_lz}") == 1

    artefact_file2.unlink()
    artefact_file3 = example_lz / "sites" / "misplaced_3.txt"
    artefact_file3.touch()

    assert pool(f"check {example_lz}") == 1

    artefact_file3.unlink()
    artefact_file4 = (
        example_lz
        / "data"
        / "sensor_from_xyz"
        / "sensor_abc_from_xyz"
        / "misplaced_4.txt"
    )
    artefact_file4.touch()

    assert pool(f"check {example_lz}") == 1


def test_check_corrupt_yaml_file(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"
    assert pool(f"create-example {example_lz}") == 0

    artefact_file = example_lz / "data" / "sensor_from_xyz" / "source_type.yaml"
    with open(artefact_file, "a") as stf:
        stf.write("-\n")

    assert pool(f"check {example_lz}") == 1


def test_check_empty_source(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"
    assert pool(f"create-example {example_lz}") == 0

    artefact_file = (
        example_lz / "data" / "sensor_from_xyz" / "sensor_abc_from_xyz" / "source.yaml"
    )
    artefact_file.unlink()
    with open(artefact_file, "w+") as stf:
        stf.write("name: \n" "description: \n" "serial: \n")

    assert pool(f"check {example_lz}") == 1


def test_check_duplicate_variable(tmp_path, pool, setup_cli, regtest):
    dlz = tmp_path / "dlz"

    assert pool(f"start-develop {dlz}") == 0
    pool(f"add variable {dlz} name=Temperature unit=C description=")
    assert pool(f"check {dlz}") == 0

    variable_yaml = dlz / "data" / "variables.yaml"
    with open(variable_yaml, "a") as py:
        py.write("-\n" "  name: Temperature\n" "  unit: C\n" "  description:\n")
    assert pool(f"check {dlz}") == 1


def test_check_missing_raw(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    raw = (
        example_lz
        / "data"
        / "sensor_from_xyz"
        / "sensor_abc_from_xyz"
        / "raw_data"
        / "data-001.raw"
    )
    raw.unlink()

    assert pool(f"check {example_lz}") == 1


def test_check_wrongly_named_raw(tmp_path, pool, setup_cli, regtest):
    example_lz = tmp_path / "example_lz"

    assert pool(f"create-example {example_lz}") == 0

    raw_folder = (
        example_lz / "data" / "sensor_from_xyz" / "sensor_abc_from_xyz" / "raw_data"
    )
    raw_file = raw_folder / "data-001.raw"

    assert pool(f"check {example_lz}") == 0

    raw_file.rename(raw_folder / "data-001")
    assert pool(f"check {example_lz}") == 1

    (raw_folder / "data-001").rename(raw_folder / "data.raw")
    assert pool(f"check {example_lz}") == 0

    (raw_folder / "data.raw").rename(raw_folder / "dat-001.raw")
    assert pool(f"check {example_lz}") == 0

    (raw_folder / "dat-001.raw").rename(raw_folder / "001.raw")
    assert pool(f"check {example_lz}") == 0

    (raw_folder / "001.raw").rename(raw_folder / "data_323-353-2324-522_223_33-22.raw")
    assert pool(f"check {example_lz}") == 0
