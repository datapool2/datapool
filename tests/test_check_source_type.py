#!/usr/bin/env python


def test_add_regular_source_type(
    add, check, example_landing_zone, example_landing_zone_config
):
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source_type",
            name="new_source_type",
            description="",
            manufacturer="me",
            special_values=[
                dict(categorical_value="NA", numerical_value=-1),
                dict(categorical_value="OVERFLOW", numerical_value=-2),
                dict(categorical_value="UNDERFLOW", numerical_value=-3),
            ],
        )
        >= 0
    )
    assert check(example_landing_zone) == 0


def test_renaming_source_type(
    add, overwrite, check, example_landing_zone, example_landing_zone_config
):
    # how can I see whether this worked? It seems to not work...
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source_type",
            name="new_source_type",
            description="",
            manufacturer="me",
            special_values=[
                dict(categorical_value="NA", numerical_value=-1),
                dict(categorical_value="OVERFLOW", numerical_value=-2),
                dict(categorical_value="UNDERFLOW", numerical_value=-3),
            ],
        )
        == 0
    )

    assert check(example_landing_zone) == 0

    overwrite(
        example_landing_zone / "data" / "new_source_type" / "source_type.yaml",
        name="new_source_type_2",
    )

    check(example_landing_zone) == 0


def test_changing_special_value_definition_of_source_type(
    add, overwrite, check, example_landing_zone, example_landing_zone_config
):
    # how can I see whether this worked?
    assert (
        add(
            example_landing_zone_config,
            example_landing_zone,
            "source_type",
            name="new_source_type",
            description="",
            manufacturer="me",
            special_values=[
                dict(categorical_value="NA", numerical_value=-1),
                dict(categorical_value="OVERFLOW", numerical_value=-2),
                dict(categorical_value="UNDERFLOW", numerical_value=-3),
            ],
        )
        == 0
    )

    check(example_landing_zone)

    overwrite(
        example_landing_zone / "data" / "new_source_type" / "source_type.yaml",
        special_values=[
            dict(categorical_value="NA", numerical_value=-1),
            dict(categorical_value="INBETWEENFLOW", numerical_value=-4),
        ],
    )

    check(example_landing_zone)
