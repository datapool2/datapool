gitlab-runner exec docker\
	--docker-services_privileged true\
       	--docker-privileged\
       	--env https_proxy=http://proxy.ethz.ch:3128\
       	--env CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX=docker.io\
        --env CI_COMMIT_REF_NAME=main\
        --env CI_COMMIT_SHORT_SHA=eca51321\
	test_ansible_script
