#!/bin/bash
#
# workflow.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#
ROOT=$(mktemp -d)
export ETC=${ROOT}/etc
LZ=${ROOT}/lz
DLZ=${ROOT}/dlz
RLZ=${ROOT}/rlz
BACKUP_LZ=${ROOT}/backup_lz
mkdir ${BACKUP_LZ}

export PDB=1

if [[ $(uname) == Linux ]];
then 
	SED_INPLACE="sed -i "
else
	SED_INPLACE="sed -i '' "
fi

set -e


function setup {
    # https://unix.stackexchange.com/questions/55913/
    FREE_PORT="$(python -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1])')";


    pool init-config --use-sqlitedb ${LZ}\
        backup_landing_zone.folder=${BACKUP_LZ}\
        http_server.port=${FREE_PORT} \
	http_server.log_file=${ROOT}/datapool.log

    pool check-config
    pool init-db
    pool create-example ${DLZ}

    pool add source ${DLZ} name=another_source source_type=sensor_from_xyz description= project=sis_datapool serial=00000

    ${SED_INPLACE} -e 's/sensor_abc_from_xyz/another_source/g' ${DLZ}/data/sensor_from_xyz/conversion.py

    pool check ${DLZ}

    if false; then
        tmux split-window -d -e "ETC=${ETC}" -e "TMUX_PARENT_PANE_ID=#{pane_id}" 'pool --pdb run-server'
    else
        tmux split-window -d -e "ETC=${ETC}" -e "TMUX_PARENT_PANE_ID=#{pane_id}" 'pool --pdb run-server; read  -n 1 -p "PRESS ENTER TO CLOSE WINDOWS"'
    fi
    echo
    echo wait for server to start
    while true; do
        echo try to connect to port ${FREE_PORT}
        curl 127.0.0.1:${FREE_PORT} &>/dev/null && break
        sleep 1
    done

    function finish {
        sleep 1
        kill $(cat ${ETC}/datapool.pid)
    }
    trap finish EXIT

    set -e
    pool update-operational ${DLZ}

    sleep 5
    pool show signal --no-pager --max-rows 20
    echo
    pool show binary_data --no-pager --max-rows 10
    echo
    pool show lab_result --no-pager --max-rows 10
    echo

}



function update_lab_results {
    cp ${DLZ}/lab_results/lab_results.csv /tmp/x.csv
    tail -n 17 /tmp/x.csv >> ${DLZ}/lab_results/lab_results.csv
    echo
    echo -----------------------------------------------
    cat ${DLZ}/lab_results/lab_results.csv
    echo -----------------------------------------------
    echo

    pool update-operational ${DLZ}

    sleep 5
}


function add_meta_action_type {

    pool add meta_log_type ${DLZ} name=abc description=
    echo
    pool add meta_action_type ${DLZ} meta_log_type=abc name=ddd description=dddd
    echo

    # (cd ${ROOT}; bash)
    pool update-operational ${DLZ}

    sleep 5
}

function add_source {

    pool add source ${DLZ} name=abc source_type=sensor_from_xyz description= project=sis_datapool serial=00000
    echo

    # (cd ${ROOT}; bash)
    pool update-operational ${DLZ}

    sleep 5
}


function rename_site {

    pool show site --no-pager

    rm -rf ${RLZ}
    sudo pool start-rename --what site ${RLZ} test_site example_site

    echo
    echo

    pool check ${RLZ} || true
    echo
    echo

    source_yaml=$(find ${RLZ} -name source.yaml | head -1)
    source_folder=$(dirname $source_yaml)
    mkdir -p $source_folder/binary_data

    chmod a+w $source_folder/binary_data
    cat > $source_folder/binary_data/binary_data_spec.yaml<<EOL
site: example_site
parameter: images
timestamp: 2020-12-22 13:30:00
file: images.npz
EOL

    cp ${DLZ}/data/*/*/binary_data/images.npz $source_folder/binary_data

    ${SED_INPLACE} -e 's/test_site/example_site/g' \
         ${RLZ}/lab_results/lab_results.csv \
         ${RLZ}/data/*/*/conversion.* \
         ${RLZ}/data/*/conversion.* \
         ${RLZ}/meta_data/*/*/meta_data_history.yaml

    check ${RLZ}
    echo

    pool update-operational ${RLZ}
    echo

    sleep 5
}


function add_parameter {

    pool add parameter ${DLZ} name=abc unit=1 description="this is a very long parameter description which should span multiple lines in the final parameters.yaml file and this caused issues before when updating the landing zone"
    pool update-operational ${DLZ}
    sleep 5
}

function rename_parameter {
    rm -rf ${RLZ}
    sudo pool start-rename --what parameter ${RLZ} "Water Level" "Whatter Levl"

    echo
    pool check ${RLZ} || true
    echo
    chmod -R a+rwx ${RLZ}
    set -x
    ${SED_INPLACE} -e 's/Water Level/Whatter Levl/g' ${RLZ}/data/*/*/conversion.*
    ${SED_INPLACE} -e 's/Water Level/Whatter Levl/g' ${RLZ}/data/*/conversion.*
    set +x

    pool update-operational ${RLZ}

    sleep 5
}

function rename_source {
    rm -rf ${RLZ}
    sudo pool start-rename --what source ${RLZ} sensor_abc_from_xyz example_sensor
    sudo sudo pool check ${RLZ} || true

    ${SED_INPLACE} -e 's/sensor_abc_from_xyz/example_sensor/g' ${RLZ}/meta_data/*/*/meta_data_history.yaml
    pool update-operational ${RLZ}

    echo
    echo
    echo
    echo

    sudo pool start-rename --what source ${RLZ}_2 example_sensor example_sensor_2
    pool check ${RLZ}_2 || true
    ${SED_INPLACE} -e 's/example_sensor/example_sensor_2/g' ${RLZ}_2/meta_data/*/*/meta_data_history.yaml
    echo
    for U in ${BACKUP_LZ}/data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data*raw*; do
        set -x 
        cp $U ${RLZ}_2/data/sensor_from_xyz/example_sensor_2/raw_data/data-001.raw
        set +x
        echo restored ${RLZ}_2/data/sensor_from_xyz/example_sensor_2/raw_data/$U
    done

    pool update-operational ${RLZ}_2
}


function rename_source_type {
    rm -rf ${RLZ}
    sudo pool start-rename --what source_type ${RLZ} sensor_from_xyz example_sensor_type
    pool check ${RLZ}
    pool update-operational ${RLZ}

}

function rename_project {
    rm -rf ${RLZ}
    sudo pool start-rename --what project ${RLZ} sis_datapool datapool2
    pool check ${RLZ} || true
    ${SED_INPLACE} -e 's/sis_datapool/datapool2/g' ${RLZ}/data/*/*/source.yaml
    pool update-operational ${RLZ}

}

function rename_person {
    rm -rf ${RLZ}
    sudo pool start-rename --what person ${RLZ} schmittu uwe.schmitt
    pool check ${RLZ} || true
    ${SED_INPLACE} -e 's/schmittu/uwe.schmitt/g' ${RLZ}/lab_results/lab_results.csv
    ${SED_INPLACE} -e 's/schmittu/uwe.schmitt/g' ${RLZ}/meta_data/*/*/meta_data_history.yaml
    pool update-operational ${RLZ}

}

function rename_meta_action_type {
    rm -rf ${RLZ}
    sudo pool start-rename --what meta_action_type ${RLZ} checked check
    pool check ${RLZ} || true
    # ${SED_INPLACE} -e 's/schmittu/uwe.schmitt/g' ${RLZ}/lab_results/lab_results.csv
    # ${SED_INPLACE} -e 's/schmittu/uwe.schmitt/g' ${RLZ}/meta_data/*/*/meta_data_history.yaml
    ${SED_INPLACE} -e 's/checked/check/g' ${RLZ}/meta_data/sensor_abc_from_xyz/test_site/meta_data_history.yaml
    pool update-operational ${RLZ}

}

function rename_meta_log_type {
    rm -rf ${RLZ}
    sudo pool start-rename --what meta_log_type ${RLZ} new_sensor sensor_new
    pool check ${RLZ} || true
    ${SED_INPLACE} -e 's/new_sensor/sensor_new/g' ${RLZ}/meta_data/meta_action_types.yaml\
                                                  ${RLZ}/meta_data/*/*/meta_data_history.yaml
    pool update-operational ${RLZ}

}

setup
# update_lab_results
# add_meta_action_type
# add_parameter
# add_source

#rename_meta_log_type
#rename_meta_action_type
#rename_person
rename_source
rename_site
rename_parameter
rename_source_type
rename_project

sleep 666
