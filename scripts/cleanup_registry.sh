#!/usr/bin/env bash
#
# cleanup_registry.sh
# Copyright (C) 2023 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#
#
set -e

# one day: 60 * 60 * 24 = 86400
CLEANUP_SECONDS=${CLEANUP_SECONDS:-86400}

CURL="curl --silent --header PRIVATE-TOKEN:$PRIVATE_TOKEN_CI"

function repository_id {
   ${CURL} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories | jq '.[] | select (.path=="'${CI_PROJECT_PATH}'") | .id';
}

REPO_ID=$(repository_id)

function list_tags {
    ${CURL} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${REPO_ID}/tags | tee | jq -r '.[] | select (.name|test("datapool-temp-[0-9a-z]{8}$")) | .name';
}


for tag in $(list_tags); do
    created_at=$(${CURL} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${REPO_ID}/tags/$tag  | jq -r .created_at)
    seconds=$(date -d "$created_at" +%s)
    now=$(date +%s)
    age_in_seconds=$(( now - seconds ))
    echo "image with tag $tag is $age_in_seconds seconds old"
    if [[ $age_in_seconds -gt ${CLEANUP_SECONDS} ]]; then
        echo delete $tag
        $CURL -X DELETE ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${REPO_ID}/tags/$tag
    fi
done
