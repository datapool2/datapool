#!/usr/bin/env python

from sqlalchemy import (
    Column,
    Integer,
    LargeBinary,
    MetaData,
    Table,
    create_engine,
    insert,
)
from sqlalchemy.sql import text

conn_str = "postgresql://postgres:datapoolrocks666@127.0.0.1:5432/datapool"

engine = create_engine(conn_str, echo=True)

if 0:
    engine.execute(
        text(
            """
    CREATE EXTENSION postgis;
    CREATE TABLE raster_test
    (id INTEGER,
     data RASTER);
    """
        )
    )
engine.execute(
    text(
        """
    SET postgis.gdal_enabled_drivers = 'ENABLE_ALL';
    """
    )
)
res = engine.execute(
    text(
        """
    SELECT short_name 
     FROM ST_GDALDrivers();
    """
    )
)
print(list(res))

meta = MetaData(bind=engine)

data = open("masked-1990.tif", "rb").read()
schema = Table(
    "raster_test",
    meta,
    Column("id", Integer(), primary_key=True),
    Column("data", LargeBinary(), nullable=False),
)

# engine.execute(insert(schema).values(data=data))

engine.execute(
    text(
        """
    INSERT INTO raster_test VALUES(NULL, ST_FromGDALRaster(:data));
    """,
    ),
    data=data,
)
