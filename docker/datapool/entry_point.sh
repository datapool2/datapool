#!/usr/bin/env bash
umask 0002

if [[ $1 != pool ]]; then
    exec $@
fi

export PGPASSWORD=${DATAPOOL_PASSWORD}
pg='psql -h postgis -U datapool -d datapool'

while true; do
    echo -n "check if database is up and initialized.. "
    $pg -c 'select count(*) from raster_data;' &>/dev/null && break
    echo "data base not up yet..."
    sleep 5
done
echo "data base is up!"

if test -f ${ETC:-/etc}/datapool/datapool.ini; then
    echo "${ETC:-/etc}/datapool/datapool.ini exists"
else
    echo 'setup config and database'
    pool admin init-config /lz \
       db.connection_string=postgresql://datapool:${DATAPOOL_PASSWORD}@postgis:${POSTGRES_PORT}/datapool \
       backup_landing_zone.folder=/backup_lz \
       http_server.log_file=$HOME/datapool.log \
       server.pid_file=$HOME/datapool.pid \
       site.fields=${SITE_FIELDS}
    pool admin check-config
    pool admin init-db
    $pg < /tmp/setup_db.sql
fi

if [[ $3 == run-server ]]; then
    # can be left over from crashed container:
    rm -f /datapool.pid
fi

cd /development

test -x /extra/setup.sh && { echo "run /extra/setup.sh"; bash /extra/setup.sh; }

exec "$@"
