CREATE TABLE for_shiny
(
signal_id      integer,
signal_quality_id integer,
timestamp      timestamp NOT NULL,
value          double precision,
unit           varchar,
variable_name varchar,
source_type_name  varchar,
source_name    varchar,
site_name      varchar
);

SELECT create_hypertable('for_shiny', 'timestamp');

CREATE INDEX ix_for_shiny_variable_name ON for_shiny(variable_name);
CREATE INDEX ix_for_shiny_site_name ON for_shiny(site_name);
CREATE INDEX ix_for_shiny_signal_id ON for_shiny(signal_id);


\echo 'create function'
CREATE OR REPLACE FUNCTION public.update_for_shiny(last_n_days interval) RETURNS integer AS $$

DECLARE
   counter    INTEGER := 1;

BEGIN

        INSERT INTO for_shiny

        WITH LIMIT_TS AS (select min(timestamp) from signal where timestamp >= current_date - LAST_N_DAYS)
        SELECT signal.signal_id,
                   signal_quality.signal_quality_id,
                   signal.timestamp,
                   value,
                   unit,
                   variable.name,
                   source_type.name,
                   source.name,
                   site.name
            FROM   signal
                   inner join site
                           ON signal.site_id = site.site_id
                   inner join variable
                           ON signal.variable_id = variable.variable_id
                   inner join source
                           ON signal.source_id = source.source_id
                   inner join source_type
                           ON source.source_type_id = source_type.source_type_id
                   inner join signals_signal_quality_association
                           ON signals_signal_quality_association.signal_id =
                              signal.signal_id
                   inner join signal_quality
                           ON signals_signal_quality_association.signal_quality_id =
                              signal_quality.signal_quality_id
            WHERE
                   signal.timestamp >= (SELECT * FROM LIMIT_TS)
                   AND signal_quality.quality_id = 7
            ON CONFLICT DO NOTHING;

        GET DIAGNOSTICS counter = ROW_COUNT;
        RETURN counter;

END;
$$ LANGUAGE plpgsql;

\echo 'create hypertable'
ALTER TABLE signal DROP CONSTRAINT signal_pkey CASCADE;
SELECT create_hypertable('signal', 'timestamp');

\echo 'set datapool_reader permissions'

-- changes privileges for the datapool_reader triggers warnings related
-- to internal postgis tables, we suppress these:
SET client_min_messages = 'ERROR';
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM datapool_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO datapool_reader;
-- enable warnings again
SET client_min_messages = 'WARNING';
