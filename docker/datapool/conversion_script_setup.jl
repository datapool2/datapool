#=
    conversion_script_setup
    Copyright © 2021 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

    Distributed under terms of the MIT license.
=#

using Pkg
Pkg.add("DataFrames")
Pkg.add("CSV")
