--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: timescaledb; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS timescaledb WITH SCHEMA public;


--
-- Name: EXTENSION timescaledb; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION timescaledb IS 'Enables scalable inserts and complex queries for time-series data';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';


--
-- Name: postgis_raster; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_raster WITH SCHEMA public;


--
-- Name: EXTENSION postgis_raster; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_raster IS 'PostGIS raster types and functions';


--
-- Name: update_for_shiny(interval); Type: FUNCTION; Schema: public; Owner: datapool
--

CREATE FUNCTION public.update_for_shiny(last_n_days interval) RETURNS integer
    LANGUAGE plpgsql
    AS $$

DECLARE
   counter    INTEGER := 1;

BEGIN

        INSERT INTO for_shiny

        WITH LIMIT_TS AS (select min(timestamp) from signal where timestamp >= current_date - LAST_N_DAYS)
        SELECT signal.signal_id,
                   signal_quality.signal_quality_id,
                   signal.timestamp,
                   value,
                   unit,
                   parameter.name,
                   source_type.name,
                   source.name,
                   site.name
            FROM   signal
                   inner join site
                           ON signal.site_id = site.site_id
                   inner join parameter
                           ON signal.parameter_id = parameter.parameter_id
                   inner join source
                           ON signal.source_id = source.source_id
                   inner join source_type
                           ON source.source_type_id = source_type.source_type_id
                   inner join signals_signal_quality_association
                           ON signals_signal_quality_association.signal_id =
                              signal.signal_id
                   inner join signal_quality
                           ON signals_signal_quality_association.signal_quality_id =
                              signal_quality.signal_quality_id
            WHERE
                   signal.timestamp >= (SELECT * FROM LIMIT_TS)
                   AND signal_quality.quality_id = 7
            ON CONFLICT DO NOTHING;

        GET DIAGNOSTICS counter = ROW_COUNT;
        RETURN counter;

END;
$$;


ALTER FUNCTION public.update_for_shiny(last_n_days interval) OWNER TO datapool;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: binary_data; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.binary_data (
    binary_data_id integer NOT NULL,
    data bytea NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    variable_id integer NOT NULL,
    source_id integer NOT NULL,
    site_id integer
);


ALTER TABLE public.binary_data OWNER TO datapool;

--
-- Name: binary_data_binary_data_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.binary_data_binary_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.binary_data_binary_data_id_seq OWNER TO datapool;

--
-- Name: binary_data_binary_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.binary_data_binary_data_id_seq OWNED BY public.binary_data.binary_data_id;


--
-- Name: for_shiny; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.for_shiny (
    signal_id integer,
    signal_quality_id integer,
    "timestamp" timestamp without time zone NOT NULL,
    value double precision,
    unit character varying,
    parameter_name character varying,
    source_type_name character varying,
    source_name character varying,
    site_name character varying
);


ALTER TABLE public.for_shiny OWNER TO datapool;

--
-- Name: lab_result; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.lab_result (
    lab_result_id integer NOT NULL,
    lab_identifier character varying NOT NULL,
    variable_id integer NOT NULL,
    sample_identifier character varying NOT NULL,
    filter_lab character varying NOT NULL,
    dilution_lab double precision,
    method_lab character varying NOT NULL,
    value_lab double precision NOT NULL,
    description_lab character varying,
    timestamp_start_lab timestamp without time zone,
    timestamp_end_lab timestamp without time zone NOT NULL,
    site_id integer NOT NULL,
    filter_sample character varying NOT NULL,
    dilution_sample double precision,
    timestamp_sample timestamp without time zone,
    method_sample character varying,
    description_sample character varying
);


ALTER TABLE public.lab_result OWNER TO datapool;

--
-- Name: lab_result_lab_result_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.lab_result_lab_result_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lab_result_lab_result_id_seq OWNER TO datapool;

--
-- Name: lab_result_lab_result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.lab_result_lab_result_id_seq OWNED BY public.lab_result.lab_result_id;


--
-- Name: lab_result_person_lab_association; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.lab_result_person_lab_association (
    lab_result_id integer NOT NULL,
    person_id integer NOT NULL
);


ALTER TABLE public.lab_result_person_lab_association OWNER TO datapool;

--
-- Name: lab_result_person_sample_association; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.lab_result_person_sample_association (
    lab_result_id integer NOT NULL,
    person_id integer NOT NULL
);


ALTER TABLE public.lab_result_person_sample_association OWNER TO datapool;

--
-- Name: meta_action_type; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_action_type (
    meta_action_type_id integer NOT NULL,
    meta_log_type_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.meta_action_type OWNER TO datapool;

--
-- Name: meta_action_type_meta_action_type_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_action_type_meta_action_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_action_type_meta_action_type_id_seq OWNER TO datapool;

--
-- Name: meta_action_type_meta_action_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_action_type_meta_action_type_id_seq OWNED BY public.meta_action_type.meta_action_type_id;


--
-- Name: meta_data; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_data (
    meta_data_id integer NOT NULL,
    source_id integer NOT NULL,
    site_id integer NOT NULL,
    description character varying,
    additional_meta_info json
);


ALTER TABLE public.meta_data OWNER TO datapool;

--
-- Name: meta_data_history; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_data_history (
    meta_data_history_id integer NOT NULL,
    meta_data_id integer NOT NULL,
    meta_log_type_id integer NOT NULL,
    meta_action_type_id integer,
    meta_flag_id integer,
    person_id integer NOT NULL,
    timestamp_start timestamp without time zone NOT NULL,
    timestamp_end timestamp without time zone NOT NULL,
    comment character varying,
    additional_meta_info json
);


ALTER TABLE public.meta_data_history OWNER TO datapool;

--
-- Name: meta_data_history_meta_data_history_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_data_history_meta_data_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_data_history_meta_data_history_id_seq OWNER TO datapool;

--
-- Name: meta_data_history_meta_data_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_data_history_meta_data_history_id_seq OWNED BY public.meta_data_history.meta_data_history_id;


--
-- Name: meta_data_meta_data_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_data_meta_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_data_meta_data_id_seq OWNER TO datapool;

--
-- Name: meta_data_meta_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_data_meta_data_id_seq OWNED BY public.meta_data.meta_data_id;


--
-- Name: meta_flag; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_flag (
    meta_flag_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.meta_flag OWNER TO datapool;

--
-- Name: meta_flag_meta_flag_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_flag_meta_flag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_flag_meta_flag_id_seq OWNER TO datapool;

--
-- Name: meta_flag_meta_flag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_flag_meta_flag_id_seq OWNED BY public.meta_flag.meta_flag_id;


--
-- Name: meta_log_type; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_log_type (
    meta_log_type_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.meta_log_type OWNER TO datapool;

--
-- Name: meta_log_type_meta_log_type_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_log_type_meta_log_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_log_type_meta_log_type_id_seq OWNER TO datapool;

--
-- Name: meta_log_type_meta_log_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_log_type_meta_log_type_id_seq OWNED BY public.meta_log_type.meta_log_type_id;


--
-- Name: meta_picture; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_picture (
    picture_id integer NOT NULL,
    meta_data_id integer,
    meta_data_history_id integer,
    filename character varying NOT NULL,
    description character varying,
    data bytea
);


ALTER TABLE public.meta_picture OWNER TO datapool;

--
-- Name: meta_picture_picture_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_picture_picture_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_picture_picture_id_seq OWNER TO datapool;

--
-- Name: meta_picture_picture_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_picture_picture_id_seq OWNED BY public.meta_picture.picture_id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.person (
    person_id integer NOT NULL,
    abbreviation character varying NOT NULL,
    name character varying NOT NULL,
    email character varying
);


ALTER TABLE public.person OWNER TO datapool;

--
-- Name: person_person_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.person_person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_person_id_seq OWNER TO datapool;

--
-- Name: person_person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.person_person_id_seq OWNED BY public.person.person_id;


--
-- Name: picture; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.picture (
    picture_id integer NOT NULL,
    site_id integer NOT NULL,
    filename character varying NOT NULL,
    description character varying,
    "timestamp" timestamp without time zone,
    data bytea
);


ALTER TABLE public.picture OWNER TO datapool;

--
-- Name: picture_picture_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.picture_picture_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.picture_picture_id_seq OWNER TO datapool;

--
-- Name: picture_picture_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.picture_picture_id_seq OWNED BY public.picture.picture_id;


--
-- Name: project; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.project (
    project_id integer NOT NULL,
    title character varying NOT NULL,
    abbrev character varying,
    description character varying
);


ALTER TABLE public.project OWNER TO datapool;

--
-- Name: project_project_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.project_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_project_id_seq OWNER TO datapool;

--
-- Name: project_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.project_project_id_seq OWNED BY public.project.project_id;


--
-- Name: quality; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.quality (
    quality_id integer NOT NULL,
    flag character varying NOT NULL,
    method character varying
);


ALTER TABLE public.quality OWNER TO datapool;

--
-- Name: quality_quality_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.quality_quality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quality_quality_id_seq OWNER TO datapool;

--
-- Name: quality_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.quality_quality_id_seq OWNED BY public.quality.quality_id;


--
-- Name: raster_data; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.raster_data (
    raster_data_id integer NOT NULL,
    source_id integer,
    parameter_id integer,
    "timestamp" timestamp without time zone,
    data public.raster
);


ALTER TABLE public.raster_data OWNER TO datapool;

--
-- Name: raster_data_raster_data_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.raster_data_raster_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raster_data_raster_data_id_seq OWNER TO datapool;

--
-- Name: raster_data_raster_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.raster_data_raster_data_id_seq OWNED BY public.raster_data.raster_data_id;


--
-- Name: signal; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.signal (
    signal_id integer NOT NULL,
    value double precision NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    variable_id integer NOT NULL,
    source_id integer NOT NULL,
    site_id integer,
    coord_x character varying,
    coord_y character varying,
    coord_z character varying
);


ALTER TABLE public.signal OWNER TO datapool;

--
-- Name: signal_quality; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.signal_quality (
    signal_quality_id integer NOT NULL,
    quality_id integer NOT NULL,
    "timestamp" timestamp without time zone,
    author character varying
);


ALTER TABLE public.signal_quality OWNER TO datapool;

--
-- Name: signal_quality_signal_quality_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.signal_quality_signal_quality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.signal_quality_signal_quality_id_seq OWNER TO datapool;

--
-- Name: signal_quality_signal_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.signal_quality_signal_quality_id_seq OWNED BY public.signal_quality.signal_quality_id;


--
-- Name: signal_signal_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.signal_signal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.signal_signal_id_seq OWNER TO datapool;

--
-- Name: signal_signal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.signal_signal_id_seq OWNED BY public.signal.signal_id;


--
-- Name: signals_signal_quality_association; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.signals_signal_quality_association (
    signal_id integer NOT NULL,
    signal_quality_id integer NOT NULL
);


ALTER TABLE public.signals_signal_quality_association OWNER TO datapool;

--
-- Name: site; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.site (
    site_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.site OWNER TO datapool;

--
-- Name: site_field; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.site_field (
    site_field_id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.site_field OWNER TO datapool;

--
-- Name: site_field_site_field_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.site_field ALTER COLUMN site_field_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.site_field_site_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: site_field_values; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.site_field_values (
    site_field_value_id integer NOT NULL,
    site_id integer NOT NULL,
    site_field_id integer NOT NULL,
    value character varying
);


ALTER TABLE public.site_field_values OWNER TO datapool;

--
-- Name: site_field_values_site_field_value_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.site_field_values ALTER COLUMN site_field_value_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.site_field_values_site_field_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: site_site_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.site ALTER COLUMN site_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.site_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: source; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.source (
    source_id integer NOT NULL,
    source_type_id integer NOT NULL,
    project_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    serial character varying
);


ALTER TABLE public.source OWNER TO datapool;

--
-- Name: source_source_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.source ALTER COLUMN source_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.source_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: source_type; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.source_type (
    source_type_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    manufacturer character varying
);


ALTER TABLE public.source_type OWNER TO datapool;

--
-- Name: source_type_source_type_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.source_type_source_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.source_type_source_type_id_seq OWNER TO datapool;

--
-- Name: source_type_source_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.source_type_source_type_id_seq OWNED BY public.source_type.source_type_id;


--
-- Name: special_value_definition; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.special_value_definition (
    special_value_definition_id integer NOT NULL,
    source_type_id integer NOT NULL,
    description character varying,
    categorical_value character varying NOT NULL,
    numerical_value double precision NOT NULL
);


ALTER TABLE public.special_value_definition OWNER TO datapool;

--
-- Name: special_value_definition_special_value_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.special_value_definition_special_value_definition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.special_value_definition_special_value_definition_id_seq OWNER TO datapool;

--
-- Name: special_value_definition_special_value_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.special_value_definition_special_value_definition_id_seq OWNED BY public.special_value_definition.special_value_definition_id;


--
-- Name: variable; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.variable (
    variable_id integer NOT NULL,
    name character varying NOT NULL,
    unit character varying NOT NULL,
    description character varying
);


ALTER TABLE public.variable OWNER TO datapool;

--
-- Name: variable_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.variable_variable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.variable_variable_id_seq OWNER TO datapool;

--
-- Name: variable_variable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.variable_variable_id_seq OWNED BY public.variable.variable_id;


--
-- Name: binary_data binary_data_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.binary_data ALTER COLUMN binary_data_id SET DEFAULT nextval('public.binary_data_binary_data_id_seq'::regclass);


--
-- Name: lab_result lab_result_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result ALTER COLUMN lab_result_id SET DEFAULT nextval('public.lab_result_lab_result_id_seq'::regclass);


--
-- Name: meta_action_type meta_action_type_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_action_type ALTER COLUMN meta_action_type_id SET DEFAULT nextval('public.meta_action_type_meta_action_type_id_seq'::regclass);


--
-- Name: meta_data meta_data_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data ALTER COLUMN meta_data_id SET DEFAULT nextval('public.meta_data_meta_data_id_seq'::regclass);


--
-- Name: meta_data_history meta_data_history_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history ALTER COLUMN meta_data_history_id SET DEFAULT nextval('public.meta_data_history_meta_data_history_id_seq'::regclass);


--
-- Name: meta_flag meta_flag_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_flag ALTER COLUMN meta_flag_id SET DEFAULT nextval('public.meta_flag_meta_flag_id_seq'::regclass);


--
-- Name: meta_log_type meta_log_type_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_log_type ALTER COLUMN meta_log_type_id SET DEFAULT nextval('public.meta_log_type_meta_log_type_id_seq'::regclass);


--
-- Name: meta_picture picture_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_picture ALTER COLUMN picture_id SET DEFAULT nextval('public.meta_picture_picture_id_seq'::regclass);


--
-- Name: person person_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.person ALTER COLUMN person_id SET DEFAULT nextval('public.person_person_id_seq'::regclass);


--
-- Name: picture picture_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.picture ALTER COLUMN picture_id SET DEFAULT nextval('public.picture_picture_id_seq'::regclass);


--
-- Name: project project_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project ALTER COLUMN project_id SET DEFAULT nextval('public.project_project_id_seq'::regclass);


--
-- Name: quality quality_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.quality ALTER COLUMN quality_id SET DEFAULT nextval('public.quality_quality_id_seq'::regclass);


--
-- Name: raster_data raster_data_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.raster_data ALTER COLUMN raster_data_id SET DEFAULT nextval('public.raster_data_raster_data_id_seq'::regclass);


--
-- Name: signal signal_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal ALTER COLUMN signal_id SET DEFAULT nextval('public.signal_signal_id_seq'::regclass);


--
-- Name: signal_quality signal_quality_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal_quality ALTER COLUMN signal_quality_id SET DEFAULT nextval('public.signal_quality_signal_quality_id_seq'::regclass);


--
-- Name: source_type source_type_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source_type ALTER COLUMN source_type_id SET DEFAULT nextval('public.source_type_source_type_id_seq'::regclass);


--
-- Name: special_value_definition special_value_definition_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition ALTER COLUMN special_value_definition_id SET DEFAULT nextval('public.special_value_definition_special_value_definition_id_seq'::regclass);


--
-- Name: variable variable_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.variable ALTER COLUMN variable_id SET DEFAULT nextval('public.variable_variable_id_seq'::regclass);


--
-- Name: binary_data binary_data_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.binary_data
    ADD CONSTRAINT binary_data_pkey PRIMARY KEY (binary_data_id);


--
-- Name: lab_result lab_result_lab_identifier_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_lab_identifier_key UNIQUE (lab_identifier);


--
-- Name: lab_result lab_result_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_pkey PRIMARY KEY (lab_result_id);


--
-- Name: meta_action_type meta_action_type_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_action_type
    ADD CONSTRAINT meta_action_type_pkey PRIMARY KEY (meta_action_type_id);


--
-- Name: meta_data_history meta_data_history_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_pkey PRIMARY KEY (meta_data_history_id);


--
-- Name: meta_data meta_data_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_pkey PRIMARY KEY (meta_data_id);


--
-- Name: meta_data meta_data_source_id_site_id_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_source_id_site_id_key UNIQUE (source_id, site_id);


--
-- Name: meta_flag meta_flag_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_flag
    ADD CONSTRAINT meta_flag_pkey PRIMARY KEY (meta_flag_id);


--
-- Name: meta_log_type meta_log_type_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_log_type
    ADD CONSTRAINT meta_log_type_pkey PRIMARY KEY (meta_log_type_id);


--
-- Name: meta_picture meta_picture_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_picture
    ADD CONSTRAINT meta_picture_pkey PRIMARY KEY (picture_id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (person_id);


--
-- Name: picture picture_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.picture
    ADD CONSTRAINT picture_pkey PRIMARY KEY (picture_id);


--
-- Name: picture picture_site_id_filename_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.picture
    ADD CONSTRAINT picture_site_id_filename_key UNIQUE (site_id, filename);


--
-- Name: project project_abbrev_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_abbrev_key UNIQUE (abbrev);


--
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (project_id);


--
-- Name: quality quality_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.quality
    ADD CONSTRAINT quality_pkey PRIMARY KEY (quality_id);


--
-- Name: raster_data raster_data_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.raster_data
    ADD CONSTRAINT raster_data_pkey PRIMARY KEY (raster_data_id);


--
-- Name: signal_quality signal_quality_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal_quality
    ADD CONSTRAINT signal_quality_pkey PRIMARY KEY (signal_quality_id);


--
-- Name: site_field site_field_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.site_field
    ADD CONSTRAINT site_field_pkey PRIMARY KEY (site_field_id);


--
-- Name: site_field_values site_field_values_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.site_field_values
    ADD CONSTRAINT site_field_values_pkey PRIMARY KEY (site_field_value_id);


--
-- Name: site site_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (site_id);


--
-- Name: source source_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_pkey PRIMARY KEY (source_id);


--
-- Name: source_type source_type_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source_type
    ADD CONSTRAINT source_type_pkey PRIMARY KEY (source_type_id);


--
-- Name: special_value_definition special_value_definition_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_pkey PRIMARY KEY (special_value_definition_id);


--
-- Name: special_value_definition special_value_definition_source_type_id_numerical_value_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_source_type_id_numerical_value_key UNIQUE (source_type_id, numerical_value);


--
-- Name: variable variable_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (variable_id);


--
-- Name: for_shiny_timestamp_idx; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX for_shiny_timestamp_idx ON public.for_shiny USING btree ("timestamp" DESC);


--
-- Name: ix_binary_data_timestamp; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_binary_data_timestamp ON public.binary_data USING btree ("timestamp");


--
-- Name: ix_for_shiny_parameter_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_for_shiny_parameter_name ON public.for_shiny USING btree (parameter_name);


--
-- Name: ix_for_shiny_signal_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_for_shiny_signal_id ON public.for_shiny USING btree (signal_id);


--
-- Name: ix_for_shiny_site_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_for_shiny_site_name ON public.for_shiny USING btree (site_name);


--
-- Name: ix_meta_action_type_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_action_type_name ON public.meta_action_type USING btree (name);


--
-- Name: ix_meta_data_history_meta_data_history_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_meta_data_history_id ON public.meta_data_history USING btree (meta_data_history_id);


--
-- Name: ix_meta_data_history_timestamp_end; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_timestamp_end ON public.meta_data_history USING btree (timestamp_end);


--
-- Name: ix_meta_data_history_timestamp_start; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_timestamp_start ON public.meta_data_history USING btree (timestamp_start);


--
-- Name: ix_meta_data_meta_data_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_meta_data_id ON public.meta_data USING btree (meta_data_id);


--
-- Name: ix_meta_flag_meta_flag_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_flag_meta_flag_id ON public.meta_flag USING btree (meta_flag_id);


--
-- Name: ix_meta_log_type_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_meta_log_type_name ON public.meta_log_type USING btree (name);


--
-- Name: ix_meta_picture_picture_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_picture_picture_id ON public.meta_picture USING btree (picture_id);


--
-- Name: ix_person_abbreviation; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_person_abbreviation ON public.person USING btree (abbreviation);


--
-- Name: ix_picture_picture_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_picture_picture_id ON public.picture USING btree (picture_id);


--
-- Name: ix_project_project_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_project_project_id ON public.project USING btree (project_id);


--
-- Name: ix_quality_quality_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_quality_quality_id ON public.quality USING btree (quality_id);


--
-- Name: ix_signal_quality_quality_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_signal_quality_quality_id ON public.signal_quality USING btree (quality_id);


--
-- Name: ix_signal_quality_signal_quality_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_signal_quality_signal_quality_id ON public.signal_quality USING btree (signal_quality_id);


--
-- Name: ix_signal_timestamp; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_signal_timestamp ON public.signal USING btree ("timestamp");


--
-- Name: ix_signals_signal_quality_association_signal_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_signals_signal_quality_association_signal_id ON public.signals_signal_quality_association USING btree (signal_id);


--
-- Name: ix_signals_signal_quality_association_signal_quality_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_signals_signal_quality_association_signal_quality_id ON public.signals_signal_quality_association USING btree (signal_quality_id);


--
-- Name: ix_site_field_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_site_field_name ON public.site_field USING btree (name);


--
-- Name: ix_site_field_site_field_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_site_field_site_field_id ON public.site_field USING btree (site_field_id);


--
-- Name: ix_site_field_values_site_field_value_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_site_field_values_site_field_value_id ON public.site_field_values USING btree (site_field_value_id);


--
-- Name: ix_site_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_site_name ON public.site USING btree (name);


--
-- Name: ix_site_site_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_site_site_id ON public.site USING btree (site_id);


--
-- Name: ix_source_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_source_name ON public.source USING btree (name);


--
-- Name: ix_source_type_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_source_type_name ON public.source_type USING btree (name);


--
-- Name: ix_source_type_source_type_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_source_type_source_type_id ON public.source_type USING btree (source_type_id);


--
-- Name: ix_variable_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_variable_name ON public.variable USING btree (name);


--
-- Name: for_shiny ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: datapool
--

CREATE TRIGGER ts_insert_blocker BEFORE INSERT ON public.for_shiny FOR EACH ROW EXECUTE FUNCTION _timescaledb_internal.insert_blocker();


--
-- Name: signal ts_insert_blocker; Type: TRIGGER; Schema: public; Owner: datapool
--

CREATE TRIGGER ts_insert_blocker BEFORE INSERT ON public.signal FOR EACH ROW EXECUTE FUNCTION _timescaledb_internal.insert_blocker();


--
-- Name: binary_data binary_data_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.binary_data
    ADD CONSTRAINT binary_data_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: binary_data binary_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.binary_data
    ADD CONSTRAINT binary_data_source_id_fkey FOREIGN KEY (source_id) REFERENCES public.source(source_id);


--
-- Name: binary_data binary_data_variable_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.binary_data
    ADD CONSTRAINT binary_data_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES public.variable(variable_id);


--
-- Name: lab_result_person_lab_association lab_result_person_lab_association_lab_result_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_lab_association
    ADD CONSTRAINT lab_result_person_lab_association_lab_result_id_fkey FOREIGN KEY (lab_result_id) REFERENCES public.lab_result(lab_result_id);


--
-- Name: lab_result_person_lab_association lab_result_person_lab_association_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_lab_association
    ADD CONSTRAINT lab_result_person_lab_association_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(person_id);


--
-- Name: lab_result_person_sample_association lab_result_person_sample_association_lab_result_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_sample_association
    ADD CONSTRAINT lab_result_person_sample_association_lab_result_id_fkey FOREIGN KEY (lab_result_id) REFERENCES public.lab_result(lab_result_id);


--
-- Name: lab_result_person_sample_association lab_result_person_sample_association_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_sample_association
    ADD CONSTRAINT lab_result_person_sample_association_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(person_id);


--
-- Name: lab_result lab_result_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: lab_result lab_result_variable_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES public.variable(variable_id);


--
-- Name: meta_action_type meta_action_type_meta_log_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_action_type
    ADD CONSTRAINT meta_action_type_meta_log_type_id_fkey FOREIGN KEY (meta_log_type_id) REFERENCES public.meta_log_type(meta_log_type_id);


--
-- Name: meta_data_history meta_data_history_meta_action_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_action_type_id_fkey FOREIGN KEY (meta_action_type_id) REFERENCES public.meta_action_type(meta_action_type_id);


--
-- Name: meta_data_history meta_data_history_meta_data_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_data_id_fkey FOREIGN KEY (meta_data_id) REFERENCES public.meta_data(meta_data_id);


--
-- Name: meta_data_history meta_data_history_meta_flag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_flag_id_fkey FOREIGN KEY (meta_flag_id) REFERENCES public.meta_flag(meta_flag_id);


--
-- Name: meta_data_history meta_data_history_meta_log_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_log_type_id_fkey FOREIGN KEY (meta_log_type_id) REFERENCES public.meta_log_type(meta_log_type_id);


--
-- Name: meta_data_history meta_data_history_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(person_id);


--
-- Name: meta_data meta_data_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: meta_data meta_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_source_id_fkey FOREIGN KEY (source_id) REFERENCES public.source(source_id);


--
-- Name: meta_picture meta_picture_meta_data_history_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_picture
    ADD CONSTRAINT meta_picture_meta_data_history_id_fkey FOREIGN KEY (meta_data_history_id) REFERENCES public.meta_data_history(meta_data_history_id);


--
-- Name: meta_picture meta_picture_meta_data_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_picture
    ADD CONSTRAINT meta_picture_meta_data_id_fkey FOREIGN KEY (meta_data_id) REFERENCES public.meta_data(meta_data_id);


--
-- Name: picture picture_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.picture
    ADD CONSTRAINT picture_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: signal_quality signal_quality_quality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal_quality
    ADD CONSTRAINT signal_quality_quality_id_fkey FOREIGN KEY (quality_id) REFERENCES public.quality(quality_id);


--
-- Name: signal signal_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal
    ADD CONSTRAINT signal_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: signal signal_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal
    ADD CONSTRAINT signal_source_id_fkey FOREIGN KEY (source_id) REFERENCES public.source(source_id);


--
-- Name: signal signal_variable_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signal
    ADD CONSTRAINT signal_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES public.variable(variable_id);


--
-- Name: signals_signal_quality_association signals_signal_quality_association_signal_quality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.signals_signal_quality_association
    ADD CONSTRAINT signals_signal_quality_association_signal_quality_id_fkey FOREIGN KEY (signal_quality_id) REFERENCES public.signal_quality(signal_quality_id);


--
-- Name: source source_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(project_id);


--
-- Name: source source_source_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_source_type_id_fkey FOREIGN KEY (source_type_id) REFERENCES public.source_type(source_type_id);


--
-- Name: special_value_definition special_value_definition_source_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_source_type_id_fkey FOREIGN KEY (source_type_id) REFERENCES public.source_type(source_type_id);


--
-- Name: TABLE binary_data; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.binary_data TO datapool_reader;


--
-- Name: TABLE for_shiny; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.for_shiny TO datapool_reader;


--
-- Name: TABLE lab_result; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.lab_result TO datapool_reader;


--
-- Name: TABLE lab_result_person_lab_association; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.lab_result_person_lab_association TO datapool_reader;


--
-- Name: TABLE lab_result_person_sample_association; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.lab_result_person_sample_association TO datapool_reader;


--
-- Name: TABLE meta_action_type; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_action_type TO datapool_reader;


--
-- Name: TABLE meta_data; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_data TO datapool_reader;


--
-- Name: TABLE meta_data_history; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_data_history TO datapool_reader;


--
-- Name: TABLE meta_flag; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_flag TO datapool_reader;


--
-- Name: TABLE meta_log_type; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_log_type TO datapool_reader;


--
-- Name: TABLE meta_picture; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_picture TO datapool_reader;


--
-- Name: TABLE person; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.person TO datapool_reader;


--
-- Name: TABLE picture; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.picture TO datapool_reader;


--
-- Name: TABLE project; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.project TO datapool_reader;


--
-- Name: TABLE quality; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.quality TO datapool_reader;


--
-- Name: TABLE raster_data; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.raster_data TO datapool_reader;


--
-- Name: TABLE signal; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.signal TO datapool_reader;


--
-- Name: TABLE signal_quality; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.signal_quality TO datapool_reader;


--
-- Name: TABLE signals_signal_quality_association; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.signals_signal_quality_association TO datapool_reader;


--
-- Name: TABLE site; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.site TO datapool_reader;


--
-- Name: TABLE site_field; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.site_field TO datapool_reader;


--
-- Name: TABLE site_field_values; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.site_field_values TO datapool_reader;


--
-- Name: TABLE source; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.source TO datapool_reader;


--
-- Name: TABLE source_type; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.source_type TO datapool_reader;


--
-- Name: TABLE special_value_definition; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.special_value_definition TO datapool_reader;


--
-- Name: TABLE variable; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.variable TO datapool_reader;


--
-- PostgreSQL database dump complete
--

