--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: meta_flag; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_flag (
    meta_flag_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE public.meta_flag OWNER TO datapool;

--
-- Name: meta_flag_meta_flag_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_flag_meta_flag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_flag_meta_flag_id_seq OWNER TO datapool;

--
-- Name: meta_flag_meta_flag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_flag_meta_flag_id_seq OWNED BY public.meta_flag.meta_flag_id;


--
-- Name: meta_flag meta_flag_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_flag ALTER COLUMN meta_flag_id SET DEFAULT nextval('public.meta_flag_meta_flag_id_seq'::regclass);


--
-- Name: meta_flag meta_flag_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_flag
    ADD CONSTRAINT meta_flag_pkey PRIMARY KEY (meta_flag_id);


--
-- Name: ix_meta_flag_meta_flag_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_flag_meta_flag_id ON public.meta_flag USING btree (meta_flag_id);


--
-- Name: TABLE meta_flag; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_flag TO datapool_reader;


--
-- PostgreSQL database dump complete
--

