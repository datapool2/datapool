--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: quality; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.quality (
    quality_id integer NOT NULL,
    flag character varying NOT NULL,
    method character varying
);


ALTER TABLE public.quality OWNER TO datapool;

--
-- Name: quality_quality_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.quality_quality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quality_quality_id_seq OWNER TO datapool;

--
-- Name: quality_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.quality_quality_id_seq OWNED BY public.quality.quality_id;


--
-- Name: quality quality_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.quality ALTER COLUMN quality_id SET DEFAULT nextval('public.quality_quality_id_seq'::regclass);


--
-- Name: quality quality_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.quality
    ADD CONSTRAINT quality_pkey PRIMARY KEY (quality_id);


--
-- Name: ix_quality_quality_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_quality_quality_id ON public.quality USING btree (quality_id);


--
-- Name: TABLE quality; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.quality TO datapool_reader;


--
-- PostgreSQL database dump complete
--

