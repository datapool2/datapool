#!/usr/bin/env bash
N=$(basename $(dirname $(realpath $BASH_SOURCE)))
$psql -c "DELETE FROM $N"

cat ./$N.csv | $psql -c "COPY $N FROM STDIN WITH (FORMAT CSV, HEADER, DELIMITER '	')"
