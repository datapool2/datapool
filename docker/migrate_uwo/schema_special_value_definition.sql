--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: special_value_definition; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.special_value_definition (
    special_value_definition_id integer NOT NULL,
    source_type_id integer NOT NULL,
    description character varying,
    categorical_value character varying NOT NULL,
    numerical_value double precision NOT NULL
);


ALTER TABLE public.special_value_definition OWNER TO datapool;

--
-- Name: special_value_definition_special_value_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.special_value_definition_special_value_definition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.special_value_definition_special_value_definition_id_seq OWNER TO datapool;

--
-- Name: special_value_definition_special_value_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.special_value_definition_special_value_definition_id_seq OWNED BY public.special_value_definition.special_value_definition_id;


--
-- Name: special_value_definition special_value_definition_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition ALTER COLUMN special_value_definition_id SET DEFAULT nextval('public.special_value_definition_special_value_definition_id_seq'::regclass);


--
-- Name: special_value_definition special_value_definition_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_pkey PRIMARY KEY (special_value_definition_id);


--
-- Name: special_value_definition special_value_definition_source_type_id_numerical_value_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_source_type_id_numerical_value_key UNIQUE (source_type_id, numerical_value);


--
-- Name: special_value_definition special_value_definition_source_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.special_value_definition
    ADD CONSTRAINT special_value_definition_source_type_id_fkey FOREIGN KEY (source_type_id) REFERENCES public.source_type(source_type_id);


--
-- Name: TABLE special_value_definition; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.special_value_definition TO datapool_reader;


--
-- PostgreSQL database dump complete
--

