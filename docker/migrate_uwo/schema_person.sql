--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: person; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.person (
    person_id integer NOT NULL,
    abbreviation character varying NOT NULL,
    name character varying NOT NULL,
    email character varying
);


ALTER TABLE public.person OWNER TO datapool;

--
-- Name: person_person_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.person_person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_person_id_seq OWNER TO datapool;

--
-- Name: person_person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.person_person_id_seq OWNED BY public.person.person_id;


--
-- Name: person person_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.person ALTER COLUMN person_id SET DEFAULT nextval('public.person_person_id_seq'::regclass);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (person_id);


--
-- Name: ix_person_abbreviation; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_person_abbreviation ON public.person USING btree (abbreviation);


--
-- Name: TABLE person; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.person TO datapool_reader;


--
-- PostgreSQL database dump complete
--

