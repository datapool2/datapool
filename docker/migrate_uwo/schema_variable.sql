--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: variable; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.variable (
    variable_id integer NOT NULL,
    name character varying NOT NULL,
    unit character varying NOT NULL,
    description character varying
);


ALTER TABLE public.variable OWNER TO datapool;

--
-- Name: variable_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.variable_variable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.variable_variable_id_seq OWNER TO datapool;

--
-- Name: variable_variable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.variable_variable_id_seq OWNED BY public.variable.variable_id;


--
-- Name: variable variable_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.variable ALTER COLUMN variable_id SET DEFAULT nextval('public.variable_variable_id_seq'::regclass);


--
-- Name: variable variable_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (variable_id);


--
-- Name: ix_variable_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_variable_name ON public.variable USING btree (name);


--
-- Name: TABLE variable; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.variable TO datapool_reader;


--
-- PostgreSQL database dump complete
--

