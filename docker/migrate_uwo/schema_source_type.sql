--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: source_type; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.source_type (
    source_type_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    manufacturer character varying
);


ALTER TABLE public.source_type OWNER TO datapool;

--
-- Name: source_type_source_type_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.source_type_source_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.source_type_source_type_id_seq OWNER TO datapool;

--
-- Name: source_type_source_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.source_type_source_type_id_seq OWNED BY public.source_type.source_type_id;


--
-- Name: source_type source_type_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source_type ALTER COLUMN source_type_id SET DEFAULT nextval('public.source_type_source_type_id_seq'::regclass);


--
-- Name: source_type source_type_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source_type
    ADD CONSTRAINT source_type_pkey PRIMARY KEY (source_type_id);


--
-- Name: ix_source_type_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_source_type_name ON public.source_type USING btree (name);


--
-- Name: ix_source_type_source_type_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_source_type_source_type_id ON public.source_type USING btree (source_type_id);


--
-- Name: TABLE source_type; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.source_type TO datapool_reader;


--
-- PostgreSQL database dump complete
--

