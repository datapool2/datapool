#!/usr/bin/env bash
N=$(basename $(dirname $(realpath $BASH_SOURCE)))

$psql -c "DELETE FROM site"
$psql -c "DELETE FROM site_field"
$psql -c "DELETE FROM site_field_values"

cat ./$N.csv | \
while IFS=$'\t' read -r site_id name description street postcode city country coord_x coord_y coord_z
do
 	echo $site_id,$name,descriptiositen
done | $psql -c "COPY site FROM STDIN WITH (FORMAT CSV, HEADER)"

ID=1
for P in street postcode city country coord_x coord_y coord_z; do
     echo $ID,$P
     ID=$(( $ID + 1 ))
done | $psql -c "COPY site_field FROM STDIN WITH (FORMAT CSV)"


SID=1
cat ./$N.csv | tail -n +2 | tee 2 | \
while IFS=$'\t' read -r site_id name description street postcode city country coord_x coord_y coord_z
do
 	echo $SID,$site_id,1,$street
     	SID=$(( $SID + 1 ))
 	echo $SID,$site_id,2,$postcode
     	SID=$(( $SID + 1 ))
 	echo $SID,$site_id,3,$city
     	SID=$(( $SID + 1 ))
 	echo $SID,$site_id,4,$country
     	SID=$(( $SID + 1 ))
 	echo $SID,$site_id,5,$coord_x
     	SID=$(( $SID + 1 ))
 	echo $SID,$site_id,6,$coord_y
     	SID=$(( $SID + 1 ))
	echo $(echo $SID,$site_id,7,$coord_z)
     	SID=$(( $SID + 1 ))
done | sed -e "s/\r$n//g" | tee out | $psql -c "COPY site_field_values FROM STDIN WITH (FORMAT CSV)"

