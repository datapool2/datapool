--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: meta_data_history; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_data_history (
    meta_data_history_id integer NOT NULL,
    meta_data_id integer NOT NULL,
    meta_log_type_id integer NOT NULL,
    meta_action_type_id integer,
    meta_flag_id integer,
    person_id integer NOT NULL,
    timestamp_start timestamp without time zone NOT NULL,
    timestamp_end timestamp without time zone NOT NULL,
    comment character varying,
    additional_meta_info json
);


ALTER TABLE public.meta_data_history OWNER TO datapool;

--
-- Name: meta_data_history_meta_data_history_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_data_history_meta_data_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_data_history_meta_data_history_id_seq OWNER TO datapool;

--
-- Name: meta_data_history_meta_data_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_data_history_meta_data_history_id_seq OWNED BY public.meta_data_history.meta_data_history_id;


--
-- Name: meta_data_history meta_data_history_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history ALTER COLUMN meta_data_history_id SET DEFAULT nextval('public.meta_data_history_meta_data_history_id_seq'::regclass);


--
-- Name: meta_data_history meta_data_history_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_pkey PRIMARY KEY (meta_data_history_id);


--
-- Name: ix_meta_data_history_meta_data_history_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_meta_data_history_id ON public.meta_data_history USING btree (meta_data_history_id);


--
-- Name: ix_meta_data_history_timestamp_end; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_timestamp_end ON public.meta_data_history USING btree (timestamp_end);


--
-- Name: ix_meta_data_history_timestamp_start; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_history_timestamp_start ON public.meta_data_history USING btree (timestamp_start);


--
-- Name: meta_data_history meta_data_history_meta_action_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_action_type_id_fkey FOREIGN KEY (meta_action_type_id) REFERENCES public.meta_action_type(meta_action_type_id);


--
-- Name: meta_data_history meta_data_history_meta_data_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_data_id_fkey FOREIGN KEY (meta_data_id) REFERENCES public.meta_data(meta_data_id);


--
-- Name: meta_data_history meta_data_history_meta_flag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_flag_id_fkey FOREIGN KEY (meta_flag_id) REFERENCES public.meta_flag(meta_flag_id);


--
-- Name: meta_data_history meta_data_history_meta_log_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_meta_log_type_id_fkey FOREIGN KEY (meta_log_type_id) REFERENCES public.meta_log_type(meta_log_type_id);


--
-- Name: meta_data_history meta_data_history_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data_history
    ADD CONSTRAINT meta_data_history_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(person_id);


--
-- Name: TABLE meta_data_history; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_data_history TO datapool_reader;


--
-- PostgreSQL database dump complete
--

