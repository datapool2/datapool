#!/usr/bin/env bash
N=$(basename $(dirname $(realpath $BASH_SOURCE)))

$psql  -c "DELETE FROM $N"
cat ./$N.csv | \
while IFS=$'\t' read -r project_id title description; do
 	echo $project_id,$title,,$description
done | $psql -c "COPY $N FROM STDIN WITH (FORMAT CSV, HEADER)"
