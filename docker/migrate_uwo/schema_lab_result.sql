--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: lab_result; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.lab_result (
    lab_result_id integer NOT NULL,
    lab_identifier character varying NOT NULL,
    variable_id integer NOT NULL,
    sample_identifier character varying NOT NULL,
    filter_lab character varying NOT NULL,
    dilution_lab double precision,
    method_lab character varying NOT NULL,
    value_lab double precision NOT NULL,
    description_lab character varying,
    timestamp_start_lab timestamp without time zone,
    timestamp_end_lab timestamp without time zone NOT NULL,
    site_id integer NOT NULL,
    filter_sample character varying NOT NULL,
    dilution_sample double precision,
    timestamp_sample timestamp without time zone,
    method_sample character varying,
    description_sample character varying
);


ALTER TABLE public.lab_result OWNER TO datapool;

--
-- Name: lab_result_lab_result_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.lab_result_lab_result_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lab_result_lab_result_id_seq OWNER TO datapool;

--
-- Name: lab_result_lab_result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.lab_result_lab_result_id_seq OWNED BY public.lab_result.lab_result_id;


--
-- Name: lab_result lab_result_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result ALTER COLUMN lab_result_id SET DEFAULT nextval('public.lab_result_lab_result_id_seq'::regclass);


--
-- Name: lab_result lab_result_lab_identifier_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_lab_identifier_key UNIQUE (lab_identifier);


--
-- Name: lab_result lab_result_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_pkey PRIMARY KEY (lab_result_id);


--
-- Name: lab_result lab_result_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: lab_result lab_result_variable_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result
    ADD CONSTRAINT lab_result_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES public.variable(variable_id);


--
-- Name: TABLE lab_result; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.lab_result TO datapool_reader;


--
-- PostgreSQL database dump complete
--

