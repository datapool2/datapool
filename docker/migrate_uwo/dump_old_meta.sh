#!/usr/bin/env bash
for N in source_type \
 special_value_definition \
 quality \
 lab_result_person_sample_association \
 person \
 lab_result_person_lab_association \
 meta_data_history \
 meta_flag \
 project \
 lab_result \
 meta_log_type \
 meta_action_type \
 source \
 site \
 parameter \
 meta_data;
do
	mkdir -p $N
	docker exec -it productive-postgis-1 psql -U postgres -d datapool -c "COPY $N TO STDOUT WITH (FORMAT CSV, HEADER, DELIMITER '	')" > $N/$N.csv
done
