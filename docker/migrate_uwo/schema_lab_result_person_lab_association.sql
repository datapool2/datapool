--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: lab_result_person_lab_association; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.lab_result_person_lab_association (
    lab_result_id integer NOT NULL,
    person_id integer NOT NULL
);


ALTER TABLE public.lab_result_person_lab_association OWNER TO datapool;

--
-- Name: lab_result_person_lab_association lab_result_person_lab_association_lab_result_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_lab_association
    ADD CONSTRAINT lab_result_person_lab_association_lab_result_id_fkey FOREIGN KEY (lab_result_id) REFERENCES public.lab_result(lab_result_id);


--
-- Name: lab_result_person_lab_association lab_result_person_lab_association_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.lab_result_person_lab_association
    ADD CONSTRAINT lab_result_person_lab_association_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.person(person_id);


--
-- Name: TABLE lab_result_person_lab_association; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.lab_result_person_lab_association TO datapool_reader;


--
-- PostgreSQL database dump complete
--

