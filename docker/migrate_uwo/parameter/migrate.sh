#!/usr/bin/env bash
N=$(basename $(dirname $(realpath $BASH_SOURCE)))
$psql -c "DELETE FROM variable"

cat ./$N.csv | $psql -c "COPY variable FROM STDIN WITH (FORMAT CSV, HEADER, DELIMITER '	')"
