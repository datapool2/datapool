export psql="docker exec -i productive-postgis-1 psql -U postgres -d datapool"


for N in \
 project \
 source_type \
 special_value_definition \
 source \
 person \
 quality \
 variable \
 meta_flag \
 meta_log_type \
 meta_action_type \
 site \
 site_field \
 site_field_value \
 lab_result \
 meta_data \
 ;
do
	echo reset id $N
	$psql -c "SELECT pg_catalog.setval(pg_get_serial_sequence('$N', '${N}_id'), (SELECT MAX(${N}_id) FROM ${N})+1);"
done

$psql -c "SELECT pg_catalog.setval(pg_get_serial_sequence('site_field_value', 'site_field_value_id'), (SELECT MAX(site_field_value_id) FROM site_field_value)+1);"
