--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: project; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.project (
    project_id integer NOT NULL,
    title character varying NOT NULL,
    abbrev character varying,
    description character varying
);


ALTER TABLE public.project OWNER TO datapool;

--
-- Name: project_project_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.project_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_project_id_seq OWNER TO datapool;

--
-- Name: project_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.project_project_id_seq OWNED BY public.project.project_id;


--
-- Name: project project_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project ALTER COLUMN project_id SET DEFAULT nextval('public.project_project_id_seq'::regclass);


--
-- Name: project project_abbrev_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_abbrev_key UNIQUE (abbrev);


--
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (project_id);


--
-- Name: ix_project_project_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_project_project_id ON public.project USING btree (project_id);


--
-- Name: TABLE project; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.project TO datapool_reader;


--
-- PostgreSQL database dump complete
--

