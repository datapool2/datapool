#!/usr/bin/env bash
set -eu

PORT_PROD=5434
PORT_MIGRATION=5434

export psql="docker exec -i productive-postgis-1 psql -U postgres -d datapool"


if false; then
	echo backup tables
	echo
	$psql -c "CREATE SCHEMA backup IF NOT EXISTS;" || true

	for N in \
	 meta_data_history \
	 meta_data \
	 lab_result_person_lab_association \
	 lab_result_person_sample_association \
	 lab_result \
	 site \
	 meta_action_type \
	 meta_log_type \
	 meta_flag \
	 parameter \
	 quality \
	 person \
	 source \
	 special_value_definition \
	 source_type \
	 project \
	 ;
	do
		echo move $N
		$psql -c "ALTER TABLE $N SET SCHEMA backup;" || true
	done
fi

if true; then
	echo 
	echo create tables
	echo

	for N in \
	 project \
	 source_type \
	 special_value_definition \
	 source \
	 person \
	 quality \
	 variable \
	 meta_flag \
	 meta_log_type \
	 meta_action_type \
	 site \
	 site_field \
	 site_field_values \
	 lab_result \
	 lab_result_person_sample_association \
	 lab_result_person_lab_association \
	 meta_data \
	 meta_data_history \
	 ;
	do
	      echo $N
	      $psql -c "DROP TABLE IF EXISTS $N CASCADE;"
	      $psql < schema_$N.sql
	done
fi

echo 
echo load data
echo

for N in \
 project \
 source_type \
 special_value_definition \
 source \
 person \
 quality \
 parameter \
 meta_flag \
 meta_log_type \
 meta_action_type \
 site \
 lab_result \
 lab_result_person_sample_association \
 lab_result_person_lab_association \
 meta_data \
 ;
do
	echo $N
	( cd $N; ./migrate.sh ) > /dev/null
done

$psql <<EOF
alter table binary_data drop constraint  binary_data_parameter_id_fkey; -- FOREIGN KEY (parameter_id) REFERENCES backup.parameter(parameter_id)
alter table binary_data drop constraint  binary_data_site_id_fkey;  --  FOREIGN KEY (site_id) REFERENCES backup.site(site_id)
alter table binary_data drop constraint  binary_data_source_id_fkey;  -- FOREIGN KEY (source_id) REFERENCES backup.source(source_id)
alter table meta_picture drop constraint meta_picture_meta_data_history_id_fkey; -- FOREIGN KEY (meta_data_history_id) REFERENCES backup.meta_data_history(meta_data_history_id)
alter table meta_picture drop constraint meta_picture_meta_data_id_fkey;  -- FOREIGN KEY (meta_data_id) REFERENCES backup.meta_data(meta_data_id)
alter table picture drop constraint picture_site_id_fkey; -- FOREIGN KEY (site_id) REFERENCES backup.site(site_id)
alter table signal_quality drop constraint signal_quality_quality_id_fkey; -- FOREIGN KEY (quality_id) REFERENCES backup.quality(quality_id)

alter table binary_data add constraint  binary_data_parameter_id_fkey FOREIGN KEY (parameter_id) REFERENCES parameter(parameter_id);
alter table binary_data add constraint  binary_data_site_id_fkey FOREIGN KEY (site_id) REFERENCES site(site_id);
alter table binary_data add constraint  binary_data_source_id_fkey FOREIGN KEY (source_id) REFERENCES source(source_id);
alter table meta_picture add constraint meta_picture_meta_data_history_id_fkey FOREIGN KEY (meta_data_history_id) REFERENCES meta_data_history(meta_data_history_id);
alter table meta_picture add constraint meta_picture_meta_data_id_fkey FOREIGN KEY (meta_data_id) REFERENCES meta_data(meta_data_id);
alter table picture add constraint picture_site_id_fkey FOREIGN KEY (site_id) REFERENCES site(site_id);
alter table signal_quality add constraint signal_quality_quality_id_fkey FOREIGN KEY (quality_id) REFERENCES quality(quality_id);

alter table signal add constraint signal_source_id_fkey FOREIGN KEY (source_id) REFERENCES source(source_id) NOT VALID;
alter table signal add constraint signal_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES variable(variable_id) NOT VALID;
alter table signal add constraint signal_site_id_fkey FOREIGN KEY (site_id) REFERENCES site(site_id) NOT VALID;
EOF
