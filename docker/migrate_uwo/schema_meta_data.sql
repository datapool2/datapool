--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: meta_data; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.meta_data (
    meta_data_id integer NOT NULL,
    source_id integer NOT NULL,
    site_id integer NOT NULL,
    description character varying,
    additional_meta_info json
);


ALTER TABLE public.meta_data OWNER TO datapool;

--
-- Name: meta_data_meta_data_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

CREATE SEQUENCE public.meta_data_meta_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_data_meta_data_id_seq OWNER TO datapool;

--
-- Name: meta_data_meta_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: datapool
--

ALTER SEQUENCE public.meta_data_meta_data_id_seq OWNED BY public.meta_data.meta_data_id;


--
-- Name: meta_data meta_data_id; Type: DEFAULT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data ALTER COLUMN meta_data_id SET DEFAULT nextval('public.meta_data_meta_data_id_seq'::regclass);


--
-- Name: meta_data meta_data_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_pkey PRIMARY KEY (meta_data_id);


--
-- Name: meta_data meta_data_source_id_site_id_key; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_source_id_site_id_key UNIQUE (source_id, site_id);


--
-- Name: ix_meta_data_meta_data_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_meta_data_meta_data_id ON public.meta_data USING btree (meta_data_id);


--
-- Name: meta_data meta_data_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(site_id);


--
-- Name: meta_data meta_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.meta_data
    ADD CONSTRAINT meta_data_source_id_fkey FOREIGN KEY (source_id) REFERENCES public.source(source_id);


--
-- Name: TABLE meta_data; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.meta_data TO datapool_reader;


--
-- PostgreSQL database dump complete
--

