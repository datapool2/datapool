--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: source; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.source (
    source_id integer NOT NULL,
    source_type_id integer NOT NULL,
    project_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    serial character varying
);


ALTER TABLE public.source OWNER TO datapool;

--
-- Name: source_source_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.source ALTER COLUMN source_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.source_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: source source_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_pkey PRIMARY KEY (source_id);


--
-- Name: ix_source_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_source_name ON public.source USING btree (name);


--
-- Name: source source_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(project_id);


--
-- Name: source source_source_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_source_type_id_fkey FOREIGN KEY (source_type_id) REFERENCES public.source_type(source_type_id);


--
-- Name: TABLE source; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.source TO datapool_reader;


--
-- PostgreSQL database dump complete
--

