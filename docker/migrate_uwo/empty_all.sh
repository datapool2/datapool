#!/usr/bin/env bash
set -eu

for N in \
 meta_data_history \
 meta_data \
 source \
 special_value_definition \
 source_type \
 project \
 person \
 quality \
 lab_result_person_sample_association \
 person \
 lab_result_person_lab_association \
 meta_data_history \
 meta_flag \
 lab_result \
 meta_action_type \
 meta_log_type \
 source \
 site \
 variable \
 ;
do
	echo DELETE $N
	psql -h 127.0.0.1 --port 5433 -U postgres -d datapool -c "DELETE FROM $N"
done
