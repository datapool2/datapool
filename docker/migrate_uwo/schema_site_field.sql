--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: site_field; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.site_field (
    site_field_id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.site_field OWNER TO datapool;

--
-- Name: site_field_site_field_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.site_field ALTER COLUMN site_field_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.site_field_site_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: site_field site_field_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.site_field
    ADD CONSTRAINT site_field_pkey PRIMARY KEY (site_field_id);


--
-- Name: ix_site_field_name; Type: INDEX; Schema: public; Owner: datapool
--

CREATE UNIQUE INDEX ix_site_field_name ON public.site_field USING btree (name);


--
-- Name: ix_site_field_site_field_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_site_field_site_field_id ON public.site_field USING btree (site_field_id);


--
-- Name: TABLE site_field; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.site_field TO datapool_reader;


--
-- PostgreSQL database dump complete
--

