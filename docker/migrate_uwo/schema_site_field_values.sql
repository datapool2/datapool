--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: site_field_values; Type: TABLE; Schema: public; Owner: datapool
--

CREATE TABLE public.site_field_values (
    site_field_value_id integer NOT NULL,
    site_id integer NOT NULL,
    site_field_id integer NOT NULL,
    value character varying
);


ALTER TABLE public.site_field_values OWNER TO datapool;

--
-- Name: site_field_values_site_field_value_id_seq; Type: SEQUENCE; Schema: public; Owner: datapool
--

ALTER TABLE public.site_field_values ALTER COLUMN site_field_value_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.site_field_values_site_field_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: site_field_values site_field_values_pkey; Type: CONSTRAINT; Schema: public; Owner: datapool
--

ALTER TABLE ONLY public.site_field_values
    ADD CONSTRAINT site_field_values_pkey PRIMARY KEY (site_field_value_id);


--
-- Name: ix_site_field_values_site_field_value_id; Type: INDEX; Schema: public; Owner: datapool
--

CREATE INDEX ix_site_field_values_site_field_value_id ON public.site_field_values USING btree (site_field_value_id);


--
-- Name: TABLE site_field_values; Type: ACL; Schema: public; Owner: datapool
--

GRANT SELECT ON TABLE public.site_field_values TO datapool_reader;


--
-- PostgreSQL database dump complete
--

