#!/usr/bin/env python

import pytest

from observe_logs import main


@pytest.fixture
def lines():
    return [
        "datapool | timestamp_00 before error line 0",
        "datapool | timestamp_01 before error line 1",
        "datapool | timestamp_02 before error line 2",
        "datapool | timestamp_03 before error line 3",
        "datapool | timestamp_04 before error line 4",
        "datapool | timestamp_05 error: error line 1",
        "datapool | timestamp_06 error: error line 2",
        "datapool | timestamp_07 error: error line 3",
        "datapool | timestamp_08 after error line 1",
        "datapool | timestamp_09 after error line 2",
        "datapool | timestamp_10 after error line 3",
        "datapool | timestamp_11 after error line 4",
        "datapool | timestamp_12 after error line 5",
        "datapool | timestamp_13 error: error2 line 1",
        "datapool | timestamp_14 error: error2 line 2",
        "datapool | timestamp_15 after error2 line 1",
        "datapool | timestamp_16 after error2 line 2",
        "datapool | timestamp_17 after error2 line 3",
    ]


def test_error(lines):
    def get_docker_compose_logs(since):
        return lines

    notifications = []

    def send_error_notification(txt):
        nonlocal notifications
        notifications.extend(l for l in txt.split("\n"))

    continue_looping = True

    def continue_():
        nonlocal continue_looping
        result = continue_looping
        continue_looping = False
        return result

    main(
        get_docker_compose_logs,
        send_error_notification,
        wait_time=1,
        continue_=continue_,
    )

    tobe = (
        [lines[i].split("|")[1].strip() for i in (1, 2, 3, 4, 5, 6, 7)]
        + ["", ""]
        + [lines[i].split("|")[1].strip() for i in (9, 10, 11, 12, 13, 14)]
        + ["", ""]
    )

    assert notifications == tobe


def test_error_grouping(lines):
    def get_docker_compose_logs(since):
        nonlocal iterations
        iterations += 1
        if iterations == 1:
            return lines[:8]
        elif iterations == 2:
            return lines[8:]

    def send_error_notification(txt):
        nonlocal notifications
        nonlocal batches
        batches += 1
        notifications.extend(l for l in txt.split("\n"))

    def continue_():
        nonlocal iterations
        return iterations < 2

    iterations = 0
    notifications = []
    batches = 0

    main(
        get_docker_compose_logs,
        send_error_notification,
        wait_time=3,
        continue_=continue_,
    )

    tobe = (
        [lines[i].split("|")[1].strip() for i in (1, 2, 3, 4, 5, 6, 7)]
        + ["", ""]
        + [lines[i].split("|")[1].strip() for i in (9, 10, 11, 12, 13, 14)]
        + ["", ""]
    )
    assert notifications == tobe

    assert batches == 1

    batches = 0
    iterations = 0
    notifications = []
    main(
        get_docker_compose_logs,
        send_error_notification,
        wait_time=0.5,
        continue_=continue_,
    )

    tobe = (
        [lines[i].split("|")[1].strip() for i in (1, 2, 3, 4, 5, 6, 7)]
        + ["", ""]
        + [lines[i].split("|")[1].strip() for i in (9, 10, 11, 12, 13, 14)]
        + ["", ""]
    )
    assert notifications == tobe

    assert batches == 2


def test_restart():
    lines = [
        "datapool | timestamp_00 before restart line 1",
        "datapool | timestamp_01 before restart line 2",
        "datapool | timestamp_02 before restart line 3",
        "datapool | timestamp_03 before restart line 4",
        "datapool | timestamp_04 before restart line 5",
        "datapool | timestamp_05 before restart line 6",
        "datapool | timestamp_06 > this is datapool version",
        "datapool | timestamp_07 after restart line 1",
        "datapool | timestamp_08 after restart line 2",
    ]

    def get_docker_compose_logs(since):
        return lines

    error_notifications = []

    def send_error_notification(txt):
        nonlocal error_notifications
        error_notifications.extend(l for l in txt.split("\n"))

    restart_notifications = []

    def send_restart_notification(txt):
        nonlocal restart_notifications
        restart_notifications.extend(l for l in txt.split("\n"))

    continue_looping = True

    def continue_():
        nonlocal continue_looping
        result = continue_looping
        continue_looping = False
        return result

    main(
        get_docker_compose_logs,
        send_error_notification,
        send_restart_notification,
        1,
        continue_,
    )

    assert error_notifications == []

    assert restart_notifications == [
        lines[i].split("|")[1].strip() for i in (0, 1, 2, 3, 4, 5, 6)
    ]
