import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send(smtp_server, kind, body, from_, to):
    if not smtp_server:
        print("SKIP SENDING EMAIL, SMTP_SERVER NOT SET")
        return
    if not from_:
        print("SKIP SENDING EMAIL, FROM ADDRESS NOT SET")
        return
    if not to:
        print("SKIP SENDING EMAIL, TO ADDRESS NOT SET")
        return

    message = MIMEMultipart("alternative")
    message["Subject"] = f"datapool alert: {kind}"
    message["From"] = from_
    message["To"] = to

    # Create the plain-text and HTML version of your message
    html = f"""
    <html>
      <body>
        <pre>
{body}
        </pre>
      </body>
    </html>
    """

    # Turn these into plain/html MIMEText objects
    part = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part)

    # Create secure connection with server and send email
    with smtplib.SMTP(smtp_server) as server:
        # server.set_debuglevel(1)
        server.sendmail(
            from_, [ti.strip() for ti in to.split(",")], message.as_string()
        )


if __name__ == "__main__":
    send(
        "smtp0.ethz.ch",
        "test\ntest",
        "schmitt@ethz.ch",
        "schmittu@ethz.ch, uwe.schmitt@id.ethz.ch",
    )
