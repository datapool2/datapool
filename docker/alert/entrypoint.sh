#!/bin/bash
#
# entrypoint.sh
# Copyright (C) 2022 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.

exec python3 -u /observe_logs.py
