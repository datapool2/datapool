#!/usr/bin/env python

import os
import re
import subprocess
import time
from threading import Thread

from sendmail import send

smtp_server = os.environ.get("SMTP_SERVER", "")
from_ = os.environ.get("EMAIL_FROM", "")
to = os.environ.get("EMAIL_TO", "")


def run(cmd):
    output = subprocess.check_output(
        cmd, shell=True, stderr=subprocess.STDOUT, text=True
    )
    return output.split("\n")


def send_restart_notification(output):
    print("SEND RESTART NOTIFICATION")
    print(output)
    send(smtp_server, "restart detected", output, from_, to)


def send_error_notification(output):
    print("SEND ERROR NOTIFICATION")
    print(output)
    send(smtp_server, "error detected", output, from_, to)


ansi_escape = re.compile(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])")


def remove_ansi_colors(line):
    return ansi_escape.sub("", line)


class DelayedErrorSender(Thread):
    def __init__(self, send_error_notification, wait_time=240):
        self.send_error_notification = send_error_notification
        self.wait_time = wait_time
        self.lines = []
        self.running = True
        self.timestamp = time.time()
        super().__init__()

    def update(self, lines):
        self.lines.extend(lines)
        self.timestamp = time.time()
        print(self.timestamp, "got", len(self.lines), "error lines")

    def run(self):
        try:
            self._run()
        except:
            import traceback

            traceback.print_exc()

    def _run(self):
        self.timestamp = time.time()
        while self.running:
            if self.lines and time.time() >= self.timestamp + self.wait_time:
                print(
                    "collected",
                    len(self.lines),
                    "error lines and will send message now.",
                )
                self.send_error_notification("\n".join(self.lines))
                self.lines = []
                self.timestamp = time.time()
            started = time.time()
            while self.running and time.time() < started + self.wait_time:
                time.sleep(0.2)

        if self.lines:
            print(
                "collected",
                len(self.lines),
                "error lines and will send message now.",
            )
            self.send_error_notification("\n".join(self.lines))


def get_docker_compose_logs(since):
    os.chdir("/docker")
    return run(f"docker-compose logs datapool --timestamps {since}")


def main(
    get_docker_compose_logs=get_docker_compose_logs,
    send_error_notification=send_error_notification,
    send_restart_notification=send_restart_notification,
    wait_time=240,
    continue_=lambda: True,
):

    error_sender = DelayedErrorSender(send_error_notification, wait_time)
    error_sender.start()

    since = ""
    seen = set()

    timestamps = []
    errors = []

    previous_lines = []
    error_tracking_on = False

    while True:

        lines = get_docker_compose_logs(since)
        for line in lines:

            id_, _, output = line.partition("|")
            output = remove_ansi_colors(output.strip())
            timestamp, _, rest = output.partition(" ")

            # avoid duplicates
            if timestamp in seen:
                continue
            seen.add(timestamp)

            rest = rest.lstrip()

            if rest.startswith("> this is datapool version"):
                errors = []
                error_tracking_on = False
                send_restart_notification("\n".join(previous_lines + [output]))

            if rest.startswith("error:"):
                if not error_tracking_on:
                    # file location information and other context:
                    errors.extend(previous_lines[-4:])
                error_tracking_on = True
                errors.append(output)
            else:
                error_tracking_on = False

            previous_lines.append(output)
            while len(previous_lines) > 20:
                previous_lines = previous_lines[1:]

            if timestamp.strip():
                timestamps.append(timestamp)

        if errors:
            if errors[-1] != "":
                errors.append("")
                errors.append("")
            error_sender.update(errors)
            errors = []

        if timestamps:
            since = f"--since {max(timestamps)}"

        # limit size of seen:
        seen = set(sorted(seen)[-10000:])

        # checking here instead in "while" above speeds up tests because
        # we avoid waiting after the last iteration:
        if not continue_():
            break

        time.sleep(2.0)

    print("shutdown")
    error_sender.running = False
    error_sender.join()


if __name__ == "__main__":
    main()
