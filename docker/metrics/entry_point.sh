#!/usr/bin/env bash
set -e

export PGPASSWORD=${DATAPOOL_PASSWORD}


function folder_usage {
    metrics=$1 folder=$2
    value=$(du -sb $folder | cut -f 1)
    echo "${metrics} ${value}"
}

pg='psql -h postgis -U datapool -d datapool'

function run_sql_query {
    QUERY=$1
    OUTPUT=$($pg -f ${QUERY})
    REGEXP="[eE]xecution [tT]ime: ([0-9.]+) "

    [[ ${OUTPUT} =~ ${REGEXP} ]];

    FILE_NAME=$(basename ${QUERY})
    QUERY_NAME="${FILE_NAME%.*}"
    echo dp_performance_${QUERY_NAME} ${BASH_REMATCH[1]}
}

function run_sql_queries {
    for Q in /queries/*.sql; do
        run_sql_query $Q
    done
}

while true; do
    echo -n "check if database is up.. "
    $pg -c 'select 1' && break
    echo "data base not up yet..."
    sleep 5
done
echo "data base is up!"

sleep 120

while true;
do
    folder_usage dp_postres_db_size /postgres
    folder_usage dp_backup_lz_size /backup_lz
    folder_usage dp_lz_size /lz
    folder_usage dp_development_lz_size /development
    folder_usage dp_log_file_size /logs
    run_sql_queries
    echo >&2
    date >&2
    echo ran metrics scripts successfully >&2
    echo  >&2
    sleep 60m

    # wait until after midnight
    while true; do
        HOUR=$(date "+%H")
        if [[ 10#$HOUR -eq 00 ]]; then
            break
        fi
        sleep 60m
    done

done > /text_file_collector/dp_metrics.prom
