\timing
EXPLAIN ANALYZE WITH signals AS
(
        SELECT     signal_id::INTEGER
        FROM       signal
        inner join site
        ON         signal.site_id = site.site_id
        WHERE      site.name = '166_luppmenweg'
        AND        '2019-09-01 00:00:00' :: timestamp <= signal.timestamp
        AND        signal.timestamp <= '2019-10-01 00:00:00' :: timestamp
),
signals_with_quality_1 AS
(
    SELECT signal_id
    FROM   signals_signal_quality_association
    join   signal_quality
    ON     signals_signal_quality_association.signal_quality_id = signal_quality.signal_quality_id
    WHERE  quality_id = 1
    AND    signals_signal_quality_association.signal_id = ANY( array(select signal_id::INTEGER FROM signals))
)
SELECT   timestamp,
         value,
         unit,
         variable.name,
         source_type.name,
         source.name
FROM     signal
join     variable
ON       signal.variable_id = variable.variable_id
join     source
ON       signal.source_id = source.source_id
join     source_type
ON       source.source_type_id = source_type.source_type_id
WHERE    signal_id = ANY(ARRAY(SELECT signal_id::INTEGER FROM signals_with_quality_1))
ORDER BY timestamp ASC;
