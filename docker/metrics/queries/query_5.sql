\timing
EXPLAIN ANALYZE
SELECT   timestamp, value, unit, variable_name, source_type_name, source_name 
FROM     for_shiny
WHERE      '2020-01-01' :: timestamp <= for_shiny.timestamp
AND      for_shiny.timestamp <= '2020-07-01' :: timestamp
AND     for_shiny.site_name = '164_luppmenweg'
AND      for_shiny.variable_name = ANY( array[ 'rainfall intensity',
'flow rate',
'water level',
'average flow velocity',
'dielectric permittivity',
'water temperature',
'air temperature-sewer',
 'water temperature']
)
ORDER BY timestamp ASC;

