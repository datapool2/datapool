docker setup for datapool
=========================

Requirements on server:

- git
- docker
- docker-compose

Preparation ubuntu
------------------

```shell
$ sudo apt-update
$ sudo apt install -y git docker.io docker-compose
```


Installation
============

1. Clone this repository and enter the docker folder.

   ```shell
   $ git clone https://gitlab.com/datapool2/datapool.git
   $ cd datapool/docker
   $ cp .env.example .env
   ```
2. Setup folders for the home of **development landing zones**, the **operational landing zone**, the **backup
   landing zone**, **extra scripts** you might want to have accessible for the datapool. E.g.

   ```shell
   $ sudo mkdir -p /data/lz
   $ sudo mkdir -p /data/backup_lz
   $ sudo mkdir -p /data/development
   $ sudo mkdir -p /data/extra
   ```

   Beyond that also create folders for the data base files and datapool log files:
   ```shell
   $ sudo mkdir -p /data/postgres
   $ sudo mkdir -p /data/logs
   ```
   This can be adapted as you wish.

3. Adjust the settings for this installation in the`.env` file.

   - `POSTGRES_PASSWORD` is the password for the postgres role in the dockerized database.
   - `DATAPOOL_PASSWORD` is the password for the datapool role in the dockerized database.
   - `DATAPOOL_READER_PASSWORD` is the password for the datapool-reader role in the dockerized database.
   - `POSTGRES_PORT` is port for the postgres db on the host (default is 5432).
   - `EXTRA_DATA` is designed to store additional scripts that are supposed to be accessible for conversion scripts.
   - `LANDING_ZONE` is the root folder of the landing zone on the host server.
   - `BACKUP_LANDING_ZONE` is the root folder of the backup landing zone on the host server.
   - `DEVELOPMENT_LANDING_ZONES_ROOT` is the folder host server which will be used for development
      landing zones. Contrary to a manual setup development landing zones can not be created at arbitrary
      locations on the host machine, but must be located within `DEVELOPMENT_LANDING_ZONES_ROOT`.
   - `HTTP_PORT` is the port to query status, logs and metrics of the datapool server.
   - `CPU_ARCHITECTURE` specifies the architecture of the docker containers to use (**beta**)

   > Example:
   > ```
   > POSTGRES_DB_HOST=/data/postgresql/data
   > POSTGRES_PASSWORD=postgres_pw
   > POSTGRES_PORT=5432
   >
   > DATAPOOL_PASSWORD=datapool_pw
   > DATAPOOL_READER_PASSWORD=datapool_reader_pw
   >
   > EXTRA_DATA=/data/extra
   > LANDING_ZONE=/data/landing_zone
   > BACKUP_LANDING_ZONE=/data/backup_landing_zone
   > DEVELOPMENT_LANDING_ZONES_ROOT=/data/development
   >
   > HTTP_PORT=8000
   >
   > CPU_ARCHITECTURE=amd64
   > ```


4. Change settings for `EXTRA_DATA`, `LANDING_ZONE`, `BACKUP_LANDING_ZONE` and `DEVELOPMENT_LANDING_ZONES_ROOT` in
   `.env` accordingly.

   Then (up to personal modifications):

   ```shell
   $ sudo chgrp docker /data/development
   $ sudo chmod g+ws /data/development
   $ sudo chgrp docker /data/lz
   $ sudo chmod g+ws /data/lz
   $ sudo chgrp docker /data/extra
   $ sudo chmod g+ws /data/extra
   ```

5. Build containers

   ```shell
   $ sudo docker-compose build
   ```

6. To setup service which also restarts after reboot:

   ```shell
   $ sudo cp datapool.service /etc/systemd/system/
   $ sudo systemctl enable datapool
   $ sudo systemctl start datapool
   $ sudo systemctl status datapool
   ```

7. To install `pool` command:

   ```shell
   $ sudo cp pool /usr/local/bin
   $ chgrp docker /usr/local/bin/pool
   $ chmod o-x /usr/local/bin/pool
   ```

8. To allow user `USER-ABC` to use `pool` command:

    ```shell
    $ sudo usermod -aG docker USER-ABC
    ```

    The user  `USER-ABC` should be able to run `pool` commands **after login** now.
