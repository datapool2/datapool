#!/usr/bin/env python

import csv
import os
import re
from pathlib import Path
from typing import List

from fastapi import FastAPI, Header, Response
from pydantic import BaseModel, constr

from model import Session, is_valid_token

app = FastAPI()


@app.get("/")
def index():
    return dict(message="this is the datapool direct data importer API.")


_regex_date_time = (
    r"((\d{4})-(\d{1,2})-(\d{1,2})[ :](\d{1,2}):(\d{2}):(\d{2}))"
    "|"
    r"((\d{4})/(\d{1,2})/(\d{1,2})[ :](\d{1,2}):(\d{2}):(\d{2}))"
)


class Signal(BaseModel):

    timestamp: constr(regex=_regex_date_time)
    variable: str
    site: str
    value: float


class Signals(BaseModel):

    path: Path
    data: List[Signal]


@app.post("/signals")
def _signals(
    response: Response,
    signals: Signals,
    token: str | None = Header(default=None),
):
    with Session() as db:
        if not is_valid_token(db, token):
            response.status_code = 400
            return dict(message="invalid token")

    os.seteuid(1111)

    header = Signal.__fields__
    rows = []
    for signal in signals.data:
        rows.append([getattr(signal, field) for field in Signal.__fields__])

    if signals.path.is_absolute():
        target = (Path("/lz") / signals.path.relative_to("/")).resolve()
        if target.parent.name == "direct_data" and not target.parent.exists():
            try:
                print("create", target.parent)
                target.parent.mkdir()
            except IOError as e:
                response.status_code = 400
                return dict(error=str(e))
        if target.is_relative_to("/lz") and target.parent.exists():
            try:
                with target.open("w") as fh:
                    writer = csv.writer(fh, delimiter=";")
                    writer.writerow(header)
                    writer.writerows(rows)
            except Exception as e:
                response.status_code = 400
                return dict(error=str(e))

            return dict(message="wrote data.")

    response.status_code = 400
    return dict(message="invalid path")
