#!/usr/bin/env bash
#
# test_api.sh
# Copyright (C) 2023 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

TOKEN=1213d8b218658ae75f1d
TOKEN=04f3ab16b1d83678117c
TOKEN=804ed755a4863b23f47e
TOKEN=92c14f181d29575acb3b

SERVER=127.0.0.1:9999
URL=http://${SERVER}/signals

DATA=$(python3 << EOM
import json
print(json.dumps({
   "path": "/data/sensor_from_xyz/sensor_abc_from_xyz/direct_data/data.csv",
   "data": [
          { "timestamp": "2008-09-15 15:53:00", "variable": "Temperature", "site": "test_site", "value": 1.111 },
   ]
}))
EOM
)

echo "$data"

curl -k -X POST -d "$DATA" -H "Content-Type: application/json" -H"token: $TOKEN" $URL
echo
