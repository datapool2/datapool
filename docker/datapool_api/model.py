#!/usr/bin/env python

import time
from secrets import token_hex

from sqlalchemy import create_engine, select
from sqlalchemy.exc import OperationalError, StatementError
from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    mapped_column,
    sessionmaker,
)
from sqlalchemy.orm.query import Query as _Query


class Base(DeclarativeBase):
    pass


def random_token():
    return token_hex(10)


class Token(Base):

    __tablename__ = "token"

    token_id: Mapped[int] = mapped_column(primary_key=True)
    token: Mapped[str] = mapped_column(default=random_token)


DB_URI = "sqlite:///database/tokens.db"

# https://stackoverflow.com/questions/55457069/
engine = create_engine(
    DB_URI,
    pool_size=10,
    max_overflow=2,
    pool_recycle=300,
    pool_pre_ping=True,
    pool_use_lifo=True,
)
Base.metadata.create_all(engine)


class RetryingQuery(_Query):
    """https://stackoverflow.com/questions/53287215/retry"""

    __max_retry_count__ = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __iter__(self):
        attempts = 0
        while True:
            attempts += 1
            try:
                return super().__iter__()
            except OperationalError as ex:
                if "server closed the connection unexpectedly" not in str(ex):
                    raise
                if attempts <= self.__max_retry_count__:
                    sleep_for = 2 ** (attempts - 1)
                    print(
                        "Database connection error: retrying Strategy => sleeping"
                        " for {}s and will retry (attempt #{} of {}).\n"
                        "Detailed query impacted: {}".format(
                            sleep_for, attempts, self.__max_retry_count__, ex
                        )
                    )
                    time.sleep(sleep_for)
                    continue
                else:
                    raise
            except StatementError as ex:
                if "reconnect until invalid transaction is rolled back" not in str(ex):
                    raise
                self.session.rollback()


Session = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, query_cls=RetryingQuery
)


def new_token(session):
    token = Token()
    session.add(token)
    session.commit()
    return token.token


def is_valid_token(session, token):
    matches = session.execute(select(Token).where(Token.token == token)).all()
    return bool(matches)


if __name__ == "__main__":
    with Session() as db:
        token = new_token(db)
        print("NEW TOKEN:", token)
