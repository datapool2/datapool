#!/bin/bash
#
# entrypoint.sh
# Copyright (C) 2022 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.


for P in /lz/data/*/*; do
    test -d "$P" && mkdir -p "$P/direct_data"
    test -d "$P" && chown -R 1111 "$P/direct_data"
done

exec uvicorn app:app --log-level debug --host 0.0.0.0 --port 80
