#!/usr/bin/env bash
#
# test_docker_api.sh
# Copyright (C) 2023 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#
#


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd ${SCRIPT_DIR}

date

./pool create-example elz

date
./pool update-operational --copy-raw-files elz

{ docker compose logs -f datapool & echo $! > pid; } | {
        while read line; do
                echo $line | grep -q "removed /lz/data/sensor_from_xyz/raw_data/data-001.raw" && break;
        done
        kill -9 $(cat pid)
}


TOKEN=$(docker compose exec datapool_api python /model.py  2>/dev/null| cut -d":" -f 2)
TOKEN=$(echo $TOKEN)   # trim

URL=http://docker:9999/signals

DATA=$(python3 << EOM
import json
print(json.dumps({
   "path": "/data/sensor_from_xyz/sensor_abc_from_xyz/direct_data/data.csv",
   "data": [
          { "timestamp": "2008-09-15 15:53:00", "variable": "Temperature", "site": "test_site", "value": 1.111 },
   ]
}))
EOM
)

curl --fail-with-body -o - -k -X POST -d "$DATA" -H "Content-Type: application/json" -H"token: $TOKEN" $URL
echo

sleep 3

docker compose logs datapool --tail 50 | grep "removed data/sensor_from_xyz/sensor_abc_from_xyz/direct_data/data.csv"
