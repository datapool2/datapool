#!/bin/bash
#
# init-user-db.sh
# Copyright (C) 2021 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

# template from https://github.com/docker-library/docs/blob/master/postgres/README.md

set -e

# postgres recognizes md5 encoded passwords when they start with "md5".
# we do this such that the exact passwords do not show up in the docker logs.
# from https://stackoverflow.com/questions/17429040
DATAPOOL_PASSWORD_MD5=$(echo -n ${DATAPOOL_PASSWORD}datapool | md5sum | tr -d " -")
DATAPOOL_READER_PASSWORD_MD5=$(echo -n ${DATAPOOL_READER_PASSWORD}datapool_reader | md5sum | tr -d " -")

psql --echo-all -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

    CREATE EXTENSION postgis VERSION '3.3.2';
    CREATE EXTENSION postgis_raster;

    CREATE USER datapool;
    ALTER USER datapool WITH PASSWORD 'md5$DATAPOOL_PASSWORD_MD5';

    CREATE USER datapool_reader;
    ALTER USER datapool_reader WITH PASSWORD 'md5$DATAPOOL_READER_PASSWORD_MD5';

    ALTER DATABASE $POSTGRES_DB OWNER TO datapool;
    SET ROLE datapool;

    -- we must create this table before datapool does, since the default
    -- type for data is BINARY:
    CREATE TABLE raster_data (
           raster_data_id SERIAL PRIMARY KEY,
           source_id INTEGER,
           variable_id INTEGER,
           timestamp  TIMESTAMP,
           data RASTER
    );

EOSQL
