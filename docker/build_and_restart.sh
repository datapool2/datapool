#!/usr/bin/env bash

set -e
set -x

docker-compose build

function observe {

    IFS=$'\n'
    journalctl -u datapool -f | while read -r -t7 line; do 
        printf "%b\n" $line;
    done
}

systemctl restart datapool
observe
