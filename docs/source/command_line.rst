Command references
===================

.. program-output:: pool --help

.. program-output:: pool admin init-config --help

.. program-output:: pool admin check-config --help

.. program-output:: pool admin init-db --help

.. program-output:: pool check --help

.. program-output:: pool start-develop --help

.. program-output:: pool update-operational --help

.. program-output:: pool add --help

.. program-output:: pool start-rename --help

.. program-output:: pool delete-entity --help

.. program-output:: pool delete-facts --help

.. program-output:: pool admin show --help
