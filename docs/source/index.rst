.. datapool documentation master file, created by
   sphinx-quickstart on Mon Aug 29 11:06:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to datapool's documentation!
====================================

The datapool is the EAWAG data warehouse package designed for storing sensor data.

If you want to work with data stored in the datapool, read the
data user instruction.

If you want to load measurements data in the data pool, read the data provider instructions.

Content:
--------
.. toctree::
   :maxdepth: 2


   instruction_data_user
   for_data_providers
   installation
   database_layout
   usage_examples
   command_line
   how_to_work_on_documentation

   obsolete




The datapool source code can be found `here <https://gitlab.com/datapool2/datapool>`_.
