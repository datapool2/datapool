Datapool on Raspbery Pi
=======================

Do you find the datapool software intriguing? The easiest way to try it out is on a Raspberry Pi.
You can download an Ubuntu image with the datapool preinstalled.

You'll need an ``Rasberry Pi 4`` with ideally ``4GB RAM`` and a ``16GB Micro SD Card``.

Ideally you use the linux ``dd`` command-line-tool to write the packed image to the Micro SD Card.

.. code-block:: bash

    gzip -dc /path/to/datapool_rpi4.img.gz | sudo dd of=/dev/X oflag=sync

The image comes with a basic tool to generate data, so that you can practice using the datapool easily.

.. code-block:: none

    $ datgen --help

    usage: datgen [-h] [--col COL] [--days DAYS] [--s-freq S_FREQ] [--offs OFFS]
                  [--daily-sd DAILY_SD] [--n-uni N_UNI] [--n-nor N_NOR]
                  [--f-coeff F_COEFF [F_COEFF ...]] [--s-date S_DATE]
                  fn

    Generate time series data, to be provided to the datapool.

    positional arguments:
      fn                    Filename of data file to be generated.

    optional arguments:
      -h, --help            show this help message and exit
      --col COL             Column name for time series.
      --days DAYS           Generated time series lenght in days.
      --s-freq S_FREQ       Sample frequency in minutes.
      --offs OFFS           Signal value for mean of data
      --daily-sd DAILY_SD   Randomly sampled normal distributed factor, which introduces
                            variance from one day to another.
      --n-uni N_UNI         Uniform noise applied over the entire sampling duration.
      --n-nor N_NOR         Normal noise applied over the entire sampling duration.
      --f-coeff F_COEFF [F_COEFF ...]
                            Fourier Series coefficients, that make up the signals base curve
                            (a0, a1, b1, a2, b2,...). List length must be an uneven number!
      --s-date S_DATE       Specifies the star$ pool add parameter example_parametert date of the time series format: Ymd -> 20201231




Example Set-Up
--------------

In order to add sensors to your datapool you need to create entities like sources, sites, parameters, usw.
To achieve this we'll use a development landing zone, which is a copy of the operational landing zone.
In this dlz we'll add all entities we desire and later check, if all added entries are valid, before updating
the operational landing zone.

    1. Create a ``development\_landing\_zone`` named **example\_setup**.

    .. code-block:: none

        $ pool start-develop example_setup

    2. Add entities

    We'll be adding entities to the datapool via the included command-line-tool.
    Two possible methods are available. You can run the pool add command without details on the entity you want to add
    and fill in the prompted questions or you can provide details in the first place.

    We'll demonstrate both options. The second method is a good one for an automated setup.

    .. NOTE::
        You can always adapt details in the created ``yaml`` files in the dlz after running the pool command.

    Please add
        - a ``project`` called **example\_project** (prompt option)
        - a ``parameter`` called **example\_parameter** (prompt option)
        - a ``site`` called **example\_site** (prompt option)
        - a ``source\_type`` called **example\_source\_type** (prompt option)
        - a ``source`` called **example\_source** (detail option)


    .. code-block:: none

        $ pool add project example_setup
        $ pool add parameter example_setup
        $ pool add site example_setup
        $ pool add source_type example_setup
        $ pool add source example_setup name="example_source" description="This source is just an example, but in theory you should write here everything there is to know about your sensor." serial="001" source_type="example_source_type" project="example_project"



