Usage Examples
======================

Renaming
--------

**Example 1: Rename parameter**

For this example we'll be using the command line tool **fd**. You might need to install it (`sudo apt install fd-find`).

1. Get a root shell:

    .. code-block:: none

        $ sudo bash


2. Create a renaming landing zone (rlz):

    .. code-block:: none

        $ pool start-rename --what parameter rlz 'parameter_old' 'parameter_new'


3. Replace parameter name in all conversion scripts:

    .. code-block:: none

        $ fd conversion rlz | xargs -n 1 sed -i"" -e "s/\bparameter_old\b/parameter_new/g"


4. Run pool check:

    .. code-block:: none

        $ pool check rlz


5. Update-operational:

    .. code-block:: none

        $ pool check rlz


.. NOTE::
    If pool check fails, you might want to check if you have empty files or broken raw files in your rlz.

    find empty files to later delete them

    .. code-block:: none

        fd -g 'data-*.raw' /data/development/rlz/data/ | xargs -n 1 wc -l | sort -n -r



Deletion
--------

**Example 1: Delete signals for a given source and time**

    1. Dry run for delete signals out of the database:

    .. code-block:: none

        $ pool delete-facts signals "source==fake_source_name" "timestamp>=2020-01-01 00:00:00"


    2. Execute the delete requires 2 **--force** flags and admin privileges **root**:

    .. code-block:: none

        $ sudo pool delete-facts signals "source==fake_source_name" "timestamp>=2020-01-01 00:00:00" --force --force


**Example 2: Delete many of signals for a given source and time**

    Delete large amounts of data. Due to the pre-collection of to delete signals
    the RAM can fill up for large quantities of data.
    One way to circumvent this is to loop over sections of the data.
    This example will delete all the data for the year 2020 for given **source** without any dry run!!.

    .. code-block:: shell

        for start in 2021-01-01 2020-10-01 2020-08-01 2020-06-01 2020-04-01 2020-02-01 2020-01-01 ; do
            echo "$start"
            sudo pool delete-facts signals "source==fake_source_name" "timestamp>$start"  --force --force
        done;


**Example 3: Delete Parameter for a given source and time**

    1. Dry run for deleting an entity out of the database:

    .. code-block:: none

        $ pool delete-entity --what parameter "parameter_name"


    2. Execute the delete requires 2 **--force** flags and admin privileges **root**:

    .. code-block:: none

        $ sudo pool delete-entity --what parameter "parameter_name" --force --force