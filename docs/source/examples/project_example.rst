.. code-block:: YAML

    -
        title: project_1
        description: What a great project!!
    -
        title: project_2
        description: And another great project!!
