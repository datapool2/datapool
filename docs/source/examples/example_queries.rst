List all the columns (*) from the source table:

.. code-block:: sql

        SELECT * FROM source;

List all sites with their descriptions and check how many images each site has.
A couple of remarks on this query.
  1. We select the name and the description from the site table.
  2. When we write ``site s`` as on the line ``FROM site s``, we are creating an alias for the table site.
  3. We left join to the site table the picture table, allowing for the possibility of having sites without any image.
  4. Count(p.picture_id) combined with GROUP BY s.site_id will count all pictures having the same s.site_id

.. code-block:: sql

        SELECT
              s."name",
              s.description,
              COUNT(p.picture_id)
        FROM site s
        LEFT JOIN picture p ON p.site_id = s.site_id
        GROUP BY s.site_id



Get all signals measured at site ``site_A`` within a given time interval ``t_start, t_end``.
The correct format for timestamp would be ``'2017-07-19 18:15:00.000'``.
On this case, we join the table signal with the tables site, variable, source and source_type and filter
the result using the clause ``WHERE``.

.. code-block:: sql

        SELECT
            s.timestamp,
            s.value,
            v.unit ,
            v."name" AS "Unit Name",
            s3."name" AS "Source Name",
            st."name" AS "Source Type Name"
        FROM signal s
            JOIN site s2 ON s2.site_id = s.site_id
            JOIN variable v ON v.variable_id = s.variable_id
            JOIN "source" s3 ON s3.source_id = s.source_id
            JOIN source_type st ON st.source_type_id = s3.source_type_id
        WHERE
            s.name = 'Site_A'
            AND s."timestamp" >= 't_start'
            AND s."timestamp" <= 't_end'


Get all signals of a certain source and parameter ``source_A``, ``parameter_A`` within a given time interval ``t_start, t_end``.
In this case we use the clause ORDER BY to retrieve the data ordered by timestamp ascending.

.. code-block:: sql
        SELECT
            s."timestamp",
            s.value,
            v.unit,
            v."name",
            s2.name as "Source Name",
            s2.serial,
            st."name" as "Source-type Name",
            s3."name" "Site Name",
            s2."name" "Source Name"
        FROM signal s
          JOIN variable v ON v.variable_id = s.variable_id
          JOIN "source" s2 ON s2.source_id = s.source_id
          JOIN source_type st ON st.source_type_id = s2.source_type_id
          JOIN site s3 ON s3.site_id = s.site_id
        WHERE s2.name = 'source_A'
          AND v."name" = 'parameter_A'
          AND 't_start'::timestamp <= s.timestamp
          AND s.timestamp <= 't_end'::timestamp
        ORDER BY s.timestamp ASC;

*One thing to keep in mind is that in SQL, the order of the clauses is important:*

- SELECT
- FROM/JOIN
- WHERE
- GROUP BY
- HAVING
- ORDER BY
- LIMIT/OFFSET

Retrieve the variables that are recorded for a certain source_type ``source_type_A``.
DISTINCT is used to just display one line for each variable.

.. code-block:: sql

        SELECT
            DISTINCT v."name"
        FROM variable v
          JOIN signal s ON s.variable_id = v.variable_id
          JOIN "source" s2 ON s2.source_id = s.source_id
          JOIN source_type st on st.source_type_id = s2.source_type_id
        WHERE st.name = 'source_type_A'
          AND '2020-07-19 18:15:00.000'::timestamp <= s.timestamp
          AND s.timestamp <= '2020-07-20 18:15:00.000'::timestamp;


Retrieve all sites that belong to a certain source ``source_A``:

.. code-block:: sql

        SELECT
            s."name"
        FROM site s
          JOIN meta_data md ON md.site_id = s.site_id
          JOIN "source" s2 on s2.source_id = md.source_id
        WHERE s2.name = 'source_A'

Count the entries for all data in the database and group by source, and variable, aggregated weekly:

.. code-block:: sql

        WITH count_table AS (
            SELECT
                count(variable_id),
                variable_id,
                source_id,
                date_trunc('week', timestamp)
            FROM signal
            GROUP BY
                date_trunc('week', timestamp),
                variable_id,
                source_id
        )
        SELECT
            count_table.count AS value_count,
            v.name AS variable_name,
            source.name AS source_name,
            count_table.date_trunc AS date_trunc
        FROM count_table
        INNER JOIN variable v on v.variable_id  = count_table.variable_id
        INNER JOIN source ON source.source_id = count_table.source_id
        ORDER BY date_trunc desc;


Get all source that have data of variable  ``<my_variable>``:

.. code-block:: sql

        SELECT s."name"
        FROM "source" s
          JOIN signal s2 ON s.source_id = s2.source_id
          JOIN variable v ON v.variable_id = s2.variable_id
        WHERE v."name" = '<my_variable>'
        GROUP BY s.name;


Get a datetime range for all data stored for a certain ``<my_source>``:

.. code-block:: sql

        SELECT
          min(s."timestamp"),
          max(s."timestamp")
        FROM signal s
        JOIN "source" s2 ON s2.source_id = s.source_id
        WHERE s2.name = '<my_source>'



Get the special value definition for a source_type X ``<my_source_type>``

.. code-block:: sql

        SELECT
            svd.numerical_value
        FROM source_type st
            LEFT JOIN special_value_definition svd ON st.source_type_id = svd.source_type_id
        WHERE st.name='<my_source_type>'
        GROUP BY svd.numerical_value;


Get the source_type for a source ``<my_source>``

.. code-block:: sql

        SELECT
            source_type.name
        FROM source
        LEFT JOIN source_type ON source.source_type_id = source_type.source_type_id
        WHERE source.name='<my_source>';


Get the n ``<number_of_signals>`` newest signals:

.. code-block:: sql

        WITH signals as (
		        SELECT
                s.timestamp,
                s.value,
                v.unit,
                v.name AS variable_name,
                s2.NAME        AS source_name,
                s2.serial      AS source_serial,
                st.NAME   AS source_type_name
            FROM signal s
                INNER JOIN site s3 ON s.site_id = s3.site_id
                INNER JOIN variable v  ON s.variable_id = v.variable_id
                INNER JOIN source s2 ON s.source_id = s2.source_id
                INNER JOIN source_type st ON s2.source_type_id = st.source_type_id

            WHERE timestamp >= (Now() - interval '1 month')
            ORDER BY s.timestamp DESC
            limit <number_of_signals>
        )

        SELECT
            s.timestamp,
            s.value,
            s.unit,
            s.variable_name,
            s.source_name,
            s.source_serial,
            s.source_type_name
        FROM signals s
        GROUP BY
            s.timestamp,
            s.value,
            s.variable_name,
            s.unit,
            s.source_type_name,
            s.source_name,
            s.source_serial;


Get the latest signals for each source.

.. code-block:: sql

        SELECT
          swc."Source Name",
          swc.laststamp
        FROM source_with_coordinates swc
