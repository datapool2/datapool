.. code-block:: YAML

    name: wildbach_kempttalstr
    description: The site is the river wildbach flowing underneath the Kempttalstrasse.
    street: Kempttalstrasse
    postcode: 8320
    city: Fehraltdorf
    coordinates:
        x: 2698752
        y: 1249699
        z: 526
    pictures:
        -
            path: images/1.JPG
            description: the bridge with the Kempttalstrasse going over the small river Wildbach
        -
            path: images/2.JPG
            description: The zone beneath the bridge where the pressure sensor is installed
        -
            path: images/3.JPG
            description: The installed pressure sensor is within the tube.
    country: Switzerland
