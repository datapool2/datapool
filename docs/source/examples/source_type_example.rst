.. code-block:: YAML

    name: source_category_xyz
    description: all sources in this category belong to the xyz group
    manufacturer: XYZ Group
    special_values:
        -
            description: not a number
            categorical_value: NA
            numerical_value: -6666.0

