On a clean directory with a virtual environment active, install the `datapool_client package <https://github.com/Eawag-SWW/datapool_client>`_
following the instructions on the repository.

To set up the configuration to connect to the Datapool, we need to run the following code:


.. code-block:: python

      from datapool_client import set_defaults

      # connection parameters (example)
      instance = "DEFAULT"
      host = "ip.to.datapool.host"
      port = "5432"
      database = "db_name"
      user = "db_user_name"
      password = "db_user_password"

      # this function sets the default parameters programmatically
      set_defaults(
        instance=instance,
        host=host,
        port=port,
        database=database,
        user=user,
        password=password
      )

After running this code we run the following lines to make sure we are able to access the DataPool.

.. code-block:: python

      from datapool_client import DataPool
      dp = DataPool()

If the connection is correct we would receive the following message:

.. code-block:: none

      You are successfully connected to the database!

Once we have set up the default connection we don't need to run the ``set_defaults()`` function again.

The only exception would be on the case we would like to set up a second connection to another datapool.
On that case, we first run ``set_defaults()`` with ``instance = "My_instance_name"`` and then, we connect
to the Datapool with ``dp = Datapool(instance="My_instance_name")``.


Example SQL queries using datapool_client package
'''''''''''''

If we want to run our specific query and return a dataframe:

.. code-block:: python

    data_dataframe = dp.query_df(
        "select * from site;"
    )

On this case we have queried for all the information on the site table. The output is a pandas dataframe.

.. code-block:: python

    source = "your_source_name"
    start = "2018-07-11 00:00:00"
    end = "2018-07-11 23:55:00"
    variable = "your_variable_name"
    data = dp.signal.get(
        source_name=source,
        variable_name=variable,
        start=start,
        end=end
    )

This query returns all the signals from the ``source = "your_surce_name"`` between start and end date, for a defined variable.

We explore a broader usage of sql queries to the datapool on the section :ref:`Example SQL queries<example-sql-queries>`.
