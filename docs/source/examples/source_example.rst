.. code-block:: YAML

    name: bt_dl958_166_luppmenweg
    description: Replacement logger for bt_dl922_166_luppmenweg and bt_dl947_166_luppmenweg
    serial: 958
    project: project_1

