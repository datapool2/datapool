
.. code-block:: r

	# Example conversion script for R
	# Aug 04 2017 -- Simon Dicht
	# EDIT Jan 08 2018 -- Lukas Keller


	convert <- function(raw_file, output_file){
	  data.raw <- read.delim(raw_file, sep=";", skip=1, header=F, dec = ".", stringsAsFactors=F)
	  
	  #remove apostrophe so 1'000 can be a number
	  data.raw[,5] <- gsub("'","", data.raw[,5])
	  
	  # set special values for OL and NA
	  data.raw[,4][is.na(data.raw[,5])] <- -9999
	  data.raw[,5][is.na(data.raw[,5])] <- -9999 
	  data.raw[data.raw == "OL"] <- -8888
	  
	  # define location ID
	  pos <- "11e_russikerstr"

	  time <- as.POSIXct(data.raw[,1], format = "%d.%m.%Y %H:%M:%S", tz="UTC")
	  time <- format(time, "%Y-%m-%d %H:%M:%S")
	  
	  # if ncol(data.raw == 8)...

	  # creating the matrix with all fields
	  data<-data.frame(time, "flow rate", data.raw[,4],pos)
	  data<-rbind(data, setNames(data.frame(time, "average flow velocity", data.raw[,5], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "water level",           data.raw[,6], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "Water temperature",     data.raw[,7], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "battery voltage",       data.raw[,8], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "battery charge",        data.raw[,9], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "internal temperature",  data.raw[,11], pos), names(data)))
	  data<-rbind(data, setNames(data.frame(time, "RSSI",                  data.raw[,10], pos), names(data)))
	  
	  # deleate all rows with NA
	  #data=na.omit(data)
	  
	  # naming the columns
	  colnames(data) <- c("timestamp", "parameter", "value","site")
	  
	  #write.csv2(data, file="Test", row.names=FALSE )
	  
	  write.table(data, file=output_file, row.names=FALSE, col.names=TRUE,
				  quote=FALSE, sep=";")
	}
