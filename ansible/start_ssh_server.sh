#!/bin/bash
#
# start_ssh_server.sh
# Copyright (C) 2023 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#
#
docker build -t ssh-server ssh-server/


docker run\
    --name sshserver\
    -v ~/.ssh:/ssh\
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v $(pwd)/ssh-server/sshd_config:/etc/ssh/sshd_config\
    -p 2222:22\
    -p 8001:8000\
    -d\
    ssh-server
