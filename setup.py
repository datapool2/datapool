#!/usr/bin/env python

import os

from setuptools import find_packages, setup

requirements = [
    "pandas",
    "sqlalchemy<2",
    "click",
    "psutil",
    "psycopg2-binary",
    "prometheus-client",
    "watchdog>=3",
    "flask",
    "requests",
]


with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read().replace(".. :changelog", "")


doclink = """
Documentation
-------------

The full documentation can be generated with Sphinx"""


PACKAGE_PATH = os.path.abspath(os.path.join(__file__, os.pardir))

setup(
    name="datapool",
    version="2.4.1",
    description="rewrite of datapool sensor data warehouse",
    long_description=readme + "\n\n" + doclink + "\n\n" + history,
    author="Uwe Schmitt",
    author_email="uwe.schmitt@id.ethz.ch",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    include_package_data=True,
    install_requires=requirements,
    license="MIT License",
    zip_safe=False,
    keywords="datapool",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points="""
        [console_scripts]
        pool=datapool.main:main
    """,
)
