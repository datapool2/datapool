#!/usr/bin/env bash
exec 3< <(gitlab-runner exec docker --env SLEEP=99999 --timeout 99999 system_tests)

while true;
    do read <&3 line; echo "$line";
    if [[ "$line" == *'sleep ${SLEEP:-0}'* ]]; then
        break
    fi
done

ID=$(docker ps | grep runner | grep concurrent | grep build | cut -d " " -f 1)
docker exec -it ${ID} bash
