#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd ${SCRIPT_DIR}
export RUNNING_SYSTEM_TESTS=1

set -eu

echo "check if datapool_reader has read permissions on database"

PGPASSWORD=${DATAPOOL_READER_PASSWORD} psql -h postgis -U datapool_reader -d datapool | grep -v "permission denied" <<EOL
    select count(*) from signal;
    select count(*) from site;
    select count(*) from raster_data;
EOL

py.test --run-cli-tests -s tests/test_raster_data.py
py.test ${1:-} tests/
