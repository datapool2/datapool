#!/bin/bash
#
# run_versus_pg.sh
# Copyright (C) 2022 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

export POSTGRES_SERVER=127.0.0.1
export POSTGRES_USER=
export POSTGRES_PASSWORD=

py.test tests
