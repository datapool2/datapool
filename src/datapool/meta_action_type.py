#!/usr/bin/env python

from collections import Counter
from pathlib import Path

from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    Table,
    delete,
    select,
    update,
)

from datapool.poolkit.database import (
    Base,
    check_constraints,
    commit_dbo,
    fetch_many,
    find_differences,
)
from datapool.poolkit.yaml import List, NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class MetaActionTypeDecl(Base):
    _schema = Table(
        "meta_action_type",
        Base._metadata,
        Column("meta_action_type_id", Integer(), primary_key=True),
        Column(
            "meta_log_type_id",
            ForeignKey("meta_log_type.meta_log_type_id"),
            nullable=False,
        ),
        Column("name", String(), unique=False, index=True, nullable=False),
        Column("description", String()),
    )


meta_action_type_yaml_example = """
-
   meta_log_type: new_sensor
   name: installed
   description: new sensor installed
-
   meta_log_type: new_sensor
   name: checked
   description: new sensor checked
"""


class MetaActionTypeYaml(Yaml):
    entity_name = "meta_action_type"
    is_fact_dimension = True
    depends_on = ["meta_log_type"]

    location = "meta_data/meta_action_types.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        ("meta_data/meta_action_types.yaml", meta_action_type_yaml_example)
    ]

    dbos = [MetaActionTypeDecl]

    mapping = lambda config: List(
        {
            "meta_log_type": NotEmpty(str),
            "name": NotEmpty(MetaActionTypeDecl.name),
            "description": MetaActionTypeDecl.description,
        }
    )

    @classmethod
    def _read_yaml(cls, meta_action_types_file_path):
        if meta_action_types_file_path.exists():
            try:
                return parse_file(meta_action_types_file_path)
            except YamlError as e:
                return YamlError(f"file {meta_action_types_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["meta_log_type", "name", "description"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        from .meta_log_type import MetaLogTypeYaml

        meta_action_types_file_path = root_folder / cls.location
        meta_action_types = cls._read_yaml(meta_action_types_file_path)
        if isinstance(meta_action_types, Exception):
            raise meta_action_types

        meta_log_type = ask("meta_log_type")
        name = ask("name", not_empty=True)

        log_type_paths = list(MetaLogTypeYaml.find_by_name(root_folder, meta_log_type))
        if not log_type_paths:
            raise ValueError(f"meta_log_type with title '{meta_log_type}' is unknown")
        if len(log_type_paths) > 1:
            raise ValueError(
                f"internal error: meta_log_type with title '{meta_log_type}' found"
                " multiple times"
            )
        if isinstance(log_type_paths[0], Exception):
            raise log_type_paths[0]

        meta_action_type_mapping = {
            (p["name"], p["meta_log_type"]): p for p in meta_action_types
        }

        if (name, meta_log_type) in meta_action_type_mapping:
            raise ValueError(
                f"meta_action_type with name '{name}' and meta_log_type"
                f" '{meta_log_type}' exists already"
            )

        description = ask("description")

        meta_action_types.append(
            dict(meta_log_type=meta_log_type, name=name, description=description)
        )

        parent = meta_action_types_file_path.parent
        parent.mkdir(parents=True, exist_ok=True)

        with meta_action_types_file_path.open("w") as fh:
            print_yaml(meta_action_types, fh=fh)

        return meta_action_types_file_path

    @classmethod
    def find_by_name(cls, root_folder, name):
        meta_action_types = cls._read_yaml(root_folder / cls.location)
        if isinstance(meta_action_types, Exception):
            yield meta_action_types
            return
        found_error = False
        for error in super().check_yaml(
            root_folder, cls.location, meta_action_types, None
        ):
            yield error
            found_error = True
        if found_error:
            return
        for entry in meta_action_types:
            if entry["name"] == name:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = MetaActionTypeDecl._schema
        yield engine.execute(
            select([schema.c.meta_action_type_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        found_error = False
        for error in super().check_yaml(root_folder, rel_path, yaml, config):
            yield error
            found_error = True
        if found_error:
            return

        counter = Counter(
            (entry.get("name"), entry.get("meta_log_type")) for entry in yaml
        )
        for (name, meta_log_type), count in counter.items():
            if name is None or meta_log_type is None:
                continue
            if count > 1:
                yield ValueError(
                    f"meta_action_type with name '{name}' and meta_log_type"
                    f" '{meta_log_type}' appears {count} times"
                )

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, meta_action_types):
        from .meta_log_type import MetaLogTypeDecl

        existing_meta_log_types_by_name = {}
        existing_meta_log_types_by_id = {}
        for dbo in fetch_many(engine, MetaLogTypeDecl):
            existing_meta_log_types_by_name[dbo["name"]] = dbo.meta_log_type_id
            existing_meta_log_types_by_id[dbo["meta_log_type_id"]] = dbo.name

        existing_meta_action_types = {}
        for dbo in fetch_many(engine, MetaActionTypeDecl):
            existing_meta_action_types[dbo["name"], dbo["meta_log_type_id"]] = dbo

        to_check = {}
        found_error = False
        for item in meta_action_types:
            meta_log_type = item["meta_log_type"]
            meta_log_type_id = existing_meta_log_types_by_name.get(meta_log_type)
            if meta_log_type_id is None:
                yield ValueError(
                    f"meta_log_type with name '{meta_log_type}' does not exit"
                )
                found_error = True
                continue
            to_check[item["name"], meta_log_type_id] = item
            item["meta_log_type_id"] = meta_log_type_id
            del item["meta_log_type"]

        if found_error:
            return

        messages = []

        existing_meta_action_type_dbos = fetch_many(engine, MetaActionTypeDecl)

        (
            new_meta_action_type_dbos,
            modified_meta_action_type_dbos_ok,
            modified_meta_action_type_dbos_not_ok,
            deleted_meta_action_type_dbos,
            same,
        ) = find_differences(
            MetaActionTypeDecl,
            meta_action_types,
            existing_meta_action_type_dbos,
            ["name", "meta_log_type_id"],
            ["meta_action_type_id"],
        )

        for meta_action_type_dbo in deleted_meta_action_type_dbos:
            name = meta_action_type_dbo["name"]
            log_type_id = meta_action_type_dbo["meta_log_type_id"]
            log_type = existing_meta_log_types_by_id[log_type_id]
            messages.append(
                f"meta_action_type '{name}' with log_type '{log_type}' was deleted"
            )

        if messages:
            yield from map(ValueError, sorted(messages))
            return

        # invalid mods are not possible here

        yield new_meta_action_type_dbos + modified_meta_action_type_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config).specification)
        if messages:
            yield from map(ValueError, messages)
            return
        for dbo in dbos:
            commit_dbo(engine, dbo)

    @classmethod
    def delete_from_db(cls, config, engine, meta_action_type_id):
        engine.execute(
            delete(MetaActionTypeDecl._schema).where(
                MetaActionTypeDecl._schema.c.meta_action_type_id == meta_action_type_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        meta_action_types = cls._read_yaml(yaml_path)
        if isinstance(meta_action_types, Exception):
            yield meta_action_types
            return
        meta_action_types = [p for p in meta_action_types if p["name"] != name]
        with yaml_path.open("w") as fh:
            print_yaml(meta_action_types, fh=fh)

        yield f"deleted meta_action_type '{name}' from {yaml_path}"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
        except YamlError as e:
            yield e
            return
        found = False
        for meta_action_type in data:
            if meta_action_type["name"] == old_name:
                found = True
                meta_action_type["name"] = new_name
        if found is False:
            yield ValueError(f"meta_action_type with name '{old_name}' does not exist")
            return

        try:
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except IOError as e:
            yield e
            return

        yield (
            f"changed meta_action_type name in {yaml_path} from '{old_name}' to"
            " '{new_name}'"
        )

        lz_root = yaml_path.parent.parent

        all_children = [
            p.relative_to(lz_root)
            for p in (lz_root / "meta_data").glob("**/*")
            # if p != lz_root / yaml_path
        ]

        renames = dict(zip(all_children, all_children))
        yield Path("meta_data/meta_action_types.yaml"), renames

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        meta_action_type_id = rename_data["item_id"]
        table = MetaActionTypeDecl._schema
        engine.execute(
            delete(table).where(table.c.meta_action_type_id == meta_action_type_id)
        )
        yield f"deleted meta_action_type with meta_action_type_id={meta_action_type_id}"

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        meta_action_type_id = rename_data["item_id"]
        table = MetaActionTypeDecl._schema
        engine.execute(
            update(table)
            .where(table.c.meta_action_type_id == meta_action_type_id)
            .values(name=new_name)
        )
        yield (
            "renamed meta_action_type with"
            f" meta_action_type_id={meta_action_type_id} to {new_name}"
        )

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        return
        yield
