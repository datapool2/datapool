#!/usr/bin/env python

from collections import Counter
from pathlib import Path

from sqlalchemy import Column, Integer, String, Table, delete, select, update

from datapool.poolkit.database import (
    Base,
    check_constraints,
    commit_dbo,
    fetch_many,
    find_differences,
    find_duplicates,
)
from datapool.poolkit.yaml import List, NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class PersonDecl(Base):
    _schema = Table(
        "person",
        Base._metadata,
        Column("person_id", Integer(), primary_key=True),
        Column("abbreviation", String(), index=True, unique=True, nullable=False),
        Column("name", String(), nullable=False),
        Column("email", String()),
    )


person_yaml_example = """
-
   abbreviation: schmittu
   name: Uwe Schmitt
   email: uwe.schmitt@ethz.ch
-
   abbreviation: dollc
   name: Carina Doll
   email: carina.doll@eawag.ch
"""


class PersonYaml(Yaml):
    entity_name = "person"
    is_fact_dimension = True

    # single unique yaml might be special!? -> considers this when abstrating the
    # framework later?

    location = "persons.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [("persons.yaml", person_yaml_example)]

    dbos = [PersonDecl]

    mapping = lambda config: List(
        {
            "abbreviation": PersonDecl.abbreviation,
            "name": NotEmpty(PersonDecl.name),
            "email": PersonDecl.email,
        }
    )

    @classmethod
    def _read_yaml(cls, persons_file_path):
        if persons_file_path.exists():
            try:
                return parse_file(persons_file_path)
            except YamlError as e:
                return YamlError(f"file {persons_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["abbreviation", "name", "email"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        persons_file_path = root_folder / cls.location
        persons = cls._read_yaml(persons_file_path)
        if isinstance(persons, Exception):
            raise persons

        abbreviations_to_persons = {p["abbreviation"]: p for p in persons}

        abbreviation = ask("abbreviation", not_empty=True)
        if abbreviation in abbreviations_to_persons:
            raise ValueError(
                f"person with abbreviation '{abbreviation}' exists already"
            )

        name = ask("name", not_empty=True)
        email = ask("email")

        persons.append(dict(abbreviation=abbreviation, name=name, email=email))

        with persons_file_path.open("w") as fh:
            print_yaml(persons, fh=fh)

        return persons_file_path

    @classmethod
    def find_by_name(cls, root_folder, abbreviation):
        persons = cls._read_yaml(root_folder / cls.location)
        if isinstance(persons, Exception):
            yield persons
            return
        for entry in persons:
            if entry["abbreviation"] == abbreviation:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, abbreviation):
        schema = PersonDecl._schema
        yield engine.execute(
            select([schema.c.person_id]).where(schema.c.abbreviation == abbreviation)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)
        counter = Counter(entry.get("abbreviation") for entry in yaml)
        for abbreviation, count in counter.items():
            if abbreviation is None:
                continue
            if count > 1:
                yield ValueError(
                    f"person with name '{abbreviation}' appears {count} times"
                )

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, persons):
        messages = []

        duplicates, unique_persons = find_duplicates(persons, ["abbreviation"])

        reported_abbreviations = set()
        for duplicate in duplicates:
            abbreviation = duplicate["abbreviation"]
            if abbreviation in reported_abbreviations:
                continue
            reported_abbreviations.add(abbreviation)
            messages.append(f"duplicate entry with abbreviation '{abbreviation}'")

        existing_person_dbos = fetch_many(engine, PersonDecl)
        (
            new_person_dbos,
            modified_person_dbos_ok,
            modified_person_dbos_not_ok,
            deleted_person_dbos,
            same,
        ) = find_differences(
            PersonDecl, unique_persons, existing_person_dbos, ["abbreviation"], []
        )

        for person_dbo in deleted_person_dbos:
            abbreviation = person_dbo["abbreviation"]
            messages.append(f"person with abbreviation '{abbreviation}' was deleted")

        for person_dbo in modified_person_dbos_not_ok:
            abbreviation = person_dbo["abbreviation"]
            messages.append(f"person with abbreviation '{abbreviation}' was modified")

        if messages:
            yield from map(ValueError, messages)
            return

        yield new_person_dbos + modified_person_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config).specification)
        if messages:
            yield from map(ValueError, messages)
            return
        for dbo in dbos:
            commit_dbo(engine, dbo)

    @classmethod
    def delete_from_db(cls, config, engine, person_id):
        engine.execute(
            delete(PersonDecl._schema).where(
                PersonDecl._schema.c.person_id == person_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, abbreviation):
        persons = cls._read_yaml(yaml_path)
        if isinstance(persons, Exception):
            yield persons
            return
        persons = [p for p in persons if p["abbreviation"] != abbreviation]
        with yaml_path.open("w") as fh:
            print_yaml(persons, fh=fh)

        yield f"deleted person with abbreviation '{abbreviation}' from {yaml_path}"

    @classmethod
    def rename(cls, old_abbreviation, new_abbreviation, yaml_path):
        try:
            data = parse_file(yaml_path)
        except YamlError as e:
            yield e
            return
        found = False
        for person in data:
            if person["abbreviation"] == old_abbreviation:
                found = True
                person["abbreviation"] = new_abbreviation
        if found is False:
            yield ValueError(
                f"person with abbreviation '{old_abbreviation}' does not exist"
            )
            return

        try:
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except IOError as e:
            yield e
            return

        yield (
            f"changed person name in {yaml_path} from '{old_abbreviation}' to"
            f"'{new_abbreviation}'"
        )

        renames = {}
        lz_root = yaml_path.parent
        for p in (lz_root / "meta_data").glob("*/*/images/*"):
            p = p.relative_to(lz_root)
            renames[p] = p
        yield Path("persons.yaml"), renames

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        person_id = rename_data["item_id"]
        table = PersonDecl._schema
        engine.execute(delete(table).where(table.c.person_id == person_id))
        yield f"deleted person with person_id={person_id}"

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_abbreviation = rename_data["new_name"]
        person_id = rename_data["item_id"]
        table = PersonDecl._schema
        engine.execute(
            update(table)
            .where(table.c.person_id == person_id)
            .values(abbreviation=new_abbreviation)
        )
        yield (
            f"renamed persons abbreviation with person_id={person_id} to"
            f" {new_abbreviation}"
        )

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        return
        yield
