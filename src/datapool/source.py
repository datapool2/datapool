#!/usr/bin/env python

import glob
import pathlib
import shutil

from sqlalchemy import (
    Column,
    ForeignKey,
    Identity,
    Integer,
    String,
    Table,
    delete,
    select,
    update,
)

from datapool.poolkit.database import (
    Base,
    ModifiedDbo,
    check_constraints,
    check_modifications,
    commit_dbo,
    create_dbo,
    fetch_one,
)
from datapool.poolkit.utils import free_text_to_path_name
from datapool.poolkit.yaml import NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml

from .signal import conversion_spec_yaml


class SourceDecl(Base):
    _schema = Table(
        "source",
        Base._metadata,
        Column("source_id", Integer(), Identity(always=True), primary_key=True),
        Column(
            "source_type_id", ForeignKey("source_type.source_type_id"), nullable=False
        ),
        Column("project_id", ForeignKey("project.project_id"), nullable=False),
        Column("name", String(), unique=True, index=True, nullable=False),
        Column("description", String()),
        Column("serial", String()),
    )


source_yaml_example = """
name: sensor_abc_from_xyz
description: All the details of the this source.
serial: 000-111-222-333
project: sis_datapool
"""


class SourceYaml(Yaml):
    entity_name = "source"
    is_fact_dimension = True

    # TO IMPLEMENT ORDER OF CHECKS / HANDLING WHEN READING YAMLS
    depends_on = ["source_type", "project"]

    location = "data/*/*/source.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        ("data/sensor_from_xyz/sensor_abc_from_xyz/source.yaml", source_yaml_example)
    ]

    dbos = [SourceDecl]

    mapping = lambda config: {
        "name": NotEmpty(SourceDecl.name),
        "description": SourceDecl.description,
        "serial": SourceDecl.serial,
        "project": NotEmpty(str),
    }

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)
        name = yaml.get("name")
        if name and rel_path.parent.name != name:
            yield ValueError(
                f"source_type name '{name}' does not match folder name"
                f" '{rel_path.parent}'"
            )

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def find_by_name(cls, root_folder, name):
        for p in glob.glob(str(root_folder / cls.location)):
            p = pathlib.Path(p)
            try:
                data = parse_file(p)
            except YamlError as e:
                yield e
                return
            if data["name"] == name:
                yield p

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = SourceDecl._schema
        yield engine.execute(
            select([schema.c.source_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def prepare(cls, root_folder, rel_path, data):
        root_folder = pathlib.Path(root_folder)
        source_type_yaml_full_path = (
            root_folder / rel_path
        ).parent.parent / "source_type.yaml"
        source_type_yaml_rel_path = source_type_yaml_full_path.relative_to(root_folder)
        if not source_type_yaml_full_path.exists():
            yield ValueError(f"did not find {source_type_yaml_rel_path}")
            return

        source_type = parse_file(source_type_yaml_full_path)
        assert "name" in source_type, "order handling in poolkit failed!"
        data["source_type_name"] = source_type["name"]
        yield data

    @classmethod
    def check_against_db(cls, config, engine, data):
        from .projects import ProjectDecl
        from .source_type import SourceTypeDecl

        project_name = data["project"]
        project_dbo = fetch_one(engine, ProjectDecl, title=project_name)
        del data["project"]

        if not project_dbo:
            yield ValueError(f"project with name '{project_name}' does not exit")
            return

        messages = []
        source_dbo = fetch_one(engine, SourceDecl, name=data["name"])

        if not source_dbo:
            source_type_dbo = fetch_one(
                engine, SourceTypeDecl, name=data["source_type_name"]
            )
            assert source_type_dbo is not None, "internal error"

            data["source_type_id"] = source_type_dbo.source_type_id
            del data["source_type_name"]

            data["project_id"] = project_dbo.project_id

            source_dbo = create_dbo(SourceDecl, data)
        else:
            messages.extend(
                check_modifications(
                    SourceDecl,
                    source_dbo,
                    data,
                    [],
                    [
                        "source_id",
                        "source_type_id",
                        "source_type_name",
                        "project_id",
                        "description",
                    ],
                )
            )
            if not messages:
                data["project_id"] = project_dbo.project_id
                data = {**source_dbo._data.copy(), **data}
                del data["source_type_name"]

                source_dbo = ModifiedDbo(SourceDecl._schema, data)

        if messages:
            yield from map(ValueError, messages)
            return
        yield [source_dbo]

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config))
        if messages:
            yield from map(ValueError, messages)
            return
        commit_dbo(engine, dbos[0])
        return
        yield

    @classmethod
    def create_new_fields(cls, config):
        return ["source_type", "name", "description", "serial", "project"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        from .projects import ProjectYaml
        from .source_type import SourceTypeYaml

        source_type = ask("source_type")

        path = SourceTypeYaml.find_source_type(root_folder, source_type)
        if path is None:
            raise ValueError(f"source_type '{source_type}' is unknown")

        name = ask("name", not_empty=True)
        description = ask("description")
        serial = ask("serial")

        project = ask("project")
        project_paths = list(ProjectYaml.find_by_name(root_folder, project))

        if not project_paths:
            raise ValueError(f"project with title '{project}' is unknown")
        if len(project_paths) > 1:
            raise RuntimeError("internal error")

        if isinstance(project_paths[0], Exception):
            raise project_paths[0]

        data = dict(
            name=name,
            description=description,
            serial=serial,
            project=project,
        )

        sub_folder = free_text_to_path_name(name)

        source_folder = path.parent / sub_folder
        source_yaml_path = source_folder / "source.yaml"

        if source_yaml_path.exists():
            raise ValueError(
                f"folder {source_yaml_path} exists, you must choose a different name"
            )

        source_folder.mkdir(parents=True, exist_ok=True)

        with source_yaml_path.open("w") as fh:
            print_yaml(data, fh=fh)

        raw_data_folder = source_folder / "raw_data"
        raw_data_folder.mkdir(parents=True, exist_ok=True)

        with (raw_data_folder / "conversion_spec.yaml").open("wb") as fh:
            fh.write(conversion_spec_yaml.load())

        return source_yaml_path

    @classmethod
    def delete_from_db(cls, config, engine, source_id):
        engine.execute(
            delete(SourceDecl._schema).where(
                SourceDecl._schema.c.source_id == source_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        folder = yaml_path.parent
        try:
            shutil.rmtree(folder)
        except IOError as e:
            yield IOError(f"could not remove folder {folder}: {e}")
        yield f"deleted {folder} and its content"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
            data["name"] = new_name
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except (YamlError, IOError) as e:
            yield e
            return

        yield f"changed name in {yaml_path}"

        lz_root = yaml_path.parent.parent.parent.parent

        affected_files_before = [yaml_path.relative_to(lz_root)]

        conversion_scripts = [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob(f"*/{old_name}/conversion.*")
        ]
        conversion_specs = [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob(
                f"*/{old_name}/raw_data/conversion_spec.yaml"
            )
        ]
        binary_folders = [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob(f"*/{old_name}/binary_data")
        ]
        affected_files_before += conversion_scripts
        affected_files_before += conversion_specs
        affected_files_before += binary_folders
        affected_files_before += [yaml_path.relative_to(lz_root).parent]

        renames = {}
        new_folder_name = free_text_to_path_name(new_name)
        for p in affected_files_before:
            parts = p.parts
            new_parts = parts[:2] + (new_folder_name,) + parts[3:]
            renames[p] = pathlib.Path(*new_parts)

        new_folder_path = yaml_path.parent.parent / new_folder_name
        try:
            yaml_path.parent.rename(new_folder_path)
        except IOError as e:
            yield e
            return

        yield f"renamed folder {yaml_path.parent} to {new_folder_path}"
        yield new_folder_path.relative_to(lz_root) / "source.yaml", renames

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        source_id = rename_data["item_id"]
        table = SourceDecl._schema
        engine.execute(delete(table).where(table.c.source_id == source_id))
        yield f"deleted source with source_id={source_id}"

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        source_id = rename_data["item_id"]
        table = SourceDecl._schema
        engine.execute(
            update(table).where(table.c.source_id == source_id).values(name=new_name)
        )
        yield f"renamed source with source_id={source_id} to {new_name}"

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        for old_path in rename_data["renames"].keys():
            p = root_folder / old_path
            if p.is_dir():
                shutil.rmtree(p)
                yield f"removed folder {p}"
            else:
                p.unlink()
                yield f"removed file {p}"

        # cleanup folders
        # sort to delete subfolders first:
        for old_path in sorted(rename_data["renames"].keys(), key=len, reverse=True):
            p = root_folder / old_path
            if not p.is_dir():
                p = p.parent
            while p != root_folder:
                if p.exists() and not list(p.iterdir()):
                    p.rmdir()
                    yield f"removed empty folder {p}"
                    p = p.parent
                else:
                    break
