#!/usr/bin/env python


import csv

from datapool.poolkit.type_handling import date_time_parser
from datapool.poolkit.utils import is_number

TIMESTAMPS = ("timestamp_start_lab", "timestamp_end_lab", "timestamp_sample")
NUMBERS = ("value_lab", "dilution_lab", "dilution_sample")
MULTI_FIELDS = ("person_abbrev_lab", "person_abbrev_sample")

FIX_COLUMN_NAMES = set(
    (
        (
            "lab_identifier",
            "variable",
            "sample_identifier",
            "filter_lab",
            "method_lab",
            "description_lab",
            "site",
            "filter_sample",
            "method_sample",
            "description_sample",
        )
        + TIMESTAMPS
        + NUMBERS
        + MULTI_FIELDS
    )
)

MAYBE_EMPTY = set(
    (
        "description_lab",
        "description_sample",
        "method_sample",
        "dilution_lab",
        "timestamp_start_lab",
    )
)


def read_and_check_csv_file(path):
    try:
        reader = csv.DictReader(path.open(), delimiter=";")
    except (csv.Error, IOError) as e:
        yield e.__class__(f"failed to read {path}: {e}")
        return

    rows = list(reader)
    if not rows:
        yield ValueError(f"{path} is empty")
        return

    header = rows[0]

    unknown = set(header) - FIX_COLUMN_NAMES
    missing = FIX_COLUMN_NAMES - set(header)

    if missing or unknown:
        for m in missing:
            yield ValueError(f"column '{m}' is missing")
        for u in unknown:
            yield ValueError(f"column '{u}' is not allowed")
        return

    for row_number, row in enumerate(rows, 2):
        for col_name in header:
            value = row[col_name].strip()
            yield from check_value(col_name, row_number, value)

    yield rows


def check_value(col_name, row_number, value):
    if not value:
        if col_name in MAYBE_EMPTY:
            return
        yield ValueError(
            f"value {value} for {col_name} in row {row_number} must not be empty"
        )
        return
    if col_name in TIMESTAMPS:
        try:
            date_time_parser(value)
        except ValueError:
            yield ValueError(
                f"value {value} for {col_name} in row {row_number} is not"
                " a valid timestamp"
            )
    if col_name in NUMBERS:
        if not is_number(value):
            yield ValueError(
                f"value {value} for {col_name} in row {row_number} is not"
                " a valid number"
            )


def prepare_rows(rows):
    prepared_rows = []
    for row in rows:
        for col_name in MAYBE_EMPTY:
            row[col_name] = row[col_name] or None

        for col_name in TIMESTAMPS:
            if row[col_name]:
                row[col_name] = date_time_parser(row[col_name])
        for col_name in NUMBERS:
            if row[col_name]:
                row[col_name] = float(row[col_name])
        for col_name in MULTI_FIELDS:
            if row[col_name]:
                row[col_name] = [f.strip() for f in row[col_name].split(",")]
        prepared_rows.append(row)

    return prepared_rows
