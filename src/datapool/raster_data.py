#!/usr/bin/env python

import operator
import pathlib
import textwrap

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    LargeBinary,
    Table,
    and_,
    delete,
    func,
    join,
    select,
)
from sqlalchemy.sql import text
from sqlalchemy.sql.functions import GenericFunction

from datapool.poolkit.database import Base
from datapool.poolkit.dispatcher import CustomizedDispatcher
from datapool.poolkit.type_handling import date_parser, date_time_parser
from datapool.poolkit.utils import Asset, print_query
from datapool.poolkit.yaml import RawDataFile
from datapool.poolkit.yaml_parser import YamlError, parse_file

from .source import SourceDecl
from .variables import VariableDecl


class size_raster(GenericFunction):
    type = Integer
    name = "ST_MemSize"
    inherit_cache = True
    identifier = "size_raster"


class RasterDataDecl(Base):
    _schema = Table(
        "raster_data",
        Base._metadata,
        Column("raster_data_id", Integer(), primary_key=True),
        Column("timestamp", DateTime(), nullable=False, index=True),
        Column("variable_id", ForeignKey("variable.variable_id"), nullable=True),
        Column("source_id", ForeignKey("source.source_id"), nullable=True),
        Column("data", LargeBinary(), nullable=False),
    )


RASTER_SPEC_YAML = "raster_data_spec.yaml"


class Dispatcher(CustomizedDispatcher):
    @classmethod
    def check(cls, root_folder, rel_path, config, engine):
        if rel_path.name != RASTER_SPEC_YAML:
            files = [p.name for p in (root_folder / rel_path).parent.iterdir()]
            if RASTER_SPEC_YAML not in files:
                yield ValueError(
                    f"found data file at {rel_path} but no {RASTER_SPEC_YAML}"
                )
                return
            yield f"skip {rel_path}"
            return

        spec_path = root_folder / rel_path
        try:
            spec = parse_file(spec_path)
        except YamlError as e:
            yield YamlError(f"failed to parse {spec_path}: {e}.")
            return

        expected_keys = ["timestamp", "file"]
        optional_keys = ["variable"]

        missing = set(expected_keys) - set(spec)
        unknown = set(spec) - set(expected_keys) - set(optional_keys)

        if missing or unknown:
            for m in sorted(missing):
                yield ValueError(f"{RASTER_SPEC_YAML} has no entry for '{m}'")
            for u in sorted(unknown):
                yield ValueError(f"{RASTER_SPEC_YAML} has unknown field '{u}'")
            return

        if any(not isinstance(v, str) for v in spec.values()):
            yield ValueError(
                f"{RASTER_SPEC_YAML} malformed: field values must not be lists or"
                " mappings"
            )
            return

        timestamp = spec["timestamp"]
        try:
            timestamp = date_time_parser(timestamp)
        except ValueError:
            yield ValueError(f"timestamp '{timestamp}' is malformed")
            return

        data_path = (root_folder / rel_path).parent / spec["file"]

        if not data_path.exists():
            yield IOError(f"specified file {data_path} does not exist.")
            return

        try:
            with data_path.open():
                pass
        except IOError as e:
            yield IOError(f"failed to open {data_path} for reading: {e}.")
            return

        try:
            with open(data_path, "rb") as fh:
                data = fh.read()
        except Exception as e:
            yield ValueError(f"{data_path} might be corrupt: {e}")

        source = rel_path.parent.parent.name
        variable = spec.get("variable")

        yield from cls._insert(engine, source, variable, timestamp, data)

    @classmethod
    def _insert(cls, engine, source, variable, timestamp, data):
        source_table = SourceDecl._schema
        variable_table = VariableDecl._schema
        raster_data_table = RasterDataDecl._schema

        variable_id = engine.execute(
            select([variable_table.c.variable_id]).where(
                variable_table.c.name == variable
            )
        ).scalar()

        source_id = engine.execute(
            select([source_table.c.source_id]).where(source_table.c.name == source)
        ).scalar()

        if None in (variable_id, source_id):
            if variable_id is None:
                yield ValueError(f"variable {variable} is not known")
            if source_id is None:
                yield ValueError(f"source {source} is not known")
            return

        if variable_id is None:
            extra = (raster_data_table.c.variable_id.isnot(None),)
        else:
            extra = (raster_data_table.c.variable_id == variable_id,)

        existing = engine.execute(
            select([raster_data_table.c.data]).where(
                and_(
                    raster_data_table.c.source_id == source_id,
                    raster_data_table.c.timestamp == timestamp,
                    *extra,
                )
            )
        ).scalar()

        if existing is not None:
            yield "same data entry already exists in db"
            return

        if engine.url.drivername == "postgres":
            print("USE POSTGIS")
            engine.execute(
                text(
                    """
                    INSERT INTO raster_data (timestamp, variable_id, source_id, data)
                    VALUES (:timestamp, :variable_id, :source_id,
                            ST_FromGDALRaster(:data));
                """
                ),
                timestamp=timestamp,
                variable_id=variable_id,
                source_id=source_id,
                data=data,
            )
        else:
            engine.execute(
                text(
                    """
                    INSERT INTO raster_data (timestamp, variable_id, source_id, data)
                    VALUES (:timestamp, :variable_id, :source_id, :data);
                """
                ),
                timestamp=timestamp,
                variable_id=variable_id,
                source_id=source_id,
                data=data,
            )

        yield f"inserted {len(data)} bytes into database"

    @classmethod
    def dispatch(cls, root_folder, rel_path, config, engine):
        try:
            if rel_path.name != RASTER_SPEC_YAML:
                yield f"skip {rel_path}"
                yield False
                return
            # backup files:
            yield from cls.check(root_folder, rel_path, config, engine)
            yield [
                p.relative_to(root_folder)
                for p in (root_folder / rel_path).parent.iterdir()
            ]
        except Exception as e:
            yield e
            yield False


class RasterData(Dispatcher, RawDataFile):
    depends_on = ["source", "variable"]
    script_depends_on = []

    entity_name = "raster_data"

    def examples(config, RASTER_SPEC_YAML=RASTER_SPEC_YAML):
        _example_root = pathlib.Path(
            "data/sensor_from_xyz/sensor_abc_from_xyz/raster_data"
        )
        RASTER_EXAMPLE_FILE = "masked-1990.tif"
        RASTER_DATA_EXAMPLE = Asset(f"assets/{RASTER_EXAMPLE_FILE}")
        RASTER_DATA_SPEC_EXAMPLE = textwrap.dedent(
            """
        variable: Temperature
        timestamp: 2020-12-22 13:30:00
        file: masked-1990.tif
        """
        ).lstrip()
        return [
            (
                _example_root / RASTER_EXAMPLE_FILE,
                RASTER_DATA_EXAMPLE,
            ),
            (
                _example_root / RASTER_SPEC_YAML,
                RASTER_DATA_SPEC_EXAMPLE,
            ),
        ]

    raster_data_root = pathlib.Path("data/*/*/raster_data")

    location = (raster_data_root / "*",)

    @classmethod
    def related_files(cls):
        yield cls.raster_data_root / "*"

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def cleanup(cls, root_folder, rel_path, config, engine, backup_folder):
        folder = rel_path.parent
        for p in sorted((root_folder / folder).glob("*.npz")):
            p.unlink()
            yield f"removed {p.relative_to(root_folder)}"

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        yield from backup_folder.glob(str(cls.raster_data_root / "*"))

    @classmethod
    def data_files(cls, lz):
        # those which trigger check
        yield from lz.glob(str(cls.raster_data_root / RASTER_SPEC_YAML))

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        from .source import SourceDecl
        from .variables import VariableDecl

        raster_data_table = RasterDataDecl._schema
        pd = VariableDecl._schema
        sourced = SourceDecl._schema

        j = join(
            raster_data_table, pd, raster_data_table.c.variable_id == pd.c.variable_id
        )
        j = join(j, sourced, raster_data_table.c.source_id == sourced.c.source_id)

        is_postgres = engine.url.drivername == "postgresql"

        num_bytes = func.size_raster if is_postgres else func.length

        stmt = (
            select(
                [
                    raster_data_table.c.raster_data_id,
                    raster_data_table.c.timestamp,
                    num_bytes(raster_data_table.c.data).label("number_of_bytes"),
                    pd.c.name.label("parameter"),
                    sourced.c.name.label("source"),
                ]
            )
            .where(and_(*checks))
            .select_from(j)
        )

        header = [
            "raster_data_id",
            "timestamp",
            "number_of_bytes",
            "variable",
            "source",
        ]

        print_query(engine, stmt, header, indent, print_, max_rows)

    @classmethod
    def count_facts(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        yield engine.execute(
            select([func.count(RasterDataDecl._schema.c.raster_data_id)]).where(
                and_(*checks)
            )
        ).scalar()

    @classmethod
    def supported_fields_for_delete(cls):
        return set(
            (
                "timestamp",
                "source",
                "source_type",
                "variable",
                "project",
            )
        )

    @classmethod
    def _translate_conditions(cls, engine, conditions):
        from .projects import ProjectDecl
        from .source import SourceDecl
        from .source_type import SourceTypeDecl
        from .variables import VariableDecl

        foreign_table_schemata = {
            "variable": VariableDecl._schema,
            "source": SourceDecl._schema,
        }

        raster_data_schema = RasterDataDecl._schema

        supported_names = cls.supported_fields_for_delete()

        errors = []

        checks = []
        for name, op, value in conditions:
            if name not in supported_names:
                s = ", ".join(sorted(supported_names))
                errors.append(f"field {name} does not exist, please chose one of {s}")
                continue
            if name == "source_type":
                if op != "==":
                    errors.append("source types can only be filtered using '=='")
                    continue
                source_type_id = engine.execute(
                    select([SourceTypeDecl._schema.c.source_type_id]).where(
                        SourceTypeDecl._schema.c.name == value
                    )
                ).scalar()

                if source_type_id is None:
                    errors.append(f"source_type {value!r} does not exist")
                    continue
                source_ids = engine.execute(
                    select([SourceDecl._schema.c.source_type_id]).where(
                        SourceDecl._schema.c.source_type_id == source_type_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(raster_data_schema.c.source_id.in_(source_ids))
                continue

            if name == "project":
                if op != "==":
                    errors.append("projects can only be filtered using '=='")
                    continue
                project_id = engine.execute(
                    select([ProjectDecl._schema.c.project_id]).where(
                        ProjectDecl._schema.c.title == value
                    )
                ).scalar()

                if project_id is None:
                    errors.append(f"project with title {value!r} does not exist")
                    continue

                source_ids = engine.execute(
                    select([SourceDecl._schema.c.project_id]).where(
                        SourceDecl._schema.c.project_id == project_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(raster_data_schema.c.source_id.in_(source_ids))
                continue

            if name in foreign_table_schemata:
                schema = foreign_table_schemata[name]
                item_id = engine.execute(
                    select([getattr(schema.c, name + "_id")]).where(
                        schema.c.name == value
                    )
                ).scalar()
                if item_id is None:
                    errors.append(f"{name} {value!r} does not exist")
                    continue
                if op not in ("==", "!="):
                    errors.append(f"{name} can only be filtered using '==' and '!=")
                    continue
                if op == "==":
                    checks.append(
                        getattr(raster_data_schema.c, name + "_id") == item_id
                    )
                else:
                    checks.append(
                        getattr(raster_data_schema.c, name + "_id") != item_id
                    )
                continue

            if name.endswith("_id"):
                if op != "==":
                    errors.append("ids can only be filtered using '=='")
                    continue
                try:
                    value = int(value)
                except ValueError:
                    errors.append(f"{name} {value} is not valid integer")
                    continue

                checks.append(getattr(raster_data_schema.c, name) == value)
                continue

            if name == "timestamp":
                try:
                    value = date_time_parser(value)
                except ValueError:
                    try:
                        value = date_parser(value)
                    except ValueError:
                        errors.append(
                            ValueError(f"'{value}' is neither a timestamp nor a date")
                        )
                        continue

            op_method = {
                "<": operator.lt,
                ">": operator.gt,
                "<=": operator.le,
                ">=": operator.ge,
                "==": operator.eq,
                "!=": operator.ne,
            }[op]

            checks.append(op_method(getattr(raster_data_schema.c, name), value))

        return errors, checks

    @classmethod
    def delete(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        raster_data_table = RasterDataDecl._schema
        condition = and_(*checks)
        engine.execute(delete(raster_data_table).where(condition))
