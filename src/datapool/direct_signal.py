#!/usr/bin/env python

import pathlib

from .poolkit.dispatcher import CustomizedDispatcher
from .poolkit.fact_table import Context
from .poolkit.uniform_file_format import read_from_file
from .poolkit.utils import filter_results
from .poolkit.yaml import RawDataFile
from .signal import Signals


class Dispatcher(CustomizedDispatcher):
    @classmethod
    def check(cls, root_folder, rel_path, config, engine):
        if rel_path.suffix != ".csv":
            yield f"skip {rel_path}"
            return

        try:
            facts = read_from_file(root_folder / rel_path)
        except Exception as e:
            yield Exception(f"reading output from {rel_path} failed: {e}")
            return

        for fact in facts:
            if len(fact) == 1:
                yield Exception("data has only one column, wrong delimiter?")
            break

        found_errors, messages, _ = filter_results(
            (msg for fact in facts for msg in Signals.check_fields(fact))
        )
        yield from messages
        if found_errors:
            return

        found_errors, messages, _ = filter_results(
            (
                msg
                for msg in Signals.prepare_facts(
                    facts, (root_folder / rel_path).parent, None, None
                )
            )
        )
        yield from messages
        if found_errors:
            return

        found_errors, messages, _ = filter_results(
            (msg for msg in Signals.check_if_facts_conflict(facts)),
        )
        yield from messages
        if found_errors:
            return

        ctx = Context(engine, Signals.get_db_related_coordinates())

        found_errors, messages, _ = filter_results(
            (
                msg
                for fact in facts
                for msg in (Signals.check_fact_against_db(ctx, fact))
            )
        )
        yield from messages
        if found_errors:
            return

        yield facts, ctx

    @classmethod
    def _insert(cls, facts, ctx):
        found_errors, messages, result = filter_results(
            Signals.retrieve_existing_facts(ctx, facts)
        )
        yield from messages
        if found_errors:
            return

        new_facts, modified_facts = result
        if modified_facts:
            yield (
                f"detected {len(modified_facts)} invalid modifications of facts"
                " already in db"
            )

        duplicates = len(facts) - len(new_facts) - len(modified_facts)
        if duplicates:
            yield f"{duplicates} facts are already in db"

        if new_facts:
            yield from Signals.commit_facts(ctx, new_facts)

    @classmethod
    def dispatch(cls, root_folder, rel_path, config, engine):
        try:
            if rel_path.suffix != ".csv":
                yield f"skip {rel_path}"
                yield False
                return
            facts, ctx = None, None
            for result in cls.check(root_folder, rel_path, config, engine):
                if isinstance(result, (str, Exception)):
                    yield result
                    continue
                facts, ctx = result
            if facts is None:
                yield [rel_path]
                return
            yield from cls._insert(facts, ctx)
            yield [rel_path]

        except Exception as e:
            yield e
            yield False


class DirectSignals(Dispatcher, RawDataFile):
    depends_on = []
    script_depends_on = []

    entity_name = "direct_signals"

    def examples(config):
        return []

    direct_data_root = pathlib.Path("data/*/*/direct_data")

    location = direct_data_root / "*.csv"

    @classmethod
    def related_files(cls):
        yield cls.location

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def cleanup(cls, root_folder, rel_path, config, engine, backup_folder):
        folder = rel_path.parent
        for p in sorted((root_folder / folder).glob("*.csv")):
            p.unlink()
            yield f"removed {p.relative_to(root_folder)}"

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        yield from backup_folder.glob(str(cls.direct_data_root / "*"))

    @classmethod
    def data_files(cls, lz):
        # those which trigger check
        yield from lz.glob(str(cls.location))

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        pass

    @classmethod
    def supported_fields_for_delete(cls):
        return set()
