#!/usr/bin/env python
import atexit
import json
import shutil
from contextlib import contextmanager
from pathlib import Path

from .data_conversion import SUPPORTED_SCRIPT_EXTENSIONS
from .utils import hexdigest_file, list_folder_recursive
from .yaml import find_entity

START_STATE_FILE = ".start_state"
LOCK_FILE = ".lock"


def lock_file_path(root_folder):
    return root_folder / LOCK_FILE


def initial_content_folder(root_folder):
    return root_folder / ".initial_content"


def rename_json_path(root_folder):
    return root_folder / ".rename.json"


@contextmanager
def write_lock(folder):
    path = lock_file_path(folder)
    got_lock = False
    if not path.exists():
        try:
            path.write_text("")
            got_lock = True
        except IOError:
            got_lock = False
    atexit.register(path.unlink)
    try:
        yield got_lock
    finally:
        atexit.unregister(path.unlink)
        if got_lock:
            path.unlink()


class InvalidLandingZone(IOError):
    pass


def start_state_file_path(landing_zone):
    return landing_zone / START_STATE_FILE


def init_start_state(landing_zone):
    path = start_state_file_path(landing_zone)
    path.open("w").close()
    disable_write(path)


def list_new_or_changed_files(landing_zone):
    result = []
    rel_path_to_hexdigest = load_start_state_file(landing_zone)
    for rel_path in list_folder_recursive(landing_zone, also_folders=False):
        if rel_path not in rel_path_to_hexdigest.keys():
            result.append(Path(rel_path))
            continue
        if rel_path_to_hexdigest[rel_path] != hexdigest_file(
            Path(landing_zone) / rel_path
        ):
            result.append(Path(rel_path))
    return result


def check_if_landing_zone_is_valid(landing_zone):
    if not start_state_file_path(landing_zone).exists():
        return f"landing zone at {landing_zone} does not contain .start_state file"

    try:
        rel_paths = load_start_state_file(landing_zone)
    except Exception as e:
        return str(e)

    missing = []
    for rel_path in rel_paths:
        path = Path(landing_zone) / rel_path
        if not path.exists():
            if path.stem == "conversion":
                if any(
                    (Path(path.parent) / (path.stem + ext)).exists()
                    for ext in SUPPORTED_SCRIPT_EXTENSIONS
                ):
                    continue
            missing.append(str(path))
    if not missing:
        return None
    n = len(missing)
    if len(missing) > 5:
        mm = ", ".join(missing[:5])
        msg = "files {}, ... ({} in total) are missing from " "landing zone".format(
            mm, n
        )
    else:
        mm = ", ".join(missing)
        msg = "files {} are missing from landing zone".format(mm)
    return msg


def list_new_files(landing_zone):
    result = []
    rel_path_to_hexdigest = load_start_state_file(landing_zone)
    for rel_path in list_folder_recursive(landing_zone, also_folders=False):
        if rel_path not in rel_path_to_hexdigest.keys():
            result.append(rel_path)
    return result


def load_start_state_file(landing_zone):
    path = start_state_file_path(landing_zone)
    try:
        lines = path.open().readlines()
    except IOError as e:
        raise InvalidLandingZone(f"reading from {path} failed. reason: {e}")
    result = {}
    for line in lines:
        fields = line.rstrip().rsplit(" ", 1)
        if len(fields) != 2:
            raise InvalidLandingZone(f"{path} is corrupted") from None
        path, checksum = fields
        result[Path(path)] = checksum
    return result


def mark_start_state_as_out_of_sync(development_landing_zone):
    path = start_state_file_path(development_landing_zone)
    shutil.move(path, f"{path}.out_of_sync")


def mark_start_state_as_in_sync(development_landing_zone):
    path = start_state_file_path(development_landing_zone)
    shutil.move(f"{path}.out_of_sync", path)


def check_if_save_state_is_still_up_to_date(
    operational_landing_zone,
    development_landing_zone,
    renamed_files=None,
):
    saved_state = load_start_state_file(development_landing_zone)
    files_in_operational_landing_zone = set()
    for p in list_folder_recursive(operational_landing_zone, also_folders=False):
        e = find_entity(p)
        if e is not None and not e.copy_into_development_landing_zone():
            continue
        files_in_operational_landing_zone.add(p)
    if renamed_files:
        for p in renamed_files:
            p = Path(p)
            if p in files_in_operational_landing_zone:
                files_in_operational_landing_zone.remove(p)
    mismatch = set(files_in_operational_landing_zone) - set(saved_state)
    return mismatch


def update_start_state(operational_landing_zone, development_landing_zone, ignore=None):
    if ignore is None:
        ignore = []

    ignore = set(ignore)

    write_start_state_file(
        development_landing_zone,
        (
            p
            for p in list_folder_recursive(operational_landing_zone, also_folders=False)
            if p not in ignore
        ),
    )
    return


def write_start_state_file(landing_zone, paths):
    path = start_state_file_path(landing_zone)
    if path.exists():
        enable_write(path)
    with path.open("w") as fh:
        for p in paths:
            checksum = hexdigest_file(landing_zone / p)
            print(p, checksum, file=fh)
    disable_write(path)


def create_empty(landing_zone):
    landing_zone.mkdir(parents=True, exist_ok=True)
    init_start_state(landing_zone)


def disable_write(path):
    path.chmod(0o400)  # -r--------


def enable_write(path):
    path.chmod(0o600)  # -rw-------


def create_from(development_landing_zone, operational_landing_zone):
    already_exists = []
    for p in list_folder_recursive(operational_landing_zone, also_folders=False):
        e = find_entity(p)
        if e is not None and not e.copy_into_development_landing_zone():
            continue
        target = development_landing_zone / p
        target.parent.mkdir(parents=True, exist_ok=True)
        yield f"copy {p}"
        try:
            shutil.copyfile(operational_landing_zone / p, target)
        except IOError as e:
            yield e
            return
        already_exists.append(p)

    write_start_state_file(development_landing_zone, already_exists)
    yield "write .start_state"


def persist_initial_content(development_landing_zone):
    assert False, "must be removed"
    target = initial_content_folder(development_landing_zone)
    try:
        shutil.copytree(development_landing_zone, target)
    except IOError as e:
        yield e
        return


def read_rename_json(folder):
    path = rename_json_path(folder)
    if path.exists():
        return json.load(path.open())
    return None


def remove_rename_json(folder):
    path = rename_json_path(folder)
    try:
        path.unlink()
    except IOError as e:
        raise IOError(f"could not remove {path}: {e}")


def write_rename_json(
    folder, new_name, old_name, new_yaml, renames, item_id, affected_file_patterns
):
    to_persist = dict(
        new_name=new_name,
        old_name=old_name,
        yaml_path=str(new_yaml),
        renames={str(from_): str(to) for (from_, to) in renames.items()},
        item_id=item_id,
        affected_file_patterns=list(map(str, affected_file_patterns)),
    )

    path = rename_json_path(folder)
    try:
        with path.open("w") as fh:
            json.dump(to_persist, fh, indent=4)
    except IOError as e:
        raise IOError(f"could not write {path}: {e}")

    try:
        disable_write(path)
    except IOError as e:
        raise IOError(f"could not change file permissions of {path}: {e}")
