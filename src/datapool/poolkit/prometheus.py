#!/usr/bin/env python

from prometheus_client import Counter, Gauge

facts_processed = Gauge("dp_facts_counter", "processed facts", ["fact_table"])

files_processed_counter = Counter("dp_files_processed_counter", "processed files")

dispatched_raw_file_counter = Counter(
    "dp_dispatched_raw_file_counter", "dispatched raw files", ["fact_table"]
)

fs_event_delay_time = Gauge(
    "dp_fs_event_delay_time", "delay file modification to fs event"
)

dispatch_delay_time = Gauge("dp_dispatch_delay_time", "dispatch delay time im ms")

raw_file_conversion_time = Gauge(
    "dp_raw_file_conversion_time", "raw file conversion time im ms", ["fact_table"]
)

conversion_to_db_time = Gauge("dp_conversion_to_db_time", "fact to db time im ms")


time_from_file_creation_to_end_of_processing = Gauge(
    "dp_filestamp_to_finished_processing", "time from file timestamp to end of dispatch"
)


def metric_names():
    return [
        m._name
        for m in (
            facts_processed,
            dispatched_raw_file_counter,
            fs_event_delay_time,
            dispatch_delay_time,
            raw_file_conversion_time,
            conversion_to_db_time,
            time_from_file_creation_to_end_of_processing,
        )
    ]
