#!/usr/bin/env python

import shutil
from collections import defaultdict
from fnmatch import fnmatch
from functools import lru_cache
from pathlib import Path

from .topo_sort import topo_sort
from .type_handling import has_parser, parsers
from .utils import Asset


class SubClassRegistry(type):
    """we implement our own registry for subclasses because we
    can not delete subclasses from __subclasses__ attribute
    which makes implementing fixtures for testing difficult"""

    def __new__(cls, clsname, bases, dct):
        new_class = type.__new__(cls, clsname, bases, dct)
        for base in bases[-1:]:
            if not hasattr(base, "_subclasses"):
                base._subclasses = defaultdict(list)
            if new_class not in base._subclasses[base]:
                base._subclasses[base].append(new_class)
        return new_class


class Entity(metaclass=SubClassRegistry):
    location = None
    examples = []

    entity_name = None
    depends_on = []

    @classmethod
    def related_files(cls, rel_path):
        return []

    @classmethod
    def copy_into_development_landing_zone(cls):
        return True

    @classmethod
    def match(cls, rel_path):
        if cls.location is None:
            return False
        if isinstance(cls.location, (list, tuple)):
            return any(fnmatch_strict(rel_path, loc) for loc in cls.location)
        return fnmatch_strict(rel_path, cls.location)

    @classmethod
    def list_subclasses(cls):
        seeds = cls._subclasses[cls][:]
        result = []
        while seeds:
            node = seeds.pop()
            if not node._subclasses[node]:
                result.append(node)
            else:
                seeds.extend(node._subclasses[node])
        # ensure same order for every call:
        return sorted(result, key=str)

    @classmethod
    def list_subclasses_in_correct_order(cls):
        nodes = cls.list_subclasses()
        name_to_node = {node.entity_name: node for node in nodes}
        dependencies = defaultdict(list)
        for node in nodes:
            for req in node.depends_on:
                required_node = name_to_node.get(req)
                assert (
                    required_node is not None
                ), f"{node} specifieds {req} which is not defined"
                dependencies[node].append(required_node)
        sorted_ = topo_sort(nodes, dependencies)
        return sorted_

    @classmethod
    def backup_files(cls, root_folder, rel_path):
        return [rel_path]

    @classmethod
    def check_before_delete(cls, config, engine, item_id):
        yield True


def fnmatch_strict(path, pattern):
    parts_path = Path(path).parts
    parts_pattern = Path(pattern).parts
    if len(parts_path) != len(parts_pattern):
        return False
    for path_i, pattern_i in zip(parts_path, parts_pattern):
        if not fnmatch(path_i, pattern_i):
            return False
    return True


class RawDataFile(Entity):
    @classmethod
    def setup_conversion(cls, landing_zone, rel_path):
        return [], [], None

    @classmethod
    def check_fields(cls, row):
        return
        yield

    @classmethod
    def copy_into_development_landing_zone(cls):
        return False

    @classmethod
    def get_db_related_coordinates(cls):
        return {}

    @classmethod
    def prepare_facts(cls, facts, script_path, raw_data_files, conversion_spec_path):
        return
        yield

    @classmethod
    def check_if_facts_conflict(cls, signals):
        return
        yield

    @classmethod
    def check_fact_against_db(cls, ctx, fact):
        return
        yield

    @classmethod
    def retrieve_existing_facts(cls, ctx, facts):
        return
        yield

    @classmethod
    def commit_facts(cls, ctx, fact):
        return
        yield

    @classmethod
    def count_facts(cls, engine, conditions):
        return
        yield

    @classmethod
    def summary(cls, engine, max_rows, conditions):
        return
        yield

    @classmethod
    def data_files_from_backup(cls, backup_lz):
        raise NotImplementedError()

    @classmethod
    def data_files(cls, lz):
        raise NotImplementedError()

    @classmethod
    def match_conversion_script(cls, rel_path):
        raise NotImplementedError()


class Yaml(Entity):
    dbos = []
    mapping = {}
    is_fact_dimension = True

    @classmethod
    def prepare(cls, root_folder, rel_path, yaml):
        yield yaml

    @classmethod
    def check_yaml(cls, root_folder, rel_path, data, config, *, is_dispatcher=False):
        for msg in _check_yaml(data, cls.mapping(config)):
            yield ValueError(msg)

    @classmethod
    def check_conflicts(cls, yamls):
        raise NotImplementedError()

    @classmethod
    def check_against_db(cls, config, engine, data):
        raise NotImplementedError()

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        raise NotImplementedError()

    @classmethod
    def create_new(cls, config, root_folder, ask):
        raise NotImplementedError()

    @classmethod
    def create_new_fields(cls, config):
        raise NotImplementedError()

    @classmethod
    def table_names(cls):
        names = set()
        for yaml_decl in cls.list_subclasses():
            for dbo in yaml_decl.dbos:
                names.add(dbo._schema.name)
        return names

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        raise NotImplementedError()

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        raise NotImplementedError()

    @classmethod
    def rename_in_db(cls, engine, rename_data):
        raise NotImplementedError()

    @classmethod
    def prepare_rename_in_db(cls, engine, rename_data):
        raise NotImplementedError()

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def get_identifier_field(cls):
        return "name"


def has_child_which_is_fact_dimension(spec):
    for entity in spec.list_subclasses():
        if has_child_which_is_fact_dimension(entity):
            return True
    return spec.is_fact_dimension


def sort_entities(root_folder, rel_paths):
    entity_classes = Entity.list_subclasses_in_correct_order()
    sorted_paths = []

    # ensure same results every time, else regression tests might break:
    rel_paths = sorted(rel_paths)
    for entity_class in entity_classes:
        for rel_path in rel_paths:
            if entity_class.match(rel_path):
                sorted_paths.append(rel_path)
    return sorted_paths


def sort_for_update_operational(landing_zone, rel_paths):
    sorted_paths = []

    patterns = [
        pattern
        for cls in Entity.list_subclasses_in_correct_order()
        for pattern in cls.related_files()
    ]

    seen_patterns = set()

    # ensure same results every time, else regression tests might break:
    rel_paths = sorted(rel_paths)
    for pattern in patterns:
        if pattern in seen_patterns:
            continue
        seen_patterns.add(pattern)
        for rel_path in sorted(rel_paths):
            if fnmatch_strict(rel_path, pattern):
                sorted_paths.append(rel_path)
                continue
    assert len(sorted_paths) == len(rel_paths), "looks like we had multiple matches"
    return sorted_paths


def collect_yaml_names():
    entity_names = []
    for yaml_decl in Yaml.list_subclasses_in_correct_order():
        if yaml_decl.entity_name is not None:
            entity_names.append(yaml_decl.entity_name)
    return entity_names


def flatten(what):
    # https://stackoverflow.com/questions/12472338
    if what == []:
        return what
    if isinstance(what[0], list):
        return flatten(what[0]) + flatten(what[1:])
    return what[:1] + flatten(what[1:])


class List:
    def __init__(self, specification):
        self.specification = specification


def default_for(specification):
    if isinstance(specification, List):
        return []
    elif isinstance(specification, dict):
        return {}
    return None


class Annotation:
    pass


class Optional(Annotation):
    def __init__(self, entry):
        self.entry = entry


class NotEmpty(Annotation):
    def __init__(self, entry):
        self.entry = entry


class Apply(Annotation):
    def __init__(self, function, entry):
        self.function = function
        self.entry = entry


def collect_examples(config):
    result = []
    for yaml_decl in Entity.list_subclasses_in_correct_order():
        for rel_path, content in yaml_decl.examples(config):
            if isinstance(content, Asset):
                content = content.load()
            result.append((rel_path, content))
    return result


@lru_cache()
def collect_locations():
    result = []
    for yaml_decl in Entity.list_subclasses_in_correct_order():
        result.append((yaml_decl, yaml_decl.location))
    return result


def find_entity(rel_path):
    return _find(Entity, rel_path)


def find_raw_data_decl(rel_path):
    return _find(RawDataFile, rel_path)


def find_yaml(rel_path):
    return _find(Yaml, rel_path)


def is_conversion_script(rel_path):
    return any(
        entity.match_conversion_script(rel_path)
        for entity in RawDataFile.list_subclasses()
    )


def _find(cls, rel_path):
    matches = []
    for entity in cls.list_subclasses():
        if entity.match(rel_path):
            matches.append(entity)
    if len(matches) == 1:
        return matches[0]
    if len(matches) == 0:
        return None
    raise RuntimeError(f"found multiple declarations {matches} matching {rel_path}")


def find_entity_by_name(entity_name):
    for spec in Entity.list_subclasses_in_correct_order():
        if spec.entity_name == entity_name:
            return spec


def yaml_entity_names():
    return sorted(set(spec.entity_name for spec in Yaml.list_subclasses()))


def raw_data_entity_names():
    return sorted(set(spec.entity_name for spec in RawDataFile.list_subclasses()))


def to_dbos(rel_path, data):
    yaml_spec = find_entity(rel_path)
    assert yaml_spec is not None
    return yaml_spec.to_dbos(data)


def separate_allowed_files(landing_zone, rel_paths):
    patterns = [
        pattern for cls in Entity.list_subclasses() for pattern in cls.related_files()
    ]

    allowed, not_allowed = [], []
    for rel_path in rel_paths:
        for pattern in patterns:
            if fnmatch_strict(rel_path, pattern):
                allowed.append(rel_path)
                break
        else:
            not_allowed.append(rel_path)
    return allowed, not_allowed


def _check_yaml(data, specification):
    if isinstance(specification, dict):
        if not isinstance(data, dict):
            yield f"specification expected dict but got {data}"
            return
        yield from _check_mapping(data, specification)
    elif isinstance(specification, List):
        if not isinstance(data, list):
            yield f"specification expected list but got {data}"
            return
        yield from _check_list(data, specification)


def _check_mapping(data, specification):
    for name, field in specification.items():
        if isinstance(field, Optional):
            field = field.entry
            if name not in data or not data[name]:
                continue
            else:
                value = data[name]
        else:
            if not isinstance(data, dict):
                yield "expected mapping but got list"
                continue
            if name not in data:
                yield f"entry for '{name}' is missing"
                continue
            value = data[name]

        if isinstance(field, NotEmpty):
            if not value:
                yield f"entry for '{name}' is empty"
                continue
            field = field.entry

        if isinstance(field, List):
            if not isinstance(value, list):
                yield f"entry for '{name}' is not a list"
            else:
                yield from _check_list(value, field)
        elif isinstance(field, dict):
            if not isinstance(value, dict):
                yield f"entry for '{name}' is not a mapping"
            else:
                yield from _check_mapping(value, field)
        else:
            yield from _check_value(name, value, field)

    unknown = set(data.keys()) - set(specification.keys())
    if unknown:
        yield f"entries {', '.join(sorted(unknown))} are not allowed."


def _check_list(data, specification):
    if not isinstance(data, list):
        yield f"expected list, got {type(data)}"
        return

    for entry in data:
        yield from _check_yaml(entry, specification.specification)


def _check_value(name, value, field):
    if field in (int, float, str):
        parser = field
    else:
        type_ = field.type.__class__
        if not has_parser(type_):
            yield f"can not handle column type {type_}"
            return

        parser = parsers[type_]
    try:
        parser(value)
    except ValueError as e:
        yield f"{name}: {e}"


def list_all_data_files(lz):
    return sorted(
        p for spec in RawDataFile.list_subclasses() for p in spec.data_files(lz)
    )


def list_all_scripts(lz):
    rel_paths = [path.relative_to(lz) for path in lz.glob("**/*")]
    return sorted(rel_path for rel_path in rel_paths if is_conversion_script(rel_path))


def coordinates(spec):
    result = set()
    nodes = Entity.list_subclasses()
    name_to_node = {node.entity_name: node for node in nodes}

    def collect_dependencies(spec):
        for dependency in spec.depends_on:
            if dependency not in result:
                result.add(dependency)
                collect_dependencies(name_to_node[dependency])

    collect_dependencies(spec)
    return result


def collect_potentially_affected_files(entity_name):
    patterns = []
    for spec in RawDataFile.list_subclasses():
        if entity_name in coordinates(spec):
            if isinstance(spec.location, (list, tuple)):
                patterns.extend(spec.location)
            else:
                patterns.append(spec.location)
        if entity_name in spec.script_depends_on:
            patterns.extend(spec.conversion_script_locations)

    return patterns


def copy_latest_raw_files(operational_lz, backup_lz, target_folder):
    files = [
        file
        for spec in RawDataFile.list_subclasses()
        for file in spec.data_files_from_backup(backup_lz)
    ]

    for p in operational_lz.glob("**"):
        (target_folder / p.relative_to(operational_lz)).mkdir(
            parents=True, exist_ok=True
        )

    grouped = defaultdict(list)
    for p in files:
        folder = p.parent
        # file names are like data-xxx.raw.20201216-154019-147417:
        ts = p.suffixes[-1][1:]  # remove leading dot
        grouped[folder].append((ts, p))

    for _, files in sorted(grouped.items()):
        # fetch 2 newest files:
        for _, latest_p in sorted(files)[-2:]:
            target_p = target_folder / latest_p.relative_to(backup_lz)
            target_p_without_ts = target_p.parent / target_p.stem
            if not target_p.parent.exists():
                yield (
                    f"could not restore file {target_p_without_ts}, maybe because of"
                    " previous rename"
                )
                continue
            try:
                shutil.copy(latest_p, target_p_without_ts)
                yield f"restored {target_p_without_ts.relative_to(target_folder)}"
            except IOError as e:
                yield IOError(f"copy {latest_p} to {target_p_without_ts} failed: {e}")
