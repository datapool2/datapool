# encoding: utf-8
from __future__ import absolute_import, division, print_function

import os
import sys
import time
import traceback
import types
from collections import defaultdict
from datetime import datetime

from sqlalchemy import JSON as _JSON
from sqlalchemy import Column, MetaData, Table, cast, create_engine, exc, inspect
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql.base import PGDialect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import SingletonThreadPool
from sqlalchemy.sql import and_, select
from sqlalchemy.sql.schema import (
    ForeignKeyConstraint,
    PrimaryKeyConstraint,
    UniqueConstraint,
)

from .errors import InvalidOperationError
from .type_handling import json_parser, parsers
from .utils import FrozenDict, get_logger, hexdigest, print_table, terminal_size


class Dbo:
    def __init__(self, schema, data):
        self.__dict__["_schema"] = schema
        self.__dict__["_data"] = data

    def __getattr__(self, name):
        return self._data[name]

    def __setattr__(self, name, value):
        self._data[name] = value

    __getitem__ = __getattr__

    def keys(self):
        return self._data.keys()

    def get(self, value):
        return self._data.get(value)

    def __eq__(self, other):
        if not isinstance(other, Dbo):
            return False
        return self._data == other._data

    def __str__(self):
        return str(self._data)

    def __repr__(self):
        return repr(self._data)


class ExistingDbo(Dbo):
    pass


class ModifiedDbo(Dbo):
    pass


class NewDbo(Dbo):
    pass


class Base:
    _metadata = MetaData()
    _setup_procedures = []

    def __init_subclass__(cls):
        schema = cls._schema
        if schema is not None:
            for column in schema.columns:
                setattr(cls, column.name, column)

    @classmethod
    def register_db_setup_procedure(cls, function):
        Base._setup_procedures.append(function)


def collect_tables():
    return Base._metadata.tables.values()


def collect_table_names():
    return Base._metadata.tables.keys()


def patch_engine(engine):
    def session(self):
        path = self.url.database
        try:
            os.system(f"sqlite3 {path}")
        except Exception as e:
            print(e)

    engine.session = types.MethodType(session, engine)
    return engine


def connect_to_db(db_config, *, verbose=False, attempts=5, delay=1):
    if db_config.connection_string.startswith("sqlite"):
        from sqlite3 import dbapi2 as sqlite

        engine = create_engine(
            db_config.connection_string,
            module=sqlite,
            echo=verbose,
            poolclass=SingletonThreadPool,
        )

    else:
        # pool_pre_ping: check if connection is valid befor submitting a
        # command, if not: remove old connections from pool and setup fresh one.
        try:
            engine = create_engine(
                db_config.connection_string, echo=verbose, pool_pre_ping=True
            )
        except Exception as e:
            raise IOError(
                "could not connect to database using the connection"
                f"string {db_config.connection_string}. Is the database"
                " reachable? Or is the connection string malformed, e.g."
                " you forgot to specify a port?"
                f" Internal error which raised this issue is {str(e).strip()}."
            ) from None
    stored_exception = None
    for attempt in range(attempts):
        try:
            engine.connect()
        except exc.OperationalError as e:
            stored_exception = e
            get_logger().info(
                "could not connect, sleep for {} sencds and try again".format(delay)
            )
            time.sleep(delay)
            continue
        else:
            break

    else:
        raise InvalidOperationError(
            "could not connect to {}. Error is {}".format(
                db_config.connection_string, stored_exception
            )
        ) from None

    get_logger().info("connected to db %s", db_config.connection_string)
    return patch_engine(engine)


_open_sessions = {}


_existing_handler = sys.excepthook


def print_open_sessions():
    if not _open_sessions:
        return
    print(" DUMP OPEN SESSIONS ".center(80, "-"), file=sys.stderr)
    print(file=sys.stderr)

    for session, (tb, when) in _open_sessions.items():
        print(session, "was created", when, ">", file=sys.stderr)
        for line in tb:
            print("   ", line.rstrip(), file=sys.stderr)
        print("-" * 80, file=sys.stderr)
        print(file=sys.stderr)


_hook_is_installed = False


def install_excepthook():
    def new_excepthook(*a, **kw):
        print_open_sessions()
        _existing_handler(*a, **kw)

    global _hook_is_installed
    if not _hook_is_installed:
        sys.excepthook = new_excepthook
        _hook_is_installed = True


def _close(self):
    del _open_sessions[self]
    # no super() available here, so:
    self.__class__.close(self)


def create_session(engine):
    Session = sessionmaker(bind=engine)
    session = Session()
    # attach method to existing object:
    session.close = types.MethodType(_close, session)
    # extract caller, remove this function from the traceback:
    _open_sessions[session] = (traceback.format_stack()[:-1], datetime.now())

    # we yield dbos which usually get invalid (expire) after commit, then attributes can
    # not be accessed any more and eg pretty_printing in tests fails. we disable this:
    session.expire_on_commit = False
    return session


def setup_db(config, *, verbose=False):
    """creates tables in database"""
    if check_if_tables_exist(config.db):
        raise InvalidOperationError(
            "use setup_fresh_db, the tables in {!r} already exist".format(
                config.db.connection_string
            )
        )
    engine = connect_to_db(config.db, verbose=verbose)
    Base._metadata.create_all(engine)
    for setup_procedure in Base._setup_procedures:
        setup_procedure(engine, config)
    get_logger().info("created all tables of db {}".format(config.db.connection_string))
    return engine


def check_if_tables_exist(db_config, *, verbose=False):
    engine = connect_to_db(db_config, verbose=verbose)
    declared_names = collect_table_names()
    if not declared_names:
        raise RuntimeError("no tables declared, did you setup a datamodel?")
    existing_names = inspect(engine).get_table_names()
    engine.dispose()

    return set(declared_names) <= set(existing_names)


def setup_fresh_db(config, *, verbose=False):
    """creates tables in database, deletes already existing data if present"""
    if not check_if_tables_exist(config.db):
        raise InvalidOperationError(
            "use setup_db, the tables in {!r} do not exist".format(
                config.db.connection_string
            )
        )
    engine = connect_to_db(config.db, verbose=verbose)
    Base._metadata.drop_all(engine)
    setup_db(config, verbose=verbose)


def get_table_names():
    """fetches all table names for Dbo classes declared here"""
    return sorted(Base._metadata.tables.keys())


def copy_db(
    db_config_source,
    db_config_destination,
    *,
    delete_existing,
    copy_only=None,
    verbose=False,
):
    get_logger().info(
        "copy tables {} -> {}".format(
            db_config_source.connection_string, db_config_destination.connection_string
        )
    )
    engine_source = connect_to_db(db_config_source, verbose=verbose)
    engine_destination = connect_to_db(db_config_destination, verbose=verbose)

    table_names_source = collect_table_names()
    existing_names_destination = inspect(engine_destination).get_table_names()

    if set(table_names_source) & set(existing_names_destination):
        if not delete_existing:
            raise InvalidOperationError(
                "can not copy {} -> {}, some tables already exist on "
                "target db".format(
                    db_config_source.connection_string,
                    db_config_destination.connection_string,
                )
            )
    if delete_existing or not set(table_names_source) & set(existing_names_destination):
        Base._metadata.drop_all(engine_destination)
        Base._metadata.create_all(engine_destination)

    for table_name in sorted(table_names_source):
        if copy_only is not None and table_name not in copy_only:
            continue
        yield table_name
        _copy_table(
            engine_source, engine_destination, table_name, delete_existing, verbose
        )

    engine_source.dispose()
    engine_destination.dispose()


def _copy_table(
    engine_source, engine_destination, table_name, delete_existing, verbose
):
    session = sessionmaker(bind=engine_source)()
    destination = sessionmaker(bind=engine_destination)()

    session_meta = MetaData(bind=engine_source)

    get_logger().info("copy schema of table {}".format(table_name))

    table = Table(table_name, session_meta, autoload=True)

    Base = declarative_base()

    class NewRecord(Base):
        __table__ = table

    columns = table.columns.keys()
    get_logger().info("copy rows of table {}".format(table_name))
    for record in session.query(table).all():
        data = dict([(str(column), getattr(record, column)) for column in columns])
        destination.merge(NewRecord(**data))

    get_logger().info("commit changes for table {}".format(table_name))
    destination.commit()


def _dump_table(
    engine,
    session,
    session_meta,
    table_name,
    indent="",
    print_=print,
    file=sys.stdout,
    max_rows=None,
    ignore_cols=None,
    use_pager=False,
    limit_rows=None,
):
    table = Table(table_name, session_meta, autoload=True)
    columns = table.columns.keys()
    query = session.query(table)
    if limit_rows is not None:
        query = query.limit(limit_rows)
    _, t_height = terminal_size()
    rows = []
    for record in query:
        row = []
        for column in columns:
            if ignore_cols is not None and column in ignore_cols:
                continue
            data = getattr(record, column)
            if isinstance(data, bytes):
                data = hexdigest(data)
            if isinstance(data, str) and len(data) > 1000:
                digest = hexdigest(data.encode("utf-8"))
                data = f"<long string len={len(data)} digest={digest}>"
            row.append(data)
        rows.append(row)
        if use_pager and len(rows) >= t_height - 3:
            rows.sort()
            print_table(columns, rows, indent=indent, print_=print_, max_rows=max_rows)
            input("press enter")
            rows.clear()

    rows.sort()
    print_table(columns, rows, indent=indent, print_=print_, max_rows=max_rows)


def dump_db(
    db_config,
    table_names=None,
    print_=print,
    max_rows=None,
    ignore_cols=None,
    use_pager=False,
    limit_rows=None,
):
    engine = connect_to_db(db_config)
    session = sessionmaker(bind=engine)()
    session_meta = MetaData(bind=engine)
    if table_names is None:
        table_names = inspect(engine).get_table_names()

    for table_name in table_names:
        print_("table {}:".format(table_name))
        print_("")
        _dump_table(
            engine,
            session,
            session_meta,
            table_name,
            indent="   ",
            print_=print_,
            max_rows=max_rows,
            ignore_cols=ignore_cols,
            use_pager=use_pager,
            limit_rows=limit_rows,
        )
        print_("")

    engine.dispose()


def fetch_one(engine, decl, **constraints):
    result = fetch_many(engine, decl, **constraints)
    if not result:
        return None
    if len(result) > 1:
        raise ValueError(
            f"got {len(result)} results for fetching {decl} with constraints"
            f" {constraints}"
        )
    return result[0]


class JSON(_JSON):
    """JSON for sqlite2 etc, but JSONB for postgres"""

    def dialect_impl(self, dialect):
        if isinstance(dialect, PGDialect):
            return JSONB()
        result = super().dialect_impl(dialect)
        return result


parsers[JSON] = json_parser


def fetch_latest(engine, dbo):
    schema = dbo._schema
    clauses = []
    for name, value in dbo._data.items():
        col = getattr(schema.c, name)
        if isinstance(col.type, JSON) and isinstance(engine.dialect, PGDialect):
            clauses.append(cast(col, JSONB()) == cast(value, JSONB()))
        else:
            clauses.append(col == value)

    where = and_(*clauses)
    index_field = [column for column in dbo._schema.columns if column.primary_key][0]
    keys = schema.c.keys()

    stmt = (
        select([getattr(schema.c, column) for column in keys])
        .where(where)
        .order_by(index_field.desc())
        .limit(1)
    )
    results = engine.execute(stmt).fetchall()
    return ExistingDbo(schema, dict(zip(keys, results[0])))


def fetch_many(engine, decl, **constraints):
    schema = decl._schema
    if constraints:
        where = and_(
            *[getattr(schema.c, name) == value for name, value in constraints.items()]
        )
    else:
        where = True
    keys = schema.c.keys()
    results = engine.execute(
        select([getattr(schema.c, column) for column in keys]).where(where)
    ).fetchall()

    return [ExistingDbo(schema, dict(zip(keys, row))) for row in results]


def create_dbo(decl, data):
    return NewDbo(decl._schema, data)


def check_modifications(decl, dbo, data, allowed_to_change, ignore):
    keys = (set(dbo.keys()) | set(data.keys())) - set(ignore)
    for key in keys:
        if key in allowed_to_change:
            continue
        value_dbo = dbo.get(key)
        value_json = data.get(key)
        if value_dbo != value_json:
            yield ValueError(
                f"forbidden modification of {key} from {value_dbo!r} to {value_json!r}"
            )


def find_duplicates(data, keys):
    grouped = defaultdict(list)
    for entry in data:
        key = tuple(entry[key] for key in keys)
        grouped[key].append(entry)

    duplicates = [
        entry for entries in grouped.values() for entry in entries if len(entries) > 1
    ]

    uniques = [
        entry for entries in grouped.values() for entry in entries if len(entries) == 1
    ]

    return duplicates, uniques


def find_differences(decl, data, dbos, keys, allowed_modifications):
    data_mapping = {tuple([entry[key] for key in keys]): entry for entry in data}
    dbo_mapping = {tuple([dbo[key] for key in keys]): dbo for dbo in dbos}

    common = set(data_mapping.keys()) & set(dbo_mapping.keys())
    new = set(data_mapping.keys()) - set(dbo_mapping.keys())
    deleted = set(dbo_mapping.keys()) - set(data_mapping.keys())

    # deleted_dbos = {dbo_mapping[k] for k in deleted}
    # deleted_dbos_in_order = [dbo for dbo in dbos if dbo in deleted_dbos]
    deleted_dbos = [dbo_mapping[k] for k in deleted]

    # new_dbo_data_mapping = {NewDbo(decl._schema, data_mapping[k]) for k in new}

    # we keep new dbos in incoming order such that yaml file entries and db
    # entries are in sync:
    new_entries = {FrozenDict(data_mapping[k]) for k in new}
    new_dbos_in_order = [
        NewDbo(decl._schema, entry)
        for entry in data
        if FrozenDict(entry) in new_entries
    ]

    same = []
    allowed = []
    not_allowed = []

    for k in common:
        data = data_mapping[k]
        dbo = dbo_mapping[k]

        if all(data[field] == dbo[field] for field in data.keys()):
            same.append(ModifiedDbo(decl._schema, data))
        elif all(
            data[field] == dbo[field]
            for field in data.keys()
            if field not in allowed_modifications
        ):
            updated_data = {**dbo._data.copy(), **data}
            allowed.append(ModifiedDbo(decl._schema, updated_data))
        else:
            not_allowed.append(ModifiedDbo(decl._schema, data))

    return (
        new_dbos_in_order,
        allowed,
        not_allowed,
        deleted_dbos,
        same,
    )


def commit_dbos(engine, dbos):
    for dbo in dbos:
        commit_dbo(engine, dbo)


def commit_dbo(engine, dbo):
    if isinstance(dbo, ExistingDbo):
        return dbo
    if isinstance(dbo, ModifiedDbo):
        primary_keys = [c.name for c in dbo._schema.primary_key.columns]
        where = and_(
            *[getattr(dbo._schema.c, name) == dbo._data[name] for name in primary_keys]
        )
        data = dbo._data.copy()
        for key in primary_keys:
            del data[key]
        engine.execute(dbo._schema.update().where(where).values(**data))
        return dbo
    elif isinstance(dbo, NewDbo):
        engine.execute(dbo._schema.insert().values(**dbo._data))
        return fetch_latest(engine, dbo)
    else:
        raise NotImplementedError()


def check_constraints(engine, dbos, mapping):
    messages = []
    for dbo in dbos:
        if isinstance(dbo, NewDbo):
            _check_constraints(engine, dbo, messages, mapping)
    return messages


def _check_constraints(engine, dbo, messages, mapping):
    _check_unique_columns(engine, dbo, messages, mapping)
    _check_unique_constraints(engine, dbo, messages, mapping)


def _check_unique_columns(engine, dbo, messages, mapping):
    unique_cols = [c for c in dbo._schema.columns if c.unique]
    for column in unique_cols:
        col_name = column.name
        value = getattr(dbo, col_name)
        existing = fetch_one(engine, dbo, **{col_name: value})
        if existing and existing != dbo:
            entry = resolve(col_name, mapping)
            messages.append(f"entry with {entry}={value} exists already")


def _check_unique_constraints(engine, dbo, messages, mapping):
    for constraint in dbo._schema.constraints:
        if isinstance(constraint, (PrimaryKeyConstraint, ForeignKeyConstraint)):
            continue
        assert isinstance(constraint, UniqueConstraint)
        column_names = [column.name for column in constraint.columns]
        to_check = {name: dbo._data[name] for name in column_names}
        existing = fetch_one(engine, dbo, **to_check)
        if existing and existing != dbo:
            conflicting = {
                resolve(name, mapping): value
                for name, value in to_check.items()
                if not name.endswith("_id")
            }

            conflicts = ", ".join(
                f"{k}={v!r}" for (k, v) in sorted(conflicting.items())
            )
            messages.append(f"entry {conflicts} already exists")
    return


def resolve(col_name, mapping):
    result = _resolve(col_name, mapping, "")
    assert result is not None, f"configuration issue, can not resolve {col_name}"
    return result


def _resolve(col_name, mapping, path):
    from .yaml import List, NotEmpty, Optional

    for key, value in mapping.items():
        if isinstance(value, (Optional, NotEmpty)):
            value = value.entry
        if isinstance(value, Column):
            if col_name == value.name:
                return path + key

        if isinstance(value, List):
            result = _resolve(col_name, value.specification, key + "/")
            if result is not None:
                return result
