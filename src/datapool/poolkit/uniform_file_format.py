# encoding: utf-8

import csv


def read_from_file(path):
    if not path.exists():
        raise ValueError(f"{path} does not exist")

    with open(path, "r", encoding="ascii") as fh:
        return list(_read_from_fh(fh))


def _read_from_fh(fh):
    reader = csv.reader(fh, delimiter=";")
    lines = list(reader)
    if not lines:
        raise ValueError("file has no header line")
    header = [f.strip() for f in lines[0]]
    for i, row in enumerate(lines[1:]):
        if len(row) < len(header):
            # i + 1, we skipped header
            raise ValueError("row {} ({!r}) is incomplete".format(i + 1, row))
        if len(row) > len(header):
            # i + 1, we skipped header
            raise ValueError("row {} ({!r}) is to long".format(i + 1, row))

        row = (cell.strip() for cell in row)
        row_dict = dict(zip(header, row))
        yield row_dict
