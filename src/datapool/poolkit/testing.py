#!/usr/bin/env python

import os
import pathlib
import re
import shutil
import socket
import sys
import time
from contextlib import closing
from functools import partial
from io import StringIO

import pytest
import pytest_regtest

from datapool.poolkit.commands.add import _add
from datapool.poolkit.commands.check import _check
from datapool.poolkit.commands.create_example import create_example
from datapool.poolkit.commands.run_server import ServerState, run_server_async
from datapool.poolkit.commands.update_operational import _update_operational
from datapool.poolkit.config import MagicConfig
from datapool.poolkit.config_handling import config_for_develop_db
from datapool.poolkit.database import setup_db
from datapool.poolkit.yaml_parser import parse_file, print_yaml


@pytest.fixture(autouse=True)
def patch_etc(tmp_path):
    etc = tmp_path  # / "etc"
    os.environ["ETC"] = str(etc)
    return etc


class Print:
    def __init__(self, *streams):
        self.streams = streams

    def __call__(self, *a, **kwargs):
        # fix some click kwargs:
        for kw in ("fg", "bg"):
            if kw in kwargs:
                del kwargs[kw]
        if "nl" in kwargs:
            del kwargs["nl"]
            kwargs["end"] = ""
        for stream in self.streams:
            kwargs["file"] = stream
            print(*a, **kwargs)
            stream.flush()


@pytest.fixture
def print_(regtest):
    return Print(regtest)


@pytest_regtest.register_converter_post
def fix_regtest_output(txt):
    fixed = []
    for line in txt.split("\n"):
        line = re.sub(r"port [0-9]{4,5}", "port <PORT>", line)
        line = re.sub(r"[0-9]+(\.[0-9]+)? msec", "<TIME> msec", line)
        line = re.sub(r"SIGUSR1 [0-9]+", "SIGUSR1 <PID>", line)
        line = re.sub(r"output_[0-9]+\.csv", "output_<TIME_ID>.csv", line)
        line = re.sub(
            r"required time: \d+:\d{2}:\d{2}\.\d{6}", "required time: <TIME>", line
        )
        # fix backup file extension:
        if "backup" in line:
            line = re.sub(r"\.[0-9]{8}-[0-9]{6}-[0-9]{6}", ".<TIMESTAMP>", line)
        # fix isue with pytest shortening temp pathes for long function names,
        # replace trailing numbers by 0:
        line = re.sub(
            r"<pytest_tempdir>/(test_[a-zA-Z_]*)[0-9]*/",
            r"<pytest_tempdir>/\g<1>0/",
            line,
        )
        fixed.append(line)
    return "\n".join(fixed)


@pytest.fixture
def free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        port = s.getsockname()[1]
    yield port


class _RunServer:
    def __init__(self, tmp_path, print_, db_config=None):
        self.print_ = print_

        self.scheduled = []
        self.config = config = MagicConfig()
        config.python.executable = sys.executable
        config.conversion.block_size = 100

        self.root_folder = config.landing_zone.folder = tmp_path / "lz"
        config.backup_landing_zone.folder = tmp_path / "backup_lz"

        config.landing_zone.folder.mkdir()
        config.backup_landing_zone.folder.mkdir()
        config.http_server.log_file = tmp_path / "datapool_server.log"

        config.site.fields = "street,postcode,city,country"

        if db_config is None:
            self.config.db = config_for_develop_db(config)[0].db
        else:
            self.config.db = db_config
        setup_db(self.config)

    def __call__(
        self, max_time_seconds, until_state, max_dispatch, debug=False, print_=None
    ):
        print_ = print_ or self.print_
        self.server_loop = server_loop = run_server_async(
            self.config, False, print_, print_, True
        )

        num_dispatch = 0
        stopped = False
        last_enter_wait = time.time()
        last_state = ServerState.WAITING
        for state in server_loop:
            if debug:
                print_(f"{state} {num_dispatch} {state == until_state} {stopped}")
            if state == ServerState.WAITING:
                if last_state != ServerState.WAITING:
                    last_enter_wait = time.time()
                    if self.scheduled:
                        function = self.scheduled.pop(0)
                        if debug:
                            print_(f"RUN {function}")
                        # events of same file within small timespan might be suppressed
                        time.sleep(0.1)
                        function(self.root_folder, print_)
                else:
                    if time.time() > last_enter_wait + max_time_seconds:
                        if debug:
                            print_("TIMEOUT")
                        break

            if state == until_state and num_dispatch >= max_dispatch:
                if debug:
                    print_(f"DISPATCHED {max_dispatch} FILES")
                    print_(f"REACHED STATE {state}")
                break

            if state == ServerState.DISPATCHING:
                num_dispatch += 1

            last_state = state

        try:
            server_loop.send(True)
            for state in server_loop:
                if debug:
                    print_((state, num_dispatch, state == until_state, stopped))
        except StopIteration:
            pass

        return state

    def shutdown(self):
        self.server_loop.send(True)

    def schedule(self, function, *args):
        self.scheduled.append(partial(function, *args))


@pytest.fixture(scope="function")
def run_server(tmp_path, print_):
    yield _RunServer(tmp_path, print_)


@pytest.fixture
def fake_ask():
    def _fake_ask(**settings):
        def ask(field, *a, **kw):
            return settings[field]

        return ask

    yield _fake_ask


@pytest.fixture
def check(regtest, tmp_path):
    class CheckFunction:
        def __init__(self):
            self.record_stream = StringIO()
            self.print_ = Print(regtest, self.record_stream)
            self.config = config = MagicConfig()
            config.site.fields = "street,postcode,city,country"
            config.python.executable = sys.executable
            config.conversion.block_size = 100
            self.call_count = 0

        def __call__(self, folder, operational_lz=None):
            # clear stream, see https://stackoverflow.com/questions/4330812:
            self.record_stream.truncate(0)
            self.record_stream.seek(0)
            config = self.config
            if operational_lz is not None:
                config.landing_zone.folder = pathlib.Path(operational_lz)
            else:
                config.landing_zone.folder = tmp_path / f"lz_{self.call_count}"
                config.landing_zone.folder.mkdir()
                self.call_count += 1
                config.db = config_for_develop_db(config)[0].db
                setup_db(config)

            return _check(
                self.config,
                folder,
                result_folder=None,
                verbose=False,
                print_ok=self.print_,
                print_err=self.print_,
            )

        def assert_recorded_output_contains(self, what):
            output = self.record_stream.getvalue()
            if what not in output:
                raise AssertionError(f"did not find {what!r} in\n{output}")

    yield CheckFunction()


def create_example_landing_zone_fixture(config_factory):
    @pytest.fixture
    def example_landing_zone_config(tmp_path, print_):
        config = config_factory(tmp_path)
        yield config

    @pytest.fixture
    def example_landing_zone(example_landing_zone_config, tmp_path, print_):
        temp_landing_zone = tmp_path / "development_landing_zone"
        create_example(
            temp_landing_zone,
            reset=False,
            print_ok=print_,
            print_err=print_,
            config=example_landing_zone_config,
        )
        yield temp_landing_zone

    return example_landing_zone, example_landing_zone_config


@pytest.fixture
def overwrite(print_):
    def overwrite_function(path, **settings):
        data = parse_file(pathlib.Path(path))
        data.update(settings)
        with path.open("w") as fh:
            print_yaml(data, fh=fh)

        print_(f"\n{path!s}")
        print_(f"\n{path.read_text()}")

    yield overwrite_function


@pytest.fixture
def write():
    def write_function(path, **settings):
        with path.open("w") as fh:
            print_yaml(settings, fh=fh)

    yield write_function


@pytest.fixture
def add(print_, fake_ask):
    def add_function(config, folder, what, print__=print_, **data):
        return _add(
            config,
            folder,
            what,
            data,
            None,
            print_ok=print__,
            print_err=print__,
            raise_exceptions=True,
        )

    yield add_function


@pytest.fixture(autouse=True, scope="session")
def proto_operational_lz(tmp_path_factory, datamodel):
    development_landing_zone = tmp_path_factory.mktemp("development_landing_zone")
    stream = StringIO()
    print_ = Print(stream)

    lz = tmp_path_factory.mktemp("landing_zone")
    run_server = _RunServer(lz, print_)

    create_example(
        development_landing_zone,
        reset=False,
        print_ok=print_,
        print_err=print_,
        config=run_server.config,
    )

    def update(root_folder, _):
        _update_operational(
            run_server.config,
            development_landing_zone,
            False,
            True,
            False,
            print_,
            print_,
        )

    run_server.schedule(update)

    exit_code = run_server(
        max_time_seconds=5,
        until_state=ServerState.WAITING,
        max_dispatch=1,
        print_=print_,
    )

    if exit_code != 0:
        assert stream.getvalue() is None, stream.getvalue()

    run_server.output = stream.getvalue()

    return run_server


@pytest.fixture(scope="function")
def operational_lz(proto_operational_lz, print_, tmp_path):
    def setup(silent=True):
        if not silent:
            print_(proto_operational_lz.output)
        config = proto_operational_lz.config.copy()
        lz = tmp_path / "setup_for_testing"
        shutil.copytree(config.landing_zone.folder, lz)
        config.landing_zone.folder = lz
        config.db = config_for_develop_db(config)[0].db
        return config

    yield setup
