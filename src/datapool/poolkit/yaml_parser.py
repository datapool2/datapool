#!/usr/bin/env python

import sys


class YamlError(ValueError):
    pass


def quote(line):
    chars = []
    quoted = False
    escaped = False
    line = list(line)
    for char in line:
        if not escaped and char == "\\":
            escaped = True
            continue
        if not escaped and char == '"':
            quoted = not quoted
        if (quoted or escaped) and char in r":-":
            char = rf"\{char}"
        chars.append(char)
        escaped = False
    return "".join(chars)


def unescape(line):
    chars = []
    escaped = False
    for char in line:
        if char == "\\":
            if not escaped:
                escaped = True
                continue
        chars.append(char)
        escaped = False
    return "".join(chars)


def is_list_entry(line):
    return line.strip().startswith("-")


def is_mapping_entry(line):
    parts = line.split(r"\:")
    return any(":" in part for part in parts)


class Reader:
    def __init__(self, yaml_doc):
        self._prepare(yaml_doc)

    def _prepare(self, yaml_doc):
        lines = merge_quoted(yaml_doc).splitlines()
        lines = [line.partition("#")[0] for line in lines]
        lines = [line.rstrip() for line in lines if line.strip()]

        self.current_indent = 0
        self.current_line = None

        lines_fixed = []
        for line in lines:
            line = quote(line)
            if (
                lines_fixed and not is_mapping_entry(line) and not is_list_entry(line)
            ):  # line.lstrip().startswith("-"):
                lines_fixed[-1] += " " + line.lstrip()
            else:
                lines_fixed.append(line)

        self.lines = []
        for line in lines_fixed:
            if is_list_entry(
                line
            ):  # line_stripped.startswith("-") and not line_stripped.endswith("-"):
                indent, _, rest = line.partition("-")
                self.lines.append(indent + "-")
                if rest:
                    rest_indent = len(rest) - len(rest.lstrip(" "))
                    self.lines.append(indent + " " * rest_indent + rest)
            else:
                self.lines.append(line)

        self._current_index = -1
        self.advance()
        if self.current_indent > 0:
            raise YamlError("first line is indented")
        if self.finished:
            raise YamlError("yaml is empty")

    def advance(self):
        if self._current_index == len(self.lines) - 1:
            self.finished = True
            return

        self._current_index += 1
        next_line = self.lines[self._current_index]
        indent = len(next_line) - len(next_line.lstrip(" "))

        self.current_indent = indent
        self.current_line = next_line.lstrip()
        self.finished = False


def merge_quoted(text):
    p2 = -1
    while True:
        p1 = text.find('"', p2 + 1)
        if p1 == -1:
            break
        p2 = text.find('"', p1 + 1)
        if p2 == -1:
            raise YamlError('missing closing " in text')
        text = text[:p1] + text[p1:p2].replace("\n", "") + text[p2:]
    return text


def parse(reader):
    if is_mapping_entry(reader.current_line):
        return _parse_dict(reader)
    elif is_list_entry(reader.current_line):
        return _parse_list(reader)
    else:
        return _parse_value(reader)


def _parse_dict(reader):
    result = {}
    current_indent = reader.current_indent
    while True:
        key, _, value = reader.current_line.partition(":")
        reader.advance()

        key = unescape(key.strip())
        value = unescape(value.strip(" ").strip('"'))
        if not value:
            if reader.current_indent <= current_indent:
                value = ""
            else:
                if reader.finished:
                    raise YamlError("premature end of yaml file")
                value = parse(reader)

        result[key] = value

        if reader.finished or reader.current_indent < current_indent:
            return result

        if reader.current_indent != current_indent:
            raise YamlError(f"indention issue at line {reader._current_index}")


def _parse_value(reader):
    value = reader.current_line.strip()
    reader.advance()
    return unescape(value)


def _parse_list(reader):
    result = []
    current_indent = reader.current_indent
    while True:
        reader.advance()
        value = parse(reader)
        result.append(value)
        if reader.finished or reader.current_indent < current_indent:
            return result
        if reader.current_indent != current_indent:
            raise YamlError(f"indention issue at line {reader._current_index}")


def parse_file(path):
    data = path.read_text()
    return parse_doc(data)


def parse_doc(txt):
    return parse(Reader(txt))


def print_yaml(data, indent="", fh=sys.stdout):
    if isinstance(data, list):
        for entry in data:
            print(indent + "-", file=fh)
            print_yaml(entry, indent + "    ", fh)
    elif isinstance(data, dict):
        for key, value in data.items():
            if isinstance(value, (dict, list)):
                print(indent + f"{key}:", file=fh)
                print_yaml(value, indent + "    ", fh)
            else:
                if isinstance(value, str):
                    value = format_long_text(value, indent + "  ")
                print(indent + f"{key}: {value}".rstrip(), file=fh)
    elif isinstance(data, str):
        print(indent + format_long_text(data, indent), file=fh)
    elif isinstance(data, (float, int, bool)):
        print(indent + str(data), file=fh)


def wrap(txt):
    if not txt:
        return ""
    segments = [txt[0]]
    for i in range(1, len(txt)):
        c = txt[i]
        last_c = segments[-1][-1]
        if (c == " " and last_c != " ") or (c != " " and last_c == " "):
            segments.append(c)
        else:
            segments[-1] += c

    result = [""]
    for segment in segments:
        if len(result[-1]) > 100:
            result.append("")
        result[-1] += segment
    return result


def format_long_text(txt, indent, width=100):
    lines = wrap(txt)
    delim = '"' if (":" in txt or "-" in txt or len(lines) > 1) else ""
    return delim + "\n".join(lines) + delim
