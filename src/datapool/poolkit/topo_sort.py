#!/usr/bin/env python


def topo_sort(nodes, dependencies):
    used = set()
    result = []
    for start_node in nodes:
        if start_node not in used:
            result.extend(_topo_sort(start_node, dependencies, used, start_node))
    return result


def _topo_sort(node, dependencies, used, start_node):
    result = []
    for dependency in dependencies.get(node, []):
        if dependency not in used:
            if dependency == start_node:
                raise ValueError("cycle detected")
            result.extend(_topo_sort(dependency, dependencies, used, start_node))
    result.append(node)
    used.add(node)
    return result
