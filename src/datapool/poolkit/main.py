#!/usr/bin/env python
import inspect
import os
import pathlib
import sys
from dataclasses import dataclass
from functools import partial, wraps

import click

from .click_utils import OrderedGroup

print_ok = partial(click.secho)
print_err = partial(click.secho, fg="red")

start_pdb = False


def is_root():
    return os.getuid() == 0


@dataclass
class AppConfig:
    name: str
    command_name: str
    version: str


app_config = AppConfig("poolkit", "poolkit", "0.0.0")


def set_app_config(_app_config):
    global app_config
    app_config = _app_config


def setup_main():
    def _main(pdb):
        if pdb:
            global start_pdb
            start_pdb = True

    # needed to make f-strings work for doc strings:
    return create_cli_command(
        [
            click.group(cls=OrderedGroup),
            click.option(
                "--pdb",
                is_flag=True,
                help="start python debugger in case of exceptions.",
            ),
        ],
        _main,
        f"""
        the `{app_config.command_name}` command provides sub commands to run imports
        from eawag data warehouse landing zones and supports test runs during
        development and integration of data sources
        """,
    )


def setup_admin(main):
    @main.group(cls=OrderedGroup)
    def admin():
        """commands for datatpool administration"""

    return admin


def print_banner():
    def _print_banner(function):
        @wraps(function)
        def inner(*a, **kw):
            self_ = inspect.stack()[2].frame.f_locals.get("self")
            is_cli_call = self_ is not None
            if is_cli_call:
                command_name = self_.name
            else:
                command_name = function.__name__.lstrip("_")
            click.secho(
                f"> this is {app_config.name} version {app_config.version}", fg="green"
            )
            click.secho("> {}".format(command_name), fg="blue")
            print()
            return function(*a, **kw)

        return inner

    return _print_banner


def setup_version(main):
    # trick to make f-strings work for doc strings:
    def _version():
        print("{} {}".format(app_config.name, app_config.version))

    return create_cli_command(
        [main.command("version")],
        _version,
        f"prints current version of {app_config.name} software",
    )


def inject_pdb(function):
    @wraps(function)
    def inner(*a, **kw):
        try:
            return function(*a, **kw)
        except SystemExit:
            raise
        except Exception as e:
            import pdb
            import traceback

            global start_pdb
            traceback.print_exc()
            if start_pdb or os.getenv("PDB"):
                type, value, tb = sys.exc_info()
                pdb.post_mortem(tb)
            else:
                print()
                print("set environment variable PDB to start debugger automatically.")
                raise e

    return inner


def setup_init_config(main):
    def _init_config(
        verbose, landing_zone_folder, use_sqlitedb, force_count, extra_config_settings
    ):
        from .commands.init_config import init_config

        sys.exit(
            init_config(
                pathlib.Path(landing_zone_folder),
                use_sqlitedb,
                force_count > 1,
                extra_config_settings,
                print_ok,
                print_err,
                verbose,
                app_config,
            )
        )

    return create_cli_command(
        [
            main.command("init-config"),
            click.option(
                "--verbose",
                is_flag=True,
                help="dumps lots of output from interaction with db",
            ),
            click.option("--use-sqlitedb", is_flag=True, help="use sqlite db"),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to overwrite existing config files",
            ),
            click.argument("landing-zone-folder", type=str),
            click.argument("extra-config-settings", type=str, nargs=-1),
            print_banner(),
            inject_pdb,
        ],
        _init_config,
        f"""initializes /etc/{app_config.name}/ folder with config files.

         landing_zone_folder must be a non-existing folder on the current machine.
    """,
    )


def setup_check_config(main):
    def _check_config(verbose):
        from .commands.check_config import check_config

        sys.exit(check_config(print_ok, print_err, verbose))

    return create_cli_command(
        [
            main.command("check-config"),
            click.option(
                "--verbose",
                is_flag=True,
                help="dumps lots of output from interaction with db",
            ),
            print_banner(),
            inject_pdb,
        ],
        _check_config,
        f"checks if config file(s) in /etc/{app_config.name} are valid.",
    )


def setup_init_db(main):
    def _init_db(verbose, force_count):
        from .commands.init_db import init_db

        sys.exit(init_db(force_count > 1, verbose, print_ok, print_err))

    return create_cli_command(
        [
            main.command("init-db"),
            click.option(
                "--verbose",
                is_flag=True,
                help="dumps lots of output from interaction with db",
            ),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to overwrite existing db",
            ),
            print_banner(),
            inject_pdb,
        ],
        _init_db,
        """creates empty tables in operational database. Run check_config first to see
        if the configured data base settings are valid.
        """,
    )


def setup_show(main):
    def _show(what, no_pager, max_rows):
        from .commands.show import show

        sys.exit(show(what, no_pager, max_rows, print_ok, print_err))

    return create_cli_command(
        [
            main.command("show"),
            click.argument("what", type=str, nargs=1, required=True),
            click.option(
                "--no-pager", is_flag=True, help="suppress paging of larger output"
            ),
            click.option(
                "--max-rows",
                type=click.INT,
                default=None,
                help="number of rows to fetch from db, default is all",
            ),
            print_banner(),
            inject_pdb,
        ],
        _show,
        """
        shows table from data base
        """,
    )


def setup_create_example(main):
    def _create_example(example_landing_zone_folder, force_count):
        from .commands.create_example import create_example

        reset = force_count > 1
        sys.exit(
            create_example(
                pathlib.Path(example_landing_zone_folder), reset, print_ok, print_err
            )
        )

    return create_cli_command(
        [
            main.command("create-example"),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to overwrite existing folder",
            ),
            click.argument("example-landing-zone-folder", type=str),
            print_banner(),
            inject_pdb,
        ],
        _create_example,
        """setup landing zone in given folder with example files.
        """,
    )


def setup_check(main):
    def _check(development_landing_zone_folder, result_folder, verbose):
        from .commands.check import check

        sys.exit(
            check(
                pathlib.Path(development_landing_zone_folder),
                result_folder,
                verbose,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("check"),
            click.option(
                "--result-folder",
                type=str,
                default=None,
                help="provide target for results",
            ),
            click.option("--verbose", is_flag=True, help="might dump lots of output"),
            click.argument("development-landing-zone-folder", type=str),
            print_banner(),
            inject_pdb,
        ],
        _check,
        """checks scripts and produced results in given landing zone. does not write to
        database.""",
    )


def setup_add(main):
    def _add(info, what, development_landing_zone_folder, settings):
        from .commands.add import add

        sys.exit(
            add(
                info,
                what,
                development_landing_zone_folder,
                settings,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("add"),
            click.option("--info", is_flag=True, help="show more detailed info"),
            click.argument("what", type=str),
            click.argument(
                "development-landing-zone-folder",
                type=str,
                required=False,
            ),
            click.argument("settings", type=str, nargs=-1),
            print_banner(),
            inject_pdb,
        ],
        _add,
        """creates new entries in development landing zone
        you can specify --info to check for available fields:

        $ pool add site --info
        """,
    )


def setup_start_develop(main):
    def _start_develop(development_landing_zone_folder, force_count, verbose):
        from .commands.start_develop import start_develop

        reset = force_count > 1
        sys.exit(
            start_develop(
                pathlib.Path(development_landing_zone_folder),
                reset,
                verbose,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("start-develop"),
            click.option(
                "--verbose",
                is_flag=True,
                help="dumps lots of output from interaction with db",
            ),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to overwrite existing db",
            ),
            click.argument("development-landing-zone-folder", type=str),
            print_banner(),
            inject_pdb,
        ],
        _start_develop,
        """setup local landing zone for adding new site / instrument / conversion
        script.  this command will clone the operational landing zone (might be empty).
        """,
    )


def setup_update_operational(main):
    def _update_operational(
        development_landing_zone_folder, force_count, verbose, copy_raw_files
    ):
        from .commands.update_operational import update_operational

        overwrite = force_count > 1
        sys.exit(
            update_operational(
                pathlib.Path(development_landing_zone_folder),
                verbose,
                overwrite,
                copy_raw_files,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("update-operational"),
            click.option("--verbose", is_flag=True, help="might dump lots of output"),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to overwrite existing landing zone in case of"
                " errors when checking",
            ),
            click.option(
                "--copy-raw-files",
                is_flag=True,
                help="copy raw files also to operational landing zone",
            ),
            click.argument("development-landing-zone-folder", type=str),
            print_banner(),
            inject_pdb,
        ],
        _update_operational,
        """updates operantional landing zone from development landing zone""",
    )


def setup_run_server(main):
    def _run_server(verbose):
        from .commands.run_server import run_server

        sys.exit(
            run_server(
                verbose,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("run-server"),
            click.option("--verbose", is_flag=True, help="might dump lots of output"),
            print_banner(),
            inject_pdb,
        ],
        _run_server,
        """creates new entries in development landing zone""",
    )


def setup_start_rename(main):
    def _start_rename(rename_landing_zone_folder, what, old_name, new_name):
        from .commands.start_rename import start_rename

        if not is_root():
            print_err("- you must run this command as root")
            sys.exit(1)

        sys.exit(
            start_rename(
                pathlib.Path(rename_landing_zone_folder),
                what,
                old_name,
                new_name,
                print_ok,
                print_err,
            )
        )

    return create_cli_command(
        [
            main.command("start-rename"),
            click.argument("rename-landing-zone-folder", type=str, nargs=1),
            click.option(
                "--what",
                type=str,
            ),
            click.argument("old_name", type=str, nargs=1),
            click.argument("new_name", type=str, nargs=1),
            print_banner(),
            inject_pdb,
        ],
        _start_rename,
        """
        This creates a special development landing zone to rename entities and related
        scripts.
        """,
    )


def setup_delete_entity(main):
    def _delete_entity(what, name, force_count, max_rows):
        from .commands.delete_entity import delete_entity

        do_delete = force_count >= 2

        if do_delete and not is_root():
            print_err("- you must run this command as root")
            sys.exit(1)

        sys.exit(delete_entity(do_delete, max_rows, what, name, print_ok, print_err))

    return create_cli_command(
        [
            main.command("delete-entity"),
            click.option(
                "--what",
                type=str,
            ),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to perform deletion, else only checks are"
                " performed and number of affected db entries is estimated",
            ),
            click.option(
                "--max-rows",
                type=click.INT,
                default=10,
                help="number of facts to print before deletion, default is 10",
            ),
            click.argument("name", type=str, nargs=1),
            print_banner(),
            inject_pdb,
        ],
        _delete_entity,
        """
        This command deletes a given entity (like site, source..) and the
        corresponding facts.
        """,
    )


def setup_delete_facts(main):
    def _delete_facts(force_count, fact_table, max_rows, filters):
        from .commands.delete_facts import delete_facts

        do_delete = force_count >= 2

        if do_delete and not is_root():
            print_err("- you must run this command as root")
            sys.exit(1)

        sys.exit(
            delete_facts(do_delete, max_rows, fact_table, filters, print_ok, print_err)
        )

    cli = app_config.command_name

    return create_cli_command(
        [
            main.command("delete-facts"),
            click.option(
                "--force",
                "force_count",
                count=True,
                help="use this twice to perform deletion, else only checks are"
                " performed and number of affected db entries is estimated",
            ),
            click.option(
                "--max-rows",
                type=click.INT,
                default=20,
                help="number of facts to print before deletion, default is 20",
            ),
            click.argument("fact-table", type=str, nargs=1),
            click.argument("filters", type=str, nargs=-1),
            print_banner(),
            inject_pdb,
        ],
        _delete_facts,
        f"""
        This command deletes all facts where all filter conditions are true.

        Example:

            {cli} delete-facts signals "timestamp>=2000-01-01" "site==abc"

        The surrounding quotes are required if symbols '<' or '>' occur in such
        expressions or if the values contain spaces.
        """,
    )


def setup_replay(main):
    def _replay(base_folder, all_files, recurse, time_min, time_max):
        from .commands.replay import replay

        sys.exit(
            replay(
                base_folder,
                not all_files,
                recurse,
                time_min,
                time_max,
                print_ok,
                print_err,
            )
        )

    cli = app_config.command_name + " ".join(sys.argv[1:-1])  # exclude --help

    return create_cli_command(
        [
            main.command("replay"),
            click.option("--recurse", is_flag=True, help="include subfolders"),
            click.option(
                "--all-files",
                is_flag=True,
                help="replay all files matching instead of broken files only",
            ),
            click.option(
                "--from",
                "time_min",
                type=str,
                nargs=1,
                help="optional minimal date/time",
                metavar="mints",
            ),
            click.option(
                "--to", "time_max", type=str, nargs=1, help="optional maximal date/time"
            ),
            click.argument("base-folder", type=str),
            print_banner(),
            inject_pdb,
        ],
        _replay,
        f"""
        This command copies files from the backup landing zone back to the operational
        landing zone for re-ingestion.

        Example:

            {cli} data/source_type_1/source_1/raw_data --from 202301 -to 20230120

        """,
    )


def create_cli_command(decorators, function, doc_string):
    # check convention assumed in print_banner calls from tests:
    assert function.__name__[0] == "_" and function.__name__[1] != "_"

    function.__doc__ = doc_string
    for decorator in decorators[::-1]:
        function = decorator(function)
    return function
