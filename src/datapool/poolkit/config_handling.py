# encoding: utf-8
from __future__ import absolute_import, division, print_function

import os
import pathlib
from contextlib import closing

from .config import MagicConfig, read_ini, write_ini
from .database import connect_to_db
from .errors import InvalidOperationError
from .julia_runner import JuliaRunner
from .matlab_runner import MatlabRunner
from .python_runner import PythonRunner
from .r_runner import RRunner
from .utils import compare_versions, find_executable, iter_to_list


def guess_config(known_config, app_name, app_version):
    """you can provide known config settings as

    known_config = { "db.connection_string:"...",
                     "worker.port": 3333 }

    which will overrung the settings we guess in this function
    """

    assert isinstance(known_config, dict)

    executables = ("matlab", "R", "julia", "python3")
    sections = ("matlab", "r", "julia", "python")

    messages = []

    config = {}
    config[f"{app_name}.__version__"] = app_version

    for executable, section in zip(executables, sections):
        path = find_executable(executable, "")
        if not path:
            messages.append("{!r} not found on $PATH".format(executable))
        config["{}.executable".format(section)] = path

    config.update(known_config)

    return default_config(config, app_name), messages


def default_config(known_config, app_name):
    """you can overwrite with calls like:

    default_config(db.connections_string="....",
                   worker.port=3333)
    """

    default_connection_string = f"postgresql://user:password@127.0.0.1:5432/{app_name}"
    default_pid_path = f"/var/run/{app_name}.pid"
    default_local_user_folder = f".{app_name}"

    etc = os.environ.get("ETC")

    def set_defaults():
        config = MagicConfig()

        config.db.connection_string = default_connection_string

        config.backup_landing_zone.folder = ""

        config.r.extension = ".r"

        config.matlab.extension = ".m"

        config.julia.extension = ".jl"
        config.julia.version = ">=1.0.1"

        config.python.extension = ".py"

        config.http_server.port = 8000
        config.http_server.log_requests = 1
        config.http_server.log_file = "/usr/local/var/log/datapool.log"

        config.conversion.block_size = 50000

        config.site.fields = "street,postcode,city,country"

        if os.getpid() == 0:
            config.server.pid_file = default_pid_path
        else:
            if etc is None:
                folder = pathlib.Path("~").expanduser() / default_local_user_folder
            else:
                folder = pathlib.Path(etc)
            folder.mkdir(parents=True, exist_ok=True)
            config.server.pid_file = str(folder / f"{app_name}.pid")

        return config

    def update_config(config, known_config):
        for key, value in known_config.items():
            fields = key.split(".")
            if len(fields) == 2:
                section, field = fields
                setattr(getattr(config, section), field, value)
            elif len(fields) == 1:
                field = fields[0]
                assert field.startswith("__")
                assert field.endswith("__")
                setattr(config, field, value)

    config = set_defaults()
    update_config(config, known_config)

    return config


def config_for_develop_db(operational_config, landing_zone=None):
    config = MagicConfig()
    path = os.path.join(
        landing_zone
        if landing_zone is not None
        else operational_config.landing_zone.folder,
        ".develop.db",
    )
    config.db.connection_string = "sqlite+pysqlite:///{}".format(path)
    config.site = operational_config.site
    return config, path


def check_folder(folder):
    if not os.path.exists(folder):
        return "folder '{}' does not exist".format(folder)

    try:
        existing_files = os.listdir(folder)
    except IOError:
        return "can not list content of folder '{}'".format(folder)

    existing_files = [f for f in existing_files if not f.startswith(".")]

    if existing_files:
        return "folder is not empty '{}'".format(folder)

    marker_file = os.path.join(folder, ".marker")
    try:
        open(marker_file, "w").close()
    except IOError:
        return "can not write to folder '{}'".format(folder)

    os.remove(marker_file)
    return None


@iter_to_list
def check_config(config, verbose):
    yield from _check_landing_zone(config)
    yield from _check_backup_landing_zone(config)
    yield from _check_db(config, verbose)
    yield from _check_runners(config)
    yield from _check_pid_file(config)
    yield from _check_webserver(config)
    yield from _check_conversion(config)
    yield from _check_site(config)


def _check_landing_zone(config):
    errors = _check(config, "landing_zone", "folder", str, False)
    yield from errors

    if not errors and config.landing_zone.folder:
        yield True, "- check landing zone '{}'".format(config.landing_zone.folder)
        msg = check_folder(config.landing_zone.folder)
        if msg is not None:
            yield False, "- {}".format(msg)


def _check_backup_landing_zone(config):
    errors = _check(config, "backup_landing_zone", "folder", str, True)
    yield from errors

    if not errors and config.backup_landing_zone.folder:
        yield (
            True,
            "- check backup folder '{}'".format(config.backup_landing_zone.folder),
        )
        msg = check_folder(config.backup_landing_zone.folder)
        if msg is not None:
            yield False, "- {}".format(msg)


def _check_db(config, verbose):
    errors = _check(config, "db", "connection_string", str, False)
    yield from errors

    if not errors:
        try:
            yield True, "- try to connect to db"
            connect_to_db(config.db, verbose=verbose)
            yield True, "  - connected to db"
        except Exception as e:
            yield False, "  - {}".format(e)


def _check_pid_file(config):
    pid_file = config.server.pid_file
    if os.path.exists(pid_file):
        if not os.access(pid_file, os.W_OK):
            yield False, "- can not write to pid file at {}".format(pid_file)
    else:
        folder = os.path.dirname(pid_file)
        if not os.path.exists(folder):
            yield (
                False,
                "- folder {} for pid file {} does not exist".format(folder, pid_file),
            )
        else:
            try:
                open(pid_file, "w").close()
                os.unlink(pid_file)
            except IOError:
                yield False, "- can not write to pid file {}".format(pid_file)


def _check_webserver(config):
    errors = _check(config, "http_server", "port", int)
    yield from errors
    errors = _check(config, "http_server", "log_requests", int)
    yield from errors
    if not errors:
        port = config.http_server.port
        if is_port_used(port):
            yield False, f"- configured webserver port {port} is already in use"
        else:
            log_file = config.http_server.log_file
            if log_file:
                try:
                    with open(log_file, "w"):
                        return
                except IOError:
                    yield False, f"- can not write to log file at {log_file}"


def _check_conversion(config):
    errors = _check(config, "conversion", "block_size", int)
    yield from errors
    if not errors:
        block_size = config.conversion.block_size
        if block_size < 1000:
            yield (
                False,
                "- configured convesion block_size {} must be >= 1000".format(
                    block_size
                ),
            )


def _check_site(config):
    fields = [s.strip() for s in config.site.fields.split(",")]
    if len(fields) != len(set(fields)):
        yield False, "- site fields has duplicate entries"


@iter_to_list
def _check(config, section, field, type, maybe_empty=True):
    if section not in config.keys():
        yield False, "- section {} missing".format(section)
    if field not in config[section].keys():
        yield False, "- field {} in section {} missing".format(field, section)

    value = config[section][field]
    if not maybe_empty and value == "":
        yield False, "- field {} in section {} has no value set".format(field, section)
    if not isinstance(value, type):
        yield (
            False,
            "- field {} in section {} is not of type {}".format(
                field, section, type.__name__
            ),
        )


def _check_runners(config):
    yield from _check_executable("r", "R", config, RRunner)
    yield from _check_executable("python", "python", config, PythonRunner)
    yield from _check_executable("matlab", "matlab", config, MatlabRunner)
    julia_ok = True
    for ok, message in _check_executable("julia", "julia", config, JuliaRunner):
        julia_ok = julia_ok and ok
        yield ok, message

    if julia_ok and config.julia.executable:
        m = JuliaRunner(config.julia.executable)
        try:
            m.start_interpreter()
        except OSError as e:
            yield False, "- can not start julia a second time: {}".format(e)
        else:
            actual_version = m.get_julia_version_tuple()
            required_version = config.julia.version

            if not compare_versions(required_version, actual_version):
                yield (
                    False,
                    (
                        "- julia interpreter is of version {}, configured is {}"
                        "".format(actual_version, required_version)
                    ),
                )
            else:
                yield True, "- checked julia version."


def _check_executable(section, name, config, runner):
    errors = _check(config, section, "extension", str, False)
    yield from errors

    errors = _check(config, section, "executable", str)
    yield from errors
    if errors:
        return

    executable = getattr(config, section).executable

    if executable == "":
        yield True, "- {} not configured, skip tests".format(name)
    else:
        yield True, "- check {} configuration + code execution".format(name)
        m = runner(executable)
        try:
            m.start_interpreter()
            yield True, "- {} interpreter works".format(name)
        except OSError as e:
            yield (
                False,
                "  - could not start {} from {}: {}".format(name, executable, e),
            )


def is_port_used(port):
    import socket

    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.settimeout(0.5)
        return sock.connect_ex(("127.0.0.1", port)) == 0


def config_folder(app_name):
    etc = pathlib.Path(os.environ.get("ETC", "/etc"))
    data_pool_folder = etc / app_name
    return data_pool_folder.absolute()


def config_ini_file_path(app_name):
    folder = config_folder(app_name)
    path = folder / f"{app_name}.ini"
    return path


def init_config(
    landing_zone_folder,
    app_config,
    sqlite_db=False,
    overwrite=False,
    extra_config=None,
    pid_file_path=None,
):
    app_name = app_config.name
    app_version = app_config.version

    config_folder_ = config_folder(app_name)
    config_ini_path = config_ini_file_path(app_name)

    if os.path.exists(config_ini_path):
        if not overwrite:
            raise InvalidOperationError(f"config file {config_ini_path} already exists")
    else:
        try:
            os.makedirs(config_folder_, exist_ok=True)
        except PermissionError:
            raise InvalidOperationError(
                "creation of {} failed. try sudo.".format(config_folder_)
            )

    known_settings = dict([("landing_zone.folder", landing_zone_folder)])
    if sqlite_db:
        db_path = landing_zone_folder / ".simple.db"
        known_settings["db.connection_string"] = "sqlite+pysqlite:///{!s}".format(
            db_path
        )

    if pid_file_path is not None:
        known_settings["server.pid_file"] = pid_file_path

    config, messages = guess_config(known_settings, app_name, app_version)

    if extra_config is not None:
        config.update(extra_config)

    try:
        write_ini(config, config_ini_path)
    except PermissionError:
        raise InvalidOperationError(
            "could not write {}. try sudo.".format(config_ini_path)
        )

    return config_ini_path, messages


def read_config(**variable_settings):
    from .main import app_config

    path = config_ini_file_path(app_config.name)
    if os.path.exists(path):
        return migrate(read_ini(path, variable_settings), app_config.version), path
    else:
        return None, path


def write_config(config):
    assert "__file__" in config.keys()
    write_ini(config, config.__file__)


def migrate(config, current_version):
    return config
