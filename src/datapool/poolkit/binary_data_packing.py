#! /usr/bin/env python
# Copyright © 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>

import struct

import numpy as np

"""
format specification:

1. overall format:

    |VV|NN|O1|...|ON|<array1>|...|<arrayN>|

    VV: 16 bit version spec (only version 0 now)
    NN: number of arrays (16 bit)
    Oi: start indices of arrays relative to  "|VV|NN|"

2. per array:

    |D|N|S1|..|SN|C|<data>

    D: dimension
    N: number of bytes in <data>
    (S1, .., SN): shape
    C: numpy type specifier
    <data>: data in C memory ordering


approach:
    - conversion script creates numpy array from whatever source
    - but also returns file name
    - we store both in database!?
"""


def pack(data):
    assert isinstance(data, list)
    for item in data:
        for key in _formats:
            if isinstance(item, key):
                type_ = key
                break
        else:
            continue
        break
    assert all(
        isinstance(item, type_) for item in data
    ), "can only pack items of same type"

    format_id = _formats[type_]
    return _packers[format_id](data)


def unpack(packed):
    (version, __), __ = _unpack_struct("HH", packed)
    assert version in _unpackers, f"unknown verions {version}"
    return _unpackers[version](packed)


def pack_arrays(numpy_arrays, *, version=0):
    segments = [pack_array(a) for a in numpy_arrays]
    lengths = [len(s) for s in segments]

    num_arrays = len(numpy_arrays)
    header = struct.pack("HH", version, num_arrays)

    relative_offsets = np.cumsum([0] + lengths)[:-1]
    header += struct.pack("Q" * len(numpy_arrays), *relative_offsets)
    return header + b"".join(segments)


def pack_images(images):
    arrays = [np.array(image) for image in images]
    return pack_arrays(arrays, version=1)


def unpack_arrays(packed, *, expected_version=0):
    (version, num_arrays), remainder = _unpack_struct("HH", packed)
    assert version == expected_version, "wrong file format version"

    offsets, remainder = _unpack_struct("Q" * num_arrays, remainder)
    arrays = []
    for offset in offsets:
        array = unpack_array(remainder[offset:])
        arrays.append(array)

    return arrays


def unpack_images(packed):
    from PIL import Image

    arrays = unpack_arrays(packed, expected_version=1)
    return [Image.fromarray(a) for a in arrays]


_formats = {np.ndarray: 0}
_packers = {0: pack_arrays}
_unpackers = {0: unpack_arrays}


def pack_array(numpy_array):
    ndim = numpy_array.ndim
    dtype = numpy_array.dtype.char.encode()  # one char encoding as bytes object
    size = numpy_array.data.nbytes  # total number of bytes
    shape = numpy_array.shape
    data = numpy_array.ravel("C").tobytes()

    return struct.pack(f"HcL{ndim}L", ndim, dtype, size, *shape) + data


def unpack_array(packed):
    (ndim, dtype, size), remainder = _unpack_struct("HcL", packed)
    shape, remainder = _unpack_struct(f"{ndim}L", remainder)
    return np.frombuffer(remainder[:size], dtype).reshape(*shape)


def _unpack_struct(fmt, data):
    """extracts data and returns values and remainder of data"""
    values = struct.unpack_from(fmt, data)
    offset = len(struct.pack(fmt, *values))
    return values, data[offset:]


def test():
    from PIL import Image

    a = np.eye((10), dtype=np.uint8)
    a[1, 2] = 7
    b = np.eye((12), dtype=np.float64)
    a[1, 2] = 7.0
    packed = pack([a, b])
    a_back, b_back = unpack(packed)
    assert np.all(a == a_back)
    assert np.all(b == b_back)

    packed = pack([Image.open("guiqwt.png"), Image.open("guiqwt.png")])
    images = unpack(packed)

    data = np.array(Image.open("guiqwt.png"))
    assert np.all(data == np.array(images[0]))


if __name__ == "__main__":
    test()
