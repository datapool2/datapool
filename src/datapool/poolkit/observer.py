# encoding: utf-8
from __future__ import absolute_import, division, print_function

import os
import pathlib
import sys
import time
from threading import Lock, Timer

from watchdog.events import FileCreatedEvent, FileModifiedEvent, FileSystemEventHandler

from .utils import ForgetFulSet

IS_MAC = sys.platform == "darwin"

CREATED_EVENT = "created"
MODIFIED_EVENT = "modified"
ILLEGAL_EVENT = "illegal"


def report_error(function):
    def wrapped(*a, **kw):
        try:
            return function(*a, **kw)
        except Exception as e:
            import traceback

            traceback.print_exc()
            print(e)

    return wrapped


class _EventHandler(FileSystemEventHandler):
    """handles file creation and modification events for .raw and .yaml files"""

    def __init__(self, root_folder, call_back, logger):
        super().__init__()
        self._call_back = call_back
        self.root_folder = root_folder
        self.logger = logger
        self.last_created = {}
        self.update_lock = Lock()
        self.recently_dispatched = ForgetFulSet(timeout=0.1)

    def call_back(self, event, full_path):
        self.recently_dispatched._cleanup()
        full_path = pathlib.Path(full_path)
        rel_path = full_path.relative_to(self.root_folder)
        if event in (CREATED_EVENT, MODIFIED_EVENT):
            if full_path in self.recently_dispatched:
                return
            self.recently_dispatched.add(full_path)
        self._call_back(event, rel_path, time.time())

    @report_error
    def on_created(self, event):
        if not isinstance(event, FileCreatedEvent):
            return
        self.logger.debug("EventHandler: creation detected: {}".format(event.src_path))
        if event.src_path.endswith(".inwrite"):
            return
        # on linux two events are fired for a created file: first a
        # CREATED_EVENT and finally a MODIFIED_EVENT when the data is written to
        # disk.  but sometimes two CREATED_EVENT are fired but no
        # MODIFIED_EVENT. to handle this we suppres change event which happen
        # to early after CREATED_EVENT
        src_path = event.src_path
        if src_path not in self.last_created:
            with self.update_lock:
                self.last_created[src_path] = time.perf_counter()
            # we delay create event slightly, because sometimes the writing
            # of the file is not finished yet:
            Timer(0.01, self.call_back, args=(CREATED_EVENT, event.src_path)).start()
            return
        now = time.perf_counter()
        if now > self.last_created[src_path] + 0.01:
            with self.update_lock:
                del self.last_created[src_path]
            self.call_back(CREATED_EVENT, event.src_path)

    @report_error
    def on_modified(self, event):
        if not isinstance(event, FileModifiedEvent):
            return
        if not os.path.exists(event.src_path):
            # on mac os the file deletion first triggers modificatin event
            return

        self.logger.debug(
            "EventHandler: modification detected: {}".format(event.src_path)
        )
        src_path = event.src_path
        if src_path.endswith(".inwrite"):
            return
        if src_path not in self.last_created:
            self.call_back(MODIFIED_EVENT, src_path)
            return
        now = time.perf_counter()
        if now > self.last_created[src_path] + 0.01:
            with self.update_lock:
                del self.last_created[src_path]
            self.call_back(MODIFIED_EVENT, src_path)

    @report_error
    def on_moved(self, event):
        """usually in-place rename for raw data files"""
        self.call_back(CREATED_EVENT, event.dest_path)

    @report_error
    def on_deleted(self, event):
        """removing a file from the landing zone is disallowed"""
        self.call_back(ILLEGAL_EVENT, event.src_path)


def start_observer(folder, call_back):
    from .utils import get_logger

    get_logger().info("start to observe {}".format(folder))
    from watchdog.observers import Observer

    event_handler = _EventHandler(folder, call_back, get_logger())
    observer = Observer()
    observer.schedule(event_handler, folder, recursive=True)
    observer.start()

    get_logger().info("started observer to observer {}".format(folder))
    return observer


def shutdown_observer(observer):
    from .utils import get_logger

    get_logger().info("try to stop observer")
    observer.stop()
    observer.join()
    get_logger().info("stopped observer")
