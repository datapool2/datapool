# encoding: utf-8
from __future__ import absolute_import, division, print_function

import contextlib
import ctypes
import functools
import glob
import hashlib
import inspect
import io
import logging
import operator
import os
import pathlib
import re
import shutil
import signal
import stat
import string
import sys
import threading
import time
import warnings
from collections import Counter, OrderedDict, UserDict
from collections.abc import Hashable, MutableSet
from datetime import date, datetime

import pkg_resources
from sqlalchemy import func, select

from .errors import DataPoolIOError
from .type_handling import (
    date_parser,
    date_time_parser,
    float_parser,
    int_parser,
    string_parser,
)


def hexdigest(data):
    """computes md5 hex digtes of given data of type bytes"""
    assert isinstance(data, bytes)
    digester = hashlib.md5()
    digester.update(data)
    return digester.hexdigest()


def hexdigest_file(path):
    data = open(path, "rb").read()
    return hexdigest(data)


def is_writeable(path):
    return bool(os.stat(path).st_mode & stat.S_IWUSR)


def check_is_writeable(folder, path=""):
    if not is_writeable(os.path.join(folder, path)):
        raise DataPoolIOError("can not write to {}".format(os.path.join(folder, path)))


def reformat_row(row, limit):
    """breaks cell to consecutive lines if len of cell content is beyond limit"""
    new_rows = []
    while True:
        new_rows.append([cell[:limit] for cell in row])
        row = [cell[limit:] for cell in row]
        if all(len(c) == 0 for c in row):
            break
    return new_rows


def format_table(header, rows, max_col_size=40, indent="", max_rows=None):
    file = io.StringIO()
    print_ = functools.partial(print, file=file)
    print_table(header, rows, max_col_size, indent, print_, max_rows)
    return file.getvalue()


def print_fact_table(
    header, top, skipped, bottom, indent="", print_=print, max_col_size=40
):
    headers = reformat_row(header, max_col_size)

    top = _fix_and_format(top, max_col_size)
    bottom = _fix_and_format(bottom, max_col_size)

    col_sizes = _max_col_sizes(headers + top + bottom)
    formatters = ["%-{}s".format(s) for s in col_sizes]

    _print_headers(headers, formatters, indent, print_, col_sizes)
    p = functools.partial(_print_row, formatters, indent, print_)

    # print content
    for row in top:
        p(row)
    if skipped:
        print_(indent + f"skipped {skipped} rows ...")
    for row in bottom:
        p(row)


def _print_row(formatters, indent, print_, row):
    print_(indent, nl=False)
    for cell, f in zip(row, formatters):
        print_(f % cell, nl=False)
        print_("  ", nl=False)
    print_("")


def _fix_and_format(rows, max_col_size):
    new_rows = []
    for row in rows:
        row = map(_fix_cell, row)
        new_rows.extend(reformat_row(list(map(str, row)), max_col_size))
    return new_rows


def _fix_cell(cell):
    if cell is None:
        cell = "-"
    elif isinstance(cell, datetime):
        cell = cell.strftime("%Y-%m-%d %H:%M:%S")
    return cell


def _max_col_sizes(rows):
    # max col sizes:
    col_sizes = list(map(len, rows[0]))
    for row in rows[1:]:
        col_sizes = list(max(s, len(c)) for (s, c) in zip(col_sizes, map(str, row)))
    return col_sizes


def _print_headers(headers, formatters, indent, print_, col_sizes):
    for header in headers:
        _print_row(formatters, indent, print_, header)
    row = [c * "-" for c in col_sizes]
    _print_row(formatters, indent, print_, row)


def print_table(header, rows, max_col_size=40, indent="", print_=print, max_rows=None):
    """pretty print table for given headers and rows.

    headers  list of strings
    rows:    list of list of values
    """

    if max_rows is not None and len(rows) > max_rows:
        inter_row = "skipped {} rows ...".format(len(rows) - max_rows)
        n = max_rows // 2
        rows = rows[:n] + rows[-(max_rows - n) + 1 :]
    else:
        n = None

    headers = reformat_row(header, max_col_size)
    rows = _fix_and_format(rows, max_col_size)

    col_sizes = _max_col_sizes(headers + rows)

    formatters = ["%-{}s".format(s) for s in col_sizes]

    _print_headers(headers, formatters, indent, print_, col_sizes)

    p = functools.partial(_print_row, formatters, indent, print_)

    if n is None:
        for row in rows:
            p(row)
        return

    for row in rows[:n]:
        p(row)
    p([inter_row])
    for row in rows[n:]:
        p(row)


def print_query(
    engine,
    query,
    columns,
    indent,
    print_,
    max_rows,
    max_col_size=40,
):
    total_rows = engine.execute(select([func.count()], None, query.subquery())).scalar()

    if max_rows is not None and total_rows > max_rows:
        inter_row = "skipped {} rows ...".format(total_rows - max_rows)
        n = max_rows // 2

        rows_top = engine.execute(query.limit(n))
        rows_bottom = engine.execute(query.offset(total_rows - n).limit(n))
        rows = list(rows_top) + list(rows_bottom)

    else:
        n = None
        rows = engine.execute(query)

    headers = reformat_row(columns, max_col_size)
    rows = _fix_and_format(rows, max_col_size)

    col_sizes = _max_col_sizes(headers + rows)

    formatters = ["%-{}s".format(s) for s in col_sizes]

    _print_headers(headers, formatters, indent, print_, col_sizes)

    p = functools.partial(_print_row, formatters, indent, print_)

    if n is None:
        for row in rows:
            p(row)
        return

    for row in rows[:n]:
        p(row)
    p([inter_row])
    for row in rows[n:]:
        p(row)


def find_executable(name, default=None):
    for folder in os.environ["PATH"].split(os.pathsep):
        path = os.path.join(folder, name)
        if os.path.exists(path):
            if os.access(path, os.X_OK):
                return path
    return default


def abs_folder(path):
    return os.path.dirname(os.path.abspath(path))


def here():
    """determines folder containing the caller of this function"""
    # we go one step up in the call stack and read __file__ as set there:
    return abs_folder(inspect.stack()[1].frame.f_globals["__file__"])


class TimeoutError(Exception):
    pass


def run_timed(function, args=None, kwargs=None, timeout_in_seconds=None):
    """run given function with given args / kwargs, stopping execution if the function
    execution needs more than timeout_in_seconds seconds.

    In case timeout_in_seconds is None we wait until the function execution ends.
    """

    args = () if args is None else args
    kwargs = {} if kwargs is None else kwargs

    if timeout_in_seconds is None:
        return function(*args, **kwargs)

    def handle_timeout(*a):
        signal.setitimer(signal.ITIMER_REAL, 0.0)
        raise TimeoutError()

    # we use signalt.setitimer here, signal.alarm only accepts integer values for the
    # time span
    signal.setitimer(signal.ITIMER_REAL, timeout_in_seconds)
    signal.signal(signal.SIGALRM, handle_timeout)
    try:
        return function(*args, **kwargs)
    finally:
        signal.setitimer(signal.ITIMER_REAL, 0.0)


def list_folder_recursive(folder, skip_hidden=True, also_folders=True, check=False):
    folder = folder.resolve()  # remove eventually preceedings dots
    for p in sorted(folder.glob("**/*")):
        if check:
            check_is_writeable(p)

        if p.name.startswith(".") and skip_hidden:
            continue

        if p.is_file() or also_folders:
            yield p.relative_to(folder)


def copy(from_, to):
    """copies file from from_ to to, if target folder does not exist all needed
    intermediate folders are created"""
    assert os.path.exists(from_)
    target_folder = os.path.dirname(to)
    if not os.path.exists(target_folder):
        os.makedirs(target_folder)
    shutil.copyfile(from_, to)


def iter_to_list(function):
    """decorator which transforms a generator function to a classic function returning a
    list with the values created by the generator.
    """

    @functools.wraps(function)
    def inner(*a, **kw):
        return list(function(*a, **kw))

    return inner


def is_int(txt):
    try:
        int(txt)
        return True
    except ValueError:
        return False


def enumerate_filename(*paths):
    next_numbers = []
    parts = []
    for path in paths:
        dirname = path.parent
        ext = path.suffix

        name_stem = re.split(r"_\d+$", path.stem)[0]
        stem = str(dirname / name_stem)
        parts.append((stem, path.suffix))
        files = glob.glob(stem + "*" + ext)
        appendices = [file[len(stem) : -len(ext)].lstrip("_") for file in files]
        numbers = [int(a) for a in appendices if is_int(a)]
        if not numbers:
            next_number = 0
        else:
            next_number = max(numbers) + 1
        next_numbers.append(next_number)
    next_number = max(next_numbers)
    return [
        pathlib.Path(stem + "_{:d}".format(next_number) + ext) for stem, ext in parts
    ]


def is_number(value):
    try:
        float(value)
    except ValueError:
        return False
    return True


def update_and_report_change(dbo, domain_obj, *fieldnames):
    modified = False
    for fieldname in fieldnames:
        existing = getattr(dbo, fieldname)
        new = getattr(domain_obj, fieldname)
        if existing != new:
            modified = True
            setattr(dbo, fieldname, new)
    if modified:
        yield dbo


def write_pid_file(config):
    pid_file = config.server.pid_file
    try:
        os.makedirs(os.path.dirname(pid_file), exist_ok=True)
        with open(pid_file, "w") as fh:
            fh.write(str(os.getpid()))
        # only owner can read and write, group and others can only read:
        os.chmod(pid_file, 0o644)
    except IOError as e:
        raise DataPoolIOError(str(e))


def is_server_running(config):
    return os.path.exists(config.server.pid_file)


def remove_pid_file(config):
    if os.path.exists(config.server.pid_file):
        os.unlink(config.server.pid_file)


@contextlib.contextmanager
def open_and_create_folders(path, mode="r"):
    dirname = os.path.dirname(path)
    try:
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(path, "w") as fh:
            yield fh
    except IOError as e:
        raise DataPoolIOError(str(e))


# https://stackoverflow.com/questions/35446015/
class OrderedCounter(Counter, OrderedDict):
    "Counter that remembers the order elements are first seen"

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, OrderedDict(self))

    def __reduce__(self):
        return self.__class__, (OrderedDict(self),)


def warn_if_generator_is_not_used(function):
    assert inspect.isgeneratorfunction(function), "decorator only works for generators"

    @functools.wraps(function)
    def wrapped(*a, **kw):
        return _iter_checker(function(*a, **kw))

    return wrapped


class _iter_checker:
    def __init__(self, function):
        self.function = function
        self.iterated = False
        stack = inspect.stack()[-1]
        self.lineno = stack.lineno
        self.filename = stack.filename

    def __iter__(self, *a, **kw):
        self.iterated = True
        return iter(self.function)

    def __del__(self):
        if not self.iterated:
            warnings.warn(
                "generator created in {}, line number {} was never used "
                "for iteration".format(self.filename, self.lineno)
            )


def compare_versions(required, actual):
    assert isinstance(required, str)
    assert isinstance(actual, tuple)

    if required.startswith(">="):
        offset = 2
        comp = operator.ge
    elif required.startswith("<="):
        offset = 2
        comp = operator.le
    elif required.startswith(">"):
        offset = 1
        comp = operator.gt
    elif required.startswith("<"):
        offset = 1
        comp = operator.lt
    elif required.startswith("=="):
        offset = 2
        comp = operator.eq
    else:
        offset = 0
        comp = operator.eq

    required_as_tuple = tuple(map(int, required[offset:].split(".")))

    return comp(actual, required_as_tuple)


try:
    libc = ctypes.cdll.LoadLibrary("libc.so.6")
    get_lwp_id = functools.partial(libc.syscall, 186)
except OSError:
    get_lwp_id = lambda: None


class EnhancedThread(threading.Thread):
    def run(self):
        self.lwp_id = get_lwp_id()
        return self.task()


# from http://code.activestate.com/recipes/52215-get-more-information-from-tracebacks/:
def print_stack_trace_plus(frame, print_ok, print_err):
    stack = []
    while frame:
        stack.append(frame)
        frame = frame.f_back
    stack.reverse()
    for frame in stack:
        print_ok("")
        print_err(
            "File {file} line {line} in {func}".format(
                func=frame.f_code.co_name,
                file=frame.f_code.co_filename,
                line=frame.f_lineno,
            )
        )
        for key, value in frame.f_locals.items():
            try:
                output = "\t%20s = %s" % (key, value)
                print_ok(output)
                # We have to be careful not to cause a new error in our error
                # printer! Calling str() on an unknown object could cause an
                # error we don't want.
            except Exception:
                value = "<ERROR WHILE PRINTING VALUE>"
                print_err("\t%20s = %s" % (key, value))


def install_signal_handler_for_debugging(print_ok=print, print_err=print):
    def signal_handler(*a):
        print_err(">>> start dump thread tracebacks")
        print_err("")
        for t in threading.enumerate():
            print_err("=" * 60)
            print_err(
                "Thread {}, lwpid = {}".format(hex(t.ident), getattr(t, "lwp_id", None))
            )
            frame = sys._current_frames().get(t.ident, None)

            print_ok("")
            print_stack_trace_plus(frame, print_ok, print_err)
            print_ok("")
        print_err(">>> done dump thread tracebacks")
        sys.stdout.flush()
        sys.stderr.flush()

    signal.signal(signal.SIGUSR1, signal_handler)


def get_logger():
    return logging.getLogger("poolkit")


class Asset:
    def __init__(self, rel_path):
        self.rel_path = rel_path
        self.caller_module = inspect.stack()[1].filename
        self.package = inspect.stack()[1].frame.f_globals["__package__"]

    def load(self):
        return pkg_resources.resource_string(self.package, self.rel_path)


def ask(input_, print_, predefined, field_name, not_empty=False, type_=str, txt=None):
    parser = {
        int: int_parser,
        float: float_parser,
        str: string_parser,
        datetime: date_time_parser,
        date: date_parser,
    }.get(type_, string_parser)

    value = os.environ.get(field_name)

    if value is None:
        value = predefined.get(field_name)

    if value is not None:
        if not_empty and not value:
            print_(f"- value for {field_name} must not be empty")
            raise KeyboardInterrupt

        value = parser(value)
        return value

    while True:
        if txt is None:
            txt = field_name
        value = input_(f"{txt} [. to abort]? ").strip()
        if value == ".":
            raise KeyboardInterrupt
        if not value:
            if not_empty:
                print_("value required, please try again")
                continue
            return value
        try:
            value = parser(value)
        except ValueError as e:
            print_("invalid input: {}".format(e.args[0]))
            print_("please try again")
            continue
        return value


def free_text_to_path_name(txt):
    txt = txt.replace(" ", "_")
    return "".join(
        filter(
            lambda c: c
            in string.ascii_lowercase + string.ascii_uppercase + string.digits + "_-",
            txt,
        )
    )


class FrozenDict(UserDict):
    def __init__(self, dict_):
        self.data = dict_

    def __setitem__(self, key, value):
        raise TypeError("this is a frozen dict which can not be modified")

    def __hash__(self):
        return hash(tuple(sorted(self.data.items())))


def filter_results(iterator, max_errors=30):
    result = None
    found_errors = False
    messages = []
    error_count = 0
    for item in iterator:
        if isinstance(item, str):
            messages.append(item)
            continue
        elif isinstance(item, Exception):
            if error_count <= max_errors:
                messages.append(item)
            error_count += 1
            found_errors = True
            continue
        result = item
        try:
            next(iterator)
        except StopIteration:
            break
        else:
            raise RuntimeError(
                f"result must be last item yielded from generator {iterator}"
            )
    if error_count > max_errors:
        messages.append(f"... skipped {error_count - max_errors} errors")
    return found_errors, messages, result


def terminal_size():
    return shutil.get_terminal_size((80, 20))


class ForgetFulSet(Hashable, MutableSet):
    def __init__(self, timeout=10):
        self._timeout = timeout
        self.data = dict()

    def add(self, item):
        self._cleanup()
        self.data[item] = time.time()

    def __contains__(self, item):
        self._cleanup()
        return item in self.data

    def _cleanup(self):
        now = time.time()
        self.data = {
            item: ts for (item, ts) in self.data.items() if now <= ts + self._timeout
        }

    __hash__ = MutableSet.__hash__

    def _not_implemented(self):
        raise NotImplementedError

    __len__ = __iter__ = discard = _not_implemented


class StopAfterError:
    pass


def check_stop_indicator(generator):
    @functools.wraps(generator)
    def wrapped(*a, **kw):
        ok = True
        for item in generator(*a, **kw):
            if isinstance(item, Exception):
                ok = False
                yield item
            elif item is not StopAfterError:
                yield item
            else:
                if not ok:
                    break

    return wrapped
