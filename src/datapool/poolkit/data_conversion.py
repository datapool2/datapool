# encoding: utf-8
from __future__ import absolute_import, division, print_function

import os
import pathlib
import time

from .errors import (
    DataPoolIOError,
    FormatError,
    InvalidConfiguration,
    InvalidOperationError,
)
from .julia_runner import JuliaRunner
from .matlab_runner import MatlabRunner
from .python_runner import PythonRunner
from .r_runner import RRunner
from .uniform_file_format import read_from_file
from .yaml_parser import parse_file

RUNNERS = {
    ".m": (MatlabRunner, "matlab"),
    ".jl": (JuliaRunner, "julia"),
    ".py": (PythonRunner, "python"),
    ".r": (RRunner, "r"),
}

SUPPORTED_SCRIPT_EXTENSIONS = RUNNERS.keys()


def remove_first_and_empty_lines(text):
    text = "\n".join(line for line in text.split("\n")[1:] if line.strip())
    return text


class ScriptRunnerFactory:
    def __init__(self, config):
        self.cached_runners = {}
        self.config = config

    def get_runner(self, path_to_script):
        __, ext = os.path.splitext(path_to_script)
        if ext in self.cached_runners:
            return self.cached_runners[ext]

        if ext not in RUNNERS.keys():
            raise InvalidOperationError(
                "do not know how to run script {}.".format(path_to_script)
            )

        runner_class, config_section = RUNNERS[ext]
        try:
            section = getattr(self.config, config_section)
        except KeyError:
            msg = "configuration {} has no section {}".format(
                self.config.__file__, config_section
            )
            raise InvalidConfiguration(msg)

        try:
            exe = section.executable
        except KeyError:
            msg = (
                "configuration {} has no field 'executable' in " "section {}"
            ).format(self.config.__file__, config_section)
            raise InvalidConfiguration(msg)

        if exe.strip() == "":
            raise InvalidConfiguration(
                "executable in section {} is not configured".format(config_section)
            )

        r = runner_class(exe)
        r.start_interpreter()
        self.cached_runners[ext] = r
        return self.cached_runners[ext]


class ConversionRunner:
    def __init__(
        self,
        config,
    ):
        self.script_runner_factory = ScriptRunnerFactory(config)
        self.block_size = config.conversion.block_size

    def _run_conversion(
        self, path_to_script, raw_data_path, conversion_spec_path, tmp_root, verbose
    ):
        if conversion_spec_path is not None:
            conversion_spec = parse_file(conversion_spec_path)
        else:
            conversion_spec = {}

        try:
            raw_data_file_content = raw_data_path.read_bytes()
        except IOError as e:
            yield DataPoolIOError("could not read raw data: {}".format(e))
            return

        runner = self.script_runner_factory.get_runner(path_to_script)

        # we create a tempfolder which encodes the path to the conversion script:
        tmp_folder = pathlib.Path(tmp_root) / (
            str(path_to_script).replace("/", "__").replace(".", "_")
        )
        tmp_folder.mkdir(parents=True, exist_ok=True)

        raw_data_lines = raw_data_file_content.split(b"\n")
        yield "got {} lines from {}".format(len(raw_data_lines), raw_data_path)

        converted = []

        block_size = int(conversion_spec.get("block_size", self.block_size))
        header_lines = int(conversion_spec.get("header_lines", 1))

        # we process raw data block wise to be able to handle huge raw files:
        for block_start in range(
            header_lines, len(raw_data_lines) - header_lines + 1, block_size
        ):
            end = min(len(raw_data_lines) - 1, block_start + block_size - 1)
            yield "convert block from {} to {}".format(block_start, end)
            try:
                for result in self._convert_block(
                    runner,
                    path_to_script,
                    raw_data_lines,
                    block_start,
                    verbose,
                    tmp_folder,
                    header_lines,
                    block_size,
                ):
                    if isinstance(result, Exception):
                        yield result
                    elif isinstance(result, str):
                        converted.append(result)
            except Exception as e:
                yield e
                return

        yield converted

    def _convert_block(
        self,
        runner,
        path_to_script,
        raw_data_lines,
        block_start,
        verbose,
        tmp_folder,
        header_lines,
        block_size,
    ):
        header = raw_data_lines[:header_lines]

        block = raw_data_lines[block_start : block_start + block_size]

        in_file = tmp_folder / f"raw_data_{block_start}.txt"
        out_file = tmp_folder / f"out_data_{block_start}.txt"

        # write raw data to temp file:

        lines = header + block
        try:
            in_file.write_bytes(b"\n".join(lines))
        except IOError as e:
            yield DataPoolIOError(
                "could not write raw data to {}. reason: {}".format(in_file, e)
            )
            return

        # run conversion script
        try:
            exit_code, error_lines = runner.run_script(
                path_to_script, in_file, out_file, verbose=verbose
            )
        except NameError as e:
            yield e
            return
        except Exception as e:
            yield InvalidOperationError("running script failed. reason: {}".format(e))
            return

        if exit_code != 0:
            yield InvalidOperationError(
                f"running script failed. exit_code is {exit_code}"
            )
            for line in error_lines:
                yield InvalidOperationError("   " + line)
            return

        # check if output file exists and can be opened for reading.
        try:
            content = out_file.read_text().strip()
            if block_start > header_lines:
                content = remove_first_and_empty_lines(content)
            yield content
        except IOError as e:
            yield DataPoolIOError(
                "created output file {} is not readable. reason: {}".format(out_file, e)
            )
        except UnicodeDecodeError as e:
            yield FormatError(
                "created output file {} contains non-ascii chraracters: {}".format(
                    out_file, e
                )
            )

    def run_conversion(
        self,
        script_path,
        raw_data_path,
        conversion_spec_path,
        *,
        verbose=False,
        status_callback=None,
        tmp_root=None,
    ):
        data = None
        for result in self._run_conversion(
            script_path, raw_data_path, conversion_spec_path, tmp_root, verbose
        ):
            if isinstance(result, (str, Exception)):
                yield result
            else:
                assert isinstance(result, list)
                data = "\n".join(result)

        if not data:
            yield DataPoolIOError("conversion script did not return data")
            return

        ts = time.time() * 1000
        file_name = f"output_{ts:.0f}.csv"
        output_file = pathlib.Path(tmp_root) / file_name
        output_file.write_text(data)
        try:
            facts = read_from_file(output_file)
        except Exception as e:
            yield Exception("reading output failed: {}".format(e))
            return

        yield output_file, facts
