from datapool.poolkit.config_handling import check_config as _check_config
from datapool.poolkit.config_handling import read_config
from datapool.poolkit.utils import is_server_running


def check_config(print_ok, print_err, verbose=False):
    """checks if current configuration is valid, eg if database access is possible, or
    if matlab can be started.
    """

    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    if is_server_running(config):
        print_err(
            "- server is already running. this command does not work with a"
            " running server."
        )
        return 1

    print_ok("- check settings in config file {}".format(config.__file__))
    error = False
    for ok, message in _check_config(config, verbose):
        if ok:
            print_ok(message)
        else:
            print_err(message)
            error = True

    if error:
        print_err("+ at least one check failed")
    else:
        print_ok("+ all checks passed", fg="green")

    return int(error)
