# encoding: utf-8
from __future__ import absolute_import, division, print_function

import enum
import os
import pathlib
import queue
import signal
import time
from contextlib import nullcontext
from datetime import datetime

from prometheus_client import Gauge

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.database import check_if_tables_exist, setup_db
from datapool.poolkit.dispatcher import Dispatcher
from datapool.poolkit.errors import InvalidOperationError
from datapool.poolkit.http_server import (
    DataPoolHttpServer,
    increment_dispatched_files_counter,
    port_is_in_use,
    reset_dispatched_files_counter,
)
from datapool.poolkit.landing_zone import lock_file_path
from datapool.poolkit.observer import (
    CREATED_EVENT,
    MODIFIED_EVENT,
    shutdown_observer,
    start_observer,
)
from datapool.poolkit.utils import (
    install_signal_handler_for_debugging,
    is_server_running,
    remove_pid_file,
    write_pid_file,
)
from datapool.poolkit.yaml import RawDataFile

queue_size = Gauge("dp_queue_size", "number of files in queue waiting to be processed")

ServerState = enum.Enum(
    "ServerState", ["STARTING", "WAITING", "DISPATCHING", "SHUTDOWN"]
)


def tee(print_, fh):
    # print_ is secho from click, so the arguments are a bit different
    # compared to Pythons print.
    def tee_print(what, **kw):
        print_(what, **kw)
        kw["file"] = fh
        kw["color"] = False
        print_(f"{datetime.now()} : {what}", **kw)

    return tee_print


def run_server(
    verbose,
    print_ok,
    print_err,
    schedule_existing_files=True,
):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    log_file = config.http_server.log_file

    if log_file:
        try:
            fh = open(log_file, "a")
        except IOError as e:
            print_err(f"- can not open log file {log_file} for appending: {e}")
            return 1
        print("\n> run-server\n", file=fh)
        print_ok = tee(print_ok, fh)
        print_err = tee(print_err, fh)
    else:
        fh = nullcontext()

    with fh:
        _run_server(config, print_ok, print_err, verbose, schedule_existing_files)


def _run_server(config, print_ok, print_err, verbose, schedule_existing_files):
    ok = _setup(config, print_ok, print_err, verbose)
    if not ok:
        return 1

    write_pid_file(config)

    install_signal_handler_for_debugging()
    print_ok("- installed signal handler")
    print_ok(
        "  run 'kill -SIGUSR1 {}' to get tracebacks of all running threads".format(
            os.getpid()
        )
    )

    signal_recorder = SignalRecorder()
    for sig in (signal.SIGTERM, signal.SIGHUP, signal.SIGINT):
        signal.signal(sig, signal_recorder.signal_handler)

    http_server = DataPoolHttpServer(
        config.http_server.port,
        config.http_server.log_file,
        config.http_server.log_requests,
        print_ok,
    )
    http_server.start()

    server_loop = run_server_async(
        config, verbose, print_ok, print_err, schedule_existing_files
    )

    try:
        for state in server_loop:
            if signal_recorder.recorded_signum is not None:
                if signal_recorder.recorded_signum == signal.SIGINT:
                    print_err("- got keyboard interrupt")
                else:
                    print_err(
                        "- received signal {}".format(
                            signal_recorder.recorded_signum or "UNKNOWN"
                        )
                    )
                server_loop.send(True)
                signal_recorder.recorded_signum = None
    except Exception:
        import traceback

        traceback.print_exc()
        state = 1

    remove_pid_file(config)
    http_server.stop()

    if state == 0:
        print_ok("+ done", fg="green")
        return 0
    else:
        print_err("+ quit")
        return state if isinstance(state, int) else 1


def _setup(config, print_ok, print_err, verbose):
    if is_server_running(config):
        print_err(
            "- found pid file at {!r}, seems like server is already running.".format(
                config.server.pid_file
            )
        )
        print_err(
            "- this could also be a relict from a crash. so delete it if you"
            " know what you are doing."
        )
        return False

    port = config.http_server.port
    if port_is_in_use(port):
        print_err("- the port {} for the http server is already in use.".format(port))
        return False

    if config.julia.executable:
        print_ok("- check startup julia")
        from datapool.poolkit.julia_runner import JuliaRunner

        r = JuliaRunner(config.julia.executable)
        r.start_interpreter()
        exit_code, error_lines = r.check_if_alive()
        if exit_code != 0:
            print_err("+ julia startup failed")
            for line in error_lines:
                print_err("   " + line)
            return False
    return True


def _setup_db(config, print_ok, print_err, verbose):
    try:
        already_setup = check_if_tables_exist(config.db)
    except InvalidOperationError as e:
        print_err("- can not check database: {}".format(e))
        return False
    if not already_setup:
        print_ok("- setup fresh db")
        try:
            setup_db(config, verbose=verbose)
        except InvalidOperationError as e:
            print_err("- can not setup database: {}".format(e))
            return False
    else:
        print_ok("- db already setup")
    return True


class SignalRecorder:
    def __init__(self):
        self.recorded_signum = None

    def signal_handler(self, signum=None, frame=None):
        if not signum:  # keyboardinterrupt
            return
        self.recorded_signum = signum


def run_server_async(
    config,
    verbose,
    print_ok,
    print_err,
    schedule_existing_files=True,
):
    """setting signal handlers only works when run in main thread,
    in testing we want to run the server in the background thread."""

    yield ServerState.STARTING

    ok = _setup_db(config, print_ok, print_err, verbose)
    if not ok:
        return 1

    reset_dispatched_files_counter()

    q, dispatcher, observer = _wire_dispatcher(
        config, print_ok, print_err, schedule_existing_files
    )

    if dispatcher is None:
        return 1

    print_ok("- main loop ready to dispatch incoming files")

    return_code = 0
    try:
        while True:
            stop = yield ServerState.WAITING
            if stop:
                break

            while True:
                try:
                    rel_path, timestamp = q.get(timeout=0.01)
                    if str(rel_path).startswith("."):
                        if str(rel_path) != ".rename.json":
                            continue

                except queue.Empty:
                    break

                stop = yield ServerState.DISPATCHING
                if stop:
                    break

                queue_size.set(q.qsize())

                print_ok("- dispatch {}".format(rel_path))
                results = dispatcher.dispatch(rel_path, timestamp)
                for result in results:
                    if isinstance(result, str):
                        print_ok("  {}".format(result))
                    else:
                        print_err("  error: {}".format(result))
                print_ok("  dispatch done")
                increment_dispatched_files_counter()
                queue_size.set(q.qsize())

            if stop:
                break

    except Exception:
        print_err("")
        print_err("main loop crashed:")
        import traceback

        print_err(traceback.format_exc())
        return_code = 1

    finally:
        yield ServerState.SHUTDOWN
        for shutdown in (observer.stop, dispatcher.engine.dispose):
            shutdown()

    yield return_code


def _wire_dispatcher(config, print_ok, print_err, schedule_existing_files):
    dispatcher = Dispatcher(config)
    q = queue.Queue()
    root_folder = pathlib.Path(config.landing_zone.folder)

    if schedule_existing_files:
        print_ok("- check pending raw data files")
        num_files = _enqueue_existing_files(q, root_folder, print_ok)
        if num_files:
            print_ok(f"  - put {num_files} files into dispatch queue.")
        else:
            print_ok("  - no files pending.")

    time.sleep(0.2)

    observer = Observer(q, root_folder, print_ok, print_err)
    has_error = observer.start()
    if has_error:
        return None, None, None

    return q, dispatcher, observer


def _enqueue_existing_files(q, root_folder, print_ok):
    rel_paths = [
        path.relative_to(root_folder)
        for decl in RawDataFile.list_subclasses()
        for path in decl.data_files(root_folder)
        if path.exists()
    ]

    def sort_by_timestamp(path):
        return path.stem.partition("-")[2]

    rel_paths.sort(key=sort_by_timestamp)

    for rel_path in rel_paths:
        print_ok(f"  - enqueue {rel_path}")
        assert (root_folder / rel_path).exists()
        q.put((rel_path, time.time()))

    return len(rel_paths)


class Observer:
    def __init__(self, q, root_folder, print_ok, print_err):
        self.q = q
        self.root_folder = root_folder
        self.print_ok = print_ok
        self.print_err = print_err
        self.lock_file = lock_file_path(root_folder)

    def _call_back(self, event, rel_path, timestamp):
        if event not in (CREATED_EVENT, MODIFIED_EVENT):
            if rel_path == self.lock_file:
                self.print_ok("- removed update lock for landing zone")
        else:
            if rel_path == self.lock_file:
                self.print_ok("- lock landing zone for updating")
            else:
                self.q.put((rel_path, timestamp))

    def start(self):
        try:
            self.observer = start_observer(self.root_folder, self._call_back)
        except Exception as e:
            self.print_err("- could not start observer: {}".format(e))
            return True
        self.print_ok("- started observer")
        self.print_ok("- observe {} now".format(self.root_folder))
        return False

    def stop(self):
        self.print_ok("- shutdown observer")
        shutdown_observer(self.observer)
        self.print_ok("- observer shutdown")
