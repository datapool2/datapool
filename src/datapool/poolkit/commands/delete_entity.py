import json
from datetime import datetime
from pathlib import Path

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.database import (
    InvalidOperationError,
    check_if_tables_exist,
    connect_to_db,
)
from datapool.poolkit.landing_zone import write_lock
from datapool.poolkit.utils import filter_results
from datapool.poolkit.yaml import (
    RawDataFile,
    find_entity_by_name,
    has_child_which_is_fact_dimension,
    yaml_entity_names,
)


def delete_entity(do_delete, max_rows, what, name, print_ok, print_err):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    return _delete_entity(config, do_delete, max_rows, what, name, print_ok, print_err)


def _delete_entity(config, do_delete, max_rows, what, name, print_ok, print_err):
    entity_names = yaml_entity_names()
    if what not in entity_names:
        valid = ", ".join(sorted(entity_names))
        print_err(f"+ entity named {what} is not known, valid names are {valid}")
        return 1

    landing_zone_root = Path(config.landing_zone.folder)
    engine = _setup_engine(config, print_err)
    if engine is None:
        return 1

    with write_lock(landing_zone_root) as got_lock:
        if not got_lock:
            print_err(
                "+ {} is locked. maybe somebody else works on it "
                "simultaneously ?".format(landing_zone_root)
            )
            return 1

        try:
            spec = find_entity_by_name(what)

            found_error, messages, yaml_path = filter_results(
                spec.find_by_name(landing_zone_root, name)
            )
            for message in messages:
                print_err(f"- {message}")

            if found_error:
                print_err("+ failed")
                return 1

            if yaml_path is None:
                identifier = spec.get_identifier_field()
                print_err(f"+ did not find {what} with {identifier} '{name}'")
                return 1

            print_ok(f"- found yaml file at {yaml_path}")

            found_error, messages, item_id = filter_results(
                spec.find_in_db(config, engine, name)
            )
            for message in messages:
                print_err(f"- {message}")

            if found_error:
                print_err("+ failed")
                return 1

            if item_id is None:
                print_err(f"- did not find {what} with name '{name}' in db")
                print_err("+ INTERNAL ERROR: landing zone and db diverged")
                return 1

            found_error, messages, ok = filter_results(
                spec.check_before_delete(config, engine, item_id)
            )
            for message in messages:
                print_err(f"- {message}")

            if found_error:
                print_err("+ failed")
                return 1

            if not ok:
                print_err("+ cancel delete")
                return 1

            if has_child_which_is_fact_dimension(spec):
                conditions = [(what, "==", name)]
                fact_deletes = _count_facts(engine, print_err, conditions)
                if fact_deletes is None:
                    print_err("+ internal error")
                    return 1
                would_will = "will" if do_delete else "would"
                print_ok(f"- number of affected facts which {would_will} be deleted:")
                for entity, count in sorted(fact_deletes.items()):
                    print_ok(f"  - {entity}: {count}")

                print_ok("- overview of affected facts:")

                _print_overviews(engine, print_ok, print_err, max_rows, conditions)

        except AssertionError:
            pass

        if do_delete and not found_error:
            if has_child_which_is_fact_dimension(spec):
                found_error, messages, _ = filter_results(
                    _delete_facts(engine, conditions)
                )
                for message in messages:
                    print_err(f"- {message}")

            found_error, messages, _ = filter_results(
                spec.delete_from_db(config, engine, item_id)
            )
            for message in messages:
                print_err(f"- {message}")
            found_error, messages, _ = filter_results(
                spec.delete_from_lz(yaml_path, name)
            )
            for message in messages:
                print_err(f"- {message}")
            print_ok(f"- deleted {what} '{name}' from db.")

    if not found_error and has_child_which_is_fact_dimension(spec):
        if do_delete and config.backup_landing_zone:
            path = _record_delete("delete_meta", config, dict(what=what, name=name))
            print_ok(f"- recorded deletion at {path}")

    if not do_delete:
        print_ok("- use '--force --force' to delete {}".format(name))
    return 1 if found_error else 0


def _count_facts(engine, print_err, conditions=None):
    if conditions is None:
        conditions = []
    counts = {}
    fields = set(f for f, *_ in conditions)
    seen = set()
    for decl in RawDataFile.list_subclasses():
        if fields - decl.supported_fields_for_delete():
            continue
        if decl.entity_name in seen:
            continue
        seen.add(decl.entity_name)
        count = None
        found_errors, messages, count = filter_results(
            decl.count_facts(engine, conditions)
        )
        for message in messages:
            print_err(f"- {message}")
        if found_errors:
            return None
        counts[decl.entity_name] = count
    return counts


def _print_overviews(engine, print_ok, print_err, max_rows, conditions):
    fields = set(f for f, *_ in conditions)
    seen = set()
    for decl in RawDataFile.list_subclasses():
        if fields - decl.supported_fields_for_delete():
            continue
        if decl.entity_name in seen:
            continue
        seen.add(decl.entity_name)
        print_ok(f"  - {decl.entity_name}:")
        print_ok("")
        found_errors, messages, result = filter_results(
            decl.summary(engine, max_rows, conditions, print_ok, "  ")
        )
        for message in messages:
            print_err(f"  - {message}")
        if found_errors:
            return None
        print_ok("")


def _record_delete(what, config, args):
    folder = Path(config.backup_landing_zone.folder) / "_deletes"
    folder.mkdir(parents=True, exist_ok=True)

    now = datetime.now().strftime("%Y%m%d-%H%M%S-%f")
    file_name = f"{what}.json.{now}"
    with (folder / file_name).open("w") as fh:
        fh.write(json.dumps(args))
    return folder / file_name


def _setup_engine(config, print_err):
    try:
        engine = connect_to_db(config.db)
    except InvalidOperationError as e:
        print_err("- {}".format(e))
        return None

    if not check_if_tables_exist(config.db):
        print_err("- database not initialized, run 'pool init-db' first")
        return None

    return engine


def _delete_facts(engine, conditions):
    fields = set(f for f, *_ in conditions)
    for decl in RawDataFile.list_subclasses():
        if fields - decl.supported_fields_for_delete():
            continue
        yield from decl.delete(engine, conditions)
