import shutil
from pathlib import Path

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.landing_zone import init_start_state
from datapool.poolkit.yaml import collect_examples


def create_example(
    development_landing_zone: Path, reset, print_ok, print_err, config=None
):
    """creates local example landing zone serving examples for the initial setup."""

    if config is None:
        config, path = read_config()
        if config is None:
            print_err(
                f"- no config file found at {path}."
                " please run 'pool init-config' first."
            )
            return 1

    print_ok("- setup example landing zone")

    if development_landing_zone.exists():
        if list(development_landing_zone.iterdir()) and not reset:
            print_err(
                f"- folder {development_landing_zone} already exists and is not empty."
            )
            return 1
        try:
            shutil.rmtree(development_landing_zone)
        except IOError as e:
            print_err(
                "- could not delete folder {}".format(
                    development_landing_zone.absolute()
                )
            )
            print_err("- error message is: {}".format(e))
            return 1

    for rel_path, yaml_doc in collect_examples(config):
        full_path = development_landing_zone / rel_path
        full_path.parent.mkdir(parents=True, exist_ok=True)
        if isinstance(yaml_doc, str):
            full_path.write_text(yaml_doc)
        elif isinstance(yaml_doc, bytes):
            full_path.write_bytes(yaml_doc)
        else:
            raise NotImplementedError(type(yaml_doc))

    init_start_state(development_landing_zone)

    print_ok("+ example landing zone created", fg="green")
    return 0
