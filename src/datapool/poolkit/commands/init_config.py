import shutil

from datapool.poolkit.config_handling import init_config as init_config_


def init_config(
    landing_zone,
    sqlite_db,
    reset,
    extra_config_settings,
    print_ok,
    print_err,
    verbose,
    app_config,
):
    """setup minimal landing zone and create default configuration"""

    extra_config = {}
    for extra_config_setting in extra_config_settings:
        if "=" not in extra_config_setting:
            print_err("+ the EXTRA_CONFIG_SETTINGS arguments must contain '='")
            return 1
        key, _, value = extra_config_setting.partition("=")
        extra_config[key] = value

    if landing_zone.exists():
        if list(landing_zone.iterdir()):
            if not reset:
                print_err(f"- folder {landing_zone} already exists and is not empty.")
                return 1
            try:
                shutil.rmtree(landing_zone)
            except IOError as e:
                print_err(
                    "- could not delete folder {}".format(landing_zone.absolute())
                )
                print_err("- error message is: {}".format(e))
                return 1

    landing_zone.mkdir(parents=True, exist_ok=True)

    print_ok("- guess settings")
    try:
        landing_zone = landing_zone.absolute()
        config_file, messages = init_config_(
            landing_zone, app_config, sqlite_db, reset, extra_config
        )
        for message in messages:
            print_err("  - {}".format(message))
        print_ok("+ created config file at {}".format(config_file), fg="green")
        print_ok(
            "  please edit these files and adapt the data base configuration to "
            "your setup",
            fg="green",
        )
    except Exception as e:
        print_err("+ something went wrong: {}".format(e))
        return 1

    return 0
