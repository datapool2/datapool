import pathlib
import shutil
import time

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.errors import DataPoolException
from datapool.poolkit.landing_zone import (
    check_if_landing_zone_is_valid,
    check_if_save_state_is_still_up_to_date,
    list_new_or_changed_files,
    mark_start_state_as_in_sync,
    mark_start_state_as_out_of_sync,
    read_rename_json,
    rename_json_path,
    start_state_file_path,
    update_start_state,
    write_lock,
)
from datapool.poolkit.utils import is_server_running
from datapool.poolkit.yaml import sort_for_update_operational

from .check import _check, _check_rename


def update_operational(
    landing_zone,
    verbose,
    overwrite,
    copy_raw,
    print_ok,
    print_err,
    delay=0.1,
):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    if not landing_zone.exists():
        print_err("- folder {} does not exist".format(landing_zone))
        return 1

    if not is_server_running(config):
        print_err("+ datapool server is not running.")
        return 1

    print_ok("- datapool server is running.")

    return _update_operational(
        config,
        landing_zone,
        verbose,
        overwrite,
        copy_raw,
        print_ok,
        print_err,
        delay=0.1,
    )


def _update_operational(
    config,
    development_landing_zone,
    verbose,
    overwrite,
    copy_raw,
    print_ok,
    print_err,
    delay=0.1,
):
    try:
        rename_data = read_rename_json(development_landing_zone)
    except PermissionError:
        print_err("+ cound not read .permission.json due to file permissions. You must")
        print_err("  check as root if you are in the process or renaming entities")
        return 1

    if rename_data is not None:
        print_ok("+ rename detected")
        if not start_state_file_path(development_landing_zone).exists():
            print_err(
                f"+ landing zone at {development_landing_zone} does not contain"
                " .start_state file"
            )
            return 1
        renamed_files = rename_data["renames"]
    else:
        message = check_if_landing_zone_is_valid(development_landing_zone)
        if message is not None:
            print_err(
                "+ landing zone at {} invalid. reason: {}".format(
                    development_landing_zone, message
                )
            )
            return 1
        renamed_files = None

    operational_landing_zone = pathlib.Path(config.landing_zone.folder)
    mismatch = check_if_save_state_is_still_up_to_date(
        operational_landing_zone, development_landing_zone, renamed_files
    )
    if mismatch:
        if rename_data:
            check_if_save_state_is_still_up_to_date(
                operational_landing_zone, development_landing_zone, renamed_files
            )
            print_ok(
                "- development_landing_zone landing zone is out of sync with"
                "operational landing zone."
            )
            print_ok(
                "  this might be caused by excessive renaming or missing data files"
            )
        else:
            print_err(
                "+ the .start_state file in the development landing zone is"
                " out of sync with the operational landing zone."
            )
            print_err("  please create a fresh development landing zone.")
        print_err(
            "+ the following files are in the operational lz but are missing"
            " in the development landing zone or in the .start_state file:"
        )
        for p in sorted(mismatch):
            print_err(f"  - {p}")
        return 1

    print_ok("- development landing zone seems to be sane.")

    with write_lock(operational_landing_zone) as got_lock:
        if not got_lock:
            print_err(
                "+ {} is locked. maybe somebody else works on it "
                "simultaneously ?".format(operational_landing_zone)
            )
            return 1

        num_files = 0
        if rename_data is not None:
            exit_code = _check_rename(
                config,
                development_landing_zone,
                None,
                verbose,
                print_ok,
                print_err,
                False,
                rename_data,
            )
            if exit_code > 0:
                return exit_code

            path = rename_json_path(development_landing_zone)
            try:
                target = operational_landing_zone / path.relative_to(
                    development_landing_zone
                )
                shutil.move(path, target)
                target.chmod(0o770)
                print_ok(f"- moved .rename.json to {operational_landing_zone}")
                time.sleep(0.1)

            except IOError as e:
                print_err(f"- copy of {path} to {operational_landing_zone} failed: {e}")
                return 1

        else:
            exit_code = _check(
                config,
                development_landing_zone,
                None,
                verbose,
                print_ok,
                print_err,
                False,
            )
            if exit_code != 0:
                print_err("+ don't update {}".format(operational_landing_zone))
                return 1

        try:
            num_files += _update(
                operational_landing_zone,
                development_landing_zone,
                copy_raw,
                print_ok,
                delay,
            )
        except DataPoolException as e:
            print_err("- update failed:")
            print_err("- {}".format(e))
            return 1

        print_ok(
            "+ copied/created {} files/folders to/in {}".format(
                num_files, operational_landing_zone
            ),
            fg="green",
        )
        return 0


def _update(
    operational_landing_zone, development_landing_zone, copy_raw, print_ok, delay
):
    to_update = list_new_or_changed_files(development_landing_zone)
    mark_start_state_as_out_of_sync(development_landing_zone)
    for rel_path in sort_for_update_operational(development_landing_zone, to_update):
        target = operational_landing_zone / rel_path
        if not copy_raw:
            if (
                target.parent.name == "raw_data"
                and rel_path.name != "conversion_spec.yaml"
            ):
                continue
        target.parent.mkdir(parents=True, exist_ok=True)
        target.parent.chmod(0o777)

        if rel_path.stem == "conversion":
            existing_scripts = target.parent.glob("conversion.*")
            for p in existing_scripts:
                p.unlink()
                print_ok(f"- deleted {p}")

        shutil.copy(development_landing_zone / rel_path, target)
        target.chmod(0o666)
        print_ok(f"- copied {rel_path}")
        time.sleep(delay)
    mark_start_state_as_in_sync(development_landing_zone)
    update_start_state(development_landing_zone, development_landing_zone)
    return len(to_update)
