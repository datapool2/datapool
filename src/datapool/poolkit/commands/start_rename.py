#!/usr/bin/env python
from pathlib import Path

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.landing_zone import (
    create_from,
    update_start_state,
    write_rename_json,
)
from datapool.poolkit.utils import filter_results
from datapool.poolkit.yaml import (
    collect_potentially_affected_files,
    copy_latest_raw_files,
    find_entity_by_name,
    yaml_entity_names,
)

from .delete_entity import _setup_engine


def start_rename(
    rename_landing_zone_folder, what, old_name, new_name, print_ok, print_err
):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    return _start_rename(
        config,
        rename_landing_zone_folder,
        what,
        old_name,
        new_name,
        print_ok,
        print_err,
    )


def _start_rename(
    config, rename_landing_zone_folder, what, old_name, new_name, print_ok, print_err
):
    entity_names = yaml_entity_names()
    if what not in entity_names:
        valid = ", ".join(sorted(entity_names))
        print_err(f"+ entity named {what} is not known, valid names are {valid}")
        return 1

    operational_landing_zone = Path(config.landing_zone.folder)
    engine = _setup_engine(config, print_err)
    if engine is None:
        return 1

    spec = find_entity_by_name(what)

    found_error, messages, yaml_path = filter_results(
        spec.find_by_name(operational_landing_zone, new_name)
    )
    for message in messages:
        print_err(f"- {message}")

    if found_error:
        print_err("+ failed")
        return 1

    if yaml_path is not None:
        print_err(f"+ {what} with name '{new_name}' already exists at {yaml_path}")
        return 1

    found_error, messages, item_id = filter_results(
        spec.find_in_db(config, engine, new_name)
    )
    for message in messages:
        print_err(f"- {message}")

    if found_error:
        print_err("+ failed")
        return 1

    if item_id is not None:
        print_err(f"- found existing {what} with name '{new_name}' in db")
        print_err("+ INTERNAL ERROR: landing zone and db diverged")
        return 1

    found_error, messages, yaml_path = filter_results(
        spec.find_by_name(operational_landing_zone, old_name)
    )

    for message in messages:
        print_err(f"- {message}")

    if found_error:
        print_err("+ failed")
        return 1

    if yaml_path is None:
        print_err(f"+ did not find {what} with name '{old_name}'")
        return 1

    print_ok(f"- found yaml file at {yaml_path}")

    found_error, messages, item_id = filter_results(
        spec.find_in_db(config, engine, old_name)
    )
    for message in messages:
        print_err(f"- {message}")

    if found_error:
        print_err("+ failed")
        return 1

    if item_id is None:
        print_err(f"- did not find {what} with name '{old_name}' in db")
        print_err("+ INTERNAL ERROR: landing zone and db diverged")
        return 1

    print_ok("- setup development landing zone for rename")
    if rename_landing_zone_folder.exists():
        print_err("  - folder {} already exists.".format(rename_landing_zone_folder))
        return 1

    print_ok("- copy files from operational landing zone.")
    ok = True

    for p in create_from(rename_landing_zone_folder, operational_landing_zone):
        if isinstance(p, Exception):
            print_err(f"  - copy failed: {p}")
            ok = False
        else:
            print_ok(f"  - {p}")

    if not ok:
        return 1

    backup_landing_zone_folder = config.backup_landing_zone.folder
    if backup_landing_zone_folder:
        backup_landing_zone_folder = Path(backup_landing_zone_folder)
        print_ok("- restore latest raw data files")
        copied_files = False
        seen = set()
        for p in copy_latest_raw_files(
            operational_landing_zone,
            backup_landing_zone_folder,
            rename_landing_zone_folder,
        ):
            if isinstance(p, Exception):
                print_err(f"  - error: {p}")
                ok = False
                continue
            else:
                if p not in seen:
                    print_ok(f"  - {p}")
                    seen.add(p)
            copied_files = True
        if not copied_files:
            print_err("- did not find data files in the backup landing zone at")
            print_err(f"  {backup_landing_zone_folder}.")
            print_err("  this is not an error in case you never imported data.")
    else:
        print_err("- no backup landing zone configured, you will have to copy raw data")
        print_err(f"  files manually into {rename_landing_zone_folder} to check your ")
        print_err("  scripts which might fail because of the rename")
    if not ok:
        return 1

    yaml_path_dev = rename_landing_zone_folder / yaml_path.relative_to(
        operational_landing_zone
    )

    found_error, messages, result = filter_results(
        spec.rename(old_name, new_name, yaml_path_dev)
    )
    for message in messages:
        if isinstance(message, Exception):
            print_err(f"- error: {message}")
        else:
            print_ok(f"- {message}")

    if found_error:
        print_err("+ failed")
        return 1

    if result is None:
        print_err(f"+ INTERNAL ERROR IN {spec.rename}")
        return 1

    yaml_path, renames = result
    (rename_landing_zone_folder / yaml_path).chmod(0o400)
    print_ok(f"- disabled write permissions for {yaml_path}, pleaese don't edit this")
    print_ok("  file until the rename process is finished.")

    update_start_state(
        rename_landing_zone_folder, rename_landing_zone_folder, ignore=renames.values()
    )

    affected_file_patterns = collect_potentially_affected_files(what)

    try:
        write_rename_json(
            rename_landing_zone_folder,
            new_name,
            old_name,
            yaml_path,
            renames,
            item_id,
            affected_file_patterns,
        )
    except IOError as e:
        print_err(f"+ error: {e}")
        return 1

    print_ok("- development landing zone for rename is ready.")
    print_ok("- please adapt conversion scripts, then run check and update-oparational")
    print_ok("+ done")
