#!/usr/bin/env python

import os
import pathlib
import shutil
import time

from datapool.poolkit.config_handling import read_config


def replay(folder, broken_only, recurse, time_min, time_max, print_ok, print_err):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    backup_lz = pathlib.Path(config.backup_landing_zone.folder)
    root_folder = backup_lz / folder

    if not root_folder.exists():
        print_err(f"path {root_folder} does not exist.")
        return 1

    if not root_folder.is_dir():
        print_err(f"path {root_folder} exists but is not a directory.")
        return 1

    lz = pathlib.Path(config.landing_zone.folder)

    MAX_ERROR = 10

    count = 0
    error_count = 0
    for path in _iter_files(
        backup_lz, folder, broken_only, recurse, time_min, time_max
    ):
        path_without_timestamp = str(path).rsplit(".", 1)[0]
        print_ok(f"- restore {path}")
        inwrite = lz / (path_without_timestamp + ".inwrite")
        try:
            # atomic write:
            shutil.copyfile(backup_lz / path, inwrite)
            os.rename(inwrite, lz / path_without_timestamp)
            time.sleep(0.01)
        except IOError as e:
            print_err(f"  - error: {e}")
            error_count += 1
        if error_count > MAX_ERROR:
            print("- abort replay after {MAX_ERROR} errors, please fix error first.")
            return 1
        time.sleep(0.01)
        count += 1

    print_ok(f"+ replayed {count} files", fg="green")
    return 0


def _iter_files(backup_lz, folder, broken_only, recurse, time_min, time_max):
    root_folder = backup_lz / folder

    glob = root_folder.rglob if recurse else root_folder.glob

    matches = []

    for p in glob("*"):
        if p.is_dir():
            continue
        if p.name.startswith("."):
            continue
        if broken_only:
            if not str(p).endswith("-broken"):
                continue
        timestamp = str(p).rsplit(".", 1)[1]
        if time_min is not None and timestamp < time_min:
            continue
        if time_max is not None and timestamp > time_max:
            continue

        matches.append((timestamp, p.relative_to(backup_lz)))

    for _, p in sorted(matches):
        yield p
