#!/usr/bin/env python


import pathlib
from functools import partial

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.utils import ask
from datapool.poolkit.yaml import collect_yaml_names, find_entity_by_name


def add(info, entity_name, lz_folder, settings, print_ok, print_err):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    supported_entities = collect_yaml_names()

    ok = True
    if entity_name not in supported_entities:
        print_err(f"- argument '{entity_name}' invalid. allowed values are:")
        for available_entity in sorted(supported_entities):
            print_err("  - {}".format(available_entity))
        return 1

    if not info and lz_folder is None:
        print_err("- you must either provide a development landing zone or use --info")
        return 1

    spec = find_entity_by_name(entity_name)
    if info:
        print_ok("- supported field names are:")
        for f in spec.create_new_fields(config):
            print_ok(f"  - {f}")
        return 0

    lz_folder = pathlib.Path(lz_folder)
    if not lz_folder.exists():
        print_err(f"- folder {lz_folder} does not exist.")
        return 1

    predefined = {}
    for setting in settings:
        if "=" not in setting:
            print_err(f"- setting '{setting}' does not contain a '='")
            ok = False
        else:
            key, _, value = setting.partition("=")
            predefined[key.strip()] = value.strip()

    unknown_keys = set(predefined) - set(spec.create_new_fields(config))

    if unknown_keys:
        print_err("- following fields are not known:")
        for key in sorted(unknown_keys):
            print_err(f"  - {key}")
        print_err("- supported field names are:")
        for f in spec.create_new_fields(config):
            print_err(f"  - {f}")
        return 1

    if not ok:
        return 1

    return _add(config, lz_folder, entity_name, predefined, input, print_ok, print_err)


def _add(
    config,
    lz_folder,
    entity_name,
    predefined,
    input_,
    print_ok,
    print_err,
    raise_exceptions=True,
):
    spec = find_entity_by_name(entity_name)
    ask_term = partial(ask, input_, print_ok, predefined)

    assert isinstance(lz_folder, pathlib.Path)

    try:
        full_path = spec.create_new(config, lz_folder, ask_term)
    except KeyboardInterrupt:
        print_err("+ canceled")
        return 1
    except ValueError as e:
        print_err("")
        print_err(f"- {e.args[0]}")
        return 1
    except Exception as e:
        if raise_exceptions:
            raise e
        print_err("")
        print_err(f"- something unexpected happened: {e.args[0]}")
        return 1

    print_ok(f"- wrote {full_path}:", fg="green")
    print_ok(full_path.read_text(), fg="blue")
    print_ok("+ please check or complete file.", fg="green")

    return 0
