#!/usr/bin/env python

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.database import check_if_tables_exist, dump_db, get_table_names


def show(what, no_pager, max_rows, print_ok, print_err):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    return _show(config, what, no_pager, max_rows, print_ok, print_err)


def _show(config, what, no_pager, max_rows, print_ok, print_err):
    if not check_if_tables_exist(config.db):
        print_err(
            "+ looks like you did not init the database, run 'init-db' command first"
        )
        return 1

    table_names = get_table_names()
    if what not in table_names:
        print_err(f"- table {what} does not exist. available tables are:")
        for table in table_names:
            print_err(f"  - {table}")
        return 1

    dump_db(
        config.db, [what], print_=print_ok, use_pager=not no_pager, limit_rows=max_rows
    )
