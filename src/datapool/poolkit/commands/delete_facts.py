#!/usr/bin/env python

from datetime import datetime

from datapool.poolkit.config_handling import read_config
from datapool.poolkit.utils import filter_results
from datapool.poolkit.yaml import find_entity_by_name, raw_data_entity_names

from .delete_entity import _record_delete, _setup_engine


def delete_facts(do_delete, max_rows, fact_table, filters, print_ok, print_err):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    return _delete_facts(
        config, do_delete, max_rows, fact_table, filters, print_ok, print_err
    )


def _delete_facts(
    config, do_delete, max_rows, fact_table, filters, print_ok, print_err
):
    entity_names = raw_data_entity_names()

    if fact_table not in entity_names:
        valid = ", ".join(sorted(entity_names))
        print_err(
            f"+ fact table named {fact_table} is not known, valid names are {valid}"
        )
        return 1

    engine = _setup_engine(config, print_err)
    if engine is None:
        return 1

    conditions = []
    found_error = False
    for filter_ in filters:
        # order matters:
        operators = ("<=", ">=", "<", ">", "!=", "==")
        for operator in operators:
            keyword, op, value = filter_.partition(operator)
            if op:
                conditions.append((keyword, op, value))
                break
        else:
            ops = ", ".join(operators)
            print_err(f"- misformed expression {filter_!r}, did not find any of {ops}")
            found_error = True
    if found_error:
        print_err("+ cancel")
        return 1

    entity = find_entity_by_name(fact_table)

    found_errors, messages, _ = filter_results(
        entity.summary(engine, max_rows, conditions, print_ok, "  ")
    )
    for message in messages:
        print_err(f"  - {message}")
    if found_errors:
        return 1

    print_ok("")

    found_errors, messages, count = filter_results(
        entity.count_facts(engine, conditions)
    )
    for message in messages:
        print_err(f"- {message}")
    if found_errors:
        return 1

    would_will = "will" if do_delete else "would"
    print_ok(f"- this operation {would_will} delete {count} facts")

    if not do_delete:
        print_ok("- use '--force --force' to delete facts.")
        return 0

    started = datetime.now()
    found_errors, messages, _ = filter_results(entity.delete(engine, conditions))
    for message in messages:
        print_err(f"- {message}")
    if found_errors:
        return 1

    print_ok("- required time: {}".format(datetime.now() - started))
    if config.backup_landing_zone:
        path = _record_delete("delete_facts", config, dict(filters=filters))
        print_ok(f"- recorded deletion at {path}")
    return 0


def _delete_associations(session, facts):
    pass
