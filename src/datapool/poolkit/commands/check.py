import contextlib
import os
import pathlib
import shutil
import tempfile
import time
from collections import defaultdict
from fnmatch import fnmatch

from datapool.poolkit.config_handling import config_for_develop_db, read_config
from datapool.poolkit.data_conversion import ConversionRunner
from datapool.poolkit.database import (
    check_if_tables_exist,
    connect_to_db,
    copy_db,
    setup_db,
)
from datapool.poolkit.dispatcher import CustomizedDispatcher
from datapool.poolkit.errors import InvalidOperationError
from datapool.poolkit.fact_table import Context
from datapool.poolkit.landing_zone import (
    check_if_landing_zone_is_valid,
    list_new_or_changed_files,
    read_rename_json,
    start_state_file_path,
)
from datapool.poolkit.utils import enumerate_filename, filter_results, print_fact_table
from datapool.poolkit.yaml import (
    RawDataFile,
    Yaml,
    find_entity,
    find_yaml,
    is_conversion_script,
    list_all_data_files,
    list_all_scripts,
    separate_allowed_files,
    sort_entities,
)
from datapool.poolkit.yaml_parser import YamlError, parse_file


def check(landing_zone, result_folder, verbose, print_ok, print_err, run_twice=True):
    config, path = read_config()
    if config is None:
        print_err(
            f"- no config file found at {path}. please run 'pool init-config' first."
        )
        return 1

    if not landing_zone.exists():
        print_err("- folder {} does not exist".format(landing_zone))
        return 1

    return _check(
        config, landing_zone, result_folder, verbose, print_ok, print_err, run_twice
    )


def _check(
    config, landing_zone, result_folder, verbose, print_ok, print_err, run_twice=True
):
    try:
        rename_data = read_rename_json(landing_zone)
    except PermissionError:
        print_err("+ cound not read .permission.json due to file permissions. You must")
        print_err("  check as root if you are in the process or renaming entities")
        return 1

    if rename_data is not None:
        print_ok("+ rename detected")
        return _check_rename(
            config,
            landing_zone,
            result_folder,
            verbose,
            print_ok,
            print_err,
            run_twice,
            rename_data,
        )

    print_ok(
        "- check names and places of changed files at landing zone {}".format(
            landing_zone
        )
    )

    message = check_if_landing_zone_is_valid(landing_zone)
    if message is not None:
        print_err(f"+ {message}")
        return 1

    all_changed_files = set(list_new_or_changed_files(landing_zone))
    valid_files, unknown_files = separate_allowed_files(landing_zone, all_changed_files)

    if not unknown_files:
        print_ok("- all filenames comply specification.")
    else:
        for unknown_file in unknown_files:
            print_err("- do not know how to handle file at {}".format(unknown_file))
        print_err("+ invalid file names or misplaced files found")
        return 1

    if not valid_files:
        print_ok("+ no new files detected")
        return 0

    matches = [
        (rel_path, find_entity(rel_path))
        for rel_path in sort_entities(landing_zone, valid_files)
    ]

    customized_handlers = [
        (rel_path, e)
        for (rel_path, e) in matches
        if e is not None and CustomizedDispatcher in e.mro()
    ]

    remaining_matches = [
        (rel_path, e)
        for (rel_path, e) in matches
        if e is not None and CustomizedDispatcher not in e.mro()
    ]

    yaml_matches = [
        (rel_path, e)
        for rel_path, e in remaining_matches
        if e is not None and Yaml in e.mro()
    ]

    conversion_scripts = set(
        rel_path for rel_path in valid_files if is_conversion_script(rel_path)
    )

    raw_data_matches = [
        (rel_path, e)
        for rel_path, e in remaining_matches
        if e is not None and RawDataFile in e.mro()
    ]

    with _setup_test_db(landing_zone, config, verbose, print_ok, print_err) as engine:
        checks_ok = True
        print_ok("")
        print_ok("- check yaml files in landing zone at {}".format(landing_zone))
        checks_ok = _run_yaml_checks(
            config, landing_zone, yaml_matches, engine, print_ok, print_err, verbose
        )

        if checks_ok:
            print_ok("")
            print_ok("- check scripts / raw data files at {}".format(landing_zone))
            checks_ok = _run_all_scripts(
                landing_zone,
                raw_data_matches,
                conversion_scripts,
                engine,
                config,
                result_folder,
                verbose,
                run_twice,
                print_ok,
                print_err,
            )
            print_ok("")
            for rel_path, spec in customized_handlers:
                print_ok(f"- check {rel_path}")
                found_errors, messages, _ = filter_results(
                    spec.check(landing_zone, rel_path, config, engine)
                )
                for message in messages:
                    print_ok(f"  - {message}")
                if found_errors:
                    print_err("- check failed")
                    checks_ok = False

    if not checks_ok:
        print_err("")
        print_err("+ checks failed. please fix this.")
        return 1

    print_ok("")
    print_ok("- all scripts checked")
    print_ok("+ congratulations: all checks succeeded.", fg="green")
    return 0


def _check_rename(
    config,
    landing_zone,
    result_folder,
    verbose,
    print_ok,
    print_err,
    run_twice,
    rename_data,
):
    if not start_state_file_path(landing_zone).exists():
        print_err(
            f"+ landing zone at {landing_zone} does not contain .start_state file"
        )
        if os.path.exists(start_state_file_path(landing_zone) + ".out_of_sync"):
            print_err(
                "+ found .start_state_out_of_sync file which indicates that a previous"
                " update-operational did not complete and left and invalid development"
                " landing zone."
            )
            print_err(
                "+ please create a new development landing zone and copy over your "
                " your changes."
            )
        return 1

    affected_files = set(pathlib.Path(p) for p in rename_data["renames"].values())
    affected_file_patterns = rename_data["affected_file_patterns"]

    new_or_changed = set(list_new_or_changed_files(landing_zone))
    ok = True
    for p in new_or_changed:
        if str(p).startswith("."):
            continue
        if p in affected_files:
            continue
        if any(fnmatch(p, pattern) for pattern in affected_file_patterns):
            continue
        if str(p).startswith(".raw"):
            continue
        print_err(f"- illegal modification/new file {p}")
        ok = False
    if not ok:
        return 1

    yaml_paths = [pathlib.Path(rename_data["yaml_path"])]
    yaml_paths += [
        p for p in affected_files if p.suffix == ".yaml" and p not in yaml_paths
    ]

    yaml_matches = [find_yaml(yaml_path) for yaml_path in yaml_paths]

    assert yaml_matches[0] is not None

    yaml_data = sorted(
        (path, match)
        for path, match in zip(yaml_paths, yaml_matches)
        if match is not None
    )

    data_files = list_all_data_files(landing_zone)
    raw_data_matches = []
    customized_checkers = []
    for data_file in data_files:
        p = data_file.relative_to(landing_zone)
        match = find_entity(p)
        if CustomizedDispatcher in match.mro():
            customized_checkers.append((p, match))
        else:
            raw_data_matches.append((p, match))

    conversion_scripts = set(list_all_scripts(landing_zone)) & new_or_changed

    with _setup_test_db(landing_zone, config, verbose, print_ok, print_err) as engine:
        checks_ok = True
        for message in yaml_matches[0].prepare_rename_in_db(
            config, engine, rename_data
        ):
            if isinstance(message, Exception):
                print_err(f"- rename in db failed: {message}")
                checks_ok = False
                continue
            if isinstance(message, str):
                print_ok(f"- {message}")
        if not checks_ok:
            print_err("+ setting up conversion scripts failed. please fix this.")
            return 1

        checks_ok = _run_yaml_checks(
            config,
            landing_zone,
            yaml_data,
            engine,
            print_ok,
            print_err,
            verbose,
        )
        if checks_ok:
            print_ok("")
            print_ok("- check scripts / raw data files at {}".format(landing_zone))
            checks_ok = _run_all_scripts(
                landing_zone,
                raw_data_matches,
                conversion_scripts,
                engine,
                config,
                result_folder,
                verbose,
                run_twice,
                print_ok,
                print_err,
            )

            print_ok("")
            for rel_path, spec in customized_checkers:
                print_ok(f"- check {rel_path}")
                found_errors, messages, _ = filter_results(
                    spec.check(landing_zone, rel_path, config, engine)
                )
                for message in messages:
                    print_ok(f"  - {message}")
                if found_errors:
                    print_err("- check failed")
                    checks_ok = False
        if not checks_ok:
            print_err("")
            print_err("+ checks failed. please fix this.")
            return 1

    print_err("")
    print_ok("+ congratulations: all checks succeeded.", fg="green")
    return 0


def _run_all_scripts(
    landing_zone,
    raw_data_matches,
    conversion_scripts,
    engine,
    config,
    original_result_folder,
    verbose,
    run_twice,
    print_ok,
    print_err,
):
    if conversion_scripts is None:
        conversion_scripts = set()

    found_errors = False

    jobs = set()
    for rel_path, spec in raw_data_matches:
        scripts, raw_data_files, conversion_spec_path = spec.setup_conversion(
            landing_zone, rel_path
        )
        if len(scripts) > 1:
            print_err(f"  - found multiple scripts at {rel_path}")
            found_errors = True
            continue
        if len(scripts) == 0:
            print_err(f"  - found no script to check {rel_path}, skip check.")
            found_errors = True
            continue

        script = scripts[0]
        jobs.add((script, tuple(raw_data_files), conversion_spec_path, spec))
        script_rel_path = script.relative_to(landing_zone)
        # might happend multiple times for script_rel_path in case we have multiple
        # raw data files for the same script:
        conversion_scripts.discard(script_rel_path)

    for script in sorted(conversion_scripts):
        print_err(f"- can not check {script} because raw data file is missing")
        found_errors = True
        if not raw_data_matches:
            # skip rest
            return False

    runner = ConversionRunner(config)
    result_folder = _setup_result_folder(original_result_folder, print_ok, print_err)

    for script_path, raw_data_files, conversion_spec_path, spec in sorted(jobs):
        new_errors, facts = _run_script(
            runner,
            script_path,
            raw_data_files,
            conversion_spec_path,
            spec,
            landing_zone,
            rel_path,
            run_twice,
            verbose,
            result_folder,
            print_ok,
            print_err,
        )
        if new_errors:
            found_errors = True
            continue

        print_ok("  - check each fact")
        # new_errors, messages, __ = filter_results(
        # itertools.chain(*[spec.check_fields(fact) for fact in facts], [0])
        # )
        new_errors, messages, __ = filter_results(
            (r for fact in facts for r in spec.check_fields(fact))
            # itertools.chain(*[spec.check_fields(fact) for fact in facts], [0])
        )
        for message in messages:
            print_err(f"    - {message}")

        if new_errors:
            found_errors = True
            continue

        new_errors, messages, __ = filter_results(
            spec.prepare_facts(facts, script_path, raw_data_files, conversion_spec_path)
        )
        for message in messages:
            print_err(f"    - {message}")

        if new_errors:
            found_errors = True
            continue

        print_ok("  - check facts consistency")
        new_errors = _report(spec.check_if_facts_conflict(facts), print_ok, print_err)
        if new_errors:
            found_errors = True
            continue

        ctx = Context(engine, spec.get_db_related_coordinates())
        print_ok("  - check facts against db")
        new_errors = _report(
            (
                error
                for fact in facts
                for error in (spec.check_fact_against_db(ctx, fact))
            ),
            print_ok,
            print_err,
            indent=4,
        )
        if new_errors:
            found_errors = True

    if original_result_folder is None:
        print_ok(f"- delete intermediate output files in {result_folder}")
        shutil.rmtree(result_folder)

    return not found_errors


def _run_script(
    runner,
    script_path,
    raw_data_files,
    conversion_spec_path,
    spec,
    landing_zone,
    rel_path,
    run_twice,
    verbose,
    result_folder,
    print_ok,
    print_err,
):
    facts = []
    for raw_data_file in raw_data_files:
        print_ok("")
        print_ok(f"  - check {script_path} on {raw_data_file}")

        for i in range(2 if run_twice else 1):
            do_backup = i == 0

            ok, needed_conv, _facts = _check_conversion(
                runner,
                landing_zone,
                spec,
                script_path,
                raw_data_file,
                conversion_spec_path,
                verbose,
                result_folder,
                do_backup,
                print_ok,
                print_err,
            )
            if i == 0:
                if _facts is not None:
                    facts = _facts

            if not ok:
                return True, facts
            else:
                print_ok(
                    "  - {} conversion needed {:.0f} msec".format(
                        "first" if i == 0 else "second", needed_conv * 1000
                    )
                )

    return False, facts


def _check_conversion(
    runner,
    lz,
    spec,
    script_path,
    data_path,
    conversion_spec,
    verbose,
    result_folder,
    backup_results,
    print_ok,
    print_err,
):
    facts = None

    started = time.time()
    for result in runner.run_conversion(
        script_path,
        data_path,
        conversion_spec,
        status_callback=False,
        verbose=verbose,
        tmp_root=result_folder,
    ):
        if isinstance(result, Exception):
            print_err("  - {}".format(result))
        elif isinstance(result, str):
            print_ok("  - {}".format(result))
        else:
            output_file, facts = result

    if facts is None:
        return False, 0, None

    needed_conv = time.time() - started

    if backup_results:
        _backup_results(result_folder, output_file, facts, script_path, print_ok)

    return True, needed_conv, facts


def _report(check_iter, print_ok, print_err, indent=2):
    error_count = 0
    has_errors = False
    MAX_ERR = 10
    for result in check_iter:
        if isinstance(result, Exception):
            error_count += 1
            has_errors = True
            if error_count > MAX_ERR:
                continue
            print_err(indent * " " + f"- {result}")
        elif isinstance(result, str):
            print_ok(indent * " " + f"- {result}")

    if error_count > MAX_ERR:
        print_err(
            indent * " "
            + "- too many errors, skipped {} errors.".format(error_count - MAX_ERR)
        )
    return has_errors


def _setup_result_folder(folder, print_ok, print_err):
    if not folder:
        return pathlib.Path(tempfile.mkdtemp())
    else:
        folder = pathlib.Path(folder)
        if folder.exists():
            if not folder.is_dir():
                print_err(f"+ given path {folder} exists but is not a folder")
                return 1
        else:
            folder.mkdir(parents=True, exist_ok=True)
            print_ok(f"- created folder {folder}")

    return folder


def _backup_results(result_folder, output_file, facts, script_path, print_ok):
    script_folder_name = script_path.parent.name
    csv_path = result_folder / (script_folder_name + ".csv")
    txt_path = result_folder / (script_folder_name + ".txt")

    csv_path, txt_path = enumerate_filename(csv_path, txt_path)
    shutil.copy(output_file, csv_path)

    if facts:
        header = list(facts[0].keys())
        rows = [fact.values() for fact in facts]
    else:
        header = rows = []

    with txt_path.open("w") as fh:

        def print_(*a, **kw):
            # fix click args:
            if "nl" in kw:
                del kw["nl"]
                kw["end"] = ""
            kw["file"] = fh
            print(*a, **kw)

        print_fact_table(header, rows, 0, [], print_=print_)
    print_ok("  - wrote conversion results to {}".format(result_folder))


@contextlib.contextmanager
def _setup_test_db(landing_zone, config, verbose, print_ok, print_err):
    config_develop, path = config_for_develop_db(config, landing_zone)

    if os.path.exists(path):
        os.unlink(path)

    ok = False
    try:
        ok = check_if_tables_exist(config.db)
    except InvalidOperationError:
        print_err("- could not connect to productive db.")
    if not ok:
        print_err(
            "- setup fresh development db. productive does not exist or is empty."
        )
        setup_db(config_develop, verbose=verbose)

    else:
        print_ok("- copy meta data from productive db")

        meta_data_tables = Yaml.table_names()

        for table_name in copy_db(
            config.db,
            config_develop.db,
            delete_existing=True,
            copy_only=meta_data_tables,
            verbose=verbose,
        ):
            print_ok("  - copy table {}".format(table_name))

    engine = connect_to_db(config_develop.db)

    yield engine

    engine.dispose()
    os.unlink(path)


def _run_yaml_checks(
    config, landing_zone, yaml_matches, engine, print_ok, print_err, verbose
):
    print_ok("")
    if not yaml_matches:
        print_ok("- no new or modified yaml files detected. skip checks.")
        return True

    yaml_matches = [(p, spec) for p, spec in yaml_matches if spec is not None]

    print_ok("- detected {} modified yaml files:".format(len(yaml_matches)))
    for rel_path, _ in yaml_matches:
        print_ok(f"  - {rel_path}")

    print_ok("")
    print_ok("- check yaml files")

    ok = True

    collected = []

    for rel_path, yaml_spec in yaml_matches:
        print_ok(f"  - check {rel_path}")

        try:
            yaml = parse_file(pathlib.Path(landing_zone) / rel_path)
        except YamlError as e:
            print_err(f"    - {e}")
            ok = False
            continue

        found_errors, messages, __ = filter_results(
            yaml_spec.check_yaml(landing_zone, rel_path, yaml, config)
        )
        for message in messages:
            print_err(f"    - {message}")

        if found_errors:
            ok = False
            continue

        collected.append((yaml, rel_path, yaml_spec))

    if found_errors:
        return False

    spec_to_yamls = defaultdict(list)
    for yaml, _, yaml_spec in collected:
        spec_to_yamls[yaml_spec].append(yaml)

    print_ok("")
    print_ok("- check for conflicts")
    for spec, yamls in spec_to_yamls.items():
        found_errors, messages, __ = filter_results(spec.check_conflicts(yamls))
        for message in messages:
            print_err(f"  - {message}")
        ok = ok and not found_errors

    if not ok:
        return False

    print_ok("- check against db")

    for yaml, rel_path, yaml_spec in collected:
        print_ok(f"  - {rel_path}")

        found_errors, messages, yaml = filter_results(
            yaml_spec.prepare(landing_zone, rel_path, yaml)
        )
        for message in messages:
            print_err(f"    - {message}")

        if found_errors:
            ok = False
            continue

        found_errors, messages, dbos = filter_results(
            yaml_spec.check_against_db(config, engine, yaml)
        )

        for message in messages:
            print_err(f"    - {message}")

        if found_errors:
            ok = False
            continue

        found_errors, messages, __ = filter_results(
            yaml_spec.commit_dbos(config, engine, dbos)
        )
        if messages:
            for message in messages:
                print_err(f"    - {message}")
        ok = ok and not found_errors

    return ok
