#!/usr/bin/env python

from collections import defaultdict

from .database import fetch_many
from .utils import FrozenDict  # noqa: F401


class Context:
    def __init__(self, engine, db_coordinates_setup):
        self.engine = engine
        self.caches = defaultdict(dict)
        self.db_coordinates_setup = db_coordinates_setup

    def get(self, field_name, value):
        if field_name not in self.caches:
            self._fill(field_name)
        return self.caches[field_name].get(value)

    def _fill(self, field_name):
        decl, key = self.db_coordinates_setup[field_name]
        for dbo in fetch_many(self.engine, decl):
            self.caches[field_name][getattr(dbo, key)] = dbo
