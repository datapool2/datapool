#  encoding: utf-8
from __future__ import absolute_import, division, print_function

import glob
import json
import os
import shutil
import tempfile
import time
from datetime import datetime
from pathlib import Path

from .data_conversion import SUPPORTED_SCRIPT_EXTENSIONS, ConversionRunner
from .database import connect_to_db
from .errors import ConsistencyError, DataPoolException, InvalidOperationError
from .fact_table import Context
from .landing_zone import remove_rename_json
from .prometheus import (
    conversion_to_db_time,
    dispatch_delay_time,
    dispatched_raw_file_counter,
    facts_processed,
    files_processed_counter,
    fs_event_delay_time,
    raw_file_conversion_time,
    time_from_file_creation_to_end_of_processing,
)
from .utils import filter_results
from .yaml import RawDataFile, find_entity, find_raw_data_decl, find_yaml
from .yaml_parser import YamlError, parse_file


def wait_until_exists(path, max_attempts=10, wait_interval=0.1):
    if not os.path.exists(path):
        for __ in range(max_attempts):
            if os.path.exists(path):
                return True
            time.sleep(wait_interval)
        return False
    return True


def default_time_provider():
    return datetime.now().strftime("%Y%m%d-%H%M%S-%f")


def mtime(path):
    if path.exists():
        try:
            return path.stat().st_ctime
        except FileNotFoundError:
            return None
    else:
        return 0  # only triggered in tests


class CustomizedDispatcher:
    @classmethod
    def dispatch(cls, root_folder, rel_path, engine, config):
        raise NotImplementedError()

    @classmethod
    def check(cls, root_folder, rel_path, engine, config):
        raise NotImplementedError()

    @classmethod
    def cleanup(cls, root_folder, rel_path, engine, config, backup_files):
        for p in backup_files:
            try:
                (root_folder / p).unlink()
            except IOError as e:
                yield e


class Dispatcher:
    def __init__(
        self,
        config,
        root_folder=None,
        engine=None,
        time_provider=default_time_provider,
        timeout=0.5,
    ):
        """
        time_provider arg allows patching for regression tests !
        """
        self.config = config
        self.time_provider = time_provider
        self.root_folder = Path(root_folder or config.landing_zone.folder)
        self.conversion_runner = ConversionRunner(config)
        self.engine = engine or connect_to_db(self.config.db)
        self.currently_dispatching = 0

    def dispatch(self, rel_path, timestamp):
        self.currently_dispatching += 1
        try:
            full_path = self.root_folder / rel_path
            file_modification_time = mtime(full_path)
            if file_modification_time is None:
                yield f"file {full_path} disappeared during processing"
                return

            delay = timestamp - file_modification_time

            if file_modification_time:
                fs_event_delay_time.set(delay)

            delay = time.time() - file_modification_time

            if file_modification_time:
                dispatch_delay_time.set(delay)

            for notification in self._dispatch(rel_path):
                if isinstance(notification, (str, Exception)):
                    yield notification
                else:
                    raise RuntimeError(f"must never happen: {notification!r}")

            delay = time.time() - file_modification_time
            if file_modification_time:
                time_from_file_creation_to_end_of_processing.set(delay)
            files_processed_counter.inc()

        finally:
            self.currently_dispatching -= 1

    def _dispatch(self, rel_path):
        ext = os.path.splitext(rel_path)[1]

        spec = find_entity(rel_path)
        if spec is not None and CustomizedDispatcher in spec.mro():
            found_errors, messages, result = filter_results(
                spec.dispatch(self.root_folder, rel_path, self.config, self.engine)
            )
            yield from messages
            if result is False:
                return

            backup_files = result

            found_error = False
            if self.config.backup_landing_zone.folder:
                for path in sorted(backup_files):
                    for result in self._backup(path, found_errors):
                        if isinstance(result, Exception):
                            found_error = True
                        yield result

            if found_error:
                yield "skip cleanup due to failed backup"
                return
            yield from spec.cleanup(
                self.root_folder, rel_path, self.config, self.engine, backup_files
            )
            return

        if ext == ".yaml":
            dispatcher_method = self.dispatch_yaml
        elif ext in SUPPORTED_SCRIPT_EXTENSIONS:
            dispatcher_method = self.dispatch_script
        elif str(rel_path) == ".rename.json":
            dispatcher_method = self.dispatch_rename
        else:
            dispatcher_method = self.dispatch_raw_file_conversion

        full_path = self.root_folder / rel_path
        # sometimes we have delay between os events and actual "existence" on linux:
        ok = wait_until_exists(full_path)
        if not ok:
            # sometimes we get two events for the same file
            return

        yield from dispatcher_method(rel_path)

    def dispatch_rename(self, rel_path):
        rename_data = json.load((self.root_folder / rel_path).open())
        rel_path_rename = rename_data["yaml_path"]
        yaml_spec = find_yaml(rel_path_rename)
        if yaml_spec is None:
            yield InvalidOperationError(f"don't know how to handle {rel_path_rename}")
            return
        yield from yaml_spec.rename_in_db(self.config, self.engine, rename_data)
        yield from yaml_spec.rename_in_operational(self.root_folder, rename_data)
        try:
            found_errors, messages, __ = filter_results(self.backup([rel_path]))
            yield from messages
            remove_rename_json(self.root_folder)
            yield "removed .rename.json"
        except IOError as e:
            yield e

    def dispatch_script(self, rel_path):
        yield from self.backup([rel_path])

    def _files_for_backup(self, rel_paths):
        for rel_path in rel_paths:
            entity = find_entity(rel_path)

            if entity is None:
                patterns = [rel_path]
            else:
                patterns = entity.backup_files(self.root_folder, rel_path)

            for pattern in patterns:
                pattern = Path(pattern)
                if not pattern.is_absolute():
                    pattern = self.full_path(pattern)

                for path in sorted(glob.glob(str(pattern))):
                    path = Path(path).relative_to(self.root_folder)
                    yield path

    def backup(self, rel_paths, found_errors=False):
        if not self.config.backup_landing_zone.folder:
            yield "skip backup, no backup folder configured"
            return

        for path in sorted(self._files_for_backup(rel_paths)):
            yield from self._backup(path, found_errors)

    def _backup(self, rel_path, found_errors):
        backup_path = Path(self.config.backup_landing_zone.folder) / rel_path
        if backup_path.is_dir():
            return
        try:
            backup_path.parent.mkdir(parents=True, exist_ok=True)
            backup_path_with_ts = f"{backup_path}.{self.time_provider()}"

            # add time stamp to make file unique and better searchable:
            if found_errors:
                backup_path_with_ts += "-broken"

            shutil.copy(self.full_path(rel_path), backup_path_with_ts)
            yield f"copied backup of {rel_path} to {backup_path_with_ts}"
        except IOError as e:
            yield IOError(f"backup of {rel_path} failed: {e}")

    def full_path(self, a):
        return self.root_folder / a

    def rel_path(self, p):
        return p.relative_to(self.root_folder)

    def dispatch_raw_file_conversion(
        self,
        rel_path_raw_data,
    ):
        spec = find_raw_data_decl(rel_path_raw_data)

        if spec is None or not issubclass(spec, RawDataFile):
            yield f"ignore file {rel_path_raw_data}"
            return

        found_errors = False
        backup_files = [rel_path_raw_data]
        full_path_raw_data = self.root_folder / rel_path_raw_data

        try:
            scripts, raw_data_files, conversion_spec_path = spec.setup_conversion(
                self.root_folder, rel_path_raw_data
            )
            if len(scripts) > 1:
                found_errors = True
                yield ConsistencyError(
                    f"found multiple scripts for {rel_path_raw_data}"
                )
                return
            if len(scripts) == 0:
                found_errors = True
                yield ConsistencyError(f"found no script for {rel_path_raw_data}")
                return

            rel_path_script = scripts[0]
            full_path_script = self.root_folder / rel_path_script

            if conversion_spec_path is not None:
                full_path_conversion_spec = self.root_folder / conversion_spec_path
            else:
                full_path_conversion_spec = None

            yield "convert {} with {}".format(rel_path_raw_data, rel_path_script)

            started_conversion = time.time()
            tmp_root = tempfile.mkdtemp()

            try:
                found_errors, messages, result = filter_results(
                    self.conversion_runner.run_conversion(
                        full_path_script,
                        full_path_raw_data,
                        full_path_conversion_spec,
                        tmp_root=tmp_root,
                    )
                )
            except Exception as e:
                yield DataPoolException(
                    f"converting {rel_path_raw_data} with {rel_path_script} failed: {e}"
                )
                return
            finally:
                shutil.rmtree(tmp_root, ignore_errors=True)

            for msg in messages:
                yield msg

            if found_errors:
                return

            empty = False
            if result is None:
                empty = True
            else:
                output_file, facts = result
                empty = not facts
                needed = time.time() - started_conversion

                yield f"wrote results of running conversion script to {output_file}"
                yield "timing: converting {} needed {:.0f} msec".format(
                    rel_path_raw_data, 1000 * needed
                )

                raw_file_conversion_time.labels(spec.entity_name).set(needed)

            if empty:
                yield "conversion produced empty file"
                backup_files = [
                    p.relative_to(self.root_folder)
                    for p in sorted(scripts + raw_data_files)
                ]

                if conversion_spec_path is not None:
                    backup_files += [conversion_spec_path.relative_to(self.root_folder)]

                return

            found_errors, messages, _ = filter_results(
                (msg for fact in facts for msg in spec.check_fields(fact))
            )
            yield from messages
            if found_errors:
                return

            found_errors, messages, _ = filter_results(
                (
                    msg
                    for msg in spec.prepare_facts(
                        facts,
                        full_path_script,
                        [full_path_raw_data],
                        full_path_conversion_spec,
                    )
                )
            )
            yield from messages
            if found_errors:
                return

            found_errors, messages, _ = filter_results(
                (msg for msg in spec.check_if_facts_conflict(facts)),
            )
            yield from messages
            if found_errors:
                return

            ctx = Context(self.engine, spec.get_db_related_coordinates())

            found_errors, messages, _ = filter_results(
                (
                    msg
                    for fact in facts
                    for msg in (spec.check_fact_against_db(ctx, fact))
                )
            )
            yield from messages
            if found_errors:
                return

            started_commit = time.time()
            found_errors, messages, result = filter_results(
                spec.retrieve_existing_facts(ctx, facts)
            )
            yield from messages
            if found_errors:
                return

            new_facts, modified_facts = result
            if modified_facts:
                yield (
                    f"detected {len(modified_facts)} invalid modifications of facts"
                    " already in db"
                )

            duplicates = len(facts) - len(new_facts) - len(modified_facts)
            if duplicates:
                yield f"{duplicates} facts are already in db"

            if new_facts:
                yield from spec.commit_facts(ctx, new_facts)

            needed = time.time() - started_commit

            conversion_to_db_time.set(needed)

            yield (
                f"timing: check and commit {rel_path_raw_data}"
                f" needed {1000 * needed:.0f} msec"
            )

            facts_processed.labels(spec.entity_name).set(len(facts))
            dispatched_raw_file_counter.labels(spec.entity_name).inc()

            backup_files = [
                p.relative_to(self.root_folder)
                for p in sorted(scripts + raw_data_files)
            ]

            if conversion_spec_path is not None:
                backup_files += [conversion_spec_path.relative_to(self.root_folder)]

        finally:
            found_error = False
            for result in self.backup(backup_files, found_errors=found_errors):
                if isinstance(result, Exception):
                    found_error = True
                yield result

            if found_error:
                yield "skip cleanup due to failed backup"
                return

            try:
                full_path_raw_data.unlink()
            except IOError as e:
                yield IOError(f"failed to remove {full_path_raw_data}: {e}")
                return
            yield f"removed {full_path_raw_data}"

    def dispatch_yaml(self, rel_path):
        found_errors = False
        try:
            try:
                yaml = parse_file(self.root_folder / rel_path)
            except YamlError as e:
                yield InvalidOperationError(f"invalid yaml {rel_path}: {e}")
                return

            yaml_spec = find_yaml(rel_path)

            if yaml_spec is None:
                yield f"ignore {rel_path}"
                return

            found_errors, messages, __ = filter_results(
                yaml_spec.check_yaml(
                    self.root_folder, rel_path, yaml, self.config, is_dispatcher=True
                )
            )
            yield from messages
            if found_errors:
                return

            found_errors, messages, yaml = filter_results(
                yaml_spec.prepare(self.root_folder, rel_path, yaml)
            )
            yield from messages
            if found_errors:
                return

            found_errors, messages, dbos = filter_results(
                yaml_spec.check_against_db(self.config, self.engine, yaml)
            )
            yield from messages
            if found_errors or dbos is None:
                return

            messages = yaml_spec.commit_dbos(self.config, self.engine, dbos)
            yield from messages

        finally:
            found_errors, messages, __ = filter_results(
                self.backup([rel_path], found_errors=found_errors)
            )
            yield from messages


def report_errors(messages, task, max_errors=30):
    errors = []
    for msg in messages:
        if isinstance(msg, str):
            yield msg
        else:
            errors.append(msg)
    if not errors:
        return
    num_errors = len(errors)
    yield f"{num_errors} errors when {task}"
    for e in errors[:max_errors]:
        yield "{}".format(e)
    if num_errors > max_errors:
        yield f"skipped {num_errors - max_errors} error messages"
