#!/usr/bin/env python
import re
from datetime import date, datetime

from sqlalchemy import Date, DateTime, Float, Integer, String


def string_parser(what):
    if what is None:
        raise ValueError(f"value {what!r} is not a string")
    return str(what)


def int_parser(what):
    try:
        return int(what)
    except ValueError:
        raise ValueError(f"value {what!r} can not be converted to integer value")


def float_parser(what):
    try:
        return float(what)
    except ValueError:
        raise ValueError(f"value {what!r} can not be converted to float value")


def _date_time_parser(value):
    """parse for match with %Y, then %Y-%m, ...."""

    for fmt in ("%Y-%m-%d:%H:%M:%S", "%Y/%m/%d %H:%M:%S"):
        for end_index in range(2, len(fmt) + 1, 3):
            try:
                return datetime.strptime(value, fmt[:end_index])
            except ValueError:
                continue
    raise ValueError(f"value {value!r} can not be converted to datetime value")


def _date_parser(value):
    """parse for match with %Y, then %Y-%m, ...."""

    for fmt in ("%Y-%m-%d", "%Y/%m/%d"):
        try:
            return datetime.strptime(value, fmt).date()
        except ValueError:
            continue
    raise ValueError(f"value {value!r} can not be converted to datetime value")


_regexs_date_time = [
    re.compile(r"^(\d{4})-(\d{1,2})-(\d{1,2})[ :](\d{1,2}):(\d{2}):(\d{2})$"),
    re.compile(r"^(\d{4})/(\d{1,2})/(\d{1,2})[ :](\d{1,2}):(\d{2}):(\d{2})$"),
]

_regexs_date = [
    re.compile(r"^(\d{4})-(\d{1,2})-(\d{1,2})$"),
    re.compile(r"^(\d{4})/(\d{1,2})/(\d{1,2})$"),
]


def date_time_parser(
    value,
    *,
    allow_date_only=False,
):
    for regex in _regexs_date_time:
        match = re.match(regex, value)
        if not match:
            continue
        try:
            year, month, day, hour, minute, second = map(int, match.groups())
        except ValueError:
            raise ValueError(f"datetime {value!r} has non-numeric entries")
        return datetime(year, month, day, hour, minute, second)

    if not allow_date_only:
        raise ValueError(
            f"datetime {value!r} is not of format YY-MM-DD HH:MM:SS"
            " YY-MM-DD:HH:MM:SS YY/MM/DD HH:MM:SS or YY/MM/DD:HH:MM:SS"
        )

    for regex in _regexs_date:
        match = re.match(regex, value)
        if not match:
            continue
        try:
            year, month, day = map(int, match.groups())
        except ValueError:
            raise ValueError(f"date {value!r} has non-numeric entries")
        return datetime(year, month, day)
    raise ValueError(
        f"datetime {value!r} is not of format YY-MM-DD HH:MM:SS"
        " YY-MM-DD:HH:MM:SS YY/MM/DD HH:MM:SS, YY/MM/DD:HH:MM:SS"
        " YY:MM:DD or YY/MM/DD"
    )


def date_parser(
    value,
):
    for regex in _regexs_date:
        match = re.match(regex, value)
        if not match:
            continue
        try:
            year, month, day = map(int, match.groups())
        except ValueError:
            raise ValueError(f"date {value!r} has non-numeric entries")
        return date(year, month, day)
    raise ValueError(f"date {value!r} is not of format YY-MM-DD or YY/MM/DD")


def json_parser(what):
    if isinstance(what, (dict, list)):
        return what
    raise ValueError(f"value {what!r} is not json")


parsers = {
    String: string_parser,
    Integer: int_parser,
    Float: float_parser,
    DateTime: date_time_parser,
    Date: date_parser,
}


def has_parser(db_col_type):
    return db_col_type in parsers
