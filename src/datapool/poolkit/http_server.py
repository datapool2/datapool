#! /usr/bin/env python
# encoding: utf-8

import importlib
import logging
import socket
from collections import deque
from contextlib import contextmanager
from datetime import datetime
from threading import Thread

from flask import Flask, current_app, request
from prometheus_client import make_wsgi_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.serving import make_server

from .utils import get_logger

logging.getLogger("werkzeug").setLevel(logging.ERROR)

# Create my app
app = Flask(__name__)


# Add prometheus wsgi middleware to route /metrics requests
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})


@app.route("/logs/")
@app.route("/logs/<int:lines>")
def logs(lines=1000):
    return latest_logs(lines, app.config["log_file"])


status = "starting"
dispatched_files_counter = 0


def set_status(new_status):
    global status
    status = new_status


def increment_dispatched_files_counter():
    global dispatched_files_counter
    dispatched_files_counter += 1


def reset_dispatched_files_counter():
    global dispatched_files_counter
    dispatched_files_counter = 0


@app.route("/")
def index():
    datapool = __package__.split(".")[0]
    version = importlib.import_module(datapool).__version__
    return dict(
        status=status,
        version=version,
        started=str(current_app.started),
        running=str(datetime.now() - current_app.started),
        dispatched_files_counter=dispatched_files_counter,
    )


def log_request():
    print(
        f"- {datetime.now()}: webserver got {request.method} request from"
        f" {request.remote_addr} for {request.url}"
    )


class BgServer(Thread):
    def __init__(self, port, log_file, app):
        super().__init__()
        app.config["log_file"] = log_file
        self.s = make_server("", port, app)

    def run(self):
        self.s.serve_forever()

    def stop(self):
        self.s.shutdown()
        self.join()


class DataPoolHttpServer:
    def __init__(self, port=8000, log_file=None, log_requests=True, print_ok=print):
        self.port = port
        self.log_file = log_file
        self.log_requests = log_requests
        self.http_server = None
        self.print_ok = print_ok

    def start(self):
        self.print_ok("- start http server on port {}".format(self.port))
        get_logger().info("started web server")
        app.started = datetime.now()
        if self.log_requests:
            app.before_request(log_request)
        self.http_server = BgServer(self.port, self.log_file, app)

        self.http_server.start()

    def stop(self):
        if self.http_server is None:
            raise RuntimeError("you must start server first.")

        self.print_ok("- stop http server")
        self.http_server.stop()
        get_logger().info("web server shut down")
        self.print_ok("- stopped http server")


def port_is_in_use(port):
    # from https://stackoverflow.com/questions/2470971

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(("localhost", port)) == 0


def _get_logs(n, log_file):
    try:
        return list(deque(iter(open(log_file, "r").readline, ""), n))
    except IOError as e:
        return [f"error: could not read {log_file}: {e}"]


@contextmanager
def patch_get_logs(function):
    globals()["_get_logs"] = function
    try:
        yield
    finally:
        globals()["_get_logs"] = _get_logs


def latest_logs(n=1000, log_file=None):
    if log_file is None:
        return "no log file configured"

    output = _get_logs(n, log_file)
    # includes code to automatically scroll down in browser, incl.
    # scroll down after reload. see also
    # https://stackoverflow.com/questions/3664381
    result = """
    <html>
        <body>
            <pre>%s
            </pre>
            <div id="end" />
        </body>

        <script>
             window.onload = function() {
                    var element = document.getElementById("end");
                    element.scrollTop = element.scrollHeight;
                    element.scrollIntoView(false);
                    console.log(element);
                };
             window.onbeforeunload  = window.onload;
        </script>
    </html>
""" % ("".join(output),)
    return result
