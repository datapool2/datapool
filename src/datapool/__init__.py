import pkg_resources

# registers classes withing poolkit framework:
from . import domain_model  # noqa: F401

__author__ = "Uwe Schmitt"
__email__ = "uwe.schmitt@id.ethz.ch"
__credits__ = "ETH Zurich, Scientific IT Services & EAWAG"

__version__ = pkg_resources.require(__package__)[0].version
