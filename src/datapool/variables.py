#!/usr/bin/env python

from collections import Counter
from pathlib import Path

from sqlalchemy import Column, Integer, String, Table, delete, select, update

from datapool.poolkit.database import (
    Base,
    commit_dbo,
    fetch_many,
    find_differences,
    find_duplicates,
)
from datapool.poolkit.yaml import List, NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class VariableDecl(Base):
    _schema = Table(
        "variable",
        Base._metadata,
        Column("variable_id", Integer(), primary_key=True),
        Column("name", String(), unique=True, index=True, nullable=False),
        Column("unit", String(), nullable=False),
        Column("description", String()),
    )


variable_yaml_example = """
-
   name: Battery Voltage
   unit: V
   description:
-
   name: Distance Reading Count
   unit: 1
   description:
-
   name: Distance
   unit: m
   description:
-
   name: Water Level
   unit: mm
   description:
-
   name: Average Flow Velocity
   unit: m/s
   description:

-
   name: Flow
   unit: m/s
   description:
-
   name: Number of Samples
   unit: 1
   description:
-
   name: Peak to Mean Ratio
   unit: 1
   description:
-
   name: Surcharge Level
   unit: mm
   description:
-
   name: Surface Flow Velocity
   unit: cm/s
   description:
-
   name: Temperature
   unit: C
   description:
-
   name: images
   unit: 1
   description: binary image
"""


class VariableYaml(Yaml):
    entity_name = "variable"
    is_fact_dimension = True

    # single unique yaml might be special!? -> considers this when abstratingt the
    # framework later?

    location = "data/variables.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [("data/variables.yaml", variable_yaml_example)]

    dbos = [VariableDecl]

    mapping = lambda config: List(
        {
            "name": NotEmpty(VariableDecl.name),
            "description": VariableDecl.description,
            "unit": VariableDecl.unit,
        }
    )

    @classmethod
    def _read_yaml(cls, variables_file_path):
        if variables_file_path.exists():
            try:
                return parse_file(variables_file_path)
            except YamlError as e:
                return YamlError(f"file {variables_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["name", "description", "unit"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        variables_file_path = root_folder / cls.location
        variables = cls._read_yaml(variables_file_path)
        if isinstance(variables, Exception):
            raise variables

        name_to_variables = {p["name"]: p for p in variables}

        name = ask("name", not_empty=True)
        if name in name_to_variables:
            raise ValueError(f"variable with name '{name}' exists already")

        description = ask("description")
        unit = ask("unit", not_empty=True)

        variables.append(dict(name=name, description=description, unit=unit))

        parent = variables_file_path.parent
        parent.mkdir(parents=True, exist_ok=True)

        with variables_file_path.open("w") as fh:
            print_yaml(variables, fh=fh)

        return variables_file_path

    @classmethod
    def find_by_name(cls, root_folder, name):
        variables = cls._read_yaml(root_folder / cls.location)
        if isinstance(variables, Exception):
            yield variables
            return
        for entry in variables:
            if entry["name"] == name:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = VariableDecl._schema
        yield engine.execute(
            select([schema.c.variable_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)
        counter = Counter(entry.get("name") for entry in yaml)
        for name, count in counter.items():
            if name is None:
                continue
            if count > 1:
                yield ValueError(f"variable with name '{name}' appears {count} times")

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, variables):
        messages = []

        duplicates, unique_variables = find_duplicates(variables, ["name"])

        reported_names = set()
        for duplicate in duplicates:
            name = duplicate["name"]
            if name in reported_names:
                continue
            reported_names.add(name)
            messages.append(f"duplicate entry with name '{name}'")

        existing_variable_dbos = fetch_many(engine, VariableDecl)
        (
            new_variable_dbos,
            modified_variable_dbos_ok,
            modified_variable_dbos_not_ok,
            deleted_variable_dbos,
            same,
        ) = find_differences(
            VariableDecl,
            unique_variables,
            existing_variable_dbos,
            ["name"],
            ["unit", "description"],
        )

        for variable_dbo in deleted_variable_dbos:
            name = variable_dbo["name"]
            messages.append(f"variable '{name}' was deleted")

        for variable_dbo in modified_variable_dbos_not_ok:
            name = variable_dbo["name"]
            messages.append(f"variable '{name}' was modified")

        if messages:
            yield from map(ValueError, messages)
            return

        yield new_variable_dbos + modified_variable_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        for dbo in dbos:
            commit_dbo(engine, dbo)
        return
        yield

    @classmethod
    def delete_from_db(cls, config, engine, variable_id):
        engine.execute(
            delete(VariableDecl._schema).where(
                VariableDecl._schema.c.variable_id == variable_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        variables = cls._read_yaml(yaml_path)
        if isinstance(variables, Exception):
            yield variables
            return
        variables = [p for p in variables if p["name"] != name]
        with yaml_path.open("w") as fh:
            print_yaml(variables, fh=fh)

        yield f"deleted variable '{name}' from {yaml_path}"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
        except YamlError as e:
            yield e
            return
        found = False
        for variable in data:
            if variable["name"] == old_name:
                found = True
                variable["name"] = new_name
        if found is False:
            yield ValueError(f"variable with name '{old_name}' does not exist")
            return

        try:
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except IOError as e:
            yield e
            return

        yield f"changed variable name in {yaml_path} from '{old_name}' to '{new_name}'"

        lz_root = yaml_path.parent.parent

        affected_files_before = [yaml_path.relative_to(lz_root)]

        all_children = [p.relative_to(lz_root) for p in (lz_root / "data").glob("**/*")]

        affected_files_before += all_children

        p = Path("data/variables.yaml")
        yield p, {p: p}

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        variable_id = rename_data["item_id"]
        table = VariableDecl._schema
        engine.execute(delete(table).where(table.c.variable_id == variable_id))
        yield f"deleted variable with variable_id={variable_id}"

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        variable_id = rename_data["item_id"]
        table = VariableDecl._schema
        engine.execute(
            update(table)
            .where(table.c.variable_id == variable_id)
            .values(name=new_name)
        )
        yield f"renamed variable with variable_id={variable_id} to {new_name}"

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        return
        yield
