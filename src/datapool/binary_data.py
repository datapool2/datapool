#!/usr/bin/env python

import operator
import pathlib

import numpy as np
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    LargeBinary,
    Table,
    and_,
    delete,
    func,
    insert,
    join,
    select,
)

from .poolkit.binary_data_packing import pack_arrays
from .poolkit.database import Base
from .poolkit.dispatcher import CustomizedDispatcher
from .poolkit.type_handling import date_parser, date_time_parser
from .poolkit.utils import Asset, print_query
from .poolkit.yaml import RawDataFile
from .poolkit.yaml_parser import YamlError, parse_file
from .site import SiteDecl
from .source import SourceDecl
from .variables import VariableDecl


class BinaryDataDecl(Base):
    _schema = Table(
        "binary_data",
        Base._metadata,
        Column("binary_data_id", Integer(), primary_key=True),
        Column("data", LargeBinary(), nullable=False),
        Column("timestamp", DateTime(), nullable=False, index=True),
        Column("variable_id", ForeignKey("variable.variable_id"), nullable=False),
        Column("source_id", ForeignKey("source.source_id"), nullable=False),
        Column("site_id", ForeignKey("site.site_id"), nullable=True),
    )


binary_data_example = Asset("assets/binary_data.npz")

binary_spec_example = """
site: test_site
variable: images
timestamp: 2020-12-22 13:30:00
file: images.npz
"""

BINARY_SPEC_YAML = "binary_data_spec.yaml"


class Dispatcher(CustomizedDispatcher):
    @classmethod
    def check(cls, root_folder, rel_path, config, engine):
        if rel_path.name != BINARY_SPEC_YAML:
            files = [p.name for p in (root_folder / rel_path).parent.iterdir()]
            if BINARY_SPEC_YAML not in files:
                yield ValueError(
                    f"found data file at {rel_path} but no {BINARY_SPEC_YAML}"
                )
                return
            yield f"skip {rel_path}"
            return

        spec_path = root_folder / rel_path
        try:
            spec = parse_file(spec_path)
        except YamlError as e:
            yield YamlError(f"failed to parse {spec_path}: {e}.")
            return

        expected_keys = ["site", "variable", "timestamp", "file"]
        missing = set(expected_keys) - set(spec)
        unknown = set(spec) - set(expected_keys)

        if missing or unknown:
            for m in sorted(missing):
                yield ValueError(f"{BINARY_SPEC_YAML} has no entry for '{m}'")
            for u in sorted(unknown):
                yield ValueError(f"{BINARY_SPEC_YAML} has unknown field '{u}'")
            return

        if any(not isinstance(v, str) for v in spec.values()):
            yield ValueError(
                f"{BINARY_SPEC_YAML} malformed: field values must not be lists or"
                " mappings"
            )
            return

        timestamp = spec["timestamp"]
        try:
            timestamp = date_time_parser(timestamp)
        except ValueError:
            yield ValueError(f"timestamp '{timestamp}' is malformed")
            return

        data_path = (root_folder / rel_path).parent / spec["file"]

        if data_path.suffix != ".npz":
            yield ValueError("can only handle .npz files.")
            return

        if not data_path.exists():
            yield IOError(f"specified file {data_path} does not exist.")
            return

        try:
            with data_path.open():
                pass
        except IOError as e:
            yield IOError(f"failed to open {data_path} for reading: {e}.")
            return

        try:
            npz_handle = np.load(data_path)
        except Exception as e:
            yield ValueError(f"{data_path} might be corrupt: {e}")
        data = pack_arrays(list(npz_handle.values()))

        source = rel_path.parent.parent.name
        site = spec["site"]
        variable = spec["variable"]

        yield from cls._insert(engine, source, site, variable, timestamp, data)

    @classmethod
    def _insert(cls, engine, source, site, variable, timestamp, data):
        site_table = SiteDecl._schema
        source_table = SourceDecl._schema
        variable_table = VariableDecl._schema
        binary_data_table = BinaryDataDecl._schema

        site_id = engine.execute(
            select([site_table.c.site_id]).where(site_table.c.name == site)
        ).scalar()

        variable_id = engine.execute(
            select([variable_table.c.variable_id]).where(
                variable_table.c.name == variable
            )
        ).scalar()

        source_id = engine.execute(
            select([source_table.c.source_id]).where(source_table.c.name == source)
        ).scalar()

        if None in (site_id, variable_id, source_id):
            if site_id is None:
                yield ValueError(f"site {site} is not known")
            if variable_id is None:
                yield ValueError(f"variable {variable} is not known")
            if source_id is None:
                yield ValueError(f"source {source} is not known")
            return

        existing = engine.execute(
            select([binary_data_table.c.data]).where(
                and_(
                    binary_data_table.c.site_id == site_id,
                    binary_data_table.c.variable_id == variable_id,
                    binary_data_table.c.source_id == source_id,
                    binary_data_table.c.timestamp == timestamp,
                    binary_data_table.c.data == data,
                )
            )
        ).scalar()

        if existing is not None:
            yield "same data entry already exists in db"
            return

        engine.execute(
            insert(binary_data_table).values(
                site_id=site_id,
                variable_id=variable_id,
                source_id=source_id,
                timestamp=timestamp,
                data=data,
            )
        )
        yield f"inserted {len(data)} bytes into database"

    @classmethod
    def dispatch(cls, root_folder, rel_path, config, engine):
        try:
            if rel_path.name != BINARY_SPEC_YAML:
                yield f"skip {rel_path}"
                yield False
                return
            # backup files:
            yield from cls.check(root_folder, rel_path, config, engine)
            yield [
                p.relative_to(root_folder)
                for p in (root_folder / rel_path).parent.iterdir()
            ]
        except Exception as e:
            yield e
            yield False


class BinaryData(Dispatcher, RawDataFile):
    depends_on = ["source", "variable", "site"]
    script_depends_on = []

    entity_name = "binary_data"

    def examples(config):
        _example_root = pathlib.Path(
            "data/sensor_from_xyz/sensor_abc_from_xyz/binary_data"
        )
        return [
            (
                _example_root / "images.npz",
                binary_data_example,
            ),
            (
                _example_root / BINARY_SPEC_YAML,
                binary_spec_example,
            ),
        ]

    binary_root = pathlib.Path("data/*/*/binary_data")

    location = (binary_root / BINARY_SPEC_YAML, binary_root / "*.npz")

    @classmethod
    def related_files(cls):
        yield cls.binary_root / "*.npz"
        yield cls.binary_root / BINARY_SPEC_YAML

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def cleanup(cls, root_folder, rel_path, config, engine, backup_folder):
        folder = rel_path.parent
        for p in [(root_folder / folder / BINARY_SPEC_YAML)] + sorted(
            (root_folder / folder).glob("*.npz")
        ):
            p.unlink()
            yield f"removed {p.relative_to(root_folder)}"

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        yield from backup_folder.glob(str(cls.binary_root / "*"))

    @classmethod
    def data_files(cls, lz):
        # those which trigger check
        yield from lz.glob(str(cls.binary_root / BINARY_SPEC_YAML))

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        from .site import SiteDecl
        from .source import SourceDecl
        from .variables import VariableDecl

        binary_data_table = BinaryDataDecl._schema
        pd = VariableDecl._schema
        sited = SiteDecl._schema
        sourced = SourceDecl._schema

        j = join(
            binary_data_table, pd, binary_data_table.c.variable_id == pd.c.variable_id
        )
        j = join(j, sited, binary_data_table.c.site_id == sited.c.site_id, isouter=True)
        j = join(j, sourced, binary_data_table.c.source_id == sourced.c.source_id)

        stmt = (
            select(
                [
                    binary_data_table.c.binary_data_id,
                    binary_data_table.c.timestamp,
                    func.length(binary_data_table.c.data).label("number_of_bytes"),
                    pd.c.name.label("variable"),
                    sourced.c.name.label("source"),
                    sited.c.name.label("site"),
                ]
            )
            .where(and_(*checks))
            .select_from(j)
        )

        header = [
            "binary_data_id",
            "timestamp",
            "number_of_bytes",
            "variable",
            "source",
            "site",
        ]

        print_query(engine, stmt, header, indent, print_, max_rows)

    @classmethod
    def count_facts(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        yield engine.execute(
            select([func.count(BinaryDataDecl._schema.c.binary_data_id)]).where(
                and_(*checks)
            )
        ).scalar()

    @classmethod
    def supported_fields_for_delete(cls):
        return set(
            (
                "timestamp",
                "site",
                "source",
                "source_type",
                "variable",
                "project",
            )
        )

    @classmethod
    def _translate_conditions(cls, engine, conditions):
        from .projects import ProjectDecl
        from .site import SiteDecl
        from .source import SourceDecl
        from .source_type import SourceTypeDecl
        from .variables import VariableDecl

        foreign_table_schemata = {
            "variable": VariableDecl._schema,
            "site": SiteDecl._schema,
            "source": SourceDecl._schema,
        }

        binary_data_schema = BinaryDataDecl._schema

        supported_names = cls.supported_fields_for_delete()

        errors = []

        checks = []
        for name, op, value in conditions:
            if name not in supported_names:
                s = ", ".join(sorted(supported_names))
                errors.append(f"field {name} does not exist, please chose one of {s}")
                continue
            if name == "source_type":
                if op != "==":
                    errors.append("source types can only be filtered using '=='")
                    continue
                source_type_id = engine.execute(
                    select([SourceTypeDecl._schema.c.source_type_id]).where(
                        SourceTypeDecl._schema.c.name == value
                    )
                ).scalar()

                if source_type_id is None:
                    errors.append(f"source_type {value!r} does not exist")
                    continue
                source_ids = engine.execute(
                    select([SourceDecl._schema.c.source_type_id]).where(
                        SourceDecl._schema.c.source_type_id == source_type_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(binary_data_schema.c.source_id.in_(source_ids))
                continue

            if name == "project":
                if op != "==":
                    errors.append("projects can only be filtered using '=='")
                    continue
                project_id = engine.execute(
                    select([ProjectDecl._schema.c.project_id]).where(
                        ProjectDecl._schema.c.title == value
                    )
                ).scalar()

                if project_id is None:
                    errors.append(f"project with title {value!r} does not exist")
                    continue

                source_ids = engine.execute(
                    select([SourceDecl._schema.c.project_id]).where(
                        SourceDecl._schema.c.project_id == project_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(binary_data_schema.c.source_id.in_(source_ids))
                continue

            if name in foreign_table_schemata:
                schema = foreign_table_schemata[name]
                item_id = engine.execute(
                    select([getattr(schema.c, name + "_id")]).where(
                        schema.c.name == value
                    )
                ).scalar()
                if item_id is None:
                    errors.append(f"{name} {value!r} does not exist")
                    continue
                if op not in ("==", "!="):
                    errors.append(f"{name} can only be filtered using '==' and '!=")
                    continue
                if op == "==":
                    checks.append(
                        getattr(binary_data_schema.c, name + "_id") == item_id
                    )
                else:
                    checks.append(
                        getattr(binary_data_schema.c, name + "_id") != item_id
                    )
                continue

            if name.endswith("_id"):
                if op != "==":
                    errors.append("ids can only be filtered using '=='")
                    continue
                try:
                    value = int(value)
                except ValueError:
                    errors.append(f"{name} {value} is not valid integer")
                    continue

                checks.append(getattr(binary_data_schema.c, name) == value)
                continue

            if name == "timestamp":
                try:
                    value = date_time_parser(value)
                except ValueError:
                    try:
                        value = date_parser(value)
                    except ValueError:
                        errors.append(
                            ValueError(f"'{value}' is neither a timestamp nor a date")
                        )
                        continue

            op_method = {
                "<": operator.lt,
                ">": operator.gt,
                "<=": operator.le,
                ">=": operator.ge,
                "==": operator.eq,
                "!=": operator.ne,
            }[op]

            checks.append(op_method(getattr(binary_data_schema.c, name), value))

        return errors, checks

    @classmethod
    def delete(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        binary_data_table = BinaryDataDecl._schema
        condition = and_(*checks)
        engine.execute(delete(binary_data_table).where(condition))
