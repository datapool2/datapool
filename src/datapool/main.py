#!/usr/bin/env python
import logging
import os


# patch getuid when used in container:
def getuid(_orig=os.getuid):
    return int(os.environ.get("HOSTUID", _orig()))


os.getuid = getuid


from datapool.poolkit.main import (  # noqa: E402
    AppConfig,
    set_app_config,
    setup_add,
    setup_admin,
    setup_check,
    setup_check_config,
    setup_create_example,
    setup_delete_entity,
    setup_delete_facts,
    setup_init_config,
    setup_init_db,
    setup_main,
    setup_replay,
    setup_run_server,
    setup_show,
    setup_start_develop,
    setup_start_rename,
    setup_update_operational,
    setup_version,
)

from . import __version__, domain_model  # noqa: F401, E402

app_config = AppConfig("datapool", "pool", __version__)
set_app_config(app_config)

logger = logging.getLogger("poolkit")
logger.addHandler(logging.StreamHandler())
logger.setLevel(int(os.environ.get("LOG_LEVEL", logging.WARNING)))

main = setup_main()

setup_add(main)
setup_check(main)
setup_create_example(main)
setup_delete_entity(main)
setup_delete_facts(main)
setup_start_develop(main)
setup_start_rename(main)
setup_update_operational(main)

admin = setup_admin(main)

setup_check_config(admin)
setup_init_config(admin)
setup_init_db(admin)
setup_run_server(admin)
setup_show(admin)
setup_version(admin)
setup_replay(admin)
