#!/usr/bin/env python

import operator
import pathlib
from collections import Counter

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Table,
    and_,
    delete,
    func,
    join,
    select,
)

from datapool.poolkit.database import (
    JSON,
    Base,
    check_constraints,
    commit_dbo,
    create_dbo,
    fetch_many,
    fetch_one,
    find_differences,
)
from datapool.poolkit.dispatcher import CustomizedDispatcher
from datapool.poolkit.type_handling import date_parser, date_time_parser
from datapool.poolkit.utils import Asset, print_query
from datapool.poolkit.yaml import List, NotEmpty, Optional, RawDataFile, _check_yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file

from .meta_action_type import MetaActionTypeDecl
from .meta_data import MetaDataDecl
from .meta_flags import MetaFlagDecl
from .meta_log_type import MetaLogTypeDecl
from .meta_picture import MetaPictureDecl
from .persons import PersonDecl
from .projects import ProjectDecl
from .site import SiteDecl
from .source import SourceDecl
from .source_type import SourceTypeDecl


class MetaDataHistoryDecl(Base):
    _schema = Table(
        "meta_data_history",
        Base._metadata,
        Column("meta_data_history_id", Integer, primary_key=True, index=True),
        Column("meta_data_id", ForeignKey("meta_data.meta_data_id"), nullable=False),
        Column(
            "meta_log_type_id",
            ForeignKey("meta_log_type.meta_log_type_id"),
            nullable=False,
        ),
        Column(
            "meta_action_type_id",
            ForeignKey("meta_action_type.meta_action_type_id"),
            nullable=True,
        ),
        Column(
            "meta_flag_id",
            ForeignKey("meta_flag.meta_flag_id"),
            nullable=True,
        ),
        Column(
            "person_id",
            ForeignKey("person.person_id"),
            nullable=False,
        ),
        Column("timestamp_start", DateTime(), nullable=False, index=True),
        Column("timestamp_end", DateTime(), nullable=False, index=True),
        Column("comment", String()),
        Column("additional_meta_info", JSON()),
    )


# source / site is given by location
meta_data_history_yaml = """
-
    site: test_site
    source: sensor_abc_from_xyz
    meta_log_type: new_sensor
    meta_action_type: installed
    meta_flag: OK
    timestamp_start: 2020-12-24 00:00:00
    timestamp_end: 2020-12-31 00:00:00
    person: schmittu
    comment:
    pictures:
        -
            path: images/IMG_0312.JPG
            description:
        -
            path: images/IMG_0732.JPG
            description: manholeD1
    additional_meta_info:
        -
            name: test_entry
        -
            weather: nice

-
    site: test_site
    source: sensor_abc_from_xyz
    meta_log_type: new_sensor
    meta_action_type: checked
    meta_flag: ALERT
    timestamp_start: 2020-12-31 00:00:00
    timestamp_end: 2020-12-31 12:00:00
    person: schmittu
    comment: I set the sensor on fire
"""

picture_0 = Asset("assets/IMG_0312.JPG")
picture_1 = Asset("assets/IMG_0732.JPG")

META_DATA_HISTORY_YAML = "meta_data_history.yaml"


class Dispatcher(CustomizedDispatcher):
    mapping = List(
        {
            "site": NotEmpty(SiteDecl.name),
            "source": NotEmpty(SourceDecl.name),
            "meta_log_type": NotEmpty(MetaLogTypeDecl.name),
            "meta_action_type": MetaActionTypeDecl.name,
            "meta_flag": MetaFlagDecl.name,
            "timestamp_start": NotEmpty(MetaDataHistoryDecl.timestamp_start),
            "timestamp_end": NotEmpty(MetaDataHistoryDecl.timestamp_end),
            "person": NotEmpty(PersonDecl.abbreviation),
            "comment": MetaDataHistoryDecl.comment,
            "additional_meta_info": Optional(MetaDataHistoryDecl.additional_meta_info),
            "pictures": Optional(
                List(
                    {
                        "path": MetaPictureDecl.filename,
                        "description": MetaPictureDecl.description,
                    }
                )
            ),
        }
    )

    @classmethod
    def check(cls, root_folder, rel_path, config, engine):
        spec_path = root_folder / rel_path
        try:
            data = parse_file(spec_path)
        except YamlError as e:
            yield YamlError(f"failed to parse {spec_path}: {e}.")
            return

        found_error = False
        for result in cls._check_yaml(root_folder, rel_path, data):
            if isinstance(result, (str, Exception)):
                yield result
            if isinstance(result, Exception):
                found_error = True
        if found_error:
            return

        cls._prepare(root_folder, rel_path, data)

        dbos = None
        for result in cls._check_against_db(engine, data):
            if isinstance(result, (str, Exception)):
                yield result
            else:
                dbos = result
                break

        if dbos is None:
            yield RuntimeError("must never happen")

        yield from cls._commit_dbos(engine, dbos)

    @classmethod
    def dispatch(cls, root_folder, rel_path, config, engine):
        try:
            if rel_path.name != META_DATA_HISTORY_YAML:
                yield f"skip {rel_path}"
                yield False
                return
            # backup files:
            yield from cls.check(root_folder, rel_path, config, engine)
            yield [rel_path] + [
                p.relative_to(root_folder)
                for p in (root_folder / rel_path).parent.glob("*/**/*")
            ]
        except Exception as e:
            import traceback

            traceback.print_exc()
            yield e
            yield False

    @classmethod
    def _check_yaml(cls, root_folder, rel_path, yaml):
        found_error = False
        for error_message in _check_yaml(yaml, cls.mapping):
            yield ValueError(error_message)
            found_error = True
        if found_error:
            return

        root_folder = pathlib.Path(root_folder)
        local_folder = (root_folder / rel_path).parent

        for entry in yaml:
            pictures = entry.get("pictures")
            if pictures is not None:
                if not isinstance(pictures, list):
                    yield ValueError(
                        f"entry pictures={pictures!r} in {rel_path} must be a list"
                    )
                else:
                    for picture in pictures:
                        full_path = pathlib.Path(local_folder) / picture["path"]
                        if not full_path.exists():
                            rel_path_img = full_path.relative_to(root_folder)
                            yield ValueError(f"picture {rel_path_img} missing")

        counter = Counter(
            [
                picture["path"]
                for entry in yaml
                for picture in entry.get("pictures", [])
                if isinstance(picture, dict) and "path" in picture
            ]
        )

        for path, count in counter.items():
            if count == 1:
                continue
            yield ValueError(f"duplicate entries for file {path}")

    @classmethod
    def _prepare(cls, root_folder, rel_path, data):
        for entry in data:
            local_folder = (root_folder / rel_path).parent
            for picture in entry.get("pictures", []):
                full_path = pathlib.Path(local_folder) / picture["path"]
                if full_path.exists():
                    picture["data"] = full_path.read_bytes()
                    picture["filename"] = pathlib.Path(picture["path"]).name
                    del picture["path"]
                else:
                    raise RuntimeError("this should have been detected earlier")

            for k in ("timestamp_start", "timestamp_end"):
                entry[k] = date_time_parser(entry[k])

    @classmethod
    def _check_against_db(cls, engine, entries):
        ok = True
        picture_dbos = []
        meta_data_history_dbos = []
        for data in entries:
            source_name = data["source"]
            site_name = data["site"]
            meta_log_type = data["meta_log_type"]
            meta_action_type = data.get("meta_action_type", "")
            meta_flag = data.get("meta_flag", "")
            person_abbreviation = data["person"]

            source_dbo = fetch_one(engine, SourceDecl, name=source_name)
            site_dbo = fetch_one(engine, SiteDecl, name=site_name)
            person_dbo = fetch_one(engine, PersonDecl, abbreviation=person_abbreviation)
            meta_log_type_dbo = fetch_one(engine, MetaLogTypeDecl, name=meta_log_type)

            if not meta_log_type_dbo:
                yield ValueError(
                    f"meta_log_type with name '{meta_log_type}' does not exist"
                )
                ok = False

            meta_action_type_id = None
            if meta_action_type and meta_log_type_dbo:
                meta_action_type_dbo = fetch_one(
                    engine,
                    MetaActionTypeDecl,
                    name=meta_action_type,
                    meta_log_type_id=meta_log_type_dbo.meta_log_type_id,
                )
                if not meta_action_type_dbo:
                    yield ValueError(
                        f"meta_action_type with name '{meta_action_type}'"
                        f" meta_log_type '{meta_log_type}' does not exist"
                    )
                    ok = False
                else:
                    meta_action_type_id = meta_action_type_dbo.meta_action_type_id

            meta_flag_id = None
            if meta_flag:
                meta_flag_dbo = fetch_one(engine, MetaFlagDecl, name=meta_flag)
                if not meta_flag_dbo:
                    yield ValueError(
                        f"meta_flag with name '{meta_flag}' does not exist"
                    )
                    ok = False
                else:
                    meta_flag_id = meta_flag_dbo.meta_flag_id

            if not source_dbo:
                yield ValueError(f"source with name '{source_name}' does not exist")
                ok = False
            if not site_dbo:
                yield ValueError(f"site with name '{site_name}' does not exist")
                ok = False
            if not person_dbo:
                yield ValueError(
                    f"person with abbreviation '{person_abbreviation}' does not exist"
                )
                ok = False

            if source_dbo and site_dbo:
                meta_data_dbo = fetch_one(
                    engine,
                    MetaDataDecl,
                    site_id=site_dbo.site_id,
                    source_id=source_dbo.source_id,
                )

                if not meta_data_dbo:
                    yield ValueError(
                        f"meta_data for site '{site_name}' and source '{source_name}'"
                        " does not exist"
                    )
                    ok = False
            if not ok:
                continue

            del data["source"]
            del data["site"]

            person_id = data["person_id"] = person_dbo.person_id
            del data["person"]

            meta_log_type_id = meta_log_type_dbo.meta_log_type_id
            data["meta_log_type_id"] = meta_log_type_id
            del data["meta_log_type"]

            data["meta_action_type_id"] = meta_action_type_id
            data.pop("meta_action_type", None)

            meta_flag_id = data["meta_flag_id"] = meta_flag_id
            data.pop("meta_flag", None)

            meta_data_id = data["meta_data_id"] = meta_data_dbo.meta_data_id

            pictures = data.get("pictures", [])
            if pictures:
                del data["pictures"]

            timestamp_start = data["timestamp_start"]
            timestamp_end = data["timestamp_end"]

            meta_data_history_dbo = fetch_one(
                engine,
                MetaDataHistoryDecl,
                person_id=person_id,
                meta_log_type_id=meta_log_type_id,
                meta_action_type_id=meta_action_type_id,
                meta_flag_id=meta_flag_id,
                meta_data_id=meta_data_id,
                timestamp_start=timestamp_start,
                timestamp_end=timestamp_end,
            )

            if not meta_data_history_dbo:
                meta_data_history_dbo = create_dbo(MetaDataHistoryDecl, data)
                new_picture_dbos = [
                    create_dbo(MetaPictureDecl, picture) for picture in pictures
                ]
                meta_data_history_dbos.append(meta_data_history_dbo)
                picture_dbos.append(new_picture_dbos)
            else:
                existing_picture_dbos = fetch_many(
                    engine,
                    MetaPictureDecl,
                    meta_data_history_id=meta_data_history_dbo.meta_data_history_id,
                )
                (
                    new_picture_dbos,
                    modified_picture_dbos_ok,
                    modified_picture_dbos_not_ok,
                    deleted_picture_dbos,
                    same,
                ) = find_differences(
                    MetaPictureDecl, pictures, existing_picture_dbos, ["filename"], []
                )

                ok = True
                for picture in sorted(
                    deleted_picture_dbos, key=lambda p: p["filename"]
                ):
                    yield ValueError(f"picture {picture['filename']} was deleted")
                    ok = False

                for picture in sorted(
                    modified_picture_dbos_not_ok, key=lambda p: p["filename"]
                ):
                    yield ValueError(f"picture {picture['filename']} was modified")
                    ok = False

                if ok:
                    picture_dbos.append(new_picture_dbos + modified_picture_dbos_ok)
                    meta_data_history_dbos.append(meta_data_history_dbo)

        if not ok:
            yield [], []
            return

        yield meta_data_history_dbos, picture_dbos

    @classmethod
    def _commit_dbos(cls, engine, dbos):
        meta_data_history_dbos, all_picture_dbos = dbos

        messages = check_constraints(engine, meta_data_history_dbos, cls.mapping)

        picture_dbos_flattened = [
            dbo for picture_dbos in all_picture_dbos for dbo in picture_dbos
        ]
        messages += check_constraints(engine, picture_dbos_flattened, cls.mapping)

        if messages:
            yield from map(ValueError, messages)
            return

        for meta_data_history_dbo, picture_dbos in zip(
            meta_data_history_dbos, all_picture_dbos
        ):
            meta_data_history_dbo = commit_dbo(engine, meta_data_history_dbo)

            for picture_dbo in picture_dbos:
                picture_dbo.meta_data_history_id = (
                    meta_data_history_dbo.meta_data_history_id
                )
                messages = check_constraints(engine, [picture_dbo], cls.mapping)
                if messages:
                    yield from messages
                    continue
                commit_dbo(engine, picture_dbo)


class MetaDataHistoryYaml(Dispatcher, RawDataFile):
    entity_name = "meta_data_history"

    depends_on = [
        "meta_flag",
        "meta_action_type",
        "meta_log_type",
        "meta_data",
        "person",
        "site",
        "source",
    ]
    script_depends_on = []

    _root = pathlib.Path("meta_data/sensor_abc_from_xyz/test_site/")

    examples = (
        lambda config, _root=_root, META_DATA_HISTORY_YAML=META_DATA_HISTORY_YAML: [
            (_root / META_DATA_HISTORY_YAML, meta_data_history_yaml),
            (_root / "images/IMG_0312.JPG", picture_0),
            (_root / "images/IMG_0732.JPG", picture_1),
        ]
    )

    location = pathlib.Path("meta_data/*/*/") / META_DATA_HISTORY_YAML

    @classmethod
    def copy_into_development_landing_zone(cls):
        return False

    @classmethod
    def related_files(cls):
        yield cls.location.parent / "images/*"
        yield cls.location

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def cleanup(cls, root_folder, rel_path, config, engine, backup_folder):
        folder = rel_path.parent
        for p in [(root_folder / folder / META_DATA_HISTORY_YAML)] + sorted(
            (root_folder / folder).glob("images/*")
        ):
            p.unlink()
            yield f"removed {p.relative_to(root_folder)}"

    dbos = [MetaDataHistoryDecl, MetaPictureDecl]

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        yield from (
            p
            for p in backup_folder.glob(str(cls.location) + ".*")
            if (backup_folder / p).is_file()
        )
        yield from backup_folder.glob(str(cls.location.parent / "images" / "*"))

    @classmethod
    def data_files(cls, lz):
        # those which trigger check
        yield from lz.glob(str(cls.location))

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        meta_data_history_table = MetaDataHistoryDecl._schema

        matd = MetaActionTypeDecl._schema
        mdd = MetaDataDecl._schema
        mfd = MetaFlagDecl._schema
        mltd = MetaLogTypeDecl._schema
        pd = PersonDecl._schema
        sited = SiteDecl._schema
        sourced = SourceDecl._schema

        j = join(
            meta_data_history_table,
            matd,
            meta_data_history_table.c.meta_action_type_id == matd.c.meta_action_type_id,
        )
        j = join(j, mdd, meta_data_history_table.c.meta_data_id == mdd.c.meta_data_id)
        j = join(j, mfd, meta_data_history_table.c.meta_flag_id == mfd.c.meta_flag_id)
        j = join(
            j,
            mltd,
            meta_data_history_table.c.meta_log_type_id == mltd.c.meta_log_type_id,
        )
        j = join(j, pd, meta_data_history_table.c.person_id == pd.c.person_id)
        j = join(j, sited, mdd.c.site_id == sited.c.site_id)
        j = join(j, sourced, mdd.c.source_id == sourced.c.source_id)

        stmt = (
            select(
                [
                    meta_data_history_table.c.meta_data_history_id,
                    meta_data_history_table.c.timestamp_start,
                    meta_data_history_table.c.timestamp_end,
                    sited.c.name,
                    sourced.c.name,
                    mltd.c.name,
                    matd.c.name,
                    mfd.c.name,
                    pd.c.abbreviation,
                ]
            )
            .where(and_(*checks))
            .select_from(j)
        )

        header = [
            "binary_data_id",
            "timestamp_start",
            "timestamp_end",
            "site",
            "source",
            "meta_log_type",
            "meta_action_type",
            "meta_flag",
            "person",
        ]

        print_query(engine, stmt, header, indent, print_, max_rows)

    @classmethod
    def count_facts(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        yield engine.execute(
            select(
                [func.count(MetaDataHistoryDecl._schema.c.meta_data_history_id)]
            ).where(and_(*checks))
        ).scalar()

    @classmethod
    def supported_fields_for_delete(cls):
        return set(
            (
                "timestamp_start",
                "timestamp_end",
                "site",
                "source",
                "project",
                "source_type",
                "meta_log_type",
                "meta_action_type",
                "meta_flag",
                "person",
            )
        )

    @classmethod
    def _translate_conditions(cls, engine, conditions):
        errors = []
        fields = [f for (f, _, _) in conditions]
        if len(fields) != len(set(fields)):
            errors.append("duplicate fields in condition")

        fields = set(fields)
        cleared = fields - set(("source", "source_id", "source_type", "project"))
        if len(cleared) < len(fields) - 1:
            errors.append(
                "you can only use one of source, source_id, source_type and project"
            )

        foreign_table_schemata = {
            "source_type": SourceTypeDecl._schema,
            "project": ProjectDecl._schema,
            "meta_log_type": MetaLogTypeDecl._schema,
            "meta_action_type": MetaActionTypeDecl._schema,
            "meta_flag": MetaFlagDecl._schema,
            "abbreviation": PersonDecl._schema,
        }

        mtdh_schema = MetaDataHistoryDecl._schema

        supported_names = cls.supported_fields_for_delete()

        checks = []
        site_id = None
        source_ids = []

        for name, op, value in conditions:
            if name not in supported_names:
                s = ", ".join(sorted(supported_names))
                errors.append(f"field {name} does not exist, please chose one of {s}")
                continue
            if name == "source_type":
                if op != "==":
                    errors.append("source types can only be filtered using '=='")
                    continue
                source_type_id = engine.execute(
                    select([SourceTypeDecl._schema.c.source_type_id]).where(
                        SourceTypeDecl._schema.c.name == value
                    )
                ).scalar()

                if source_type_id is None:
                    errors.append(f"source_type {value!r} does not exist")
                    continue
                source_ids = [
                    sid
                    for (sid,) in engine.execute(
                        select([SourceDecl._schema.c.source_type_id]).where(
                            SourceDecl._schema.c.source_type_id == source_type_id
                        )
                    )
                ]
                continue

            if name == "site":
                site_dbo = fetch_one(
                    engine,
                    SiteDecl,
                    name=value,
                )
                if site_dbo is None:
                    errors.append(f"site {value!r} does not exist")
                else:
                    site_id = site_dbo.site_id
                continue

            if name == "source":
                source_dbo = fetch_one(
                    engine,
                    SourceDecl,
                    name=value,
                )
                if source_dbo is None:
                    errors.append(f"source {value!r} does not exist")
                else:
                    source_ids = [source_dbo.source_id]
                continue

            if name == "site_id":
                site_id = value
                continue

            if name == "source_id":
                source_ids = [value]
                continue

            if name == "project":
                if op != "==":
                    errors.append("projects can only be filtered using '=='")
                    continue
                project_id = engine.execute(
                    select([ProjectDecl._schema.c.project_id]).where(
                        ProjectDecl._schema.c.title == value
                    )
                ).scalar()

                if project_id is None:
                    errors.append(f"project with title {value!r} does not exist")
                    continue

                source_ids = engine.execute(
                    select([SourceDecl._schema.c.project_id]).where(
                        SourceDecl._schema.c.project_id == project_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                continue

            if name == "person":
                if op != "==":
                    errors.append(
                        "person abbreviations can only be filtered using '=='"
                    )
                    continue
                person_id = engine.execute(
                    select([PersonDecl._schema.c.person_id]).where(
                        PersonDecl._schema.c.abbreviation == value
                    )
                ).scalar()

                if person_id is None:
                    errors.append(f"person with abbreviation {value!r} does not exist")

                checks.append(mtdh_schema.c.person_id == person_id)
                continue

            if name in foreign_table_schemata:
                schema = foreign_table_schemata[name]
                item_id = engine.execute(
                    select([getattr(schema.c, name + "_id")]).where(
                        schema.c.name == value
                    )
                ).scalar()
                if item_id is None:
                    errors.append(f"{name} {value!r} does not exist")
                    continue
                if op not in ("==", "!="):
                    errors.append(f"{name} can only be filtered using '==' and '!=")
                    continue
                if op == "==":
                    checks.append(getattr(mtdh_schema.c, name + "_id") == item_id)
                else:
                    checks.append(getattr(mtdh_schema.c, name + "_id") != item_id)
                continue

            if name.endswith("_id"):
                if op != "==":
                    errors.append("ids can only be filtered using '=='")
                    continue
                try:
                    value = int(value)
                except ValueError:
                    errors.append(f"{name} {value} is not valid integer")
                    continue

                checks.append(getattr(mtdh_schema.c, name) == value)
                continue

            if name in ("timestamp_start", "timestamp_end"):
                try:
                    value = date_time_parser(value)
                except ValueError:
                    try:
                        value = date_parser(value)
                    except ValueError:
                        errors.append(
                            ValueError(f"'{value}' is neither a timestamp nor a date")
                        )
                        continue

            op_method = {
                "<": operator.lt,
                ">": operator.gt,
                "<=": operator.le,
                ">=": operator.ge,
                "==": operator.eq,
                "!=": operator.ne,
            }[op]

            checks.append(op_method(getattr(mtdh_schema.c, name), value))

        meta_data_ids = []
        mtds = MetaDataDecl._schema
        if site_id is not None and not source_ids:
            meta_data_ids = [
                i
                for (i,) in engine.execute(
                    select([mtds.c.meta_data_id]).where(mtds.c.site_id == site_id)
                )
            ]
        elif site_id is None and source_ids:
            meta_data_ids = [
                i
                for (i,) in engine.execute(
                    select([mtds.c.meta_data_id]).where(
                        mtds.c.source_id.in_(source_ids)
                    )
                )
            ]

        else:
            meta_data_ids = [
                i
                for (i,) in engine.execute(
                    select([mtds.c.meta_data_id]).where(
                        and_(
                            mtds.c.site_id == site_id, mtds.c.source_id.in_(source_ids)
                        )
                    )
                )
            ]
        if meta_data_ids:
            checks.append(mtdh_schema.c.meta_data_id.in_(meta_data_ids))
        return errors, checks

    @classmethod
    def delete(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        mtdh_schema = MetaDataHistoryDecl._schema
        condition = and_(*checks)
        engine.execute(delete(mtdh_schema).where(condition))
