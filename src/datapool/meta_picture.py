#!/usr/bin/env python

from sqlalchemy import Column, ForeignKey, Integer, LargeBinary, String, Table

from datapool.poolkit.database import Base


class MetaPictureDecl(Base):
    _schema = Table(
        "meta_picture",
        Base._metadata,
        Column("picture_id", Integer, primary_key=True, index=True),
        Column("meta_data_id", ForeignKey("meta_data.meta_data_id"), nullable=True),
        Column(
            "meta_data_history_id",
            ForeignKey("meta_data_history.meta_data_history_id"),
            nullable=True,
        ),
        Column("filename", String(), nullable=False),
        Column("description", String()),
        Column("data", LargeBinary()),
    )
