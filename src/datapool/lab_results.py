#!/usr/bin/env python
import operator
from collections import defaultdict

from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    Table,
    and_,
    delete,
    func,
    insert,
    join,
    select,
    update,
)

from datapool.poolkit.database import Base
from datapool.poolkit.dispatcher import CustomizedDispatcher
from datapool.poolkit.type_handling import date_parser, date_time_parser
from datapool.poolkit.utils import Asset, print_query
from datapool.poolkit.yaml import RawDataFile

from .lab_results_file_format import prepare_rows, read_and_check_csv_file
from .persons import PersonDecl
from .site import SiteDecl
from .variables import VariableDecl


class LabResultDecl(Base):
    _schema = Table(
        "lab_result",
        Base._metadata,
        Column("lab_result_id", Integer(), primary_key=True),
        Column("lab_identifier", String(), nullable=False, unique=True),
        Column("variable_id", ForeignKey("variable.variable_id"), nullable=False),
        Column("sample_identifier", String(), nullable=False),
        Column("filter_lab", String(), nullable=False),
        Column("dilution_lab", Float(), nullable=True),
        Column("method_lab", String(), nullable=False),
        Column("value_lab", Float(), nullable=False),
        Column("description_lab", String(), nullable=True),
        Column("timestamp_start_lab", DateTime(), nullable=True),
        Column("timestamp_end_lab", DateTime(), nullable=False),
        Column("site_id", ForeignKey("site.site_id"), nullable=False),
        Column("filter_sample", String(), nullable=False),
        Column("dilution_sample", Float(), nullable=True),
        Column("timestamp_sample", DateTime(), nullable=True),
        Column("method_sample", String(), nullable=True),
        Column("description_sample", String(), nullable=True),
    )


class LabResultPersonSampleAssociation(Base):
    _schema = Table(
        "lab_result_person_sample_association",
        Base._metadata,
        Column("lab_result_id", ForeignKey("lab_result.lab_result_id"), nullable=False),
        Column("person_id", ForeignKey("person.person_id"), nullable=False),
    )


class LabResultPersonLabAssociation(Base):
    _schema = Table(
        "lab_result_person_lab_association",
        Base._metadata,
        Column("lab_result_id", ForeignKey("lab_result.lab_result_id"), nullable=False),
        Column("person_id", ForeignKey("person.person_id"), nullable=False),
    )


class Results:
    def __init__(self, error_limit=20):
        self.error_limit = error_limit
        self.messages = []
        self.found_error = False
        self.result = None

    def __iter__(self):
        error_count = 0
        skipped = 0
        for message in self.messages:
            if isinstance(message, str):
                yield message
                continue
            if error_count < self.error_limit:
                yield message
                error_count += 1
                continue
            skipped += 1
        if skipped:
            yield f"... skipped {skipped} error messages"


def results_from_iter(iterator):
    results = Results()
    last_element = None
    for value in iterator:
        last_element = value
        if isinstance(value, Exception):
            results.found_error = True
            results.messages.append(value)
        if isinstance(value, str):
            results.found_error = True
            results.messages.append(value)
    if not isinstance(last_element, (str, Exception)):
        results.result = last_element
    return results


class FieldsCache:
    def __init__(self, engine):
        pt = PersonDecl._schema
        self.person_id_to_abbrev = dict(
            engine.execute(select([pt.c.person_id, pt.c.abbreviation])).fetchall()
        )
        self.persons = set(self.person_id_to_abbrev.values())

        st = SiteDecl._schema
        self.site_id_to_name = dict(
            engine.execute(select([st.c.site_id, st.c.name])).fetchall()
        )
        self.sites = set(self.site_id_to_name.values())

        pt = VariableDecl._schema
        self.variable_id_to_name = dict(
            engine.execute(select([pt.c.variable_id, pt.c.name])).fetchall()
        )
        self.variables = set(self.variable_id_to_name.values())

        lt = LabResultDecl._schema
        self.lab_result_id_to_lab_identifier = dict(
            engine.execute(select([lt.c.lab_result_id, lt.c.lab_identifier])).fetchall()
        )
        self.variables = set(self.variable_id_to_name.values())


class RowChecker:
    def __init__(self, cache):
        self.fc = cache

    def check_row(self, row_number, row):
        for f in ("person_abbrev_lab", "person_abbrev_sample"):
            for abbreviation in row[f]:
                if abbreviation not in self.fc.persons:
                    yield ValueError(
                        f"person {abbreviation!r} in {f} in row {row_number} unknown"
                    )
        if row["site"] not in self.fc.sites:
            yield ValueError(f"site {row['site']!r} in row {row_number} unknown")
        if row["variable"] not in self.fc.variables:
            yield ValueError(
                f"variable {row['variable']!r} in row {row_number} unknown"
            )


class DBChecker:
    def __init__(self, engine, cache):
        self.engine = engine
        self.fc = cache
        self.found_error = False

    def check_rows(self, rows):
        forbidden_changes = [
            "timestamp_start_lab",
            "timestamp_end_lab",
            "timestamp_sample",
            "value_lab",
            "sample_identifier",
        ]

        identifiers = [row["lab_identifier"] for row in rows]
        self._setup_person_lab_abbreviations(identifiers)
        self._setup_person_sample_abbreviations(identifiers)

        db_rows = self.engine.execute(
            select([LabResultDecl._schema]).where(
                LabResultDecl._schema.c.lab_identifier.in_(identifiers)
            )
        ).fetchall()

        db_rows = [self._resolve_row(db_row) for db_row in db_rows]

        db_rows = {db_row["lab_identifier"]: db_row for db_row in db_rows}
        rows = {row["lab_identifier"]: row for row in rows}

        # restore order
        common_identifiers = set(db_rows) & set(rows)
        common = [row for id_, row in rows.items() if id_ in common_identifiers]

        ok = True
        for row in common:
            identifier = row["lab_identifier"]
            db_row = db_rows[identifier]
            for field in forbidden_changes:
                if db_row[field] != row[field]:
                    yield ValueError(
                        f"field {field} for lab_identifier {identifier} is"
                        f" '{row[field]!s}' in csv file but"
                        f" '{db_row[field]!s}' in database."
                    )
                    ok = False

        removed = set(db_rows) - set(rows)
        if removed:
            for r in sorted(removed):
                yield ValueError("row with lab_identifier {r} was removed.")
            ok = False

        if not ok:
            return

        modified_rows = []
        for row in common:
            db_row = db_rows[row["lab_identifier"]]
            for field, value in row.items():
                if field not in forbidden_changes:
                    if db_row[field] != row[field]:
                        modified_rows.append(row)
                        break

        new_identifiers = set(rows) - set(db_rows)
        new_rows = [rows[id_] for id_ in rows if id_ in new_identifiers]

        yield modified_rows, new_rows

    def _setup_person_lab_abbreviations(self, identifiers):
        asoc = LabResultPersonLabAssociation._schema
        lr = LabResultDecl._schema

        self.person_lab_abbreviations = defaultdict(list)

        for lab_result_id, person_id in self.engine.execute(
            select([asoc.c.lab_result_id, asoc.c.person_id])
            .join(lr, lr.c.lab_result_id == asoc.c.lab_result_id)
            .where(lr.c.lab_identifier.in_(identifiers))
        ).fetchall():
            person = self.fc.person_id_to_abbrev[person_id]
            lab_identifier = self.fc.lab_result_id_to_lab_identifier[lab_result_id]
            self.person_lab_abbreviations[lab_identifier].append(person)

    def _setup_person_sample_abbreviations(self, identifiers):
        asoc = LabResultPersonSampleAssociation._schema
        lr = LabResultDecl._schema

        self.person_sample_abbreviations = defaultdict(list)

        for lab_result_id, person_id in self.engine.execute(
            select([asoc.c.lab_result_id, asoc.c.person_id])
            .join(lr, lr.c.lab_result_id == asoc.c.lab_result_id)
            .where(lr.c.lab_identifier.in_(identifiers))
        ).fetchall():
            lab_identifier = self.fc.lab_result_id_to_lab_identifier[lab_result_id]
            person = self.fc.person_id_to_abbrev[person_id]
            self.person_sample_abbreviations[lab_identifier].append(person)

    def _resolve_row(self, db_row):
        db_row = dict(db_row)  # RowProxy is immutable
        lab_identifier = db_row["lab_identifier"]
        db_row["site"] = self.fc.site_id_to_name[db_row["site_id"]]
        del db_row["site_id"]
        db_row["variable"] = self.fc.variable_id_to_name[db_row["variable_id"]]
        del db_row["variable_id"]
        db_row["person_abbrev_lab"] = self.person_lab_abbreviations[lab_identifier]
        db_row["person_abbrev_sample"] = self.person_sample_abbreviations[
            lab_identifier
        ]
        return db_row


class Dispatcher(CustomizedDispatcher):
    @classmethod
    def check(cls, root_folder, rel_path, config, engine):
        results = results_from_iter(read_and_check_csv_file(root_folder / rel_path))
        yield from results
        if results.found_error:
            return

        rows = results.result

        assert rows is not None, "interal error"
        rows = prepare_rows(rows)

        fields_cache = FieldsCache(engine)
        checker = RowChecker(fields_cache)

        yield "check rows"

        results = results_from_iter(
            msg
            for row_number, row in enumerate(rows, 2)
            for msg in checker.check_row(row_number, row)
        )
        yield from results

        if results.found_error:
            return

        yield "check rows against db"
        db_checker = DBChecker(engine, fields_cache)
        results = results_from_iter(db_checker.check_rows(rows))
        yield from results
        if results.found_error:
            return

        assert results.result is not None, "internal error"
        modified_rows, new_rows = results.result

        yield "all checks passed"

        yield f"detected {len(modified_rows)} modified and {len(new_rows)} new rows"

        if modified_rows:
            update_modified_rows(engine, fields_cache, modified_rows)
            yield f"updated {len(modified_rows)} rows"
        if new_rows:
            insert_new_rows(engine, fields_cache, new_rows)
            yield f"inserted {len(new_rows)} new rows"

    @classmethod
    def dispatch(cls, root_folder, rel_path, config, engine):
        yield from cls.check(root_folder, rel_path, config, engine)
        yield [rel_path]


def update_modified_rows(engine, fc, rows):
    site_name_to_id = invert(fc.site_id_to_name)
    variable_name_to_id = invert(fc.variable_id_to_name)
    person_abbrev_to_id = invert(fc.person_id_to_abbrev)
    lab_identifier_to_id = invert(fc.lab_result_id_to_lab_identifier)

    for row in rows:
        row["site_id"] = site_name_to_id[row["site"]]
        row["variable_id"] = variable_name_to_id[row["variable"]]
        person_lab_ids = [
            person_abbrev_to_id[abbrev] for abbrev in row["person_abbrev_lab"]
        ]
        person_sample_ids = [
            person_abbrev_to_id[abbrev] for abbrev in row["person_abbrev_sample"]
        ]
        lab_result_id = lab_identifier_to_id[row["lab_identifier"]]

        for f in ("person_abbrev_lab", "person_abbrev_sample", "variable", "site"):
            del row[f]

        update_person_lab_assoc(engine, lab_result_id, person_lab_ids)
        update_person_sample_assoc(engine, lab_result_id, person_sample_ids)
        table = LabResultDecl._schema
        engine.execute(
            update(table).where(table.c.lab_result_id == lab_result_id).values(**row)
        )


def insert_new_rows(engine, fc, rows):
    site_name_to_id = invert(fc.site_id_to_name)
    variable_name_to_id = invert(fc.variable_id_to_name)
    person_abbrev_to_id = invert(fc.person_id_to_abbrev)

    for row in rows:
        row["site_id"] = site_name_to_id[row["site"]]
        row["variable_id"] = variable_name_to_id[row["variable"]]
        person_lab_ids = [
            person_abbrev_to_id[abbrev] for abbrev in row["person_abbrev_lab"]
        ]
        person_sample_ids = [
            person_abbrev_to_id[abbrev] for abbrev in row["person_abbrev_sample"]
        ]

        del row["site"]
        del row["variable"]
        del row["person_abbrev_lab"]
        del row["person_abbrev_sample"]

        table = LabResultDecl._schema
        engine.execute(insert(table).values(**row))
        lab_result_id = engine.execute(
            select([table.c.lab_result_id]).where(
                table.c.lab_identifier == row["lab_identifier"]
            )
        ).scalar()
        assert lab_result_id is not None, "internal error"

        insert_person_lab_assoc(engine, lab_result_id, person_lab_ids)
        insert_person_sample_assoc(engine, lab_result_id, person_sample_ids)


def update_person_lab_assoc(engine, lab_result_id, person_lab_ids):
    table = LabResultPersonLabAssociation._schema
    engine.execute(delete(table).where(table.c.lab_result_id == lab_result_id))
    insert_person_lab_assoc(engine, lab_result_id, person_lab_ids)


def insert_person_lab_assoc(engine, lab_result_id, person_lab_ids):
    table = LabResultPersonLabAssociation._schema
    for person_lab_id in person_lab_ids:
        engine.execute(
            insert(table).values(lab_result_id=lab_result_id, person_id=person_lab_id)
        )


def update_person_sample_assoc(engine, lab_result_id, person_sample_ids):
    table = LabResultPersonSampleAssociation._schema
    engine.execute(delete(table).where(table.c.lab_result_id == lab_result_id))
    insert_person_sample_assoc(engine, lab_result_id, person_sample_ids)


def insert_person_sample_assoc(engine, lab_result_id, person_sample_ids):
    table = LabResultPersonSampleAssociation._schema
    for person_sample_id in person_sample_ids:
        engine.execute(
            insert(table).values(
                lab_result_id=lab_result_id, person_id=person_sample_id
            )
        )


def invert(dd):
    return {v: k for (k, v) in dd.items()}


class LabResults(Dispatcher, RawDataFile):
    depends_on = ["person", "variable", "site"]
    script_depends_on = []

    entity_name = "lab_results"

    location = "lab_results/lab_results.csv"

    examples = lambda config, location=location: [
        (location, Asset("assets/lab_results.csv")),
    ]

    @classmethod
    def related_files(cls):
        yield cls.location

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        yield from backup_folder.glob(cls.location + ".*")

    @classmethod
    def match_conversion_script(cls, rel_path):
        return False

    @classmethod
    def data_files(cls, lz):
        yield from lz.glob(cls.location)

    @classmethod
    def count_facts(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        yield engine.execute(
            select([func.count(LabResultDecl._schema.c.lab_result_id)]).where(
                and_(*checks)
            )
        ).scalar()

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        labrd = LabResultDecl._schema
        pd = VariableDecl._schema
        sited = SiteDecl._schema

        j = join(labrd, pd, labrd.c.variable_id == pd.c.variable_id)
        j = join(j, sited, labrd.c.site_id == sited.c.site_id, isouter=True)

        stmt = (
            select(
                [
                    labrd.c.lab_result_id,
                    labrd.c.lab_identifier,
                    labrd.c.sample_identifier,
                    labrd.c.value_lab,
                    pd.c.name.label("variable"),
                    labrd.c.timestamp_end_lab,
                    labrd.c.description_lab,
                    sited.c.name.label("site"),
                    labrd.c.timestamp_sample,
                    labrd.c.method_sample,
                    labrd.c.description_sample,
                ]
            )
            .where(and_(*checks))
            .select_from(j)
        )

        header = [
            "lab_result_id",
            "lab_identifier",
            "sample_identifier",
            "value_lab",
            "variable",
            "timestamp_end_lab",
            "description_lab",
            "site",
            "timestamp_sample",
            "method_sample",
            "description_sample",
        ]

        print_query(engine, stmt, header, indent, print_, max_rows)

    @classmethod
    def supported_fields_for_delete(cls):
        return set(
            (
                "lab_result_id",
                "lab_identifier",
                "variable",
                "variable_id",
                "sample_identifier",
                "filter_lab",
                "dilution_lab",
                "method_lab",
                "description_lab",
                "timestamp_start_lab",
                "timestamp_end_lab",
                "site",
                "site_id",
                "filter_sample",
                "dilution_sample",
                "timestamp_sample",
                "method_sample",
                "person_lab",
                "person_sample",
                "person",
            )
        )

    @classmethod
    def _translate_conditions(cls, engine, conditions):
        from .site import SiteDecl
        from .variables import VariableDecl

        foreign_table_schemata = {
            "variable": VariableDecl._schema,
            "site": SiteDecl._schema,
        }

        lab_results_table = LabResultDecl._schema

        supported_names = cls.supported_fields_for_delete()

        errors = []

        checks = []
        person_lab_id = None
        person_sample_id = None
        for name, op, value in conditions:
            if name not in supported_names:
                s = ", ".join(sorted(supported_names))
                errors.append(f"field {name} does not exist, please chose one of {s}")
                continue

            if name in ("person", "person_lab", "person_sample"):
                if op != "==":
                    errors.append(
                        "person abbreviations can only be filtered using '=='"
                    )
                if name in ("person", "person_lab"):
                    person_lab_id = engine.execute(
                        select([PersonDecl._schema.c.person_id]).where(
                            PersonDecl._schema.c.abbreviation == value
                        )
                    ).scalar()

                    if person_lab_id is None:
                        errors.append(
                            f"person with abbreviation {value!r} does not exist"
                        )

                if name in ("person", "person_sample"):
                    if op != "==":
                        errors.append(
                            "person abbreviations can only be filtered using '=='"
                        )
                        continue
                    person_sample_id = engine.execute(
                        select([PersonDecl._schema.c.person_id]).where(
                            PersonDecl._schema.c.abbreviation == value
                        )
                    ).scalar()

                    if person_sample_id is None:
                        errors.append(
                            f"person with abbreviation {value!r} does not exist"
                        )
                continue

            if name in foreign_table_schemata:
                schema = foreign_table_schemata[name]
                item_id = engine.execute(
                    select([getattr(schema.c, name + "_id")]).where(
                        schema.c.name == value
                    )
                ).scalar()
                if item_id is None:
                    errors.append(f"{name} {value!r} does not exist")
                    continue
                if op not in ("==", "!="):
                    errors.append(f"{name} can only be filtered using '==' and '!=")
                    continue
                if op == "==":
                    checks.append(getattr(lab_results_table.c, name + "_id") == item_id)
                else:
                    checks.append(getattr(lab_results_table.c, name + "_id") != item_id)
                continue

            if name.endswith("_id"):
                if op != "==":
                    errors.append("ids can only be filtered using '=='")
                    continue
                try:
                    value = int(value)
                except ValueError:
                    errors.append(f"{name} {value} is not valid integer")
                    continue

                checks.append(getattr(lab_results_table.c, name) == value)
                continue

            if name.startswith("timestamp_"):
                try:
                    value = date_time_parser(value)
                except ValueError:
                    try:
                        value = date_parser(value)
                    except ValueError:
                        errors.append(
                            ValueError(f"'{value}' is neither a timestamp nor a date")
                        )
                        continue

            op_method = {
                "<": operator.lt,
                ">": operator.gt,
                "<=": operator.le,
                ">=": operator.ge,
                "==": operator.eq,
                "!=": operator.ne,
            }[op]

            checks.append(op_method(getattr(lab_results_table.c, name), value))

        lab_result_ids = []
        if person_sample_id is not None:
            lab_result_ids += [
                i
                for (i,) in engine.execute(
                    select(
                        [LabResultPersonSampleAssociation._schema.c.lab_result_id]
                    ).where(
                        LabResultPersonSampleAssociation._schema.c.person_id
                        == person_sample_id
                    )
                )
            ]
        if person_lab_id is not None:
            lab_result_ids += [
                i
                for (i,) in engine.execute(
                    select(
                        [LabResultPersonLabAssociation._schema.c.lab_result_id]
                    ).where(
                        LabResultPersonLabAssociation._schema.c.person_id
                        == person_lab_id
                    )
                )
            ]

        if lab_result_ids:
            checks.append(lab_results_table.c.lab_result_id.in_(set(lab_result_ids)))

        return errors, checks

    @classmethod
    def delete(cls, engine, conditions):
        return
        yield
