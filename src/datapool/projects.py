#!/usr/bin/env python
from collections import Counter

from sqlalchemy import Column, Integer, String, Table, delete, select, update

from datapool.poolkit.database import (
    Base,
    check_constraints,
    commit_dbo,
    fetch_many,
    find_differences,
    find_duplicates,
)
from datapool.poolkit.yaml import List, NotEmpty, Optional, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class ProjectDecl(Base):
    _schema = Table(
        "project",
        Base._metadata,
        Column("project_id", Integer(), primary_key=True, index=True),
        Column("title", String(), nullable=False),
        Column("abbrev", String(), nullable=True),  # unique handled on software level
        Column("description", String()),
    )


projects_yaml_example = """
-
    title: sis_datapool
    description: SIS collaboration for software development
    abbrev:
"""


class ProjectYaml(Yaml):
    entity_name = "project"
    is_fact_dimension = False

    depends_on = []

    location = "projects.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        (
            "projects.yaml",
            projects_yaml_example,
        )
    ]

    dbos = [ProjectDecl]

    mapping = lambda config: List(
        {
            "title": NotEmpty(ProjectDecl.title),
            "description": ProjectDecl.description,
            "abbrev": Optional(ProjectDecl.abbrev),
        }
    )

    @classmethod
    def _read_yaml(cls, projects_file_path):
        if projects_file_path.exists():
            try:
                return parse_file(projects_file_path)
            except YamlError as e:
                return YamlError(f"file {projects_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["title", "abbrev", "description"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        projects_file_path = root_folder / cls.location
        projects = cls._read_yaml(projects_file_path)
        if isinstance(projects, Exception):
            raise projects

        name_to_projects = {p["title"]: p for p in projects}
        abbrev_to_projects = {p["abbrev"]: p for p in projects if "abbrev" in p}

        title = ask("title", not_empty=True)
        if title in name_to_projects:
            raise ValueError(f"project with title '{title}' exists already")

        abbrev = ask("abbrev", not_empty=False)
        if abbrev and abbrev in abbrev_to_projects:
            raise ValueError(f"project with abbrev '{abbrev}' exists already")

        description = ask("description")

        p = dict(title=title, description=description)
        if abbrev:
            p["abbrev"] = abbrev

        projects.append(p)

        parent = projects_file_path.parent
        parent.mkdir(parents=True, exist_ok=True)

        with projects_file_path.open("w") as fh:
            print_yaml(projects, fh=fh)

        return projects_file_path

    @classmethod
    def find_by_name(cls, root_folder, title):
        projects = cls._read_yaml(root_folder / cls.location)
        if isinstance(projects, Exception):
            yield projects
            return
        for entry in projects:
            if entry["title"] == title:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, title):
        schema = ProjectDecl._schema
        yield engine.execute(
            select([schema.c.project_id]).where(schema.c.title == title)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)
        counter = Counter(entry.get("title") for entry in yaml)
        for title, count in counter.items():
            if title is None:
                continue
            if count > 1:
                yield ValueError(f"project with title '{title}' appears {count} times")

        counter = Counter(entry.get("abbrev") for entry in yaml)
        for abbrev, count in counter.items():
            if not abbrev:
                continue
            if count > 1:
                yield ValueError(
                    f"project with abbrev '{abbrev}' appears {count} times"
                )

    @classmethod
    def prepare(cls, root_foler, rel_path, yaml):
        for entry in yaml:
            entry["abbrev"] = entry.get("abbrev")
        yield yaml

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, projects):
        messages = []

        duplicates, unique_projects = find_duplicates(projects, ["title"])

        reported_titles = set()
        for duplicate in duplicates:
            title = duplicate["title"]
            if title in reported_titles:
                continue
            reported_titles.add(title)
            messages.append(f"duplicate entry with title '{title}'")

        duplicates, _ = find_duplicates(projects, ["abbrev"])

        reported_abbrevs = set()
        for duplicate in duplicates:
            abbrev = duplicate["abbrev"]
            if abbrev is None or abbrev in reported_abbrevs:
                continue
            reported_abbrevs.add(abbrev)
            messages.append(f"duplicate entry with abbrev '{abbrev}'")

        existing_project_dbos = fetch_many(engine, ProjectDecl)
        (
            new_project_dbos,
            modified_project_dbos_ok,
            modified_project_dbos_not_ok,
            deleted_project_dbos,
            same,
        ) = find_differences(
            ProjectDecl,
            unique_projects,
            existing_project_dbos,
            ["title"],
            ["description"],
        )

        for project_dbo in deleted_project_dbos:
            title = project_dbo["title"]
            messages.append(f"project '{title}' was deleted")

        for project_dbo in modified_project_dbos_not_ok:
            title = project_dbo["title"]
            messages.append(f"project '{title}' has illegal modification")

        if messages:
            yield from map(ValueError, messages)
            return

        yield new_project_dbos + modified_project_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config).specification)

        if messages:
            yield from map(ValueError, messages)
            return
        for dbo in dbos:
            commit_dbo(engine, dbo)

    @classmethod
    def check_before_delete(cls, config, engine, project_id):
        from .source import SourceDecl

        source_table = SourceDecl._schema
        source_names = engine.execute(
            select([source_table.c.name]).where(source_table.c.project_id == project_id)
        ).fetchall()
        if not source_names:
            yield True
            return
        yield "please delete the following sources first:"
        for [source_name] in source_names:
            yield f"  - {source_name}"
        yield False

    @classmethod
    def delete_from_db(cls, config, engine, project_id):
        engine.execute(
            delete(ProjectDecl._schema).where(
                ProjectDecl._schema.c.project_id == project_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, title):
        projects = cls._read_yaml(yaml_path)
        if isinstance(projects, Exception):
            yield projects
            return
        projects = [p for p in projects if p["title"] != title]
        with yaml_path.open("w") as fh:
            print_yaml(projects, fh=fh)

        yield f"deleted project '{title}' from {yaml_path}"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
        except YamlError as e:
            yield e
            return
        found = False
        for project in data:
            if project["title"] == old_name:
                found = True
                project["title"] = new_name
        if found is False:
            yield ValueError(f"project with title '{old_name}' does not exist")
            return

        try:
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except IOError as e:
            yield e
            return

        yield f"changed project title in {yaml_path} from '{old_name}' to '{new_name}'"

        lz_root = yaml_path.parent

        # affected_files_before = [yaml_path.relative_to(lz_root)]

        all_sources = [
            source_yaml.relative_to(lz_root)
            for source_yaml in (lz_root / "data").glob("*/*/source.yaml")
        ]

        # no renambe, but triggers checks:
        renames = {p: p for p in all_sources}

        p = yaml_path.relative_to(lz_root)
        yield p, renames

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        project_id = rename_data["item_id"]
        table = ProjectDecl._schema
        engine.execute(
            update(table).where(table.c.project_id == project_id).values(title=new_name)
        )
        yield f"renamed project with project_id={project_id} to {new_name}"

    prepare_rename_in_db = rename_in_db

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        return
        yield
