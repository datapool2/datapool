#!/usr/bin/env python

from collections import Counter
from pathlib import Path

from sqlalchemy import Column, Integer, String, Table, delete, select, update

from datapool.poolkit.database import (
    Base,
    check_constraints,
    commit_dbo,
    fetch_many,
    find_differences,
    find_duplicates,
)
from datapool.poolkit.yaml import List, NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class MetaLogTypeDecl(Base):
    _schema = Table(
        "meta_log_type",
        Base._metadata,
        Column("meta_log_type_id", Integer(), primary_key=True),
        Column("name", String(), unique=True, index=True, nullable=False),
        Column("description", String()),
    )


meta_log_type_yaml_example = """
-
   name: new_sensor
   description:
-
   name: removed_sensor
   description:
"""


class MetaLogTypeYaml(Yaml):
    entity_name = "meta_log_type"
    is_fact_dimension = True

    # single unique yaml might be special!? -> considers this when abstratingt the
    # framework later?

    location = "meta_data/meta_log_types.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        ("meta_data/meta_log_types.yaml", meta_log_type_yaml_example)
    ]

    dbos = [MetaLogTypeDecl]

    mapping = lambda config: List(
        {
            "name": NotEmpty(MetaLogTypeDecl.name),
            "description": MetaLogTypeDecl.description,
        }
    )

    @classmethod
    def _read_yaml(cls, meta_log_types_file_path):
        if meta_log_types_file_path.exists():
            try:
                return parse_file(meta_log_types_file_path)
            except YamlError as e:
                return YamlError(f"file {meta_log_types_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["name", "description"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        meta_log_types_file_path = root_folder / cls.location
        meta_log_types = cls._read_yaml(meta_log_types_file_path)
        if isinstance(meta_log_types, Exception):
            raise meta_log_types

        name_to_meta_log_types = {p["name"]: p for p in meta_log_types}

        name = ask("name", not_empty=True)
        if name in name_to_meta_log_types:
            raise ValueError(f"meta_log_type with name '{name}' exists already")

        description = ask("description")

        meta_log_types.append(dict(name=name, description=description))

        parent = meta_log_types_file_path.parent
        parent.mkdir(parents=True, exist_ok=True)

        with meta_log_types_file_path.open("w") as fh:
            print_yaml(meta_log_types, fh=fh)

        return meta_log_types_file_path

    @classmethod
    def find_by_name(cls, root_folder, name):
        meta_log_types = cls._read_yaml(root_folder / cls.location)
        if isinstance(meta_log_types, Exception):
            yield meta_log_types
            return
        found_error = False
        for error in super().check_yaml(
            root_folder, cls.location, meta_log_types, None
        ):
            yield error
            found_error = True
        if found_error:
            return
        for entry in meta_log_types:
            if entry["name"] == name:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = MetaLogTypeDecl._schema
        yield engine.execute(
            select([schema.c.meta_log_type_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        found_error = False
        for error in super().check_yaml(root_folder, rel_path, yaml, config):
            yield error
            found_error = True
        if found_error:
            return
        counter = Counter(entry.get("name") for entry in yaml)
        for name, count in counter.items():
            if name is None:
                continue
            if count > 1:
                yield ValueError(
                    f"meta_log_type with name '{name}' appears {count} times"
                )

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, meta_log_types):
        messages = []

        duplicates, unique_meta_log_types = find_duplicates(meta_log_types, ["name"])

        reported_names = set()
        for duplicate in duplicates:
            name = duplicate["name"]
            if name in reported_names:
                continue
            reported_names.add(name)
            messages.append(f"duplicate entry with name '{name}'")

        existing_meta_log_type_dbos = fetch_many(engine, MetaLogTypeDecl)
        (
            new_meta_log_type_dbos,
            modified_meta_log_type_dbos_ok,
            modified_meta_log_type_dbos_not_ok,
            deleted_meta_log_type_dbos,
            same,
        ) = find_differences(
            MetaLogTypeDecl,
            unique_meta_log_types,
            existing_meta_log_type_dbos,
            ["name"],
            [],
        )

        for meta_log_type_dbo in deleted_meta_log_type_dbos:
            name = meta_log_type_dbo["name"]
            messages.append(f"meta_log_type '{name}' was deleted")

        for meta_log_type_dbo in modified_meta_log_type_dbos_not_ok:
            name = meta_log_type_dbo["name"]
            messages.append(f"meta_log_type '{name}' was modified")

        if messages:
            yield from map(ValueError, messages)
            return

        yield new_meta_log_type_dbos + modified_meta_log_type_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config).specification)
        if messages:
            yield from map(ValueError, messages)
            return
        for dbo in dbos:
            commit_dbo(engine, dbo)

    @classmethod
    def check_before_delete(cls, config, engine, meta_log_type_id):
        from .meta_action_type import MetaActionTypeDecl

        table = MetaActionTypeDecl._schema
        names = engine.execute(
            select([table.c.name]).where(table.c.meta_log_type_id == meta_log_type_id)
        ).fetchall()
        if not names:
            yield True
            return
        yield "please delete the following meta action types first:"
        for [name] in names:
            yield f"  - {name}"
        yield False

    @classmethod
    def delete_from_db(cls, config, engine, meta_log_type_id):
        engine.execute(
            delete(MetaLogTypeDecl._schema).where(
                MetaLogTypeDecl._schema.c.meta_log_type_id == meta_log_type_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        meta_log_types = cls._read_yaml(yaml_path)
        if isinstance(meta_log_types, Exception):
            yield meta_log_types
            return
        meta_log_types = [p for p in meta_log_types if p["name"] != name]
        with yaml_path.open("w") as fh:
            print_yaml(meta_log_types, fh=fh)

        yield f"deleted meta_log_type '{name}' from {yaml_path}"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
        except YamlError as e:
            yield e
            return
        found = False
        for meta_log_type in data:
            if meta_log_type["name"] == old_name:
                found = True
                meta_log_type["name"] = new_name
        if found is False:
            yield ValueError(f"meta_log_type with name '{old_name}' does not exist")
            return

        try:
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except IOError as e:
            yield e
            return

        yield (
            f"changed meta_log_type name in {yaml_path} from '{old_name}' to"
            f" '{new_name}'"
        )

        lz_root = yaml_path.parent.parent

        all_children = [
            p.relative_to(lz_root)
            for p in (lz_root / "meta_data").glob("**/*")
            # if p != lz_root / yaml_path
        ]

        renames = dict(zip(all_children, all_children))
        yield Path("meta_data/meta_log_types.yaml"), renames

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        meta_log_type_id = rename_data["item_id"]
        table = MetaLogTypeDecl._schema
        engine.execute(
            update(table)
            .where(table.c.meta_log_type_id == meta_log_type_id)
            .values(name=new_name)
        )
        yield (
            f"renamed meta_log_type with meta_log_type_id={meta_log_type_id} to"
            f" {new_name}"
        )

    prepare_rename_in_db = rename_in_db

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        return
        yield
