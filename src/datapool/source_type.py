#!/usr/bin/env python

import glob
import pathlib
import shutil
from collections import Counter

from sqlalchemy import (
    Column,
    Float,
    ForeignKey,
    Integer,
    String,
    Table,
    UniqueConstraint,
    delete,
    select,
    update,
)

from datapool.poolkit.database import (
    Base,
    ModifiedDbo,
    check_constraints,
    check_modifications,
    commit_dbo,
    create_dbo,
    fetch_many,
    fetch_one,
    find_differences,
)
from datapool.poolkit.utils import free_text_to_path_name
from datapool.poolkit.yaml import List, NotEmpty, Optional, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class SourceTypeDecl(Base):
    _schema = Table(
        "source_type",
        Base._metadata,
        Column("source_type_id", Integer, primary_key=True, index=True),
        Column("name", String(), unique=True, index=True, nullable=False),
        Column("description", String()),
        Column("manufacturer", String()),
    )


class SpecialValueDefinitionDecl(Base):
    _schema = Table(
        "special_value_definition",
        Base._metadata,
        Column("special_value_definition_id", Integer(), primary_key=True),
        Column(
            "source_type_id", ForeignKey("source_type.source_type_id"), nullable=False
        ),
        Column("description", String()),
        Column("categorical_value", String(), nullable=False),
        Column("numerical_value", Float(), nullable=False),
        UniqueConstraint("source_type_id", "numerical_value"),
    )


source_type_yaml_example = """
name: sensor_from_xyz
description: All the details of the this source type.
manufacturer: 000-111-222-333
special_values:
    - categorical_value: NA
      numerical_value: -666
      description: not a number
"""


class SourceTypeYaml(Yaml):
    entity_name = "source_type"

    location = "data/*/source_type.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        ("data/sensor_from_xyz/source_type.yaml", source_type_yaml_example)
    ]

    is_fact_dimension = False

    dbos = [SourceTypeDecl, SpecialValueDefinitionDecl]

    mapping = lambda config: {
        "name": NotEmpty(SourceTypeDecl.name),
        "description": SourceTypeDecl.description,
        "manufacturer": SourceTypeDecl.manufacturer,
        "special_values": Optional(
            List(
                {
                    "categorical_value": SpecialValueDefinitionDecl.categorical_value,
                    "numerical_value": SpecialValueDefinitionDecl.numerical_value,
                    "description": Optional(SpecialValueDefinitionDecl.description),
                }
            )
        ),
    }

    @classmethod
    def find_by_name(cls, root_folder, name):
        for p in glob.glob(str(root_folder / cls.location)):
            p = pathlib.Path(p)
            try:
                data = parse_file(p)
            except YamlError as e:
                yield e
                return
            if "name" not in data:
                yield YamlError(f"yaml file {p} is invalid: entry 'name' is missing.")
                continue
            if data["name"] == name:
                yield p

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = SourceTypeDecl._schema
        yield engine.execute(
            select([schema.c.source_type_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)

        counter = Counter(
            [sv.get("numerical_value") for sv in yaml.get("special_values", [])]
        )

        for numerical_value, count in counter.items():
            if numerical_value is None:
                continue
            if count == 1:
                continue
            yield ValueError(
                f"duplicate special value entries for numerical value {numerical_value}"
            )

        counter = Counter(
            [sv.get("categorical_value") for sv in yaml.get("special_values", [])]
        )

        for categorical_value, count in counter.items():
            if categorical_value is None:
                continue
            if count == 1:
                continue
            yield ValueError(
                "duplicate special value entries for categorical value"
                f" {categorical_value}"
            )

        name = yaml.get("name")
        if name and rel_path.parent.name != name:
            yield ValueError(
                f"source_type name '{name}' does not match folder name"
                f" '{rel_path.parent}'"
            )

    @classmethod
    def prepare(cls, root_folder, rel_path, data):
        for special_value in data.get("special_values", []):
            special_value["numerical_value"] = float(special_value["numerical_value"])
        yield data

    @classmethod
    def check_against_db(cls, config, engine, data):
        messages = []

        special_values = data.get("special_values", [])
        if special_values:
            del data["special_values"]

        source_type_dbo = fetch_one(engine, SourceTypeDecl, name=data["name"])

        if not source_type_dbo:
            source_type_dbo = create_dbo(SourceTypeDecl, data)

            new_special_value_definition_dbos = [
                create_dbo(SpecialValueDefinitionDecl, special_value)
                for special_value in special_values
            ]
            modified_special_value_definition_dbos_ok = []
        else:
            messages.extend(
                check_modifications(
                    SourceTypeDecl,
                    source_type_dbo,
                    data,
                    ["description"],
                    ["source_type_id"],
                )
            )
            special_value_definition_dbos = fetch_many(
                engine,
                SpecialValueDefinitionDecl,
                source_type_id=source_type_dbo.source_type_id,
            )
            (
                new_special_value_definition_dbos,
                modified_special_value_definition_dbos_ok,
                modified_special_value_definition_dbos_not_ok,
                deleted_special_value_definition_dbos,
                same,
            ) = find_differences(
                SpecialValueDefinitionDecl,
                special_values,
                special_value_definition_dbos,
                ["numerical_value"],
                ["categorical_value"],
            )

            for special_value_definition in deleted_special_value_definition_dbos:
                numerical_value = special_value_definition["numerical_value"]
                messages.append(
                    f"special_value_definition for numerical value {numerical_value}"
                    " was deleted"
                )

            for (
                special_value_definition
            ) in modified_special_value_definition_dbos_not_ok:
                numerical_value = special_value_definition["numerical_value"]
                messages.append(
                    f"special_value_definition for numerical value {numerical_value}"
                    " was modified"
                )
            if not messages:
                data = {**source_type_dbo._data.copy(), **data}
                source_type_dbo = ModifiedDbo(SourceTypeDecl._schema, data)

        if messages:
            yield from map(ValueError, messages)
            return

        yield (
            [source_type_dbo]
            + new_special_value_definition_dbos
            + modified_special_value_definition_dbos_ok
        )

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        source_type_dbo, *special_value_definition_dbos = dbos
        messages = check_constraints(engine, [source_type_dbo], cls.mapping(config))
        if messages:
            yield from map(ValueError, messages)
            return

        source_type_dbo = commit_dbo(engine, source_type_dbo)

        for special_value_definition_dbo in special_value_definition_dbos:
            special_value_definition_dbo.source_type_id = source_type_dbo.source_type_id
            messages = check_constraints(
                engine, [special_value_definition_dbo], cls.mapping(config)
            )
            if messages:
                yield from map(ValueError, messages)
                break
            commit_dbo(engine, special_value_definition_dbo)

    @classmethod
    def create_new_fields(cls, config):
        return ["name", "description", "manufacturer"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        name = ask("name", not_empty=True)
        description = ask("description")
        manufacturer = ask("manufacturer")

        data = dict(
            name=name,
            description=description,
            manufacturer=manufacturer,
            special_values=[
                dict(
                    categorical_value="NAN",
                    numerical_value="-9999",
                    description="encodes NAN",
                )
            ],
        )

        sub_folder = free_text_to_path_name(name)
        full_path = root_folder / "data" / sub_folder / "source_type.yaml"

        if full_path.exists():
            raise ValueError(
                f"folder {full_path} exist, you must choose a different name"
            )

        full_path.parent.mkdir(parents=True, exist_ok=True)

        with full_path.open("w") as fh:
            print_yaml(data, fh=fh)

        return full_path

    @classmethod
    def find_source_type(cls, root_folder, name):
        for item in cls.find_by_name(root_folder, name):
            if isinstance(item, Exception):
                continue
            return item
        return None

    @classmethod
    def check_before_delete(cls, config, engine, source_type_id):
        from .source import SourceDecl

        source_table = SourceDecl._schema
        source_names = engine.execute(
            select([source_table.c.name]).where(
                source_table.c.source_type_id == source_type_id
            )
        ).fetchall()
        if not source_names:
            yield True
            return
        yield "please delete the following sources first:"
        for [source_name] in source_names:
            yield f"  - {source_name}"
        yield False

    @classmethod
    def delete_from_db(cls, config, engine, source_type_id):
        engine.execute(
            delete(SpecialValueDefinitionDecl._schema).where(
                SpecialValueDefinitionDecl._schema.c.source_type_id == source_type_id
            )
        )
        engine.execute(
            delete(SourceTypeDecl._schema).where(
                SourceTypeDecl._schema.c.source_type_id == source_type_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        folder = yaml_path.parent
        try:
            shutil.rmtree(folder)
        except IOError as e:
            yield IOError(f"could not remove folder {folder}: {e}")
        yield f"deleted {folder} and its content"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
            data["name"] = new_name
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except (YamlError, IOError) as e:
            yield e
            return

        yield f"changed name in {yaml_path}"

        lz_root = yaml_path.parent.parent.parent

        affected_files_before = [yaml_path.relative_to(lz_root)]

        source_yamls = [
            p.relative_to(lz_root) for p in yaml_path.parent.glob("*/source.yaml")
        ]
        conversion_scripts = [
            p.relative_to(lz_root) for p in (lz_root / "data").glob("*/*/conversion.*")
        ]
        conversion_scripts += [
            p.relative_to(lz_root) for p in (lz_root / "data").glob("*/conversion.*")
        ]
        conversion_specs = [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob("*/*/raw_data/conversion_spec.yaml")
        ]
        conversion_specs += [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob("*/raw_data/conversion_spec.yaml")
        ]

        binary_folders = [
            p.relative_to(lz_root)
            for p in (lz_root / "data").glob(f"{old_name}/*/binary_data")
        ]
        affected_files_before += source_yamls
        affected_files_before += conversion_scripts
        affected_files_before += conversion_specs
        affected_files_before += binary_folders
        affected_files_before += [yaml_path.relative_to(lz_root).parent]

        renames = {}
        new_folder_name = free_text_to_path_name(new_name)
        for p in affected_files_before:
            parts = p.parts
            new_parts = parts[:1] + (new_folder_name,) + parts[2:]
            renames[p] = pathlib.Path(*new_parts)

        new_folder_path = yaml_path.parent.parent / new_folder_name
        try:
            yaml_path.parent.rename(new_folder_path)
        except IOError as e:
            yield e
            return

        yield f"renamed folder {yaml_path.parent} to {new_folder_path}"
        yield new_folder_path.relative_to(lz_root) / "source_type.yaml", renames

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        source_type_id = rename_data["item_id"]
        table = SourceTypeDecl._schema
        engine.execute(delete(table).where(table.c.source_type_id == source_type_id))
        yield f"deleted source_type with source_type_id={source_type_id}"
        table = SpecialValueDefinitionDecl._schema
        engine.execute(delete(table).where(table.c.source_type_id == source_type_id))
        yield (
            "deleted special value definitions with reference to"
            f" source_type_id={source_type_id}"
        )

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        source_type_id = rename_data["item_id"]
        table = SourceTypeDecl._schema
        engine.execute(
            update(table)
            .where(table.c.source_type_id == source_type_id)
            .values(name=new_name)
        )
        yield f"renamed source with source_type_id={source_type_id} to {new_name}"

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        for old_path in rename_data["renames"].keys():
            p = root_folder / old_path
            if p.is_dir():
                shutil.rmtree(p)
                yield f"removed folder {p}"
            else:
                p.unlink()
                yield f"removed file {p}"

        # cleanup folders
        # sort to delete subfolders first:
        for old_path in sorted(rename_data["renames"].keys(), key=len, reverse=True):
            p = root_folder / old_path
            if not p.is_dir():
                p = p.parent
            while p != root_folder:
                if p.exists() and not list(p.iterdir()):
                    p.rmdir()
                    yield f"removed empty folder {p}"
                    p = p.parent
                else:
                    break


class SourceTypeRawFile:
    pass
