# source,pa!/usr/bin/env python
import glob
import pathlib
import shutil
from collections import Counter

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Integer,
    LargeBinary,
    String,
    Table,
    UniqueConstraint,
    delete,
    insert,
    select,
    update,
)

from datapool.poolkit.database import (
    Base,
    ModifiedDbo,
    check_constraints,
    commit_dbo,
    create_dbo,
    fetch_many,
    fetch_one,
    find_differences,
)
from datapool.poolkit.type_handling import date_time_parser
from datapool.poolkit.utils import Asset, free_text_to_path_name
from datapool.poolkit.yaml import List, NotEmpty, Optional, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class SiteDecl(Base):
    _schema = Table(
        "site",
        Base._metadata,
        Column(
            "site_id", Integer(), Identity(always=True), primary_key=True, index=True
        ),
        Column("name", String(), unique=True, index=True, nullable=False),
        Column("description", String()),
    )


class SiteFieldDecl(Base):
    _schema = Table(
        "site_field",
        Base._metadata,
        Column(
            "site_field_id",
            Integer(),
            Identity(always=True),
            primary_key=True,
            index=True,
        ),
        Column("name", String(), unique=True, index=True, nullable=False),
    )


def init_controlled_vocabulary(engine, config):
    for field in site_fields(config):
        engine.execute(insert(SiteFieldDecl._schema).values(name=field))


def site_fields(config):
    return [s.strip() for s in config.site.fields.split(",")]


Base.register_db_setup_procedure(init_controlled_vocabulary)


class SiteFieldValueDecl(Base):
    _schema = Table(
        "site_field_value",
        Base._metadata,
        Column(
            "site_field_value_id",
            Integer(),
            Identity(always=True),
            primary_key=True,
            index=True,
        ),
        Column("site_id", ForeignKey("site.site_id"), nullable=False),
        Column("site_field_id", ForeignKey("site_field.site_field_id"), nullable=False),
        Column("value", String()),
    )


class PictureDecl(Base):
    _schema = Table(
        "picture",
        Base._metadata,
        Column("picture_id", Integer, primary_key=True, index=True),
        Column("site_id", ForeignKey("site.site_id"), nullable=False),
        Column("filename", String(), nullable=False),
        Column("description", String()),
        Column("timestamp", DateTime()),
        Column("data", LargeBinary()),
        UniqueConstraint("site_id", "filename"),
    )


site_yaml_example = """
name: test_site
description: All the details of the site.All the details
    the site.All the details of the site.All the details of the
    site.All the details of the site.All the details of the
    site.All the details of the site.
{}
# pictures are optional:
pictures:
    -
      path: images/IMG_0312.JPG
      description: outside
      # timestamp is optional:
      timestamp: 2016/02/01 12:00:00
    -
      path: images/IMG_0732.JPG
      description: manholeD1
""".strip()


picture_0 = Asset("assets/IMG_0312.JPG")
picture_1 = Asset("assets/IMG_0732.JPG")


class SiteYaml(Yaml):
    entity_name = "site"
    is_fact_dimension = True

    location = pathlib.Path("sites/*/site.yaml")

    @classmethod
    def related_files(cls):
        yield cls.location.parent / "images/*"
        yield cls.location

    def examples(config):
        lines = []
        for field in site_fields(config):
            lines.append(f"{field}: example {field}")

        return [
            ("sites/test_site/site.yaml", site_yaml_example.format("\n".join(lines))),
            ("sites/test_site/images/IMG_0312.JPG", picture_0),
            ("sites/test_site/images/IMG_0732.JPG", picture_1),
        ]

    dbos = [SiteDecl, SiteFieldDecl, SiteFieldValueDecl, PictureDecl]

    def mapping(config):
        default = {
            "name": NotEmpty(SiteDecl.name),
            "description": SiteDecl.description,
            "pictures": Optional(
                List(
                    {
                        "path": PictureDecl.filename,
                        "description": PictureDecl.description,
                        "timestamp": Optional(str),
                    }
                )
            ),
        }
        for field in site_fields(config):
            default[field] = str
        return default

    @classmethod
    def backup_files(cls, root_folder, rel_path):
        yield rel_path
        yield rel_path.parent / "images/*"

    @classmethod
    def create_new_fields(cls, config):
        return [
            "name",
            "description",
        ] + site_fields(config)

    @classmethod
    def create_new(cls, config, root_folder, ask):
        name = ask("name", not_empty=True)
        description = ask("description")
        site_data = {}
        for f in site_fields(config):
            site_data[f] = ask(f)

        data = dict(
            name=name,
            description=description,
            **site_data,
            pictures=[
                dict(
                    path="images/picture.png",
                    description="the picture is empty",
                    timestamp="2020/12/24 11:22:33",
                )
            ],
        )

        sub_folder = free_text_to_path_name(name)
        full_path = root_folder / "sites" / sub_folder / "site.yaml"

        if full_path.exists():
            raise ValueError(
                f"folder {full_path} exists, you must choose a different name"
            )

        images_folder = full_path.parent / "images"
        images_folder.mkdir(parents=True, exist_ok=True)

        (images_folder / "picture.png").open("w").close()

        with full_path.open("w") as fh:
            print_yaml(data, fh=fh)

        return full_path

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)

        for field in site_fields(config):
            if field not in yaml:
                yield ValueError(f"entry for '{field}' is missing.")

        root_folder = pathlib.Path(root_folder)
        site_folder = (root_folder / rel_path).parent

        images = set(site_folder.rglob("*/*"))

        declared_images = set()

        pictures = yaml.get("pictures")
        if pictures is not None:
            if not isinstance(pictures, list):
                yield ValueError(
                    f"entry pictures={pictures!r} in {rel_path} must be a list"
                )
            else:
                for picture in pictures:
                    full_path = pathlib.Path(site_folder) / picture["path"]
                    declared_images.add(full_path)
                    if not full_path.exists():
                        rel_path_picture = full_path.relative_to(root_folder)
                        yield ValueError(f"picture {rel_path_picture} missing")
                    timestamp = picture.get("timestamp")
                    if timestamp:
                        try:
                            date_time_parser(timestamp, allow_date_only=True)
                        except ValueError as e:
                            yield ValueError(
                                f"picture timetamp {timestamp} is invalid: {e}"
                            )

        for p in images - declared_images:
            rel_path_picture = p.relative_to(root_folder)
            yield ValueError(f"file {rel_path_picture} not referenced in site.yaml")

        counter = Counter([picture.get("path") for picture in yaml.get("pictures", [])])

        for path, count in counter.items():
            if path is None:
                continue
            if count == 1:
                continue
            yield ValueError(f"duplicate entries for file {path}")

        name = yaml.get("name")
        if name and rel_path.parent.name != name:
            yield ValueError(
                f"site name '{name}' does not match folder name '{rel_path.parent}'"
            )

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def prepare(cls, root_folder, rel_path, data):
        root_folder = pathlib.Path(root_folder)
        site_folder = (root_folder / rel_path).parent
        for picture in data.get("pictures", []):
            full_path = pathlib.Path(site_folder) / picture["path"]
            if full_path.exists():
                picture["data"] = full_path.read_bytes()
            else:
                picture["data"] = None
            picture["filename"] = pathlib.Path(picture["path"]).name
            del picture["path"]
            if "timestamp" in picture:
                picture["timestamp"] = date_time_parser(
                    picture["timestamp"], allow_date_only=True
                )
            else:
                picture["timestamp"] = None
        yield data

    @classmethod
    def find_by_name(cls, root_folder, name):
        for p in glob.glob(str(root_folder / cls.location)):
            p = pathlib.Path(p)
            try:
                data = parse_file(p)
            except YamlError as e:
                yield e
                return
            if data["name"] == name:
                yield p

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = SiteDecl._schema
        yield engine.execute(
            select([schema.c.site_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_against_db(cls, config, engine, data):
        fields = site_fields(config)

        site_field_id = {
            dbo.name: dbo.site_field_id for dbo in fetch_many(engine, SiteFieldDecl)
        }

        fields_data = [(site_field_id[name], data[name]) for name in fields]

        site_field_data = [
            dict(
                site_field_id=site_field_id,
                value=field_value,
            )
            for (site_field_id, field_value) in fields_data
        ]

        for field in fields:
            del data[field]

        ok = True
        pictures = data.get("pictures", [])
        if pictures:
            del data["pictures"]
        site_dbo = fetch_one(engine, SiteDecl, name=data["name"])

        if not site_dbo:
            site_dbo = create_dbo(SiteDecl, data)
            new_picture_dbos = [
                create_dbo(PictureDecl, picture) for picture in pictures
            ]
            modified_picture_dbos_ok = []
            site_field_value_dbos = [
                create_dbo(
                    SiteFieldValueDecl,
                    data,
                )
                for data in site_field_data
            ]
        else:
            picture_dbos = fetch_many(engine, PictureDecl, site_id=site_dbo.site_id)
            (
                new_picture_dbos,
                modified_picture_dbos_ok,
                modified_picture_dbos_not_ok,
                deleted_picture_dbos,
                same,
            ) = find_differences(
                PictureDecl,
                pictures,
                picture_dbos,
                ["filename"],
                ["timestamp", "data", "description"],
            )

            for picture in deleted_picture_dbos:
                yield ValueError(f"picture {picture['filename']} was deleted")
                ok = False

            for picture in modified_picture_dbos_not_ok:
                yield ValueError(f"picture {picture['filename']} was modified")
                ok = False

            data = {**site_dbo._data.copy(), **data}
            site_dbo = ModifiedDbo(SiteDecl._schema, data)

            site_field_value_dbos = []
            for data in site_field_data:
                site_field_id = data["site_field_id"]
                value = data["value"]
                dbo = fetch_one(
                    engine,
                    SiteFieldValueDecl,
                    site_field_id=site_field_id,
                    site_id=site_dbo.site_id,
                )
                dbo_new = ModifiedDbo(SiteFieldValueDecl._schema, dbo._data)
                dbo_new.value = value
                site_field_value_dbos.append(dbo_new)

            site_field_value_dbos = []
            for data in site_field_data:
                site_field_id = data["site_field_id"]
                value = data["value"]
                dbo = fetch_one(
                    engine,
                    SiteFieldValueDecl,
                    site_field_id=site_field_id,
                    site_id=site_dbo.site_id,
                )
                dbo_new = ModifiedDbo(SiteFieldValueDecl._schema, dbo._data)
                dbo_new.value = value
                site_field_value_dbos.append(dbo_new)

        if not ok:
            return

        yield (
            [site_dbo]
            + [new_picture_dbos + modified_picture_dbos_ok]
            + [site_field_value_dbos]
        )

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        site_dbo, picture_dbos, site_field_value_dbos = dbos
        messages = check_constraints(engine, [site_dbo], cls.mapping(config))
        if messages:
            yield from map(ValueError, messages)
            return

        site_dbo = commit_dbo(engine, site_dbo)
        for picture_dbo in picture_dbos:
            picture_dbo.site_id = site_dbo.site_id
            messages = check_constraints(engine, [picture_dbo], cls.mapping(config))
            if messages:
                yield from messages
                return

        for picture_dbo in picture_dbos:
            commit_dbo(engine, picture_dbo)

        for site_field_value_dbo in site_field_value_dbos:
            site_field_value_dbo.site_id = site_dbo.site_id
            commit_dbo(engine, site_field_value_dbo)

    @classmethod
    def delete_from_db(cls, config, engine, site_id):
        engine.execute(
            delete(SiteFieldValueDecl._schema).where(
                SiteFieldValueDecl._schema.c.site_id == site_id
            )
        )
        engine.execute(
            delete(PictureDecl._schema).where(PictureDecl._schema.c.site_id == site_id)
        )
        engine.execute(
            delete(SiteDecl._schema).where(SiteDecl._schema.c.site_id == site_id)
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        folder = yaml_path.parent
        try:
            shutil.rmtree(folder)
        except IOError as e:
            yield IOError(f"could not remove folder {folder}: {e}")
        yield f"deleted {folder} and its content"

    @classmethod
    def rename(cls, old_name, new_name, yaml_path):
        try:
            data = parse_file(yaml_path)
            data["name"] = new_name
            with yaml_path.open("w") as fh:
                print_yaml(data, fh=fh)
        except (YamlError, IOError) as e:
            yield e
            return

        yield f"changed name in {yaml_path}"

        lz_root = yaml_path.parent.parent.parent

        site_folder_path = yaml_path.parent

        # order matters:
        affected_files_before = [yaml_path.relative_to(lz_root)]
        affected_files_before += [
            p.relative_to(lz_root) for p in site_folder_path.glob("images/*")
        ]
        affected_files_before += [yaml_path.relative_to(lz_root).parent]

        renames = {}
        new_folder_name = free_text_to_path_name(new_name)
        for p in affected_files_before:
            parts = p.parts
            new_parts = parts[:1] + (new_folder_name,) + parts[2:]
            renames[p] = pathlib.Path(*new_parts)

        new_folder_path = yaml_path.parent.parent / new_folder_name
        try:
            yaml_path.parent.rename(new_folder_path)
        except IOError as e:
            yield e
            return

        yield f"renamed folder {yaml_path.parent} to {new_folder_path}"
        yield new_folder_path.relative_to(lz_root) / "site.yaml", renames

    @classmethod
    def prepare_rename_in_db(cls, config, engine, rename_data):
        site_id = rename_data["item_id"]
        table = SiteDecl._schema
        engine.execute(delete(table).where(table.c.site_id == site_id))
        yield f"deleted site with site_id={site_id}"

        table = SiteFieldValueDecl._schema
        engine.execute(delete(table).where(table.c.site_id == site_id))
        yield f"deleted site_field_value with reference to site with site_id={site_id}"

        table = PictureDecl._schema
        engine.execute(delete(table).where(table.c.site_id == site_id))
        yield f"deleted pictures with reference to site with site_id={site_id}"

    @classmethod
    def rename_in_db(cls, config, engine, rename_data):
        new_name = rename_data["new_name"]
        site_id = rename_data["item_id"]
        table = SiteDecl._schema
        engine.execute(
            update(table).where(table.c.site_id == site_id).values(name=new_name)
        )
        yield f"renamed site with site_id={site_id} to {new_name}"

    @classmethod
    def rename_in_operational(cls, root_folder, rename_data):
        for old_path in rename_data["renames"].keys():
            p = root_folder / old_path
            if p.is_dir():
                shutil.rmtree(p)
                yield f"removed folder {p}"
            else:
                p.unlink()
                yield f"removed file {p}"
