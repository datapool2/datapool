#!/usr/bin/env python
import pathlib
import shutil
from collections import Counter

from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    Table,
    UniqueConstraint,
    delete,
    select,
)

from datapool.poolkit.database import (
    JSON,
    Base,
    check_constraints,
    commit_dbo,
    create_dbo,
    fetch_many,
    fetch_one,
    find_differences,
)
from datapool.poolkit.utils import Asset, free_text_to_path_name
from datapool.poolkit.yaml import List, NotEmpty, Optional, Yaml
from datapool.poolkit.yaml_parser import print_yaml

from .meta_picture import MetaPictureDecl


class MetaDataDecl(Base):
    _schema = Table(
        "meta_data",
        Base._metadata,
        Column("meta_data_id", Integer(), primary_key=True, index=True),
        Column("source_id", ForeignKey("source.source_id"), nullable=False),
        Column("site_id", ForeignKey("site.site_id"), nullable=False),
        Column("description", String()),
        Column("additional_meta_info", JSON()),
        UniqueConstraint("source_id", "site_id"),
    )


meta_data_yaml_example = """
site: test_site
source: sensor_abc_from_xyz
description: don't know
# pictures are optional:
pictures:
    -
      path: images/IMG_0312.JPG
      description:
    -
      path: images/IMG_0732.JPG
      description: manholeD1
additional_meta_info:
    who: uwe schmitt
    when: never
    what else:
        -
           message: don't panic
        -
           message: and thanks for the fish
"""


picture_0 = Asset("assets/IMG_0312.JPG")
picture_1 = Asset("assets/IMG_0732.JPG")


class MetaDataYaml(Yaml):
    entity_name = "meta_data"
    is_fact_dimension = False

    depends_on = ["site", "source"]

    location = pathlib.Path("meta_data/*/*/meta_data.yaml")

    @classmethod
    def related_files(cls):
        yield cls.location.parent / "images/*"
        yield cls.location

    examples = lambda config: [
        (
            "meta_data/sensor_abc_from_xyz/test_site/meta_data.yaml",
            meta_data_yaml_example,
        ),
        (
            "meta_data/sensor_abc_from_xyz/test_site/images/IMG_0312.JPG",
            picture_0,
        ),
        (
            "meta_data/sensor_abc_from_xyz/test_site/images/IMG_0732.JPG",
            picture_1,
        ),
    ]

    dbos = [MetaDataDecl, MetaPictureDecl]

    mapping = lambda config: {
        "source": NotEmpty(str),
        "site": NotEmpty(str),
        "description": MetaDataDecl.description,
        "additional_meta_info": Optional(MetaDataDecl.additional_meta_info),
        "pictures": Optional(
            List(
                {
                    "path": MetaPictureDecl.filename,
                    "description": Optional(MetaPictureDecl.description),
                }
            )
        ),
    }

    @classmethod
    def backup_files(cls, root_folder, rel_path):
        yield rel_path
        yield rel_path.parent / "images/*"

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        found_error = False
        for result in super().check_yaml(root_folder, rel_path, yaml, config):
            yield result
            if isinstance(result, Exception):
                found_error = True
        if found_error:
            return

        root_folder = pathlib.Path(root_folder)
        local_folder = (root_folder / rel_path).parent

        images = set(local_folder.rglob("images/*"))
        declared_images = set()

        pictures = yaml.get("pictures", [])
        found_error = False
        for picture in pictures:
            full_path = pathlib.Path(local_folder) / picture["path"]
            declared_images.add(full_path)
            if not full_path.exists():
                rel_path_img = full_path.relative_to(root_folder)
                yield ValueError(f"picture {rel_path_img} missing")
                found_error = True

        if not is_dispatcher:
            for p in images - declared_images:
                rel_path_picture = p.relative_to(root_folder)
                yield ValueError(
                    f"file {rel_path_picture} not referenced in meta_data.yaml"
                )
                found_error = True

        counter = Counter([picture["path"] for picture in yaml.get("pictures", [])])

        for path, count in counter.items():
            if count == 1:
                continue
            yield ValueError(f"duplicate entries for file {path}")

        if "source" in yaml:
            source = yaml["source"]
            if source and rel_path.parent.parent.name != source:
                yield ValueError(
                    f"source '{source}' does not match folder name"
                    f" '{rel_path.parent.parent}'"
                )

        if "site" in yaml:
            site = yaml["site"]
            if site and rel_path.parent.name != site:
                yield ValueError(
                    f"site '{site}' does not match folder name" f" '{rel_path.parent}'"
                )

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = MetaDataDecl._schema
        yield engine.execute(
            select([schema.c.meta_data_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def prepare(cls, root_folder, rel_path, data):
        local_folder = (root_folder / rel_path).parent
        for picture in data.get("pictures", []):
            full_path = pathlib.Path(local_folder) / picture["path"]
            if full_path.exists():
                picture["data"] = full_path.read_bytes()
                picture["filename"] = pathlib.Path(picture["path"]).name
                del picture["path"]
                picture["description"] = picture.get("description")
            else:
                raise RuntimeError("this should have been detected earlier")
        yield data

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, data):
        from .site import SiteDecl
        from .source import SourceDecl

        source_name = data["source"]
        site_name = data["site"]
        source_dbo = fetch_one(engine, SourceDecl, name=source_name)
        site_dbo = fetch_one(engine, SiteDecl, name=site_name)

        ok = True
        if not source_dbo:
            yield ValueError(f"source with name '{source_name}' does not exist")
            ok = False
        if not site_dbo:
            yield ValueError(f"site with name '{site_name}' does not exist")
            ok = False
        if not ok:
            return

        data["source_id"] = source_dbo.source_id
        del data["source"]
        data["site_id"] = site_dbo.site_id
        del data["site"]
        meta_data_dbo = fetch_one(
            engine,
            MetaDataDecl,
            site_id=site_dbo.site_id,
            source_id=source_dbo.source_id,
        )

        pictures = data.get("pictures", [])
        if pictures:
            del data["pictures"]

        if meta_data_dbo is None:
            meta_data_dbo = create_dbo(MetaDataDecl, data)
            new_picture_dbos = [
                create_dbo(MetaPictureDecl, picture) for picture in pictures
            ]
            modified_picture_dbos_ok = []
        else:
            picture_dbos = fetch_many(
                engine, MetaPictureDecl, meta_data_id=meta_data_dbo.meta_data_id
            )
            (
                new_picture_dbos,
                modified_picture_dbos_ok,
                modified_picture_dbos_not_ok,
                deleted_picture_dbos,
                same,
            ) = find_differences(
                MetaPictureDecl, pictures, picture_dbos, ["filename"], ["description"]
            )

            ok = True
            for picture in deleted_picture_dbos:
                yield ValueError(f"picture {picture['filename']} was deleted")
                ok = False

            for picture in modified_picture_dbos_not_ok:
                yield ValueError(f"picture {picture['filename']} was modified")
                ok = False

            if not ok:
                return

        # TODO: werden geaenderte additional_meta_info auch geschrieben?
        yield [meta_data_dbo] + new_picture_dbos + modified_picture_dbos_ok
        return

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        meta_data_dbo, *picture_dbos = dbos
        messages = check_constraints(engine, [meta_data_dbo], cls.mapping(config))
        if messages:
            yield from map(ValueError, messages)
            return

        meta_data_dbo = commit_dbo(engine, meta_data_dbo)
        for picture_dbo in picture_dbos:
            picture_dbo.meta_data_id = meta_data_dbo.meta_data_id
            messages = check_constraints(engine, [picture_dbo], cls.mapping(config))
            if messages:
                yield from messages
                return

        for picture_dbo in picture_dbos:
            commit_dbo(engine, picture_dbo)

    @classmethod
    def create_new_fields(cls, config):
        return ["site", "source", "description"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        site = ask("site", not_empty=True)
        source = ask("source", not_empty=True)
        description = ask("description")

        source_folder = free_text_to_path_name(source)
        site_folder = free_text_to_path_name(site)
        full_path = (
            root_folder / "meta_data" / source_folder / site_folder / "meta_data.yaml"
        )

        if full_path.exists():
            raise ValueError(
                f"file {full_path} exists, you must choose a different source or site"
                " name"
            )

        images_folder = full_path.parent / "images"
        images_folder.mkdir(parents=True, exist_ok=True)

        (images_folder / "picture.png").open("w").close()

        data = dict(
            site=site,
            source=source,
            description=description,
            pictures=[
                dict(
                    path="images/picture.png",
                    description="the picture is empty",
                )
            ],
        )

        full_path.parent.mkdir(parents=True, exist_ok=True)
        with full_path.open("w") as fh:
            print_yaml(data, fh=fh)

        return full_path

    @classmethod
    def delete_from_db(cls, config, engine, meta_data_id):
        engine.execute(
            delete(MetaPictureDecl._schema).where(
                MetaPictureDecl._schema.c.meta_data_id == meta_data_id
            )
        )
        engine.execute(
            delete(MetaDataDecl._schema).where(
                MetaDataDecl._schema.c.meta_data_id == meta_data_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        folder = yaml_path.parent
        try:
            shutil.rmtree(folder)
        except IOError as e:
            yield IOError(f"could not remove folder {folder}: {e}")
        yield f"deleted {folder} and its content"
