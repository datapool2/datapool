#!/usr/bin/env python
import fnmatch
import operator

from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    Table,
    and_,
    delete,
    func,
    join,
    select,
)

from datapool.poolkit.database import Base
from datapool.poolkit.type_handling import date_parser, date_time_parser
from datapool.poolkit.utils import Asset, is_number, print_query
from datapool.poolkit.yaml import RawDataFile
from datapool.poolkit.yaml_parser import parse_file


class SignalDecl(Base):
    _schema = Table(
        "signal",
        Base._metadata,
        Column("signal_id", Integer(), primary_key=True),
        Column("value", Float(), nullable=False),
        Column("timestamp", DateTime(), nullable=False, index=True),
        Column("variable_id", ForeignKey("variable.variable_id"), nullable=False),
        Column("source_id", ForeignKey("source.source_id"), nullable=False),
        Column("site_id", ForeignKey("site.site_id"), nullable=True),
        Column("coord_x", String(), nullable=True),
        Column("coord_y", String(), nullable=True),
        Column("coord_z", String(), nullable=True),
    )


raw_data_example = Asset("assets/data-001.raw")
conversion_script_example = Asset("assets/conversion.py")
conversion_spec_yaml = Asset("assets/conversion_spec.yaml")


class Signals(RawDataFile):
    depends_on = ["source", "variable", "site"]
    script_depends_on = ["source", "variable", "site"]

    entity_name = "signals"
    examples = lambda config: [
        (
            "data/sensor_from_xyz/sensor_abc_from_xyz/conversion.py",
            conversion_script_example,
        ),
        (
            "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/data-001.raw",
            raw_data_example,
        ),
        (
            "data/sensor_from_xyz/sensor_abc_from_xyz/raw_data/conversion_spec.yaml",
            conversion_spec_yaml,
        ),
    ]

    location = ("data/*/*/raw_data/*.raw",)
    CONVERSION_SCRIPT_EXTENSIONS = ("py", "r", "R", "jl", "m")
    conversion_script_locations = [
        f"{rel_path_pattern}/conversion.{ext}"
        for ext in CONVERSION_SCRIPT_EXTENSIONS
        for rel_path_pattern in ("data/*/*",)
    ]

    @classmethod
    def related_files(cls):
        yield from (
            f"data/*/*/conversion.{ext}" for ext in cls.CONVERSION_SCRIPT_EXTENSIONS
        )
        yield "data/*/*/raw_data/conversion_spec.yaml"
        yield from cls.location

    @classmethod
    def match_conversion_script(cls, rel_path):
        return any(
            fnmatch.fnmatch(rel_path, f"data/*/*/conversion.{ext}")
            for ext in cls.CONVERSION_SCRIPT_EXTENSIONS
        )

    @classmethod
    def data_files_from_backup(cls, backup_folder):
        for pattern in cls.location:
            yield from backup_folder.glob(pattern + ".*")

    @classmethod
    def data_files(cls, lz):
        for pattern in cls.location:
            yield from lz.glob(pattern)

    @classmethod
    def setup_conversion(cls, landing_zone, rel_path):
        if rel_path.match("*.raw") or rel_path.match("*/raw_data/conversion_spec.yaml"):
            base_folder = landing_zone / rel_path.parent.parent
        else:
            base_folder = landing_zone / rel_path.parent

        if rel_path.match("*.raw"):
            raw_data = [landing_zone / rel_path]
        else:
            raw_data = sorted(base_folder.glob("raw_data/*.raw"))

        scripts = sorted(base_folder.glob("conversion.*"))
        # raw_data = sorted(base_folder.glob("raw_data/*.raw"))
        conversion_spec = list(base_folder.glob("raw_data/conversion_spec.yaml"))
        conversion_spec = None if not conversion_spec else conversion_spec[0]
        return scripts, raw_data, conversion_spec

    @classmethod
    def get_db_related_coordinates(cls):
        from .site import SiteDecl
        from .source import SourceDecl
        from .variables import VariableDecl

        return {
            "variable": (VariableDecl, "name"),
            "site": (SiteDecl, "name"),
            "source": (SourceDecl, "name"),
        }

    @classmethod
    def check_fields(cls, signal):
        required = set(
            (
                "timestamp",
                "variable",
                "value",
            )
        )
        optional = set(("site", "coord_x", "coord_y", "coord_z"))

        ok = True
        fields = set(signal.keys())
        wrong = fields - required - optional
        if wrong:
            wrong_str = ", ".join(f"'{s}'" for s in sorted(wrong))
            yield ValueError(f"signal attributes {wrong_str} not allowed")
            ok = False

        missing = required - fields
        if missing:
            missing_str = ", ".join(f"'{s}'" for s in sorted(missing))
            yield ValueError(f"signal attributes {missing_str} missing")
            ok = False

        if "site" in fields:
            if "coord_x" in fields or "coord_y" in fields or "coord_z" in fields:
                yield ValueError(
                    "signal has columns site and {coord_x,coord_y,coord_z}"
                )
                ok = False
        else:
            if not ("coord_x" in fields or "coord_y" in fields or "coord_z" in fields):
                yield ValueError(
                    "signal has neither column site or {coord_x,coord_y,coord_z}"
                )
                ok = False
        if not ok:
            return

        timestamp = signal.get("timestamp")
        try:
            date_time_parser(timestamp)
        except ValueError as e:
            yield e
        value = signal.get("value")
        if not is_number(value):
            yield ValueError(f"value entry '{value}' is not a valid number")
        variable = signal.get("variable", "").strip()
        if not variable:
            yield ValueError(f"variable entry '{variable}' is empty")

        if "site" in signal:
            if signal["site"] == "":
                yield ValueError("entry for site is empty")

        for name in ("coord_x", "coord_y", "coord_z"):
            v = signal.get(name)
            if v is not None and v != "":
                if not is_number(v):
                    yield ValueError(f"{name} entry {v} is not valid")

    @classmethod
    def prepare_facts(cls, signals, script_path, raw_data_files, conversion_spec_path):
        source_yaml = script_path.parent / "source.yaml"
        if not source_yaml.exists():
            yield IOError(f"file {source_yaml} does not exist")
            return

        source_data = parse_file(source_yaml)
        if "name" not in source_data:
            yield ValueError(f"file {source_yaml} has no entry for 'name'")
        source_name = source_data["name"]
        for signal in signals:
            signal["source"] = source_name
            for f in "xyz":
                signal[f"coord_{f}"] = signal.get(f"coord_{f}")
            signal["timestamp"] = date_time_parser(signal["timestamp"])
            signal["value"] = float(signal["value"])

    @classmethod
    def check_if_facts_conflict(cls, signals):
        lookup = {}
        unique_fields = ("timestamp", "variable", "source")
        duplicate_signals = 0
        for signal in signals:
            key = tuple(signal[field] for field in unique_fields)
            if key not in lookup:
                lookup[key] = signal
                continue
            existing_signal = lookup[key]
            if existing_signal == signal:
                duplicate_signals += 1
                continue
            coordinates = ", ".join(
                f"{name}={value}" for (name, value) in zip(unique_fields, key)
            )
            differences = ", ".join(
                f"{k}={existing_signal[k]} vs {signal[k]}"
                for k in signal.keys()
                if k not in unique_fields and existing_signal[k] != signal[k]
            )

            yield ValueError(
                f"conflicting signals having same ({coordinates}), but"
                f" different values: {differences}"
            )
        if duplicate_signals:
            yield f"detected {duplicate_signals} duplicate signals"

    @classmethod
    def check_fact_against_db(cls, ctx, signal):
        if "site" in signal:
            site = signal["site"]
            if ctx.get("site", site) is None:
                yield ValueError(f"site '{site}' does not exist")
        variable = signal["variable"]
        if ctx.get("variable", variable) is None:
            yield ValueError(f"variable '{variable}' does not exist")

        source = signal["source"]
        if ctx.get("source", source) is None:
            yield ValueError(f"source '{source}' does not exist")

    @classmethod
    def retrieve_existing_facts(cls, ctx, signals):
        variables = list(signal["variable"] for signal in signals)
        sources = list(signal["source"] for signal in signals)

        timestamps = list(signal["timestamp"] for signal in signals)

        variable_ids = list(
            ctx.get("variable", variable)["variable_id"] for variable in variables
        )

        source_ids = list(ctx.get("source", source)["source_id"] for source in sources)

        schema = SignalDecl._schema

        candidates = ctx.engine.execute(
            select(
                [
                    schema.c.timestamp,
                    schema.c.source_id,
                    schema.c.variable_id,
                    schema.c.site_id,
                    schema.c.coord_x,
                    schema.c.coord_y,
                    schema.c.coord_z,
                    schema.c.value,
                ]
            ).where(
                and_(
                    schema.c.variable_id.in_(set(variable_ids)),
                    schema.c.source_id.in_(set(source_ids)),
                    schema.c.timestamp.between(min(timestamps), max(timestamps)),
                )
            )
        )

        def equal(signal, signal_db):
            site_id, coord_x, coord_y, coord_z, value = signal_db
            if value != signal["value"]:
                return False
            if site_id is not None:
                if site_id != ctx.get("site", signal["site"])["site_id"]:
                    return False
                return True
            if coord_x != signal["coord_x"]:
                return False
            if coord_y != signal["coord_y"]:
                return False
            if coord_z != signal["coord_z"]:
                return False
            return True

        found = {tuple(row[:3]): row[3:] for row in candidates}
        new_signals = []
        modified_signals = []
        for timestamp, source_id, variable_id, signal in zip(
            timestamps, source_ids, variable_ids, signals
        ):
            key = (timestamp, source_id, variable_id)
            signal_db = found.get(key)
            if signal_db is None:
                new_signals.append(signal)
                continue
            if equal(signal, signal_db):
                continue
            modified_signals.append((signal, signal_db))
        yield (new_signals, modified_signals)

    @classmethod
    def commit_facts(cls, ctx, signals):
        for signal in signals:
            if "site" in signal:
                signal["site_id"] = ctx.get("site", signal.get("site"))["site_id"]
                del signal["site"]

            signal["source_id"] = ctx.get("source", signal.get("source"))["source_id"]
            del signal["source"]

            signal["variable_id"] = ctx.get("variable", signal.get("variable"))[
                "variable_id"
            ]
            del signal["variable"]

        batch_size = 10_000
        for start_idx in range(0, len(signals), batch_size):
            yield f"commit batch starting at {start_idx}"
            batch = signals[start_idx : start_idx + batch_size]
            ctx.engine.execute(SignalDecl._schema.insert(), *batch)

    @classmethod
    def count_facts(cls, engine, conditions):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        yield engine.execute(
            select([func.count(SignalDecl._schema.c.signal_id)]).where(and_(*checks))
        ).scalar()

    @classmethod
    def summary(cls, engine, max_rows, conditions, print_, indent):
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return

        from .site import SiteDecl
        from .source import SourceDecl
        from .variables import VariableDecl

        sigd = SignalDecl._schema
        pd = VariableDecl._schema
        sited = SiteDecl._schema
        sourced = SourceDecl._schema

        j = join(sigd, pd, sigd.c.variable_id == pd.c.variable_id)
        j = join(j, sited, sigd.c.site_id == sited.c.site_id, isouter=True)
        j = join(j, sourced, sigd.c.source_id == sourced.c.source_id)

        stmt = (
            select(
                [
                    sigd.c.signal_id,
                    sigd.c.timestamp,
                    sigd.c.value,
                    pd.c.name.label("variable"),
                    sourced.c.name.label("source"),
                    sited.c.name.label("site"),
                    sigd.c.coord_x,
                    sigd.c.coord_y,
                    sigd.c.coord_z,
                ]
            )
            .where(and_(*checks))
            .select_from(j)
        )

        header = [
            "signal_id",
            "timestamp",
            "value",
            "variable",
            "source",
            "site",
            "coord_x",
            "coord_y",
            "coord_z",
        ]

        print_query(engine, stmt, header, indent, print_, max_rows)

    @classmethod
    def supported_fields_for_delete(cls):
        return set(
            (
                "timestamp",
                "site",
                "site_id",
                "source",
                "source_id",
                "source_type",
                "source_type_id",
                "variable",
                "variable_id",
                "project",
                "project_id",
                "coord_x",
                "coord_y",
                "coord_z",
            )
        )

    @classmethod
    def _translate_conditions(cls, engine, conditions):
        from .projects import ProjectDecl
        from .site import SiteDecl
        from .source import SourceDecl
        from .source_type import SourceTypeDecl
        from .variables import VariableDecl

        foreign_table_schemata = {
            "variable": VariableDecl._schema,
            "site": SiteDecl._schema,
            "source": SourceDecl._schema,
        }

        signal_schema = SignalDecl._schema

        supported_names = cls.supported_fields_for_delete()

        errors = []

        checks = []
        for name, op, value in conditions:
            if name not in supported_names:
                s = ", ".join(sorted(supported_names))
                errors.append(f"field {name} does not exist, please chose one of {s}")
                continue
            if name in ("source_type", "source_type_id"):
                if op != "==":
                    errors.append("source types can only be filtered using '=='")
                    continue
                if name == "source_type":
                    source_type_id = engine.execute(
                        select([SourceTypeDecl._schema.c.source_type_id]).where(
                            SourceTypeDecl._schema.c.name == value
                        )
                    ).scalar()

                    if source_type_id is None:
                        errors.append(f"source_type {value!r} does not exist")
                        continue
                else:
                    try:
                        source_type_id = int(value)
                    except ValueError:
                        errors.append(f"{name} {value} is not valid integer")
                        continue
                source_ids = engine.execute(
                    select([SourceDecl._schema.c.source_id]).where(
                        SourceDecl._schema.c.source_type_id == source_type_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(signal_schema.c.source_id.in_(source_ids))
                continue

            if name in ("project", "project_id"):
                if op != "==":
                    errors.append("projects can only be filtered using '=='")
                    continue
                if name == "project":
                    project_id = engine.execute(
                        select([ProjectDecl._schema.c.project_id]).where(
                            ProjectDecl._schema.c.title == value
                        )
                    ).scalar()
                    if project_id is None:
                        errors.append(f"project with title {value!r} does not exist")
                        continue
                else:
                    try:
                        project_id = int(value)
                    except ValueError:
                        errors.append(f"{name} {value} is not valid integer")
                        continue

                source_ids = engine.execute(
                    select([SourceDecl._schema.c.source_id]).where(
                        SourceDecl._schema.c.project_id == project_id
                    )
                ).fetchall()
                source_ids = [sid[0] for sid in source_ids]
                checks.append(signal_schema.c.source_id.in_(source_ids))
                continue

            if name in foreign_table_schemata:
                schema = foreign_table_schemata[name]
                item_id = engine.execute(
                    select([getattr(schema.c, name + "_id")]).where(
                        schema.c.name == value
                    )
                ).scalar()
                if item_id is None:
                    errors.append(f"{name} {value!r} does not exist")
                    continue
                if op not in ("==", "!="):
                    errors.append(f"{name} can only be filtered using '==' and '!=")
                    continue
                if op == "==":
                    checks.append(getattr(signal_schema.c, name + "_id") == item_id)
                else:
                    checks.append(getattr(signal_schema.c, name + "_id") != item_id)
                continue

            if name.endswith("_id"):
                if op != "==":
                    errors.append("ids can only be filtered using '=='")
                    continue
                try:
                    value = int(value)
                except ValueError:
                    errors.append(f"{name} {value} is not valid integer")
                    continue

                checks.append(getattr(signal_schema.c, name) == value)
                continue

            if name == "timestamp":
                try:
                    value = date_time_parser(value)
                except ValueError:
                    try:
                        value = date_parser(value)
                    except ValueError:
                        errors.append(
                            ValueError(f"'{value}' is neither a timestamp nor a date")
                        )
                        continue

            op_method = {
                "<": operator.lt,
                ">": operator.gt,
                "<=": operator.le,
                ">=": operator.ge,
                "==": operator.eq,
                "!=": operator.ne,
            }[op]

            checks.append(op_method(getattr(signal_schema.c, name), value))

        return errors, checks

    @classmethod
    def delete(cls, engine, conditions):
        from .signal_quality import SignalsSignalQualityAssociationsDecl as Assoc

        assoc_table = Assoc._schema
        errors, checks = cls._translate_conditions(engine, conditions)
        if errors:
            yield from map(Exception, errors)
            return
        signal_table = SignalDecl._schema
        condition = and_(*checks)

        engine.execute(
            delete(assoc_table).where(
                assoc_table.c.signal_id.in_(
                    select([signal_table.c.signal_id]).where(condition)
                )
            )
        )

        engine.execute(delete(signal_table).where(condition))
