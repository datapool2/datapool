#!/usr/bin/env python
from collections import Counter

from sqlalchemy import Column, Integer, String, Table, delete, select

from datapool.poolkit.database import (
    Base,
    check_constraints,
    commit_dbo,
    fetch_many,
    find_differences,
    find_duplicates,
)
from datapool.poolkit.yaml import List, NotEmpty, Yaml
from datapool.poolkit.yaml_parser import YamlError, parse_file, print_yaml


class MetaFlagDecl(Base):
    _schema = Table(
        "meta_flag",
        Base._metadata,
        Column("meta_flag_id", Integer(), primary_key=True, index=True),
        Column("name", String(), nullable=False),
        Column("description", String()),
    )


meta_flags_yaml_example = """
-
    name: OK
    description: relax
-
    name: ALERT
    description: something went wrong
-
    name: SEVERE_ALERT
    description: something went terribly wrong
"""


class MetaFlagYaml(Yaml):
    entity_name = "meta_flag"
    is_fact_dimension = True

    depends_on = []

    location = "meta_data/meta_flags.yaml"

    @classmethod
    def related_files(cls):
        yield cls.location

    examples = lambda config: [
        (
            "meta_data/meta_flags.yaml",
            meta_flags_yaml_example,
        )
    ]

    dbos = [MetaFlagDecl]

    mapping = lambda config: List(
        {"name": NotEmpty(MetaFlagDecl.name), "description": MetaFlagDecl.description}
    )

    @classmethod
    def _read_yaml(cls, meta_flags_file_path):
        if meta_flags_file_path.exists():
            try:
                return parse_file(meta_flags_file_path)
            except YamlError as e:
                return YamlError(f"file {meta_flags_file_path} is invald: {e}")
        else:
            return []

    @classmethod
    def create_new_fields(cls, config):
        return ["name", "description"]

    @classmethod
    def create_new(cls, config, root_folder, ask):
        meta_flags_file_path = root_folder / cls.location
        meta_flags = cls._read_yaml(meta_flags_file_path)
        if isinstance(meta_flags, Exception):
            raise meta_flags

        name_to_meta_flags = {p["name"]: p for p in meta_flags}

        name = ask("name", not_empty=True)
        if name in name_to_meta_flags:
            raise ValueError(f"metaflag with name '{name}' exists already")

        description = ask("description")

        meta_flags.append(dict(name=name, description=description))

        parent = meta_flags_file_path.parent
        parent.mkdir(parents=True, exist_ok=True)

        with meta_flags_file_path.open("w") as fh:
            print_yaml(meta_flags, fh=fh)

        return meta_flags_file_path

    @classmethod
    def find_by_name(cls, root_folder, name):
        meta_flags = cls._read_yaml(root_folder / cls.location)
        if isinstance(meta_flags, Exception):
            yield meta_flags
            return
        for entry in meta_flags:
            if entry["name"] == name:
                yield root_folder / cls.location

    @classmethod
    def find_in_db(cls, config, engine, name):
        schema = MetaFlagDecl._schema
        yield engine.execute(
            select([schema.c.meta_flag_id]).where(schema.c.name == name)
        ).scalar()

    @classmethod
    def check_yaml(cls, root_folder, rel_path, yaml, config, is_dispatcher=False):
        yield from super().check_yaml(root_folder, rel_path, yaml, config)
        counter = Counter(entry.get("name") for entry in yaml)
        for name, count in counter.items():
            if name is None:
                continue
            if count > 1:
                yield ValueError(f"meta_flag with name '{name}' appears {count} times")

    @classmethod
    def check_conflicts(cls, yamls):
        return
        yield

    @classmethod
    def check_against_db(cls, config, engine, meta_flags):
        messages = []

        duplicates, unique_meta_flags = find_duplicates(meta_flags, ["name"])

        reported_names = set()
        for duplicate in duplicates:
            name = duplicate["name"]
            if name in reported_names:
                continue
            reported_names.add(name)
            messages.append(f"duplicate entry with name '{name}'")

        existing_meta_flag_dbos = fetch_many(engine, MetaFlagDecl)
        (
            new_meta_flag_dbos,
            modified_meta_flag_dbos_ok,
            modified_meta_flag_dbos_not_ok,
            deleted_meta_flag_dbos,
            same,
        ) = find_differences(
            MetaFlagDecl, unique_meta_flags, existing_meta_flag_dbos, ["name"], []
        )

        for meta_flag_dbo in deleted_meta_flag_dbos:
            name = meta_flag_dbo["name"]
            messages.append(f"meta_flag '{name}' was deleted")

        for meta_flag_dbo in modified_meta_flag_dbos_not_ok:
            name = meta_flag_dbo["name"]
            messages.append(f"meta_flag '{name}' was modified")

        if messages:
            yield from map(ValueError, messages)
            return

        yield new_meta_flag_dbos + modified_meta_flag_dbos_ok

    @classmethod
    def commit_dbos(cls, config, engine, dbos):
        messages = check_constraints(engine, dbos, cls.mapping(config).specification)
        if messages:
            yield from map(ValueError, messages)
            return
        for dbo in dbos:
            commit_dbo(engine, dbo)

    @classmethod
    def delete_from_db(cls, config, engine, meta_flag_id):
        engine.execute(
            delete(MetaFlagDecl._schema).where(
                MetaFlagDecl._schema.c.meta_flag_id == meta_flag_id
            )
        )
        return
        yield

    @classmethod
    def delete_from_lz(cls, yaml_path, name):
        meta_flags = cls._read_yaml(yaml_path)
        if isinstance(meta_flags, Exception):
            yield meta_flags
            return
        meta_flags = [p for p in meta_flags if p["name"] != name]
        with yaml_path.open("w") as fh:
            print_yaml(meta_flags, fh=fh)

        yield f"deleted meta_flag '{name}' from {yaml_path}"
