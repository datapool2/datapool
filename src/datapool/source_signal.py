#!/usr/bin/env python

import fnmatch

from datapool.poolkit.type_handling import date_time_parser
from datapool.poolkit.utils import Asset
from datapool.poolkit.yaml import RawDataFile

from .signal import Signals

raw_data_example = Asset("assets/data-001-sourcetype.raw")
conversion_script_example = Asset("assets/conversion_sourcetype.py")
conversion_spec_yaml = Asset("assets/conversion_spec.yaml")


class SourceSignals(Signals, RawDataFile):
    depends_on = ["source_type", "variable", "site"]
    script_depends_on = ["source_type", "variable", "site"]

    entity_name = "signals"
    examples = lambda config: [
        (
            "data/sensor_from_xyz/conversion.py",
            conversion_script_example,
        ),
        (
            "data/sensor_from_xyz/raw_data/data-001.raw",
            raw_data_example,
        ),
        (
            "data/sensor_from_xyz/raw_data/conversion_spec.yaml",
            conversion_spec_yaml,
        ),
    ]

    location = ("data/*/raw_data/*.raw",)
    CONVERSION_SCRIPT_EXTENSIONS = ("py", "r", "R", "jl", "m")
    conversion_script_locations = [
        f"{rel_path_pattern}/conversion.{ext}"
        for ext in CONVERSION_SCRIPT_EXTENSIONS
        for rel_path_pattern in ("data/*",)
    ]

    @classmethod
    def related_files(cls):
        yield from (
            f"data/*/conversion.{ext}" for ext in cls.CONVERSION_SCRIPT_EXTENSIONS
        )
        yield "data/*/conversion.r"
        yield "data/*/conversion.jl"
        yield "data/*/conversion.m"
        yield "data/*/raw_data/conversion_spec.yaml"
        yield from cls.location

    @classmethod
    def match_conversion_script(cls, rel_path):
        return any(
            fnmatch.fnmatch(rel_path, f"data/*/conversion.{ext}")
            for ext in cls.CONVERSION_SCRIPT_EXTENSIONS
        )

    @classmethod
    def check_fields(cls, signal):
        if "source" not in signal:
            yield ValueError("signal attribute 'source' is missing")
            return
        source = signal.pop("source")
        yield from super().check_fields(signal)
        signal["source"] = source

    @classmethod
    def prepare_facts(cls, signals, script_path, raw_data_files, conversion_spec_path):
        for signal in signals:
            for f in "xyz":
                signal[f"coord_{f}"] = signal.get(f"coord_{f}")
            signal["timestamp"] = date_time_parser(signal["timestamp"])
            signal["value"] = float(signal["value"])
        return
        yield
