#!/usr/bin/env python


from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table

from datapool.poolkit.database import Base


class SignalQualityDecl(Base):
    _schema = Table(
        "signal_quality",
        Base._metadata,
        Column("signal_quality_id", Integer(), primary_key=True, index=True),
        Column(
            "quality_id", ForeignKey("quality.quality_id"), index=True, nullable=False
        ),
        Column("timestamp", DateTime()),
        Column("author", String()),
    )


class QualityDecl(Base):
    _schema = Table(
        "quality",
        Base._metadata,
        Column("quality_id", Integer(), primary_key=True, index=True),
        Column("flag", String(), nullable=False),
        Column("method", String()),
    )


class SignalsSignalQualityAssociationsDecl(Base):
    _schema = Table(
        "signals_signal_quality_association",
        Base._metadata,
        Column("signal_id", ForeignKey("signal.signal_id"), index=True, nullable=False),
        Column(
            "signal_quality_id",
            ForeignKey("signal_quality.signal_quality_id"),
            index=True,
            nullable=False,
        ),
    )
