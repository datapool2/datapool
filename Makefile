.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist

help:
	@echo
	@echo "style-check - check code with flake8"
	@echo "reformat    - runs black to reformat code"
	@echo "test        - run tests quickly with the default Python"
	@echo "test-all    - run tests on every Python version with tox"
	@echo "coverage    - check code coverage quickly with the default Python"
	@echo "docs        - generate Sphinx HTML documentation, including API docs"
	@echo "sdist       - package"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

reformat:
	black -l 88 src tests

style-check:
	ruff check --line-length=88 --ignore=E731,E741 tests src setup.py

test:
	py.test tests

test-all:
	tox

coverage:
	coverage erase
	coverage run --source src -m pytest --run-cli-tests tests
	coverage combine
	coverage report -m
	coverage html
	open htmlcov/index.html

coverage-full:
	coverage run --source src -m pytest --run-cli-tests tests
	coverage report -m
	coverage html
	open htmlcov/index.html

docs:
	rm -f docs/datapool.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ src
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	open docs/build/html/index.html

sdist: clean
	python setup.py sdist
	ls -l dist
