#!/bin/bash
#
# run_taster_test.sh
# Copyright (C) 2022 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

export POSTGRES_SERVER=127.0.0.1
export POSTGRES_PASSWORD=test
export POSTGRES_PORT=5432
export POSTGRES_USER=postgres

py.test $RESET tests/test_raster_data.py

